/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <cstdint>
#include <mutex>
#include <ostream>
#include <sstream>
#include <utility>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Rec Event
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/ZipRange.h"

// RicDet
#include "RichDet/DeRichSystem.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {

    /// Scalar type for SIMD data
    using FP = SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = SIMD::FP<Rich::SIMD::DefaultScalarFP>;
    /// Type for SIMD copy numbers
    using SIMDPDCopyNumber = SIMD::INT<Rich::DAQ::PDCopyNumber::Type>;

    /// Data values for a single PD
    struct PDData {
      std::uint32_t obsSignal{0};         ///< Number of observed hits in this PD
      FP            expSignal{0};         ///< Expected signal in this PD
      FP            expBackgrd{0};        ///< Expected background in this PD
      FP            effNumPixs{0};        ///< Effective number of pixels
      using Vector = std::vector<PDData>; ///< Vector type
    };

    /// Per Rich Data Structure
    using RichPDData = DetectorArray<PanelArray<PDData::Vector>>;

    /// data cache object
    class PixBkgdsDataCache {
    private:
      // RICH system object
      const DeRichSystem* m_richSys = nullptr;

    private:
      /// Builds the cached PD initalisation data
      void getPDData();

    public:
      // disallow default constructor
      PixBkgdsDataCache() = delete;

      /// Constructor from a DeRichSystem object
      PixBkgdsDataCache( const DeRichSystem&                deRichSys, //
                         const DetectorArray<unsigned int>& pdGS )
          : m_richSys( &deRichSys ), pdGroupSize( pdGS ) {

        // Compute the min module ID
        DetectorArray<PanelArray<std::size_t>> minModN{{{999999, 999999}, {999999, 999999}}};
        for ( const auto pd : deRichSys.allPDRichSmartIDs() ) {
          // get module number
          const auto mod = pd.pdCol();
          if ( mod < minModN[pd.rich()][pd.panel()] ) { minModN[pd.rich()][pd.panel()] = mod; }
          // PD num in module. Add one so indicates total number not index in panel.
          const auto n = pd.pdNumInCol() + 1;
          if ( n > maxPDPerMod[pd.rich()] ) { maxPDPerMod[pd.rich()] = n; }
        }
        minPanelIndex = minModN;
        for ( const auto rich : Rich::detectors() ) { maxPDGPerMod[rich] = maxPDPerMod[rich] / pdGS[rich]; }

        // create the PD init data
        getPDData();

        // to do - add back some debug output
        //_ri_debug << "Min Module IDs        " << m_minPanelIndex << endmsg;
        //_ri_debug << "Max PD # In Col       " << m_maxPDPerMod << endmsg;
        //_ri_debug << "PD Group size         " << m_pdGroupSize.value() << endmsg;
        //_ri_debug << "Max PD Groups per Col " << m_maxPDGPerMod << endmsg;
      }

    public:
      /// Gets the per RICH index for the given PD ID
      inline decltype( auto ) pdCopyNumber( const LHCb::RichSmartID pdID ) const {
        // Panel index
        return m_richSys->dePDPanel( pdID )->pdNumber( pdID );
      }

      /// Gets the working data index for the given PD ID
      inline decltype( auto ) pdIndex( const LHCb::RichSmartID  pdID, //
                                       const Rich::DetectorType rich, //
                                       const Rich::Side         panel ) const noexcept {
        // Use module numbers, sub-divided into groups of requested max size
        return ( maxPDGPerMod[rich] * ( pdID.pdCol() - minPanelIndex[rich][panel] ) ) +
               ( pdID.pdNumInCol() / pdGroupSize[rich] );
      }

      /// Gets the working data index for the given PD ID
      inline decltype( auto ) pdIndex( const LHCb::RichSmartID  pdID, //
                                       const Rich::DetectorType rich ) const noexcept {
        return pdIndex( pdID, rich, pdID.panel() );
      }

      /// Gets the working data index for the given PD ID
      inline decltype( auto ) pdIndex( const LHCb::RichSmartID pdID ) const noexcept {
        return pdIndex( pdID, pdID.rich(), pdID.panel() );
      }

      /// Get the DePD object
      inline decltype( auto ) dePD( const LHCb::RichSmartID pdID ) const {
        return m_richSys->dePDPanel( pdID.rich(), pdID.panel() )->dePD( pdID );
      }

    public:
      /// PD data
      RichPDData pdData;
      /// Min group index per panel
      DetectorArray<PanelArray<std::size_t>> minPanelIndex{{}};
      /// Max number of PD per module, in each RICH
      DetectorArray<std::size_t> maxPDPerMod{{}};
      /// Number of PD groups per module
      DetectorArray<std::size_t> maxPDGPerMod{{}};
      /// PD group sizes
      DetectorArray<unsigned int> pdGroupSize{{}};
    };

    //-----------------------------------------------------------------------------

    void PixBkgdsDataCache::getPDData() {

      // loop over all PD smartIDs and extract min/max index for each
      DetectorArray<PanelArray<std::size_t>> maxIndex{{}};
      for ( const auto pd : m_richSys->allPDRichSmartIDs() ) {
        // get the max (group) index for this PD
        const auto index = pdIndex( pd );
        // save the largest
        if ( index > maxIndex[pd.rich()][pd.panel()] ) { maxIndex[pd.rich()][pd.panel()] = index; }
      }

      // resize the static data accordling
      for ( const auto rich : Rich::detectors() ) {
        for ( const auto side : Rich::sides() ) {
          auto& richD  = pdData[rich];
          auto& panelD = richD[side];
          // Max PD group index for this panel
          const auto nPDGs = maxIndex[rich][side];
          // _ri_debug << rich << " " << Rich::text( rich, side ) << " max PD index = " << nPDGs << endmsg;
          // clear and resize
          assert( nPDGs < 99999 );
          panelD.clear();
          panelD.resize( nPDGs + 1 );
        }
      }

      // Finally, loop again over all smartIDs and fill info
      const auto& pds = m_richSys->allPDRichSmartIDs();
      //  _ri_verbo << "Number of PD SmartIDs " << pds.size() << endmsg;
      for ( const auto pd : pds ) {

        //    _ri_debug << "Filling info for " << pd << endmsg;

        const auto rich = pd.rich();
        const auto side = pd.panel();

        auto& richD  = pdData[rich];
        auto& panelD = richD[side];

        // PD copy number
        // const auto index = pdCopyNumber( pd );

        // The group ID
        const auto groupID = pdIndex( pd, rich, side );

        // get the entry in the group vector
        auto& pdData = panelD[groupID];

        // Get the DePD
        const auto PD = dePD( pd );
        if ( PD ) {

          // sanity checks
          if ( UNLIKELY( pd.pdID() != PD->pdSmartID().pdID() ) ) {
            std::ostringstream mess;
            mess << "PD SmartID mis-match :- " << pd << " != " << PD->pdSmartID();
            throw Rich::Exception( mess.str() );
          }

          // add to pixel group size
          pdData.effNumPixs += PD->effectiveNumActivePixels();

          // _ri_debug << "  -> index " << index << " Group ID " << groupID << " Eff NumPixs "
          //          << PD->effectiveNumActivePixels() << endmsg;
        } else {
          std::ostringstream mess;
          mess << "  -> FAILED to load " << rich << " " << Rich::text( rich, side ) << " dePD for " << pd;
          throw Rich::Exception( mess.str() );
        }

        // end PD loop
      }
    }

  } // namespace

  //-----------------------------------------------------------------------------

  /** @class PixelBackgroundsEstiAvHPD RichPixelBackgroundsEstiAvHPD.h
   *
   *  Computes an estimate of the background contribution to each pixel
   *  given the set of track mass hypotheses.
   *
   *  Provisonally this is a SIMD version, acting on the SIMD pixels,
   *  but the algorithm (for now) is really still scalar. It is far
   *  from being a major CPU user in the overall RICH sequence...
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class SIMDPixelBackgroundsEstiAvHPD final
      : public Transformer<SIMDPixelBackgrounds( const Relations::TrackToSegments::Vector&, //
                                                 const TrackPIDHypos&,                      //
                                                 const LHCb::RichTrackSegment::Vector&,     //
                                                 const GeomEffsPerPDVector&,                //
                                                 const PhotonYields::Vector&,               //
                                                 const SIMDPixelSummaries&,                 //
                                                 const PixBkgdsDataCache& ),
                           LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, PixBkgdsDataCache>> {

  public:
    /// Standard constructor
    SIMDPixelBackgroundsEstiAvHPD( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected},
                        KeyValue{"TrackPIDHyposLocation", TrackPIDHyposLocation::Default},
                        KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"GeomEffsPerPDLocation", GeomEffsPerPDLocation::Default},
                        KeyValue{"DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable},
                        KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                        // conditions input
                        KeyValue{"DataCache", name + "-DataCache"}},
                       // outputs
                       {KeyValue{"PixelBackgroundsLocation", SIMDPixelBackgroundsLocation::Default}} ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization after creation
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        // Derived cached data
        addConditionDerivation( DeRichLocations::RichSystem,        // input
                                inputLocation<PixBkgdsDataCache>(), // output
                                [pdGroupSize = m_pdGroupSize.value()]( const DeRichSystem& deRichSys ) {
                                  return PixBkgdsDataCache{deRichSys, pdGroupSize};
                                } );
        // debug info
        if ( m_ignoreExpSignal ) { _ri_debug << "Will ignore expected signals when computing backgrounds" << endmsg; }
      } );
    }

  public:
    /// Algorithm execution via transform
    SIMDPixelBackgrounds operator()( const Relations::TrackToSegments::Vector& tkToSegs,      //
                                     const TrackPIDHypos&                      tkHypos,       //
                                     const LHCb::RichTrackSegment::Vector&     segments,      //
                                     const GeomEffsPerPDVector&                geomEffsPerPD, //
                                     const PhotonYields::Vector&               detYieldsV,    //
                                     const SIMDPixelSummaries&                 pixels,        //
                                     const PixBkgdsDataCache&                  dataCache      //
                                     ) const override;

  private:
    // properties

    /// Maximum number of iterations in background normalisation
    Gaudi::Property<unsigned int> m_maxBkgIterations{this, "MaxBackgroundNormIterations", 10,
                                                     "Maximum number of iterations in background normalisation"};

    /// Minimum pixel background value, for each RICH
    Gaudi::Property<DetectorArray<FP>> m_minPixBkg{
        this, "MinPixelBackground", {0.0f, 0.0f}, "Minimum pixel background for each RICH"};

    /// Maximum pixel background value, for each RICH
    Gaudi::Property<DetectorArray<FP>> m_maxPixBkg{
        this, "MaxPixelBackground", {9e9f, 9e9f}, "Maximum pixel background for each RICH"};

    /** Ignore the expected signal when computing the background terms.
        Effectively, will assume all observed hits are background */
    Gaudi::Property<bool> m_ignoreExpSignal{this, "IgnoreExpectedSignals", false,
                                            "Ignore expectations when calculating backgrounds"};

    /// Background 'weight' for each RICH
    Gaudi::Property<DetectorArray<float>> m_bkgWeight{
        this, "PDBckWeights", {1.0f, 1.0f}, "Weights to apply to the background terms for each RICH"};

    /// PD Group Size for each RICH
    Gaudi::Property<DetectorArray<unsigned int>> m_pdGroupSize{
        this, "PDGroupSize", {4u, 4u}, "The number of PDs to group together for the background calculation"};

  private:
    // messaging

    /// Pixel PD index error
    mutable ErrorCounter m_pixIndexRangeErr{this, "Pixel PD index out of range !!"};
    /// Track PD index error
    mutable ErrorCounter m_tkIndexRangeErr{this, "Track PD index out of range !!"};
    /// Background PD index error
    mutable ErrorCounter m_bkgIndexRangeErr{this, "Bkg PD index out of range !!"};
  };

} // namespace Rich::Future::Rec

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------

SIMDPixelBackgrounds                                                                                //
SIMDPixelBackgroundsEstiAvHPD::operator()( const Relations::TrackToSegments::Vector& tkToSegs,      //
                                           const TrackPIDHypos&                      tkHypos,       //
                                           const LHCb::RichTrackSegment::Vector&     segments,      //
                                           const GeomEffsPerPDVector&                geomEffsPerPD, //
                                           const PhotonYields::Vector&               detYieldsV,    //
                                           const SIMDPixelSummaries&                 pixels,        //
                                           const PixBkgdsDataCache&                  dataCache      //
                                           ) const {

  // the backgrounds to return. Initialize to 0
  SIMDPixelBackgrounds backgrounds( pixels.size(), SIMDFP::Zero() );

  // local cache of cluster indices
  SIMD::STDVector<SIMDPDCopyNumber> indices( pixels.size(), SIMDPDCopyNumber::Zero() );

  // The working data, copied from the default instance
  auto pdData = dataCache.pdData;

  // Zip the segment data together
  const auto segRange = Ranges::ConstZip( segments, geomEffsPerPD, detYieldsV );

  // -----------------------------------------------------------
  // Fill the observed data
  // -----------------------------------------------------------
  for ( auto&& [pixel, index] : Ranges::Zip( pixels, indices ) ) {

    // RICH and panel
    const auto rich = pixel.rich();
    const auto side = pixel.side();

    // Get the data vector for this panel
    auto& dataV = ( pdData[rich] )[side];

    // Loop over the scalar entries for this SIMD pixel
    for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {

      // only use valid scalar info
      if ( pixel.validMask()[i] ) {

        // PD ID
        const auto pd = ( pixel.smartID()[i] ).pdID();

        // Get the PD copy number index
        index[i] = dataCache.pdIndex( pd, rich, side );

        //_ri_verbo << "Hit PD " << pd << " " << index[i] << endmsg;

        // Sanity check
        if ( LIKELY( index[i] < dataV.size() ) ) {
          // the working data object for this PD group
          auto& data = dataV[index[i]];
          // sanity check. Should correspond to an entry with size > 0
          assert( data.effNumPixs > 0 );
          // count the number of hits in each PD, in each RICH
          ++( data.obsSignal );
        } else {
          ++m_pixIndexRangeErr;
        }

      } // valid check

    } // pixel scalar loop

  } // SIMD pixels

  // -----------------------------------------------------------
  // Now the expected signals, based on the track information
  // loop over tracks (via the hypo values)
  // -----------------------------------------------------------
  if ( !m_ignoreExpSignal.value() ) {

    // loop over track data
    for ( const auto&& [tkRels, tkHypo] : Ranges::ConstZip( tkToSegs, tkHypos ) ) {
      //_ri_debug << " -> Track " << tkRels.tkKey << " " << tkHypo << endmsg;

      // Loop over the segments for this track
      for ( const auto& iSeg : tkRels.segmentIndices ) {
        // extract from the segment data tuple
        const auto& [segment, geomEffs, detYield] = segRange[iSeg];

        // which RICH
        const auto rich = segment.rich();

        //_ri_debug << "  -> Segment " << iSeg << " " << rich
        //          << " " << segment.radiator()
        //          << " DetPhots=" << detYield[tkHypo] << endmsg;

        // Loop over the per PD geom. effs. for this track hypo
        for ( const auto& PD : geomEffs[tkHypo] ) {
          // expected signal for this PD
          const auto sig = detYield[tkHypo] * PD.eff;
          // index
          const auto index = dataCache.pdIndex( PD.pdID, rich );
          //_ri_verbo << "Tk  PD " << PD.pdID << index << endmsg;
          // panel data vector
          auto& dataV = ( pdData[rich] )[PD.pdID.panel()];
          // Update the PD data map with this value
          if ( LIKELY( index < dataV.size() ) ) {
            auto& data = dataV[index];
            // sanity check. Should correspond to an entry with size > 0
            assert( data.effNumPixs > 0 );
            // fill expected signal
            data.expSignal += sig;
            //_ri_debug << "   -> " << LHCb::RichSmartID(PD.first) << " DetPhots=" << sig << endmsg;
          } else {
            ++m_tkIndexRangeErr;
          }
        }
      }
    }
  }

  // -----------------------------------------------------------
  // Now compute the background terms
  // -----------------------------------------------------------

  // Obtain background term PD by PD
  for ( const auto rich : Rich::detectors() ) {
    //_ri_debug << "Computing PD backgrounds in " << rich << endmsg;

    FP rnorm = 0.0f; // normalisation value between iterations

    // iteration loop
    bool         cont = true; // loop abort
    unsigned int iter = 0;    // iteration count
    while ( cont && ++iter <= m_maxBkgIterations ) {

      //_ri_debug << " -> Iteration " << iter << endmsg;

      unsigned int nBelow( 0 ), nAbove( 0 );
      FP           tBelow( 0.0f );

      // loop over panels
      for ( auto& panelData : pdData[rich] ) {
        // Loop over PD in this panel
        for ( auto& iPD : panelData ) {
          // Only process PDs with observed hits
          if ( iPD.obsSignal > 0 ) {

            // The background for this PD
            auto& bkg = iPD.expBackgrd;

            if ( LIKELY( 1 == iter ) ) {
              // First iteration, just set background for this PD to the difference
              // between the observed and and expected number of hits in the PD
              //_ri_debug << "  -> PD " << pd << " obs. = " << obs << " exp. = " << exp << endmsg;
              bkg = static_cast<FP>( iPD.obsSignal ) - iPD.expSignal;
            } else {
              // For additional interations apply the normalisation factor
              bkg = ( bkg > 0 ? bkg - rnorm : 0 );
            }

            if ( UNLIKELY( bkg < 0.0 ) ) {
              // Count the number of PDs below expectation for this iteration
              ++nBelow;
              // save the total amount below expectation
              tBelow += fabs( bkg );
            } else if ( bkg > 0.0 ) {
              // count the number of PDs above expectation
              ++nAbove;
            }

          } // with observed hits
        }   // end loop over signal PDs
      }     // end loop over panels

      //_ri_debug << "  -> Above = " << nAbove << " Below = " << nBelow << endmsg;

      if ( UNLIKELY( nBelow > 0 && nAbove > 0 ) ) {
        // we have some PDs above and below expectation
        // calculate the amount of signal below per above PD
        rnorm = tBelow / ( static_cast<FP>( nAbove ) );
        //_ri_debug << "   -> Correction factor per PD above = " << rnorm << endmsg;
      } else {
        //_ri_debug << "  -> Aborting iterations" << endmsg;
        cont = false;
      }

    } // while loop

  } // end rich loop

  // -----------------------------------------------------------
  // Normalise the PD backgrounds
  // -----------------------------------------------------------

  // Loop over the RICH data maps
  for ( const auto rich : Rich::detectors() ) {
    // loop over panels
    for ( auto& panel : pdData[rich] ) {
      // Loop over the PD data objects
      for ( auto& pd : panel ) {
        // PDs with signal
        //_ri_verbo << "PD info " << pd.obsSignal << " " << pd.expBackgrd << " " << pd.effNumPixs << endmsg;
        if ( pd.obsSignal > 0 ) {
          // normalise background for this PD
          pd.expBackgrd = ( pd.expBackgrd > 0 ? pd.expBackgrd / pd.effNumPixs : 0 );
          // rescale by the overall weight factor for the RICH this PD is in
          pd.expBackgrd *= m_bkgWeight[rich];
          // apply min and max pixel background limits
          pd.expBackgrd = std::clamp( pd.expBackgrd, m_minPixBkg[rich], m_maxPixBkg[rich] );
        }
      }
    }
  }

  // -----------------------------------------------------------
  // Fill the background values into the output data structure
  // -----------------------------------------------------------

  for ( auto&& [pixel, bkg, index] : Ranges::Zip( pixels, backgrounds, std::as_const( indices ) ) ) {

    // RICH flags
    const auto rich = pixel.rich();
    const auto side = pixel.side();

    // get the panel data vector
    auto& dataV = ( pdData[rich] )[side];

    // Loop over the scalar entries for this SIMD pixel
    for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {

      // only process valid scalar info
      if ( pixel.validMask()[i] ) {

        // index for this PD
        const auto indx = index[i];

        // get the data object for this PD
        if ( LIKELY( indx < dataV.size() ) ) {
          // data for this PD
          auto& data = dataV[indx];
          // update the pixel background
          bkg[i] = data.expBackgrd;
          //_ri_verbo << rich << " Pix Bkg " << bkg[i] << endmsg;
        } else {
          ++m_bkgIndexRangeErr;
          bkg[i] = 0;
        }

      } // valid entries

    } // scalar loop

  } // pixel loop

  // -----------------------------------------------------------
  // All done, so return
  // -----------------------------------------------------------

  return backgrounds;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPixelBackgroundsEstiAvHPD )

//=============================================================================
