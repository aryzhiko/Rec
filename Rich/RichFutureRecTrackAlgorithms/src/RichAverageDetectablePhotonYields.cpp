/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <limits>
#include <tuple>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/StlArray.h"
#include "RichUtils/ZipRange.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichSystem.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {

    // Scalar type to work with
    using ScType = PhotonYields::Type;

    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;

    // init value to make sure we never use uninitalised data
    inline constexpr auto NaN = std::numeric_limits<float>::signaling_NaN();

    /// data cache
    class DetYieldsDataCache final {
    public:
      /// Cache pointer to Rich System
      const DeRichSystem* deRichSys = nullptr;
      /// cached energy bin efficiences
      RadiatorArray<std::array<ScType, NPhotonSpectraBins>> spectraEffs = {{{NaN}}};

    public:
      /// Disallow default constructor
      DetYieldsDataCache() = delete;

      /// Constructor from Riches
      DetYieldsDataCache( const DeRichSystem&        deRS,      //
                          const RadiatorArray<float> minPhotEn, //
                          const RadiatorArray<float> maxPhotEn )
          : deRichSys{&deRS} {

        // Get DeRich1 and DeRich2
        auto rich1 = dynamic_cast<const DeRich1*>( deRS.deRich( Rich::Rich1 ) );
        auto rich2 = dynamic_cast<const DeRich2*>( deRS.deRich( Rich::Rich2 ) );
        assert( rich1 && rich2 );

        // Do we have HPDs or PMTs
        const bool PmtActivate = ( rich1->RichPhotoDetConfig() == Rich::PMTConfig );

        // Quartz window eff
        const auto qEff = rich1->param<double>( "HPDQuartzWindowEff" );

        // Digitisation pedestal loss
        const auto pLos =
            ( PmtActivate && rich1->exists( "PMTPedestalDigiEff" ) ? rich1->param<double>( "PMTPedestalDigiEff" )
                                                                   : rich1->param<double>( "HPDPedestalDigiEff" ) );

        // Quartz window params
        const RadiatorArray<double> qWinZSize{rich1->param<double>( "Rich1GasQuartzWindowThickness" ),
                                              rich1->param<double>( "Rich1GasQuartzWindowThickness" ),
                                              rich2->param<double>( "Rich2GasQuartzWindowThickness" )};

        // Loop over radiators
        for ( const auto rad : Rich::radiators() ) {

          // Rich for this radiator
          const auto deR = deRS.deRich( rad );

          // temporary photon spectra object
          PhotonSpectra spectra( minPhotEn[rad], maxPhotEn[rad] );

          // loop over the energy bins
          for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {
            // bin efficiency
            double eff = 0.0;
            // bin energy ( in eV )
            const auto energy = spectra.binEnergy( iEnBin ) * Gaudi::Units::eV;
            if ( LIKELY( energy > 0 ) ) {
              // scale by pedestal loss and Quartz window eff.
              eff = qEff * pLos;
              // Get weighted average PD Q.E. ( scale from % to fraction )
              eff *= 0.01 * ( *( deR->nominalPDQuantumEff() ) )[energy];
              // primary mirror reflectivity
              eff *= ( *( deR->nominalSphMirrorRefl() ) )[energy];
              // secondary mirror reflectivity
              eff *= ( *( deR->nominalSecMirrorRefl() ) )[energy];
              // The Quartz window efficiency
              eff *= Rich::Maths::fast_exp( -qWinZSize[rad] / ( *( deR->gasWinAbsLength() ) )[energy] );
            }
            // save the final efficiency for this energy bin
            ( spectraEffs[rad] )[iEnBin] = eff;
          } // energy bin loop
        }   // radiator loop
      }
    };

  } // namespace

  /** @class DetectablePhotonYields RichDetectablePhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class AverageDetectablePhotonYields final
      : public MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&, //
                                         const PhotonSpectra::Vector&,          //
                                         const MassHypoRingsVector&,            //
                                         const DetYieldsDataCache& ),           //
                                LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, //
                                                                     DetYieldsDataCache>> {

  public:
    /// Standard constructor
    AverageDetectablePhotonYields( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // data inputs
                            {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                             KeyValue{"EmittedSpectraLocation", PhotonSpectraLocation::Emitted},
                             KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted},
                             // conditions input
                             KeyValue{"DataCache", name + "-DataCache"}},
                            // outputs
                            {KeyValue{"DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable},
                             KeyValue{"DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable}} ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialization after creation
    StatusCode initialize() override {
      return MultiTransformer::initialize().andThen( [&] {
        // derived data cache
        addConditionDerivation( DeRichLocations::RichSystem,                     //
                                inputLocation<DetYieldsDataCache>(),             //
                                [minPhotEn = richPartProps()->minPhotonEnergy(), //
                                 maxPhotEn = richPartProps()->maxPhotonEnergy()] //
                                ( const DeRichSystem& deS ) {
                                  return DetYieldsDataCache{deS, minPhotEn, maxPhotEn};
                                } );
      } );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                        const PhotonSpectra::Vector&          emittedSpectra, //
                        const MassHypoRingsVector&            massRings,      //
                        const DetYieldsDataCache&             dataCache       //
                        ) const override {

      // make the data to return
      OutData data;
      auto&   yieldV   = std::get<PhotonYields::Vector>( data );
      auto&   spectraV = std::get<PhotonSpectra::Vector>( data );

      // reserve sizes
      yieldV.reserve( segments.size() );
      spectraV.reserve( segments.size() );

      // Loop over input data
      for ( auto&& [segment, emitSpectra, rings] : Ranges::ConstZip( segments, emittedSpectra, massRings ) ) {

        // Which radiator
        const auto rad = segment.radiator();

        // Create the detectable photon spectra, using the same energy range
        // as the emitted spectra
        auto& detSpectra = spectraV.emplace_back( emitSpectra.minEnergy(), emitSpectra.maxEnergy() );

        // sanity checks on cached data
        assert( emitSpectra.energyBins() == dataCache.spectraEffs[rad].size() );
        assert( emitSpectra.energyBins() == detSpectra.energyBins() );

        // create the yield data
        auto& yields = yieldV.emplace_back();

        // Loop over real PID types
        for ( const auto id : activeParticlesNoBT() ) {

          // the signal
          ScType signal = 0;

          // Acceptance check.
          // Longer term should look into removing any dependence on the CK rings here,
          // to potentially allow the complete removable of them from the reco. seq.
          if ( LIKELY( !rings[id].empty() &&                            //
                       std::any_of( rings[id].begin(), rings[id].end(), //
                                    []( const auto& P )                 //
                                    { return RayTracedCKRingPoint::InHPDTube == P.acceptance(); } ) ) ) {

            // loop over the energy bins
            for ( std::size_t iEnBin = 0; iEnBin < emitSpectra.energyBins(); ++iEnBin ) {

              // scale the emitted signal in this bin by bin eff.
              const auto sig = ( emitSpectra.energyDist( id ) )[iEnBin] * ( dataCache.spectraEffs[rad] )[iEnBin];

              // if we still have some signal, save the values
              if ( LIKELY( sig > 0 ) ) {
                // Save to the output spectra for this bin
                ( detSpectra.energyDist( id ) )[iEnBin] = sig;
                // update the overall detectable signal
                signal += sig;
              }

            } // energy bin loop

          } // acceptance

          // save the yield for this hypo
          yields.setData( id, signal );

        } // loop over PID types
      }

      // return the new data
      return data;
    }
  };

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( AverageDetectablePhotonYields )

  //=============================================================================

} // namespace Rich::Future::Rec
