/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cassert>
#include <cstdint>

// Kernel
#include "Kernel/RichParticleIDType.h"

namespace Rich::Future::Rec {

  /// Templated class implementing a mass alias concept
  template <typename TYPE>
  class MassAliasArray final : public ParticleArray<TYPE> {

  private:
    /// The payload data
    using Payload = TYPE;
    /// Base class shortcut
    using Base = ParticleArray<TYPE>;
    /// Size type
    using size_type = typename Base::size_type;
    /// Index type
    using Index = std::int8_t;
    /// Indices array
    using Indices = ParticleArray<Index>;

  private:
    /// Internal mapping of which indices to use for which particle type
    Indices m_indices{0, 1, 2, 3, 4, 5, 6};

  public:
    /// Inherit constructors
    using typename Base::array;

  public:
    /// Set the alias to use for a given mass hypo
    void setMassAlias( const ParticleIDType from, const ParticleIDType to ) {
      assert( from != Rich::Unknown && (Indices::size_type)from < m_indices.size() );
      assert( to != Rich::Unknown && (Indices::size_type)to < m_indices.size() );
      m_indices[from] = to;
    }
    /// Get the mass alias
    decltype( auto ) massAlias( const ParticleIDType from ) const noexcept { return ParticleIDType{m_indices[from]}; }

  public:
    // access operators
    const Payload& operator[]( const size_type i ) const noexcept { return Base::operator[]( m_indices[i] ); }
    Payload&       operator[]( const size_type i ) noexcept { return Base::operator[]( m_indices[i] ); }
    const Payload& at( const size_type i ) const {
      assert( i < m_indices.size() );
      return Base::at( m_indices[i] );
    }
    Payload& at( const size_type i ) {
      assert( i < m_indices.size() );
      return Base::at( m_indices[i] );
    }
  };

} // namespace Rich::Future::Rec
