/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <mutex>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichDAQDefinitions.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class DetectorHits final
      : public Consumer<void( const Rich::Future::DAQ::L1Map& ), Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    KeyValue{"DecodedDataLocation", Rich::Future::DAQ::L1MapLocation::Default} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Rich::Future::DAQ::L1Map& data ) const override {

      // Count active PDs in each RICH
      DetectorArray<unsigned long long> activePDs = {{}};
      // Count hits in each RICH
      DetectorArray<unsigned long long> richHits = {{}};

      // the lock
      std::lock_guard lock( m_updateLock );

      // Loop over L1 boards
      for ( const auto& L1 : data ) {
        // loop over ingresses for this L1 board
        for ( const auto& In : L1.second ) {
          // Loop over HPDs in this ingress
          for ( const auto& PD : In.second.pdData() ) {

            // PD ID
            const auto pd = PD.second.pdID();
            if ( pd.isValid() && !PD.second.header().inhibit() ) {

              // Vector of SmartIDs
              const auto& rawIDs = PD.second.smartIDs();
              // RICH
              const auto rich = pd.rich();

              // Do we have any hits
              if ( !rawIDs.empty() ) {
                // Fill average HPD occ plot
                h_nTotalPixsPerPD[rich]->fill( rawIDs.size() );
                // count active PDs
                ++activePDs[rich];
                // count hits
                richHits[rich] += rawIDs.size();
              }
            }
          }
        }
      }

      // Loop over RICHes
      for ( const auto rich : Rich::detectors() ) {
        // Fill active PD plots
        h_nActivePDs[rich]->fill( activePDs[rich] );
        // Fill RICH hits plots
        if ( richHits[rich] > 0 ) { h_nTotalPixs[rich]->fill( richHits[rich] ); }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      for ( const auto rich : Rich::detectors() ) {

        ok &= saveAndCheck( h_nTotalPixsPerPD[rich],
                            richHisto1D( Rich::HistogramID( "nTotalPixsPerPD", rich ),
                                         "Average overall PD occupancy (nHits>0)", 0.5, 100.5, 100 ) );
        ok &=
            saveAndCheck( h_nTotalPixs[rich], richHisto1D( Rich::HistogramID( "nTotalPixs", rich ),
                                                           "Overall occupancy (nHits>0)", 0, m_maxPixels, nBins1D() ) );
        ok &= saveAndCheck( h_nActivePDs[rich], richHisto1D( Rich::HistogramID( "nActivePDs", rich ),
                                                             "# Active PDs (nHits>0)", -0.5, 3000.5, 3001 ) );
      }
      return StatusCode{ok};
    }

  private:
    /// Maximum pixels
    Gaudi::Property<unsigned int> m_maxPixels{this, "MaxPixels", 10000u};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

  private:
    // cached histograms

    /// Pixels per PD histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixsPerPD = {{}};
    /// Pixels per detector histograms
    DetectorArray<AIDA::IHistogram1D*> h_nTotalPixs = {{}};
    /// Number active PDs histograms
    DetectorArray<AIDA::IHistogram1D*> h_nActivePDs = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::Moni
