/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <mutex>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PhotonYield
   *
   *  Monitors the RICH segment expected photon yields
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class PhotonYield final : public Consumer<void( const LHCb::RichTrackSegment::Vector&, //
                                                  const PhotonYields::Vector& ),         //
                                            Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PhotonYield( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"PhotonYieldLocation", PhotonYieldsLocation::Detectable}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments, //
                     const PhotonYields::Vector&           yields ) const override {
      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over segments and yield data
      for ( auto&& [segment, yield] : Ranges::ConstZip( segments, yields ) ) {

        // Radiator info
        const auto rad = segment.radiator();
        if ( !m_rads[rad] ) continue;

        // loop over mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          // fill histo
          if ( yield[pid] > 0 ) {
            h_yields[rad][pid]->fill( yield[pid] );
            h_yieldVp[rad][pid]->fill( segment.bestMomentumMag(), yield[pid] );
          }
        }

      } // segment loop
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      using namespace Gaudi::Units;
      bool ok = true;
      // loop over active radiators
      for ( const auto rad : Rich::radiators() ) {
        if ( m_rads[rad] ) {
          // loop over active mass hypos
          for ( const auto pid : activeParticlesNoBT() ) {
            // book yield histos
            ok &= saveAndCheck( h_yields[rad][pid],                    //
                                richHisto1D( HID( "yield", rad, pid ), //
                                             "Photon Yield (>0)",      //
                                             0, m_maxYield[rad], nBins1D() ) );
            ok &= saveAndCheck( h_yieldVp[rad][pid],                          //
                                richProfile1D( HID( "yieldVp", rad, pid ),    //
                                               "Photon Yield (>0) V P (MeV)", //
                                               1.0 * GeV, 100.0 * GeV, nBins1D() ) );
          }
        }
      }
      return StatusCode{ok};
    }

  private:
    // properties

    /// Which radiators to monitor
    Gaudi::Property<RadiatorArray<bool>> m_rads{this, "Radiators", {false, true, true}};
    /// Maximum photon yield
    Gaudi::Property<RadiatorArray<float>> m_maxYield{this, "MaximumYields", {80, 80, 80}};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;
    /// Yield histograms
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_yields = {{}};
    /// Yield versus momentum
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_yieldVp = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PhotonYield )

} // namespace Rich::Future::Rec::Moni
