/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event model
#include "Event/RichPID.h"
#include "Event/Track.h"

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class TrackSelEff RichTrackSelEff.h
   *
   *  Monitors the efficiency of the RICH track selection and processing.
   *  I.e. the efficiency for which a RichPID object is produced.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class TrackSelEff final : public Consumer<void( const LHCb::Track::Selection&, //
                                                  const LHCb::RichPIDs& ),
                                            Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackSelEff( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichPIDsLocation", LHCb::RichPIDLocation::Default}} ) {
      // reset defaults in base class
      setProperty( "NBins1DHistos", 50 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Selection& tracks, //
                     const LHCb::RichPIDs&         pids ) const override {

      // Count selected tracks
      unsigned int nSelTracks( 0 );

      // loop over input tracks
      for ( const auto* tk : tracks ) {
        // Does this track have a PID result associated to it ?
        const bool sel =
            std::any_of( pids.begin(), pids.end(), [&tk]( const auto pid ) { return pid->track() == tk; } );

        // count selected tracks
        if ( sel ) { ++nSelTracks; }

        // Fill plots
        fillTrackPlots( tk, sel, "All/" );
      }

      // fill event plots
      richHisto1D( HID( "nTracks" ) )->fill( tracks.size() );
      richHisto1D( HID( "nRichTracks" ) )->fill( nSelTracks );
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      // global quantities
      richHisto1D( HID( "nTracks" ), "# Tracks / Event", -0.5, 200.5, 201 );
      richHisto1D( HID( "nRichTracks" ), "# Rich Tracks / Event", -0.5, 200.5, 201 );
      // track variable plots
      prebookHistograms( "All/" );
      // return
      return StatusCode::SUCCESS;
    }

  private:
    // Book the histos for the given track class
    void prebookHistograms( const std::string& tkClass ) {
      using namespace Gaudi::Units;

      richProfile1D( tkClass + "effVP", "RICH Track Sel. Eff. V P", 1.00 * GeV, 100.0 * GeV, nBins1D() );
      richProfile1D( tkClass + "effVPt", "RICH Track Sel. Eff. V Pt", 0.10 * GeV, 8.0 * GeV, nBins1D() );
      richProfile1D( tkClass + "effVChi2PDOF", "RICH Track Sel. Eff. V Chi^2 / D.O.F.", 0, 3, nBins1D() );
      richProfile1D( tkClass + "effVGhostProb", "RICH Track Sel. Eff. V Ghost Probability", 0.0, 0.4, nBins1D() );
      richProfile1D( tkClass + "effVCloneDist", "RICH Track Sel. Eff. V Clone Distance", 0.0, 6e3, nBins1D() );

      trackPlots( tkClass + "Selected/" );
      trackPlots( tkClass + "Rejected/" );
    }

    /// track plots
    void trackPlots( const std::string& tag ) {
      using namespace Gaudi::Units;

      richHisto1D( tag + "P", "Track Momentum", 0 * GeV, 100 * GeV, nBins1D() );
      richHisto1D( tag + "Pt", "Track Transverse Momentum", 0 * GeV, 8 * GeV, nBins1D() );
      richHisto1D( tag + "Chi2PDOF", "Track Chi^2 / D.O.F.", 0, 3, nBins1D() );
      richHisto1D( tag + "GhostProb", "Track Ghost Probability", 0.0, 0.4, nBins1D() );
      richHisto1D( tag + "CloneDist", "Track Clone Distance", 0.0, 6e3, nBins1D() );
    }

    /// Fill track plots for given class tag
    void fillTrackPlots( const LHCb::Track* track, const bool sel, const std::string& tkClass ) const {
      // cache clone dist
      const double cloneDist = track->info( LHCb::Track::AdditionalInfo::CloneDist, 5.5e3 );

      // Efficiencies plots
      const double richEff = ( sel ? 100.0 : 0.0 );
      richProfile1D( tkClass + "effVP" )->fill( track->p(), richEff );
      richProfile1D( tkClass + "effVPt" )->fill( track->pt(), richEff );
      richProfile1D( tkClass + "effVChi2PDOF" )->fill( track->chi2PerDoF(), richEff );
      richProfile1D( tkClass + "effVGhostProb" )->fill( track->ghostProbability(), richEff );
      richProfile1D( tkClass + "effVCloneDist" )->fill( cloneDist, richEff );

      // plot selection variables
      const std::string tag = ( sel ? tkClass + "Selected/" : tkClass + "Rejected/" );
      richHisto1D( tag + "P" )->fill( track->p() );
      richHisto1D( tag + "Pt" )->fill( track->pt() );
      richHisto1D( tag + "Chi2PDOF" )->fill( track->chi2PerDoF() );
      richHisto1D( tag + "GhostProb" )->fill( track->ghostProbability() );
      richHisto1D( tag + "CloneDist" )->fill( cloneDist );
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackSelEff )

} // namespace Rich::Future::Rec::Moni
