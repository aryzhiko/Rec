/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of MuonIDHlt1Alg.  */
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/MuonPID_v2.h"
#include "Event/PrIterableForwardTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/PrZip.h"
#include "Event/TrackSkin.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/IMuonMatchTool.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipAlgorithms.h"
#include "SOAExtensions/ZipContainer.h"
#include "vdt/vdtMath.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <limits>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace {
  using uint4 = std::array<unsigned, 4>;
  using xy_t  = std::pair<float, float>;

  using v2_Tracks_Zip = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackSkin>>;
  using MuonPID       = LHCb::Event::v2::MuonPID;
  using v2_MuonPIDs   = Zipping::ZipContainer<SOA::Container<std::vector, LHCb::Event::v2::MuonID>>;
  using MuonTrackExtrapolation = std::array<std::pair<float, float>, 4>;
  using MuonTrackOccupancies   = std::array<unsigned, 4>;
  // compute min/max limits for float exp(x) exponent
  constexpr float   margin   = 1000.0f; // safety margin (for any subsequent calculations)
  static const auto min_expo = vdt::fast_logf( std::numeric_limits<float>::min() * margin );
  static const auto max_expo = vdt::fast_logf( std::numeric_limits<float>::max() / margin );
} // namespace

namespace details {

  class Cache {
    static constexpr float m_lowMomentumThres  = 6000.f;
    static constexpr float m_highMomentumThres = 10000.f;

    const DeMuonDetector*  m_det           = nullptr;
    size_t                 m_stationsCount = 0;
    size_t                 m_regionsCount  = 0;
    MuonTrackExtrapolation m_regionInner, m_regionOuter;
    std::array<float, 4>   m_stationZ{{}};
    double                 m_foiFactor = 1.;

  public:
    double m_preSelMomentum = 0.;

  private:
    std::array<std::vector<double>, 3> m_foiParamX, m_foiParamY;
    std::vector<double>                m_momentumCuts;

  public:
    Cache( DeMuonDetector const& det, const Condition& FoiFactor, const Condition& PreSelMomentum,
           const Condition& FoiParametersX, const Condition& FoiParametersY, const Condition& MomentumCuts )
        : m_det{&det}
        , m_stationsCount( det.stations() )
        , m_regionsCount{det.regions() / m_stationsCount}
        , m_foiFactor{FoiFactor.param<double>( "FOIfactor" )}
        , m_preSelMomentum{PreSelMomentum.param<double>( "PreSelMomentum" )}
        , m_foiParamX{FoiParametersX.paramVect<double>( "XFOIParameters1" ),
                      FoiParametersX.paramVect<double>( "XFOIParameters2" ),
                      FoiParametersX.paramVect<double>( "XFOIParameters3" )}
        , m_foiParamY{FoiParametersY.paramVect<double>( "YFOIParameters1" ),
                      FoiParametersY.paramVect<double>( "YFOIParameters2" ),
                      FoiParametersY.paramVect<double>( "YFOIParameters3" )}
        , m_momentumCuts{MomentumCuts.paramVect<double>( "MomentumCuts" )} {

      // Update the cached DeMuon geometry (should be done by the detector element..)
      // det.fillGeoArray();
      for ( int s = 0; s != det.stations(); ++s ) {
        m_regionInner[s] = std::make_pair( det.getInnerX( s ), det.getInnerY( s ) );
        m_regionOuter[s] = std::make_pair( det.getOuterX( s ), det.getOuterY( s ) );
        m_stationZ[s]    = det.getStationZ( s );
      }

      // TODO(kazeevn, raaij) fix the upgrade CondDB and remove the hack
      // Remove parameters we don't need, in particular in case of the upgrade
      // geometry, the first set of nRegions parameters
      for ( auto params : {std::ref( m_foiParamX ), std::ref( m_foiParamY )} ) {
        for ( auto& v : params.get() ) {
          if ( v.size() != m_stationsCount * m_regionsCount ) {
            v.erase( v.begin(), v.begin() + ( 5 - m_stationsCount ) * m_regionsCount );
          }
        }
      }
    }

    /** Checks 'isMuon' given the occupancies corresponding to a track and the
     * track's momentum. The requirement differs in bins of p.
     */
    bool isMuon( const MuonTrackOccupancies& occupancies, float p ) const {
      if ( p < m_preSelMomentum ) return false;
      auto has = [&]( unsigned station ) { return occupancies[station] != 0; };
      if ( !has( 0 ) || !has( 1 ) ) return false;
      if ( p < m_momentumCuts[0] ) return true;
      if ( p < m_momentumCuts[1] ) return has( 2 ) || has( 3 );
      return has( 2 ) && has( 3 );
    }

    /** Helper function that returns the x, y coordinates of the FoI for a
     * given station and region and a track's momentum.
     */
    std::pair<float, float> foi( unsigned int station, unsigned int region, float p ) const {
      auto i = station * m_regionsCount + region;
      if ( p < 1000000.f ) { // limit to 1 TeV momentum tracks
        const auto pGev                = p / static_cast<float>( Gaudi::Units::GeV );
        auto       ellipticalFoiWindow = [&]( const auto& fp ) {
          const float expo = -fp[2][i] * pGev;
          return fp[0][i] + fp[1][i] * vdt::fast_expf( std::clamp( expo, min_expo, max_expo ) );
        };
        return {ellipticalFoiWindow( m_foiParamX ), ellipticalFoiWindow( m_foiParamY )};
      }
      return {m_foiParamX[0][i], m_foiParamY[0][i]};
    }

    /** Check whether track extrapolation is within detector acceptance.
     */
    bool inAcceptance( const MuonTrackExtrapolation& extrapolation ) const {
      auto abs_lt = []( const xy_t& p1, const xy_t& p2 ) {
        return std::abs( p1.first ) < p2.first && std::abs( p1.second ) < p2.second;
      };

      // Outer acceptance
      if ( !abs_lt( extrapolation[0], m_regionOuter[0] ) ||
           !abs_lt( extrapolation[m_stationsCount - 1], m_regionOuter[m_stationsCount - 1] ) ) {
        // Outside M2 - M5 region
        return false;
      }

      // Inner acceptance
      if ( abs_lt( extrapolation[0], m_regionInner[0] ) ||
           abs_lt( extrapolation[m_stationsCount - 1], m_regionInner[m_stationsCount - 1] ) ) {
        // Inside M2 - M5 chamber hole
        return false;
      }

      return true;
    }

    /** Project a given track into the muon stations.
     */
    template <typename State>
    MuonTrackExtrapolation extrapolateTrack( State const& state ) const {
      MuonTrackExtrapolation extrapolation;

      // Project the state into the muon stations
      // Linear extrapolation equivalent to TrackLinearExtrapolator
      for ( unsigned station = 0; station != m_stationsCount; ++station ) {
        // x(z') = x(z) + (dx/dz * (z' - z))
        extrapolation[station] = {state.x() + state.tx() * ( m_stationZ[station] - state.z() ),
                                  state.y() + state.ty() * ( m_stationZ[station] - state.z() )};
      }
      return extrapolation;
    }

    std::array<uint4, 2> countHits( const float p, const MuonTrackExtrapolation& extrapolation,
                                    const MuonHitContainer& hitContainer ) const {

      // decide the ordering to look for hits
      // The correct order to look should be momentum dependent
      // M3, M2                  3<p<6 GeV
      // (M4 or M5) and M3, M2   6<p<10 GeV
      // M5, M4, M3, M2          p>10 GeV
      const auto ordering{p >= m_lowMomentumThres ? uint4{3, 2, 1, 0} : uint4{1, 0, 3, 2}};
      uint4      occupancies{0, 0, 0, 0};
      uint4      occupancies_crossed{0, 0, 0, 0};

      /* Define an inline callable that makes it easier to check whether a hit is
       * within a given window. */
      class IsInWindow {
        xy_t                center_;
        std::array<xy_t, 4> foi_;

      public:
        // Constructor takes parameters by value and then moves them into place.
        IsInWindow( xy_t center, xy_t foi0, xy_t foi1, xy_t foi2, xy_t foi3, double sf )
            : center_{std::move( center )}
            , foi_{{std::move( foi0 ), std::move( foi1 ), std::move( foi2 ), std::move( foi3 )}} {
          std::transform( begin( foi_ ), end( foi_ ), begin( foi_ ), [sf]( const xy_t& p ) -> xy_t {
            return {p.first * sf, p.second * sf};
          } );
        }
        bool operator()( const CommonMuonHit& hit ) const {
          auto region = hit.region();
          assert( region < 4 );
          return ( std::abs( hit.x() - center_.first ) < hit.dx() * foi_[region].first ) &&
                 ( std::abs( hit.y() - center_.second ) < hit.dy() * foi_[region].second );
        }
      };

      auto makeIsInWindow = [&]( unsigned station ) {
        return IsInWindow{extrapolation[station], foi( station, 0, p ), foi( station, 1, p ),
                          foi( station, 2, p ),   foi( station, 3, p ), m_foiFactor};
      };

      for ( unsigned it = 0; it != ordering.size(); ++it ) {
        unsigned station   = ordering[it];
        auto     predicate = makeIsInWindow( station );

        const auto& hits = hitContainer.station( station ).hits();

        for ( const auto& hit : hits ) {
          if ( predicate( hit ) ) {
            occupancies[station]++;
            if ( !hit.uncrossed() || m_det->mapInRegion( station, hit.region() ) == 1 ) {
              occupancies_crossed[station]++;
            }
          }
        }

        // evaluate the occupancies from less to more occupied to avoid
        // useless loops through stations
        if ( station < 2 && occupancies[station] == 0 ) break;
        if ( station == 2 && occupancies[station] == 0 && occupancies[station + 1] == 0 && p > m_lowMomentumThres &&
             p < m_highMomentumThres )
          break;
        if ( p > m_highMomentumThres && occupancies[station] == 0 ) break;
      }

      return {occupancies, occupancies_crossed};
    }
  };

} // namespace details

template <typename MuonPIDs, typename Tracks>
using Transformer =
    Gaudi::Functional::Transformer<MuonPIDs( const Tracks&, const MuonHitContainer&, const details::Cache& ),
                                   LHCb::DetDesc::usesConditions<details::Cache>>;

template <typename Tracks, typename MuonPIDs>
class MuonIDHlt1Alg final : public Transformer<MuonPIDs, Tracks> {
  using base_class_t = Transformer<MuonPIDs, Tracks>;
  using base_class_t::debug;
  using base_class_t::msgLevel;
  using base_class_t::warning;
  using typename base_class_t::KeyValue;

public:
  MuonIDHlt1Alg( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t{name,
                     pSvcLocator,
                     // Inputs
                     {KeyValue{"InputTracks", "/Event/Rec/Track/Forward"}, // LHCb::TrackLocation::Default,
                      KeyValue{"InputMuonHits", MuonHitContainerLocation::Default},
                      KeyValue{"ConditionsCache", "MuonIDHlt1Alg-" + name + "-ConditionsCache"}},
                     // Output
                     KeyValue{"OutputMuonPID", LHCb::Event::v2::MuonPIDLocation::Default}} {}

  StatusCode initialize() override {
    return base_class_t::initialize().andThen( [&] {
      using namespace std::string_literals;
      this->template addConditionDerivation<details::Cache( const DeMuonDetector&, const Condition&, const Condition&,
                                                            const Condition&, const Condition&, const Condition& )>(
          *this, std::array{DeMuonLocation::Default, "Conditions/ParticleID/Muon/FOIfactor"s,
                            "Conditions/ParticleID/Muon/PreSelMomentum"s, "Conditions/ParticleID/Muon/XFOIParameters"s,
                            "Conditions/ParticleID/Muon/YFOIParameters"s, "Conditions/ParticleID/Muon/MomentumCuts"s} );
    } );
  }

  /** Iterates over all tracks in the current event and performs muon id on them.
   * Resulting PID objects are stored on the TES.
   */
  MuonPIDs operator()( const Tracks& tracks, const MuonHitContainer& hitContainer,
                       const details::Cache& cond ) const override {
    // counter buffers
    auto muonIDlambda = [momCutCount = m_momCutCount.buffer(), goodCount = m_goodCount.buffer(),
                         acceptanceCount = m_acceptanceCount.buffer(), isMuonCount = m_isMuonCount.buffer(),
                         isTightCount = m_isTightCount.buffer(), &hitContainer, &cond]( auto const& track ) mutable {
      ++goodCount;
      // TODO: Put to where the values are set after the
      // muonMap thing has been figured out...
      MuonPID muPid;
      muPid.setIsMuon( false );
      muPid.setIsMuonTight( false );
      // muPid.setMuonLLMu(-10000.);
      // muPid.setMuonLLBg(-10000.);
      muPid.setChi2Corr( -1. );

      auto trackmom = track.p();
      bool preSel   = ( trackmom > cond.m_preSelMomentum );
      muPid.setPreSelMomentum( preSel );
      if ( !preSel ) return muPid;
      ++momCutCount;

      // Store the muonPIDs only for tracks passing InAcceptance
      MuonTrackExtrapolation extrapolation = cond.extrapolateTrack( track.endScifiState() );
      bool                   inAcceptance  = cond.inAcceptance( extrapolation );
      muPid.setInAcceptance( inAcceptance );
      if ( !inAcceptance ) return muPid;
      ++acceptanceCount;

      auto [occupancies, occupanciesTight] = cond.countHits( trackmom, extrapolation, hitContainer );
      bool isM                             = cond.isMuon( occupancies, trackmom );
      bool isMTight                        = cond.isMuon( occupanciesTight, trackmom );
      muPid.setIsMuon( isM );
      muPid.setIsMuonTight( isMTight );
      if ( isM ) ++isMuonCount;
      if ( isMTight ) ++isTightCount;
      return muPid;
    };
    if constexpr ( std::is_same_v<MuonPIDs, LHCb::Pr::Muon::PIDs> ) {
      auto iterable_tracks_scalar = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar, true>( tracks );
      return Zipping::transform<LHCb::Pr::Muon::PIDs>( iterable_tracks_scalar, muonIDlambda );
    } else {
      return Zipping::transform<LHCb::Event::v2::MuonID, std::vector>( tracks, muonIDlambda );
    }
  }

private:
  mutable Gaudi::Accumulators::Counter<> m_momCutCount{this, "nMomentumCut"};
  mutable Gaudi::Accumulators::Counter<> m_goodCount{this, "nGoodOffline"};
  mutable Gaudi::Accumulators::Counter<> m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<> m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<> m_isTightCount{this, "nIsMuonTight"};

  Gaudi::Property<bool> useTTrack_{this, "useTTrack", false};
  Gaudi::Property<bool> m_ComputeChi2{this, "ComputeChi2", true};
};

using MuonIDHlt1Alg_v2 = MuonIDHlt1Alg<v2_Tracks_Zip, v2_MuonPIDs>;
DECLARE_COMPONENT_WITH_ID( MuonIDHlt1Alg_v2, "MuonIDHlt1Alg" )

using MuonIDHlt1Alg_pr = MuonIDHlt1Alg<LHCb::Pr::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
DECLARE_COMPONENT_WITH_ID( MuonIDHlt1Alg_pr, "MuonIDHlt1AlgPr" )
