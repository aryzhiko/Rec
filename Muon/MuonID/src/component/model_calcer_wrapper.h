/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <stddef.h>

#if defined( __cplusplus )
extern "C" {
#endif

typedef void ModelCalcerHandle;

ModelCalcerHandle* ModelCalcerCreate();
void               ModelCalcerDelete( ModelCalcerHandle* calcer );

const char* GetErrorString();

bool LoadFullModelFromFile( ModelCalcerHandle* calcer, const char* filename );

bool CalcModelPredictionFlat( ModelCalcerHandle* calcer, size_t docCount, const float** floatFeatures,
                              size_t floatFeaturesSize, double* result, size_t resultSize );

bool CalcModelPrediction( ModelCalcerHandle* calcer, size_t docCount, const float** floatFeatures,
                          size_t floatFeaturesSize, const char*** catFeatures, size_t catFeaturesSize, double* result,
                          size_t resultSize );

bool CalcModelPredictionWithHashedCatFeatures( ModelCalcerHandle* calcer, size_t docCount, const float** floatFeatures,
                                               size_t floatFeaturesSize, const int** catFeatures,
                                               size_t catFeaturesSize, double* result, size_t resultSize );

int GetStringCatFeatureHash( const char* data, size_t size );
int GetIntegerCatFeatureHash( long long val );

size_t GetFloatFeaturesCount( ModelCalcerHandle* calcer );
size_t GetCatFeaturesCount( ModelCalcerHandle* calcer );
#if defined( __cplusplus )
}
#endif
