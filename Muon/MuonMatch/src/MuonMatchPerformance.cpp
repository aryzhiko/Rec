/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <array>
#include <type_traits>

// Local
#include "MuonMatchPerformance.h"

using namespace MuonMatch;

namespace {
  using TrackToMCParticlesLinker = InputLinks<tmp::Track, LHCb::MCParticle>;
}

DECLARE_COMPONENT( MuonMatchPerformance )

//=============================================================================
//
MuonMatchPerformance::MuonMatchPerformance( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputTracks", LHCb::TrackLocation::Forward},
                 KeyValue{"LinkerLocation", Links::location( LHCb::TrackLocation::Forward )}} ) {}

//=============================================================================
//
void MuonMatchPerformance::operator()( const tmp::Tracks& tracks, const LHCb::LinksByKey& links ) const {

  // Used for array indexes!  Don't change the numbers!
  enum MatchType {
    PureGhost,
    Ghost,
    Daughter,
    Beauty,
    DirectlyBeauty,
    Charm,
    DirectlyCharm,
    Strange,
    DirectlyStrange,
    Skipped,
    Undefined,
    // Must be the last so we know the size of the array to be created
    nMatchTypes
  };

  // Temporal storage
  std::array<size_t, nMatchTypes> counters;
  for ( auto it = counters.begin(); it != counters.end(); ++it ) *it = 0;

  // Access the linker table
  const TrackToMCParticlesLinker linker( links );

  for ( const auto track : tracks ) {

    // Get the relations table
    const auto relations = linker.from( track->key() );

    const auto best = std::max_element( relations.begin(), relations.end(), []( const auto& f, const auto& s ) {
      return s.weight() > f.weight() ? s.weight() : f.weight();
    } );

    if ( best == relations.end() ) {
      ++counters[PureGhost];
      continue;
    }

    // Check that the weight of the linker satisfies the minimum
    if ( best->weight() < m_minMCMatchWeight ) {
      ++counters[Ghost];
      continue;
    }

    // Check whether the track matches with at least one of the particle IDs
    const auto particle = best->to();

    if ( std::all_of( m_daughterPIDs.begin(), m_daughterPIDs.end(),
                      [&particle]( const auto& pid ) { return particle->particleID().pid() != pid; } ) )
      continue;

    // Check whether the particle satisfies the acceptance requirements
    if ( m_acceptanceCuts && ( particle->pseudoRapidity() < 1.5 || particle->pseudoRapidity() > 5.5 ||
                               particle->p() < 2.5 * Gaudi::Units::GeV ) )
      continue;

    // Look at the parent particle
    auto mother = particle->mother();

    if ( !mother ) {
      ++counters[Undefined];
      continue;
    }

    // Checks directly on the parent
    const auto& mother_pid = mother->particleID();

    if ( std::any_of( m_skipMotherPIDs.begin(), m_skipMotherPIDs.end(),
                      [&mother_pid]( int pid ) { return mother_pid.pid() == pid; } ) ) {
      ++counters[Skipped];
      continue;
    }

    ++counters[Daughter];

    // Checks are exclusive
    ++counters[mother_pid.hasQuark( LHCb::ParticleID::bottom )
                   ? DirectlyBeauty
                   : mother_pid.hasQuark( LHCb::ParticleID::charm )
                         ? DirectlyCharm
                         : mother_pid.hasQuark( LHCb::ParticleID::strange ) ? DirectlyStrange : Undefined];

    // Check on all the parent particles
    auto any_ancestor_has_quark = [mother, &counters]( LHCb::ParticleID::Quark quark, MatchType mtype ) {
      auto ancestor = mother;

      // "ancestor" is already a valid pointer
      do {

        const auto& mpid = ancestor->particleID();

        if ( mpid.hasQuark( quark ) ) {
          ++counters[mtype];
          return;
        }

        ancestor = ancestor->mother();

      } while ( ancestor );
    };

    any_ancestor_has_quark( LHCb::ParticleID::bottom, Beauty );
    any_ancestor_has_quark( LHCb::ParticleID::charm, Charm );
    any_ancestor_has_quark( LHCb::ParticleID::strange, Strange );
  }

  // Fill the counters
  m_inputTracks += tracks.size();
  m_pureGhostTracks += counters[PureGhost];
  m_ghostTracks += counters[Ghost];
  m_matchedDaughters += counters[Daughter];
  m_directlyFromBeauty += counters[DirectlyBeauty];
  m_fromBeauty += counters[Beauty];
  m_directlyFromCharm += counters[DirectlyCharm];
  m_fromCharm += counters[Charm];
  m_directlyFromStrange += counters[DirectlyStrange];
  m_fromStrange += counters[Strange];
  m_fromSkipped += counters[Skipped];
  m_fromUndefined += counters[Undefined];
}
