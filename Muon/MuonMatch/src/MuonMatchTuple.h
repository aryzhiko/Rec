/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCHTUPLE_H
#define MUONMATCHTUPLE_H 1

// STL
#include <string>
#include <type_traits>
#include <vector>

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"

// from LHCb
#include "Associators/InputLinks.h"
#include "Associators/Location.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/ODIN.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkerWithKey.h"

// from MuonDAQ
#include "MuonDAQ/MuonHitContainer.h"

// from Local
#include "MuonMatch/Tracks_v2.h"

namespace MuonMatch {
  namespace tmp {
    using Track  = LHCb::Track;
    using Tracks = LHCb::Tracks;
  } // namespace tmp
} // namespace MuonMatch

namespace {
  using namespace MuonMatch;

  using TrackToMCParticlesLinker = InputLinks<tmp::Track, LHCb::MCParticle>;
  using MCParticleToMCHitLinker  = LinkedFrom<LHCb::MCHit, LHCb::MCParticle>;
} // namespace

/** @class MuonMatchTupleAlg MuonMatchTuple.h
 *
 *
 *  @author Miguel Ramos Pernas
 *  @date   2018-02-01
 */
class MuonMatchTupleAlg : public GaudiTupleAlg {

public:
  using GaudiTupleAlg::GaudiTupleAlg;

  /// Initialize the algorithm
  virtual StatusCode initialize() override;

protected:
  /// Magnetic field service
  ILHCbMagnetSvc* m_fieldSvc = nullptr;

  /// Fill a tuple with the event information
  void fill_with_event_info( Tuples::Tuple& tuple, const LHCb::ODIN& odin ) const;

  /// Fill a tuple with MC-match information
  void fill_with_mcmatch( Tuples::Tuple& tuple, const std::string& prefix, const LHCb::MCParticle* particle ) const;
};

/** @class MuonMatchTracksTuple MuonMatchTuple.h
 *
 *  @author Miguel Ramos Pernas
 *  @date   2018-02-01
 */
class MuonMatchTracksTuple final
    : public Gaudi::Functional::Consumer<void( const tmp::Tracks&, const Vertices&, const LHCb::ODIN&,
                                               const LHCb::LinksByKey& ),
                                         Gaudi::Functional::Traits::BaseClass_t<MuonMatchTupleAlg>> {
public:
  /// Build the class
  MuonMatchTracksTuple( const std::string& name, ISvcLocator* pSvcLocator );

  /// Execute the algorithm, filling the output tuple
  void operator()( const tmp::Tracks&, const Vertices&, const LHCb::ODIN&, const LHCb::LinksByKey& ) const override;

protected:
  Gaudi::Property<std::vector<std::underlying_type<LHCb::State::Location>::type>> m_trackStates{
      this, "TrackStates", {}, "List of track states, whose information will be added to the tuple"};
};

/** @class MuonMatchHitsTuple MuonMatchTuple.h
 *
 *  @author Miguel Ramos Pernas
 *  @date   2018-02-01
 */
class MuonMatchHitsTuple final
    : public Gaudi::Functional::Consumer<void( const MuonHitContainer&, const LHCb::ODIN&, const LHCb::MCParticles& ),
                                         Gaudi::Functional::Traits::BaseClass_t<MuonMatchTupleAlg>> {

public:
  /// Build the class
  MuonMatchHitsTuple( const std::string& name, ISvcLocator* pSvcLocator );

  /// Execute the algorithm, filling the output tuple
  void operator()( const MuonHitContainer&, const LHCb::ODIN&, const LHCb::MCParticles& ) const override;

  Gaudi::Property<std::string> m_linkerLocation{
      this, "LinkerLocation", Links::location( LHCb::TrackLocation::Forward ),
      "Path to the linker table relating LHCb::MCParticle to LHCb::MCHit objects"};
};

#endif // MUONMATCHTUPLE_H
