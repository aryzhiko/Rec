#!/usr/bin/env python
###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Create the n-tuple for the VeloUT-Muon matching studies
'''

__author__ = ['Miguel Ramos Pernas']
__email__ = ['miguel.ramos.pernas@cern.ch']

# Python
import argparse
import cppyy
import os
import warnings

from cppyy.gbl import LHCb

# Gaudi
from Configurables import Brunel, FTRawBankDecoder, GaudiSequencer, HistogramPersistencySvc, MCParticle2MCHitAlg, MuonMatchVeloUTSoA, MuonMatchHitsTuple, MuonRawToHits, MuonMatchTracksTuple, NTupleSvc, TrackSys
from GaudiPython import AppMgr
from Gaudi.Configuration import appendPostConfigAction, FileCatalog, importOptions
from PRConfig import TestFileDB


class PostConfig(object):
    def __init__(self, make_muonhits=False):
        '''
        If "make_muonhits" is set to True, then the members in the sequence
        RecoMUONSeq will be replaced MuonRawToHits.
        '''
        self._make_muonhits = make_muonhits

    def __call__(self):
        '''
        Brunel post-configuration actions.
        '''
        GaudiSequencer('RecoCALOSeq').Members = []
        GaudiSequencer('RecoCALOFUTURESeq').Members = []
        GaudiSequencer('RecoPROTOSeq').Members = []
        GaudiSequencer('RecoRICHSeq').Members = []
        GaudiSequencer('CheckRICHSeq').Members = []
        GaudiSequencer('RecoSUMMARYSeq').Members = []
        GaudiSequencer('LumiSeq').Members = []
        GaudiSequencer('MoniSeq').Members = []

        if self._make_muonhits:
            GaudiSequencer('RecoTrFastSeq').Members += [MuonRawToHits()]


def configure(printfreq, output, set_qop, seq_type):
    '''
    Configure the output data and histograms.
    '''
    name, ext = os.path.splitext(output)
    HistogramPersistencySvc().OutputFile = name + '-Brunel-histos' + ext

    NTupleSvc().Output = [
        "FILE1 DATAFILE='{}' TYP='ROOT' OPT='NEW'".format(output)
    ]

    # Remove cuts in tracks
    tracksys = TrackSys()
    tracksys.ConfigHLT1['VeloUTHLT1']['minPT'] = 0
    tracksys.ConfigHLT1['VeloUTHLT1']['IPCutVal'] = 0
    tracksys.ConfigHLT1['ForwardHLT1']['PreselectionPT'] = 0
    tracksys.ConfigHLT1['ForwardHLT1']['minPT'] = 0

    # Configure Brunel (depends on the sequence to run)
    brunel = Brunel()

    if seq_type == 'muonmatch':

        tracksys.TrackingSequence = ['TrMuonMatch']

        MuonMatchVeloUTSoA().SetQOverP = set_qop

        states_by_track_type = {
            'Velo': [],
            'Upstream': [LHCb.State.EndVelo],
            'MuonMatchVeloUT': [LHCb.State.EndVelo],
            'ForwardFast': [LHCb.State.EndVelo, LHCb.State.AtT],
        }

        postconfig = PostConfig()
    else:
        states_by_track_type = {
            'Velo': [],
            'Upstream': [LHCb.State.EndVelo],
            'ForwardFast': [LHCb.State.EndVelo, LHCb.State.AtT],
        }
        if set_qop:
            warnings.warn(
                'Specifying --set-qop has no effect if the sequence is "main"',
                Warning)
        postconfig = PostConfig(make_muonhits=True)

    tracksys.TracksToConvert = list(states_by_track_type.keys())

    appendPostConfigAction(postconfig)

    # Create associator for muon hits
    muon_assoc = MCParticle2MCHitAlg(
        'MCP2MuonMCHitAlg',
        MCHitPath='MC/Muon/Hits',
        OutputData='/Event/MC/Particles2MCMuonHits')

    # Create the n-tuples
    muon_hits_tuple = MuonMatchHitsTuple()
    muon_hits_tuple.LinkerLocation = muon_assoc.OutputData

    muon_hits_tuple_algos = [muon_assoc, muon_hits_tuple]

    tracks_tuple_algos = []
    for ttype, states in states_by_track_type.items():

        ta = MuonMatchTracksTuple('{}TracksTuple'.format(ttype))
        ta.InputTracks = 'Rec/Track/Keyed/{}'.format(ttype)
        ta.InputVertices = 'Rec/Vertex/Vector/Primary'
        ta.LinkerLocation = 'Link/Rec/Track/Keyed/{}'.format(ttype)
        ta.TrackStates = states

        tracks_tuple_algos.append(ta)

    brunel.Detectors = ['VP', 'UT', 'FT', 'Muon', 'Magnet', 'Tr']
    brunel.OutputType = 'NONE'
    brunel.RecoSequence = ['Decoding', 'TrFast']
    brunel.MainSequence = ['ProcessPhase/Reco', 'ProcessPhase/MCLinks'
                           ] + muon_hits_tuple_algos + tracks_tuple_algos
    brunel.PrintFreq = printfreq
    brunel.InputType = 'DIGI'
    brunel.Simulation = True
    brunel.WithMC = True

    return brunel


def full(data, options, printfreq, output, set_qop, seq_type):
    '''
    Run over all the input data.
    '''
    configure(printfreq, output, set_qop, seq_type)
    for mod in (data, options):
        importOptions(mod)


def test(tfdb, printfreq, output, set_qop, seq_type):
    '''
    Run a test on a TestFileDB entry.
    '''
    brunel = configure(printfreq, output, set_qop, seq_type)

    TestFileDB.test_file_db[tfdb].run(configurable=brunel)

    if tfdb == 'upgrade-magdown-sim09c-up02-minbias-xdigi':
        FTRawBankDecoder('createFTClusters').DecodingVersion = 2
    else:
        FTRawBankDecoder('createFTClusters').DecodingVersion = 6


def add_arguments(parser):
    '''
    Add arguments to the given parser.
    '''
    parser.add_argument(
        '--printfreq',
        type=int,
        default=3000,
        help='Frequency to print events')
    parser.add_argument(
        '--output',
        type=str,
        default='muonmatch-ntuple.root',
        help='Name of the ROOT output file storing the n-tuples')
    parser.add_argument(
        '--set-qop',
        action='store_true',
        help=
        'Whether to modify the momentum using the MuonMatchVeloUT algorithm or not'
    )
    parser.add_argument('--seq-type', type=str, default='main', choices=('main', 'muonmatch'),
                        help='Type of sequence to execute. "main" will execute the full '\
                        'reconstruction sequence, while "muonmatch" will simply '\
                        'run the reconstruction till the ForwardFast sequence '\
                        'runs (taking the output from the MuonMatchVeloUT '\
                        'algorithm. Only in the "muonmatch" sequence the '\
                        'previous algorithm is run.')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    subparsers = parser.add_subparsers(help='Mode to run')

    # Configure the "full" mode
    parser_full = subparsers.add_parser('full', help=full.__doc__)
    parser_full.set_defaults(configuration=full)

    parser_full.add_argument('--data', type=str, required=True,
                             help='Python file defining the files to process. It must '\
                             'also load the associated file catalog, if necessary. Must '\
                             'be in the same directory as this script.')

    parser_full.add_argument('--options', type=str, required=True,
                             help='Python file defining extra options, like the '\
                             'CondDB and DDDB tags, as well as the version '\
                             'of FTRawBankDecoder("createFTClusters"). Must '\
                             'be in the same directory as this script.')

    # Configure the "test" mode
    parser_test = subparsers.add_parser('test', help=test.__doc__)
    parser_test.set_defaults(configuration=test)

    parser_test.add_argument(
        '--tfdb',
        type=str,
        default='upgrade-magdown-sim09c-up02-minbias-xdigi',
        choices=('upgrade-magdown-sim09c-up02-minbias-xdigi',
                 'upgrade-magup-sim10-up01-34112106-xdigi',
                 'upgrade-magdown-sim10-up01-24142001-xdigi'),
        help='TestFileDB entry to process')
    parser_test.add_argument(
        '--nevts', type=int, default=-1, help='Number of events to process')

    for p in (parser_test, parser_full):
        add_arguments(p)

    opts = vars(parser.parse_args())
    conf = opts.pop('configuration')
    nevts = opts.pop('nevts', -1)

    conf(**opts)

    gaudi = AppMgr()
    gaudi.initialize()
    gaudi.run(nevts)
    gaudi.stop()
    gaudi.finalize()
