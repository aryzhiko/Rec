/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class MeasurementProviderT MeasurementProviderT.h
 *
 * Implementation of templated MeasurementProvider tool
 * see interface header for description
 *
 *  @author A. Usachov
 *  @date   15/10/2019
 */

#ifndef MEASUREMENTPROVIDERT_H
#define MEASUREMENTPROVIDERT_H 1

#include "MeasurementProviderProjector.h"

#include "Event/TrackParameters.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ToolHandle.h"
#include <type_traits>

template <typename T>
class MeasurementProviderT final : public MeasurementProviderProjector {
public:
  using MeasurementProviderProjector::MeasurementProviderProjector;

  StatusCode initialize() override;
  void       addToMeasurements( LHCb::span<LHCb::LHCbID> ids, std::vector<LHCb::Measurement>& measurements,
                                LHCb::ZTrajectory<double> const& reftraj ) const override;

  StatusCode load( LHCb::Track& ) const override {
    info() << "sorry, MeasurementProviderBase::load not implemented" << endmsg;
    return StatusCode::FAILURE;
  }

private:
  LHCb::Measurement measurement( typename T::Cluster const& cluster, bool localY ) const;
  LHCb::Measurement measurement( typename T::Cluster const& cluster, LHCb::ZTrajectory<double> const& refvector,
                                 bool localY ) const;

  DataObjectReadHandle<typename T::ClusterContainerType> m_clustersDH{this, "ClusterLocation",
                                                                      T::defaultClusterLocation()};

  Gaudi::Property<bool>                    m_useReference{this, "UseReference", true};
  ToolHandle<typename T::PositionToolType> m_positiontool = {T::positionToolName()};
  const typename T::DetectorType*          m_det          = nullptr;
};

#endif //  MEASUREMENTPROVIDERT_H
