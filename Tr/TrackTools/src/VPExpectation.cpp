/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "VPExpectation.h"

#include "Event/State.h"
#include "Event/TrackParameters.h"

#include "TrackKernel/TrackFunctors.h"
#include "TsaKernel/Line.h"

DECLARE_COMPONENT( VPExpectation )

//=============================================================================
// Initialisation
//=============================================================================
StatusCode VPExpectation::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_det = getDetIfExists<DeVP>( DeVPLocation::Default );
  if ( !m_det ) { return Error( "No detector element at " + DeVPLocation::Default ); }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Return number of hits expected
//=============================================================================
int VPExpectation::nExpected( const LHCb::Track& track ) const {
  IVPExpectation::Info expectedHits = expectedInfo( track );
  return expectedHits.n;
}

//=============================================================================
// Return info
//=============================================================================
IVPExpectation::Info VPExpectation::expectedInfo( const LHCb::Track& track ) const {

  // Work out the first and last z on the track
  double zStart = -9999.;
  double zStop  = 9999.;
  if ( track.checkFlag( LHCb::Track::Flags::Backward ) == false ) {
    // forward track
    zStart = zMin( track ) - 1e-3;
  } else {
    // backward track
    zStop = zMax( track ) + 1e-3;
  }

  return expectedInfo( track, zStart, zStop );
}

//=============================================================================
// Return number of hits expected within a given z range
//=============================================================================
int VPExpectation::nExpected( const LHCb::Track& track, const double zStart, const double zStop ) const {

  IVPExpectation::Info expectedHits = expectedInfo( track, zStart, zStop );
  return expectedHits.n;
}

//=============================================================================
// Return info for a given z range
//=============================================================================
IVPExpectation::Info VPExpectation::expectedInfo( const LHCb::Track& track, const double zStart,
                                                  const double zStop ) const {

  return scan( track, zStart, zStop );
}

//=============================================================================
// Check whether a track crosses the active area of a sensor
//=============================================================================
bool VPExpectation::isInside( const LHCb::Track& track, const unsigned int sensorNum ) const {

  // Make a line representing the track
  const LHCb::State& state = track.firstState();
  Tf::Tsa::Line      xLine( state.tx(), state.x(), state.z() );
  Tf::Tsa::Line      yLine( state.ty(), state.y(), state.z() );
  const DeVPSensor&  sensor = m_det->sensor( sensorNum );
  const double       z      = sensor.z();
  return sensor.isInsideSensor( xLine.value( z ), yLine.value( z ) );
}

//=============================================================================
// Return the number of missed hits on a track
//=============================================================================
int VPExpectation::nMissed( const LHCb::Track& track ) const {

  // Determine the z-range.
  double zStart;
  double zStop;
  if ( track.checkFlag( LHCb::Track::Flags::Backward ) == false ) {
    zStart = zBeamLine( track ) - 1e-3;
    zStop  = zMin( track );
  } else {
    zStart = zMax( track ) - 1e-3;
    zStop  = zBeamLine( track );
  }
  // Get the expected number of hits.
  IVPExpectation::Info expectedHits = scan( track, zStart, zStop );
  return expectedHits.n - nFound( track, zStart, zStop );
}

//=============================================================================
// Return the number of missed hits on a track, for a given z range
//=============================================================================
int VPExpectation::nMissed( const LHCb::Track& track, const double z ) const {

  // Determine the z-range.
  double zStart = z;
  double zStop  = z;
  if ( track.checkFlag( LHCb::Track::Flags::Backward ) == false ) {
    zStop = zMin( track ) + 1.e-3;
  } else {
    zStart = zMax( track ) - 1.e-3;
  }
  // Get the expected number of hits.
  IVPExpectation::Info expectedHits = scan( track, zStart, zStop );
  return expectedHits.n - nFound( track, zStart, zStop );
}

//=============================================================================
// Return the expected number of measurements and smallest radius
//=============================================================================
IVPExpectation::Info VPExpectation::scan( const LHCb::Track& track, const double zStart, const double zStop ) const {

  IVPExpectation::Info nHits;
  nHits.n      = 0;
  nHits.firstR = 99999.;

  m_det->runOnAllSensors( [&track, &nHits, zStart, zStop]( const DeVPSensor& sensor ) {
    // Skip sensors outside the range.
    const double z = sensor.z();
    if ( z < zStart || z > zStop ) return;
    auto          state = closestState( track, z );
    Tf::Tsa::Line xLine( state.tx(), state.x(), state.z() );
    Tf::Tsa::Line yLine( state.ty(), state.y(), state.z() );
    const double  x = xLine.value( z );
    const double  y = yLine.value( z );
    if ( !sensor.isInsideSensor( x, y ) ) return;
    ++nHits.n;
    const double r = sqrt( x * x + y * y );
    if ( nHits.firstR > r ) {
      nHits.firstX = x;
      nHits.firstY = y;
      nHits.firstZ = z;
      nHits.firstR = r;
    }
    nHits.expectedZ.push_back( z );
  } );
  return nHits;
}

//=============================================================================
// Get the lowest z coordinate of all measurements on the track
//=============================================================================
double VPExpectation::zMin( const LHCb::Track& track ) const {

  // get the hit at least z
  double z = 99999.0;
  for ( auto id : track.lhcbIDs() ) {
    if ( !id.isVP() ) continue;
    try {
      const DeVPSensor& sensor = m_det->sensor( id.vpID() );
      if ( sensor.z() < z ) z = sensor.z();
    } catch ( std::runtime_error& e ) {
      warning() << "No sensor for " << id.vpID() << endmsg;
      continue;
    }
  }
  return z;
}

//=============================================================================
// Get the largest z coordinate of all measurements on the track
//=============================================================================
double VPExpectation::zMax( const LHCb::Track& track ) const {

  double z = -99999.0;
  for ( auto id : track.lhcbIDs() ) {
    if ( !id.isVP() ) continue;
    try {
      const DeVPSensor& sensor = m_det->sensor( id.vpID() );
      if ( sensor.z() > z ) z = sensor.z();
    } catch ( std::runtime_error& e ) {
      warning() << "No sensor for " << id.vpID() << endmsg;
      continue;
    }
  }
  return z;
}

//=============================================================================
// Get the number of measurements within a given z range
//=============================================================================
int VPExpectation::nFound( const LHCb::Track& track, const double zStart, const double zStop ) const {
  int nFound = 0;
  for ( auto id : track.lhcbIDs() ) {
    if ( !id.isVP() ) continue;
    try {
      const DeVPSensor& sensor = m_det->sensor( id.vpID() );
      if ( sensor.z() >= zStart && sensor.z() <= zStop ) ++nFound;
    } catch ( std::runtime_error& e ) {
      warning() << "No sensor for " << id.vpID() << endmsg;
      continue;
    }
  }
  return nFound;
}

//=============================================================================
// Get z-position of the closest distance to the beam line
//=============================================================================
double VPExpectation::zBeamLine( const LHCb::Track& track ) const {

  const LHCb::State& state = closestState( track, 0. );
  double             z     = state.z();
  if ( state.checkLocation( LHCb::State::Location::ClosestToBeam ) ) return z;
  const Gaudi::TrackVector& vec = state.stateVector();
  // check on division by zero (track parallel to beam line!)
  if ( fabs( vec[2] ) > TrackParameters::lowTolerance || vec[3] > TrackParameters::lowTolerance ) {
    z -= ( vec[0] * vec[2] + vec[1] * vec[3] ) / ( vec[2] * vec[2] + vec[3] * vec[3] );
  }
  return z;
}
