/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Measurement.h"

#include "FTDet/DeFTMat.h"
#include "MuonDet/DeMuonChamber.h"
#include "UTDet/DeUTSector.h"
#include "VPDet/DeVPSensor.h"

const DetectorElement* LHCb::Measurement::detectorElement() const {
  return visit( []( const FT& ft ) -> const DetectorElement* { return ft.mat; },
                []( const Muon& m ) -> const DetectorElement* { return m.chamber; },
                []( const UTLite& ut ) -> const DetectorElement* { return ut.sector; },
                []( const VP& vp ) -> const DetectorElement* { return vp.sensor; } );
}

Gaudi::XYZPoint LHCb::Measurement::toLocal( const Gaudi::XYZPoint& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZPoint { return ft.mat->toLocal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZPoint { return m.chamber->toLocal( globalPoint ); },
                []( const UTLite& ) -> Gaudi::XYZPoint { throw std::string( "UTLite has no toLocal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZPoint { return vp.sensor->toLocal( globalPoint ); } );
}

Gaudi::XYZVector LHCb::Measurement::toLocal( const Gaudi::XYZVector& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZVector { return ft.mat->toLocal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZVector { return m.chamber->toLocal( globalPoint ); },
                []( const UTLite& ) -> Gaudi::XYZVector { throw std::string( "UTLite has no toLocal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZVector { return vp.sensor->toLocal( globalPoint ); } );
}

Gaudi::XYZPoint LHCb::Measurement::toGlobal( const Gaudi::XYZPoint& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZPoint { return ft.mat->toGlobal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZPoint { return m.chamber->toGlobal( globalPoint ); },
                []( const UTLite& ) -> Gaudi::XYZPoint { throw std::string( "UTLite has no toGlobal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZPoint { return vp.sensor->toGlobal( globalPoint ); } );
}

Gaudi::XYZVector LHCb::Measurement::toGlobal( const Gaudi::XYZVector& globalPoint ) const {
  return visit( [&globalPoint]( const FT& ft ) -> Gaudi::XYZVector { return ft.mat->toGlobal( globalPoint ); },
                [&globalPoint]( const Muon& m ) -> Gaudi::XYZVector { return m.chamber->toGlobal( globalPoint ); },
                []( const UTLite& ) -> Gaudi::XYZVector { throw std::string( "UTLite has no toGlobal method" ); },
                [&globalPoint]( const VP& vp ) -> Gaudi::XYZVector { return vp.sensor->toGlobal( globalPoint ); } );
}

const std::string LHCb::Measurement::name() const {
  return visit( []( const FT& ft ) -> const std::string { return ft.mat->name(); },
                []( const Muon& m ) -> const std::string { return m.chamber->name(); },
                []( const UTLite& ut ) -> const std::string { return ut.sector->name(); },
                []( const VP& vp ) -> const std::string { return vp.sensor->name(); } );
}

bool LHCb::Measurement::isSameDetectorElement( const Measurement& other ) const {
  // then check the detector element's pointers
  return visit( [&other]( const FT& ft ) -> bool { return other.getIf<FT>()->mat == ft.mat; },
                [&other]( const Muon& m ) -> bool { return other.getIf<Muon>()->chamber == m.chamber; },
                [&other]( const UTLite& ut ) -> bool { return other.getIf<UTLite>()->sector == ut.sector; },
                [&other]( const VP& vp ) -> bool { return other.getIf<VP>()->sensor == vp.sensor; } );
}
