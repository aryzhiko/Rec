/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/STLexicalCaster.h"
#include "STDet/DeITBox.h"
#include "STDet/DeITDetector.h"
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTLayer.h"
#include "STDet/DeSTSector.h"
#include "STDet/DeSTSensor.h"
#include "TrackMonitorBase.h"
#include <fstream>
#include <iostream>

using namespace LHCb;
using namespace ST;
using namespace Gaudi;
using namespace std;

/** @class STCoordinates STCoordinates.h
 *  ...
 *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-05-31
 */
class STCoordinates : public TrackMonitorBase {
public:
  /// Standard constructor
  STCoordinates( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  DeSTDetector*                m_tracker;
  Gaudi::Property<std::string> m_detType{this, "DetType", "IT"};
  Gaudi::Property<bool>        m_printsectorinfo{this, "PrintSectorInfo", true};
  Gaudi::Property<std::string> m_alignmenttag{this, "AlignmentTag", "v4_2"};
};

//-----------------------------------------------------------------------------
// Implementation file for class : STCoordinates
//
// 2010-05-31 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( STCoordinates )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
STCoordinates::STCoordinates( const std::string& name, ISvcLocator* pSvcLocator )
    : TrackMonitorBase( name, pSvcLocator ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode STCoordinates::initialize() {
  return TrackMonitorBase::initialize().andThen(
      [&] { m_tracker = getDet<DeSTDetector>( DeSTDetLocation::location( m_detType ) ); } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode STCoordinates::execute() { return StatusCode::SUCCESS; }

//=============================================================================
//  Finalize
//=============================================================================
StatusCode STCoordinates::finalize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  if ( m_printsectorinfo.value() ) {
    const DeSTDetector::Sectors& sectors = m_tracker->sectors();
    const DeSTDetector::Layers&  layers  = m_tracker->layers();

    std::string OutputNameSectors( m_detType + "Coordinates-" + m_alignmenttag + "-Sectors.txt" );
    std::string OutputNameLayers( m_detType + "Coordinates-" + m_alignmenttag + "-Layers.txt" );

    ofstream OutputSectors( OutputNameSectors.c_str() );
    ofstream OutputLayers( OutputNameLayers.c_str() );

    info() << m_detType << endmsg;
    info() << endmsg;

    if ( m_detType == "IT" ) {
      DeITDetector* m_ITtracker = dynamic_cast<DeITDetector*>( m_tracker );
      std::string   OutputNameBoxes( m_detType + "Coordinates-" + m_alignmenttag + "-Boxes.txt" );
      ofstream      OutputBoxes( OutputNameBoxes.c_str() );
      if ( m_ITtracker != 0 ) {
        const DeITDetector::Boxes&          boxes = m_ITtracker->boxes();
        DeITDetector::Boxes::const_iterator iBox  = boxes.begin();
        for ( ; iBox != boxes.end(); ++iBox ) {
          info() << ( *iBox )->nickname() << " " << ( *iBox )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endmsg;
          OutputBoxes << ( *iBox )->nickname() << " " << ( *iBox )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endl;
        }
      }
      info() << endmsg;
      OutputBoxes.close();
    }

    DeSTDetector::Layers::const_iterator iLayer = layers.begin();

    for ( ; iLayer != layers.end(); ++iLayer ) {
      info() << ( *iLayer )->nickname() << " " << ( *iLayer )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endmsg;
      OutputLayers << ( *iLayer )->nickname() << " " << ( *iLayer )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endl;
    }

    info() << endmsg;

    DeSTDetector::Sectors::const_iterator iSect = sectors.begin();

    for ( ; iSect != sectors.end(); ++iSect ) {
      info() << ( *iSect )->nickname() << " " << ( *iSect )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endmsg;
      OutputSectors << ( *iSect )->nickname() << " " << ( *iSect )->toGlobal( Gaudi::XYZPoint( 0., 0., 0. ) ) << endl;
    }

    info() << endmsg;
    OutputSectors.close();
    OutputLayers.close();
  }

  return TrackMonitorBase::finalize(); // must be called after all other actions
}

//=============================================================================
