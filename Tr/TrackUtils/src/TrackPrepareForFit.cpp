/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class TrackPrepareForFit TrackPrepareForFit.h
 *
 *  Add p (from somewhere...) to a track...
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <string>

using namespace LHCb;
using namespace Gaudi::Units;
using namespace Gaudi;

class TrackPrepareForFit : public GaudiAlgorithm {

public:
  // Constructors and destructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;

private:
  void prepare( LHCb::Track* aTrack, const int i ) const;

  Gaudi::Property<std::string> m_inputLocation{this, "inputLocation", TrackLocation::Velo};
  Gaudi::Property<double>      m_ptVelo{this, "ptVelo", 400. * MeV};
  Gaudi::Property<double>      m_curvValue{this, "curvValue", 1.0 / ( 7.0 * GeV )};
  Gaudi::Property<bool>        m_fixP{this, "fixP", true};
  Gaudi::Property<bool> m_reverseCharge{this, "reverseCharge", false}; ///< Reverse the VELO random charge assignment
};

DECLARE_COMPONENT( TrackPrepareForFit )

StatusCode TrackPrepareForFit::execute() {

  // loop
  for ( auto track : *get<Tracks>( m_inputLocation ) ) {
    const auto& ids = track->lhcbIDs();
    auto        id  = std::find_if( begin( ids ), end( ids ), []( const LHCbID& id ) { return id.isVelo(); } );
    if ( id == end( ids ) ) { return Warning( "Setting can not set random q/p for non-velo track" ); }
    int firstStrip = id->veloID().strip();
    int charge     = ( firstStrip % 2 == 0 ? -1 : 1 );
    if ( m_reverseCharge.value() ) charge *= -1;
    prepare( track, charge );
  }

  return StatusCode::SUCCESS;
}

void TrackPrepareForFit::prepare( Track* aTrack, const int charge ) const {

  // do what we have to do...
  State&       vState = aTrack->firstState();
  TrackVector& vec    = vState.stateVector();
  double       slope2 = std::max( vec( 2 ) * vec( 2 ) + vec( 3 ) * vec( 3 ), 1e-20 );
  double curv = ( m_fixP.value() ? m_curvValue.value() : charge * sqrt( slope2 ) / ( m_ptVelo * sqrt( 1. + slope2 ) ) );
  // set all the state a track has....
  for ( const auto& state : aTrack->states() ) {
    State* aState = const_cast<State*>( state );
    aState->setQOverP( curv );
    aState->setErrQOverP2( 1e-3 );
  }
}
