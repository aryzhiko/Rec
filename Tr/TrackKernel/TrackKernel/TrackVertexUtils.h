#ifndef TRACKKERNEL_TRACKVERTEXUTILS_H
#define TRACKKERNEL_TRACKVERTEXUTILS_H

/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/State.h"
#include "GaudiKernel/Point3DTypes.h"

namespace LHCb {

  namespace TrackVertexUtils {

    enum ReturnStatus { Failure, Success };

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of a track state with respect to a
    /// vertex. This is also known as the 'IPCHI2'.
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATE>
    auto vertexChi2( const STATE& state, const Gaudi::XYZPoint& vertexpos, const Gaudi::SymMatrix3x3& vertexcov );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the chi2 of the vertex of two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename STATE>
    double vertexChi2( const STATE& stateA, const STATE& stateB );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the doca between two track states
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector>
    auto doca( const StateVector& stateA, const StateVector& stateB );

    ///////////////////////////////////////////////////////////////////////////
    /// Return the distance between a track state and a point
    ///////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    auto doca( const StateVector& stateA, const XYZPoint& pos );

    /////////////////////////////////////////////////////////////////////////
    /// Compute the chi2 and decaylength of a 'particle' with respect
    /// to a vertex. Return 1 if successful.
    /// This should probably go into LHCb math.
    /////////////////////////////////////////////////////////////////////////
    ReturnStatus computeChiSquare( const Gaudi::XYZPoint& pos, const Gaudi::XYZVector& mom,
                                   const Gaudi::SymMatrix6x6& cov6, const Gaudi::XYZPoint& motherpos,
                                   const Gaudi::SymMatrix3x3& mothercov, double& chi2, double& decaylength,
                                   double& decaylengtherr );

    /////////////////////////////////////////////////////////////////////////
    /// Compute the point of the doca of two track states.  Return 1 if successful.
    /////////////////////////////////////////////////////////////////////////
    template <typename StateVector, typename XYZPoint>
    ReturnStatus poca( const StateVector& stateA, const StateVector& stateB, XYZPoint& vertex );

    /////////////////////////////////////////////////////////////////////////
    /// Add a track to a vertex represented by a position and a cov
    /// matrix. Returns the chi2 increment. This routine calls the
    /// routine below that also has a vertex weight matrix, because
    /// the formalism requires the computation of both. If you add
    /// tracks to vertex one-by-one, then it makes sense to keep the
    /// weight matrix as this is more precise.
    /////////////////////////////////////////////////////////////////////////
    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Add a track to a vertex represented by a position and a weight
    /// matrix. Both the weight matrix and the covariance matrix are
    /// computed. Returns the chi2
    /////////////////////////////////////////////////////////////////////////
    double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                        Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Computes a vertex from two track states.
    /////////////////////////////////////////////////////////////////////////
    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexweight, Gaudi::SymMatrix3x3& vertexcov );

    /////////////////////////////////////////////////////////////////////////
    /// Computes a vertex from two track states.
    /////////////////////////////////////////////////////////////////////////
    double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                   Gaudi::SymMatrix3x3& vertexcov );

  } // namespace TrackVertexUtils
} // namespace LHCb

#include "TrackKernel/TrackVertexUtils.icpp"

#endif
