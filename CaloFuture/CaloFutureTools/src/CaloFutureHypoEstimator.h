/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREHYPOESTIMATOR_H
#define CALOFUTUREHYPOESTIMATOR_H 1

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h" // Interface
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloDataFunctor.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Relations/Relation1D.h"
#include "Relations/Relation2D.h"
#include "Relations/RelationWeighted2D.h"

/** @class CaloFutureHypoEstimator CaloFutureHypoEstimator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-08-18
 */

namespace LHCb::Calo {

  class HypoEstimator : public extends<GaudiTool, Interfaces::IHypoEstimator> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double>            data( const LHCb::CaloHypo& hypo, Enum::DataType type ) const override;
    std::map<Enum::DataType, double> get_data( const LHCb::CaloHypo& hypo ) const override;

    Interfaces::IHypo2Calo* hypo2Calo() override { return m_toCaloFuture.get(); }

    const LHCb::Track* toTrack( const CaloHypo& hypo, Enum::MatchType match ) const override;

    StatusCode _setProperty( const std::string& p, const std::string& v ) override { return setProperty( p, v ); };

  private:
    using HypoTrTable2D    = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
    using ClusterTrTable2D = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
    using HypoEvalTable    = LHCb::Relation1D<LHCb::CaloHypo, float>;

    std::map<Enum::DataType, double> estimator( const LHCb::CaloHypo& hypo ) const;

    Gaudi::Property<bool> m_extrapol{this, "Extrapolation", true};
    Gaudi::Property<bool> m_seed{this, "AddSeed", false};
    Gaudi::Property<bool> m_neig{this, "AddNeighbors", false};

    DataObjectReadHandle<ClusterTrTable2D> m_clusterTrTable{
        this, "ClusterMatchLocation", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )};
    DataObjectReadHandle<HypoTrTable2D> m_electronTrTable{
        this, "ElectronMatchLocation", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ElectronMatch" )};
    DataObjectReadHandle<HypoTrTable2D> m_bremTrTable{this, "BremMatchLocation",
                                                      LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "BremMatch" )};

    Gaudi::Property<bool> m_skipC{this, "SkipChargedID", false};
    Gaudi::Property<bool> m_skipN{this, "SkipNeutralID", false};
    Gaudi::Property<bool> m_skipCl{this, "SkipClusterMatch", false};

    PublicToolHandle<IFutureCounterLevel>       counterStat{this, "CounterLevel", "FutureCounterLevel"};
    ToolHandle<Interfaces::IHypo2Calo>          m_toCaloFuture{this, "Hypo2Calo", "CaloFutureHypo2CaloFuture"};
    ToolHandle<Interfaces::IElectron>           m_electron{this, "Electron", "CaloFutureElectron"};
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0{this, "Pi0Separation", "FutureGammaPi0SeparationTool"};
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0XGB{this, "Pi0SeparationXGB", "FutureGammaPi0XGBoostTool"};
    ToolHandle<Interfaces::INeutralID>          m_neutralID{this, "NeutralID", "FutureNeutralIDTool"};

    DeCalorimeter* m_ecal = nullptr;
  };
} // namespace LHCb::Calo
#endif // CALOFUTUREHYPOESTIMATOR_H
