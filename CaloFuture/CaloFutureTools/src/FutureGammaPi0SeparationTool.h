/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "GaudiAlg/GaudiTool.h"

// Math
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

// using namespace LHCb;

#include "TMV_MLP_inner.C"
#include "TMV_MLP_middle.C"
#include "TMV_MLP_outer.C"

/** @class FutureGammaPi0SeparationTool FutureGammaPi0SeparationTool.h
 *
 *
 *  @author Miriam Calvo Gomez
 *  @date   2019-03-28
 */
namespace LHCb::Calo {
  class GammaPi0Separation : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double> isPhoton( const LHCb::CaloHypo& hypo ) const override;
    std::optional<double> isPhoton( Observables const& observables ) const override;
    std::optional<double> isPhoton( LHCb::span<const double> v ) const override {
      return photonDiscriminant( int( v[0] ), v[1], v[2], v[3], v[4], v[5], v[6] );
    }
    std::optional<Observables> observables( const CaloHypo& hypo ) const override;

  private:
    std::optional<double> photonDiscriminant( int area, double r2, double r2r4, double asym, double kappa, double Eseed,
                                              double E2 ) const;

    Gaudi::Property<float> m_minPt{this, "MinPt", 2000.};

    std::unique_ptr<ReadMLPOuter>  m_reader0;
    std::unique_ptr<ReadMLPMiddle> m_reader1;
    std::unique_ptr<ReadMLPInner>  m_reader2;

    const DeCalorimeter* m_ecal = nullptr;
  };
} // namespace LHCb::Calo
