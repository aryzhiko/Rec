/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
// from Event
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/RecHeader.h"
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <string>
#include <unordered_map>

// local
#include "FutureGammaPi0XGBoostTool.h"
#include <fstream>

using namespace LHCb;
using namespace Gaudi::Units;
//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0XGBoostTool
//
// 2018-03-24 : author @sayankotor
//-----------------------------------------------------------------------------

// Declare nessesary help functionality
namespace {

  size_t getClusterType( const CaloCellID& id ) {

    const int      CaloFutureNCol[4] = {64, 32, 16, 16};
    const int      CaloFutureNRow[4] = {52, 20, 12, 12};
    const unsigned Granularity[3]    = {1, 2, 3};

    const unsigned ClusterSize = 5;
    int            type( id.area() );
    int xOffsetOut  = std::min( int( id.col() - ( 32 - CaloFutureNCol[type] * Granularity[type] / 2 ) ), // left edge
                               int( 31 + CaloFutureNCol[type] * Granularity[type] / 2 - id.col() ) );   // right edge
    int yOffsetOut  = std::min( int( id.row() - ( 32 - CaloFutureNRow[type] * Granularity[type] / 2 ) ),
                               int( 31 + CaloFutureNRow[type] * Granularity[type] / 2 - id.row() ) );
    int innerWidth  = CaloFutureNCol[type + 1] * ( type != 2 ? Granularity[type] : 1 ); // process inner hole specially
    int innerHeight = CaloFutureNRow[type + 1] * ( type != 2 ? Granularity[type] : 1 ); // process inner hole specially

    int xOffsetIn    = std::min( int( id.col() - ( 31 - innerWidth / 2 ) ), int( 32 + innerWidth / 2 - id.col() ) );
    int yOffsetIn    = std::min( int( id.row() - ( 31 - innerHeight / 2 ) ), int( 32 + innerHeight / 2 - id.row() ) );
    const int margin = ( ClusterSize - 1 ) / 2;
    bool      outerBorder = xOffsetOut < margin || yOffsetOut < margin;
    bool      innerBorder = xOffsetIn > -margin && yOffsetIn > -margin;
    if ( innerBorder )
      return type + 3;
    else if ( outerBorder )
      return type + 6;
    return type;
  }

} // namespace

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::GammaPi0XGBoost, "FutureGammaPi0XGBoostTool" )

namespace LHCb::Calo {
  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode GammaPi0XGBoost::initialize() {

    StatusCode sc = base_class::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc;          // error printed already by GaudiAlgorithm

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "==> Initialize" << endmsg;

    /// Retrieve geometry of detector
    m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    if ( !m_ecal ) return StatusCode::FAILURE;

    std::string paramRoot = getenv( "PARAMFILESROOT" );

    m_xgb[0].emplace( paramRoot + "/data/GammaPi0XgbTool_simpl0.model" );
    m_xgb[1].emplace( paramRoot + "/data/GammaPi0XgbTool_simpl1.model" );
    m_xgb[2].emplace( paramRoot + "/data/GammaPi0XgbTool_simpl2.model" );
    m_xgb[3].emplace( paramRoot + "/data/GammaPi0XgbTool_bound03.model" );
    m_xgb[4].emplace( paramRoot + "/data/GammaPi0XgbTool_bound14.model" );
    m_xgb[5].emplace( paramRoot + "/data/GammaPi0XgbTool_bound25.model" );
    m_xgb[6].emplace( paramRoot + "/data/GammaPi0XgbTool_bound06.model" );
    m_xgb[7].emplace( paramRoot + "/data/GammaPi0XgbTool_bound17.model" );
    m_xgb[8].emplace( paramRoot + "/data/GammaPi0XgbTool_bound28.model" );

    return StatusCode::SUCCESS;
  }

  //=============================================================================
  // Main execution
  //=============================================================================

  std::optional<double> GammaPi0XGBoost::isPhoton( const LHCb::CaloHypo& hypo ) const {
    if ( LHCb::CaloMomentum( &hypo ).pt() < m_minPt ) return std::nullopt;
    const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo );
    if ( !cluster ) return std::nullopt;
    int cluster_type = getClusterType( cluster->seed() );
    if ( cluster_type > static_cast<int>( m_xgb.size() ) ) {
      warning() << "GammaPi0XGBoost: Unsupperted cluster type" << endmsg;
      return std::nullopt;
    }
    std::array<double, 25> rawEnergyVector{0};
    bool                   rawEnergy = GetRawEnergy( hypo, cluster_type, rawEnergyVector );
    if ( !rawEnergy ) return std::nullopt;
    return m_xgb[cluster_type]->getClassifierValues( rawEnergyVector );
  }

  bool GammaPi0XGBoost::GetRawEnergy( const LHCb::CaloHypo& hypo, int cluster_type,
                                      LHCb::span<double, 25> rawEnergy ) const {

    const LHCb::CaloDigits*  digits_full = m_digits.get();
    const LHCb::CaloCluster* cluster =
        LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo ); // OD 2014/05 - change to Split Or Main  cluster

    if ( !digits_full || !cluster ) return false;

    LHCb::CaloCellID centerID = cluster->seed();

    auto col_numbers = std::array{(int)centerID.col() - 2, (int)centerID.col() - 1, (int)centerID.col(),
                                  (int)centerID.col() + 1, (int)centerID.col() + 2};
    auto row_numbers = std::array{(int)centerID.row() - 2, (int)centerID.row() - 1, (int)centerID.row(),
                                  (int)centerID.row() + 1, (int)centerID.row() + 2};

    auto rawEnergy_ = [&rawEnergy]( int i, int j ) -> double& { return rawEnergy[i * 5 + j]; };
    std::fill( rawEnergy.begin(), rawEnergy.end(), 0. );

    if ( cluster_type > 2 ) {
      for ( auto col_number : col_numbers ) {
        for ( auto row_number : row_numbers ) {
          const auto id_  = LHCb::CaloCellID( centerID.calo(), centerID.area(), row_number, col_number );
          auto*      test = digits_full->object( id_ );
          if ( test ) {
            rawEnergy_( col_number - (int)centerID.col() + 2, row_number - (int)centerID.row() + 2 ) = test->e();
          } else {
            continue;
          }
        }
      }
    } else {
      for ( auto col_number : col_numbers ) {
        for ( auto row_number : row_numbers ) {
          const auto id_  = LHCb::CaloCellID( centerID.calo(), centerID.area(), row_number, col_number );
          auto*      test = digits_full->object( id_ );
          if ( test ) {
            rawEnergy_( col_number - (int)centerID.col() + 2, row_number - (int)centerID.row() + 2 ) = test->e();
          } else {
            continue;
          }
        }
      }
    }
    return true;
  }
} // namespace LHCb::Calo
