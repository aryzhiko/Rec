/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "CaloFutureHypoEstimator.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiKernel/IRegistry.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypoEstimator
//
// 2010-08-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::HypoEstimator, "CaloFutureHypoEstimator" )

namespace LHCb::Calo {

  StatusCode HypoEstimator::initialize() {
    StatusCode sc = GaudiTool::initialize(); // must be executed first

    m_toCaloFuture.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    auto& h2c = dynamic_cast<IProperty&>( *m_toCaloFuture );
    h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
    h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
    h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();

    m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

    return sc;
  }

  //=============================================================================

  // ------------
  std::optional<double> HypoEstimator::data( const LHCb::CaloHypo& hypo, LHCb::Calo::Enum::DataType type ) const {
    auto data = estimator( hypo );
    auto it   = data.find( type );
    if ( it != data.end() ) return it->second;
    return {};
  }

  std::map<Enum::DataType, double> HypoEstimator::get_data( const LHCb::CaloHypo& hypo ) const {
    return estimator( hypo );
  }

  // ------------ FROM HYPO
  std::map<Enum::DataType, double> HypoEstimator::estimator( const LHCb::CaloHypo& hypo ) const {
    const auto*                      etable = m_electronTrTable.get();
    const auto*                      btable = m_bremTrTable.get();
    const auto*                      ctable = m_clusterTrTable.get();
    std::map<Enum::DataType, double> data{};
    using namespace LHCb::Calo::Enum;

    LHCb::CaloMomentum mom( &hypo );
    data[DataType::HypoE]  = mom.e();
    data[DataType::HypoEt] = mom.pt();
    data[DataType::HypoM] =
        mom.mass(); // for mergedPi0 hypothesis only (->for all clusters - toDo in CaloFutureMergedPi0Alg)
    // using extra-digits
    const LHCb::CaloHypo::Digits& digits = hypo.digits();

    // electron matching
    if ( !m_skipC ) {
      if ( etable ) {
        const auto range = etable->relations( &hypo );
        if ( !range.empty() ) {
          data[DataType::ElectronMatch] = range.front().weight();
          if ( range.front() ) {
            LHCb::ProtoParticle dummy;
            dummy.setTrack( range.front() );
            dummy.addToCalo( &hypo );
            // CaloFutureElectron->caloTrajectory must be after addToCalo
            auto hypoPair = m_electron->getElectronBrem( dummy );
            if ( hypoPair ) data[DataType::TrajectoryL] = m_electron->caloTrajectoryL( dummy, CaloPlane::ShowerMax );
          }
        }
      }

      // brem matching
      if ( btable ) {
        const auto range = btable->relations( &hypo );
        if ( !range.empty() ) { data[DataType::BremMatch] = range.front().weight(); }
      }
    }

    // link 2 cluster :
    const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo );

    // Prs info
    if ( !cluster ) {
      Warning( "Cluster point to NULL", StatusCode::SUCCESS ).ignore();
    } else {
      // Ecal seed
      if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                            LHCb::CaloDigitStatus::SeedCell );
           iseed != cluster->entries().end() ) {
        const LHCb::CaloDigit* seed   = iseed->digit();
        const LHCb::CaloCellID idseed = seed->cellID();
        double                 sum9   = 0.;
        double                 sum1   = 0.;
        // PrsE4
        std::array<double, 4> Prse4s = {};
        std::array<double, 9> Prse9  = {};

        for ( const auto& id : digits ) {

          if ( !id ) continue;
          LHCb::CaloCellID id2 = id->cellID();
          if ( abs( (int)( id )->cellID().row() - (int)idseed.row() ) < 2 &&
               abs( (int)( id )->cellID().col() - (int)idseed.col() ) < 2 ) {
            // Build sum1 and sum9
            //
            sum9 += id->e();
            if ( id->cellID().row() == idseed.row() && id->cellID().col() == idseed.col() ) sum1 = id->e();
            // Prs4
            //
            if ( id2.col() <= idseed.col() && id2.row() >= idseed.row() ) Prse4s[0] += ( id )->e();
            if ( id2.col() >= idseed.col() && id2.row() >= idseed.row() ) Prse4s[1] += ( id )->e();
            if ( id2.col() >= idseed.col() && id2.row() <= idseed.row() ) Prse4s[2] += ( id )->e();
            if ( id2.col() <= idseed.col() && id2.row() <= idseed.row() ) Prse4s[3] += ( id )->e();
            // Select the 3X3 cluster
            //
            int dcol     = (int)id2.col() - (int)idseed.col() + 1;
            int drow     = (int)id2.row() - (int)idseed.row() + 1;
            int indexPrs = drow * 3 + dcol;
            Prse9[indexPrs] += id->e();
          }
        }
        //    Build E19 and E49
        //
        double Prse4max =
            std::accumulate( Prse4s.begin(), Prse4s.end(), 0., []( double l, double r ) { return std::max( l, r ); } );
        data[DataType::PrsE4Max] = Prse4max;
        if ( sum9 > 0. ) {
          double sum9Inv         = 1. / sum9;
          data[DataType::PrsE49] = Prse4max * sum9Inv;
          data[DataType::PrsE19] = sum1 * sum9Inv;
        } else {
          data[DataType::PrsE49] = 0.;
          data[DataType::PrsE19] = 0.;
        }
        data[DataType::PrsE1] = Prse9[0];
        data[DataType::PrsE2] = Prse9[1];
        data[DataType::PrsE3] = Prse9[2];
        data[DataType::PrsE4] = Prse9[3];
        data[DataType::PrsE5] = Prse9[4];
        data[DataType::PrsE6] = Prse9[5];
        data[DataType::PrsE7] = Prse9[6];
        data[DataType::PrsE8] = Prse9[7];
        data[DataType::PrsE9] = Prse9[8];
      }
    }

    auto insert_if = [&]( DataType key, std::optional<double> val ) {
      if ( val ) data[key] = val.value();
    };

    auto observables_gamma_pi0 = m_GammaPi0->observables( hypo );
    if ( observables_gamma_pi0 ) {
      // gamma/pi0 separation
      insert_if( DataType::isPhoton, m_GammaPi0->isPhoton( observables_gamma_pi0.value() ) );
      // 1- Ecal variables :
      data[DataType::isPhotonEcl]   = observables_gamma_pi0->Ecl;
      data[DataType::isPhotonFr2]   = observables_gamma_pi0->fr2;
      data[DataType::isPhotonFr2r4] = observables_gamma_pi0->fr2r4;
      data[DataType::isPhotonAsym]  = observables_gamma_pi0->fasym;
      data[DataType::isPhotonKappa] = observables_gamma_pi0->fkappa;
      data[DataType::isPhotonEseed] = observables_gamma_pi0->Eseed;
      data[DataType::isPhotonE2]    = observables_gamma_pi0->E2;
    }

    insert_if( DataType::isPhotonXGB, m_GammaPi0XGB->isPhoton( hypo ) );
    // 1- Ecal variables :

    if ( cluster ) {
      using namespace LHCb::Calo::Enum;

      if ( cluster->entries().empty() ) {
        if ( counterStat->isQuiet() ) counter( "Empty cluster" ) += 1;
        return {};
      }
      if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                            LHCb::CaloDigitStatus::SeedCell );
           iseed != cluster->entries().end() ) {

        const LHCb::CaloDigit* seed = iseed->digit();
        if ( !seed ) {
          if ( counterStat->isQuiet() ) counter( "Seed points to NULL" ) += 1;
          return {};
        }

        double eEcal             = cluster->e();
        data[DataType::ClusterE] = eEcal;
        //
        double          cSize      = m_ecal->cellSize( cluster->seed() );
        Gaudi::XYZPoint cCenter    = m_ecal->cellCenter( cluster->seed() );
        double          asX        = ( cluster->position().x() - cCenter.x() ) / cSize;
        double          asY        = ( cluster->position().y() - cCenter.y() ) / cSize;
        data[DataType::ClusterAsX] = asX;
        data[DataType::ClusterAsY] = asY;

        data[DataType::E1]     = seed->e();
        auto   it              = data.find( DataType::HypoE );
        double eHypo           = ( it != data.end() ) ? it->second : 0;
        data[DataType::E1Hypo] = eHypo > 0. ? ( seed->e() ) / eHypo : -1.;
        LHCb::CaloCellID sid   = seed->cellID();
        data[DataType::CellID] = sid.all();
        data[DataType::Spread] = cluster->position().spread()( 1, 1 ) + cluster->position().spread()( 0, 0 );
        // E4
        std::array<double, 4> e4s       = {0, 0, 0, 0};
        double                e9        = 0.;
        double                ee9       = 0.; // full cluster energy without fraction applied
        double                e2        = 0.;
        bool                  hasShared = false;
        int                   code      = 0.;
        int                   mult      = 0.;
        int                   nsat      = 0; // number of saturated cells
        for ( const auto& ie : cluster->entries() ) {
          const LHCb::CaloDigit* dig = ie.digit();
          if ( !dig ) continue;
          double ecel = dig->e() * ie.fraction();
          if ( ( ( ie.status() & LHCb::CaloDigitStatus::UseForEnergy ) != 0 ||
                 ( ie.status() & LHCb::CaloDigitStatus::UseForCovariance ) != 0 ) &&
               m_ecal->isSaturated( dig->e(), dig->cellID() ) )
            nsat++;
          LHCb::CaloCellID id = dig->cellID();
          if ( id.area() != sid.area() || abs( (int)id.col() - (int)sid.col() ) > 1 ||
               abs( (int)id.row() - (int)sid.row() ) > 1 )
            continue;
          if ( id.col() <= sid.col() && id.row() >= sid.row() ) e4s[0] += ecel;
          if ( id.col() >= sid.col() && id.row() >= sid.row() ) e4s[1] += ecel;
          if ( id.col() >= sid.col() && id.row() <= sid.row() ) e4s[2] += ecel;
          if ( id.col() <= sid.col() && id.row() <= sid.row() ) e4s[3] += ecel;
          e9 += ecel;
          // new info
          ee9 += dig->e();
          mult++;
          if ( ie.status() & LHCb::CaloDigitStatus::SharedCell ) hasShared = true;
          if ( !( id == sid ) && ecel > e2 ) {
            e2     = ecel;
            int dc = (int)id.col() - (int)sid.col() + 1;
            int dr = (int)id.row() - (int)sid.row() + 1;
            code   = 3 * dr + dc;
          }
        }
        data[DataType::Saturation] = nsat;
        double e4max               = 0;
        for ( auto ih = e4s.begin(); e4s.end() != ih; ++ih ) {
          if ( *ih >= e4max ) e4max = *ih;
        }

        code = mult * 10 + code;
        if ( hasShared ) code *= -1;
        data[DataType::ClusterCode] = code;
        data[DataType::ClusterFrac] = ( e9 > 0. ? e9 / ee9 : -1 );

        data[DataType::E4]  = e4max;
        data[DataType::E9]  = e9;
        data[DataType::E49] = ( e9 > 0. ? e4max / e9 : 0. );
        data[DataType::E19] = ( e9 > 0. ? seed->e() / e9 : -1. );

        double eHcal              = m_toCaloFuture->energy( *cluster, CaloCellCode::CaloIndex::HcalCalo );
        data[DataType::ToHcalE]   = eHcal;
        data[DataType::Hcal2Ecal] = ( eEcal > 0 ) ? eHcal / eEcal : 0.;
      }

      // cluster-match chi2
      if ( !m_skipCl ) {
        const LHCb::CaloCluster* clus = cluster;
        // special trick for split cluster : use full cluster matching
        if ( hypo.hypothesis() == LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
          clus = LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo, false ); // get the main cluster
        if ( ctable ) {
          const auto& range = ctable->relations( clus );
          if ( !range.empty() ) { data[DataType::ClusterMatch] = range.front().weight(); }
        }
      }
    }

    // Estimator MUST be at the end (after all inputs are loaded)

    auto get_data = [&]( LHCb::Calo::Enum::DataType type ) {
      auto d = data.find( type );
      return d != data.end() ? d->second : 0.;
    };

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

    auto obs = LHCb::Calo::Interfaces::INeutralID::Observables{.clmatch = get_data( DataType::ClusterMatch ),
                                                               .e19     = get_data( DataType::E19 ),
                                                               .hclecl  = get_data( DataType::Hcal2Ecal ),
                                                               .sprd    = get_data( DataType::Spread )};
#pragma GCC diagnostic pop

    insert_if( DataType::isNotH, m_neutralID->isNotH( hypo, obs ) );
    insert_if( DataType::isNotE, m_neutralID->isNotE( hypo, obs ) );
    return data;
  }

  const LHCb::Track* HypoEstimator::toTrack( const CaloHypo& hypo, Enum::MatchType match ) const {
    using namespace LHCb::Calo::Enum;
    switch ( match ) {
    case MatchType::ClusterMatch: {
      const auto* ctable = m_clusterTrTable.get();
      if ( ctable ) {
        const LHCb::CaloCluster* cluster = ( hypo.hypothesis() == LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
                                               ? LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo, false )
                                               : LHCb::CaloFutureAlgUtils::ClusterFromHypo( &hypo );
        const auto& range = ctable->relations( cluster );
        if ( !range.empty() ) { return range.front(); }
      }
      break;
    }
    case MatchType::ElectronMatch: {
      const auto* etable = m_electronTrTable.get();
      if ( etable ) {
        const auto range = etable->relations( &hypo );
        if ( !range.empty() ) { return range.front(); }
      }
      break;
    }
    case MatchType::BremMatch: {
      const auto* btable = m_bremTrTable.get();
      if ( btable ) {
        const auto range = btable->relations( &hypo );
        if ( !range.empty() ) { return range.front(); }
      }
      break;
    }
    }
    return nullptr;
  }
} // namespace LHCb::Calo
