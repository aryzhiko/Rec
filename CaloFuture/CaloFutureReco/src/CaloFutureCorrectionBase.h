/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "DetDesc/Condition.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/compose.h"
#include "Kernel/CaloCellID.h"
#include "Relations/IRelationWeighted2D.h"
#include "boost/container/static_vector.hpp"
#include "vdt/vdtMath.h"
#include <variant>

static const InterfaceID IID_CaloFutureCorrectionBase( "CaloFutureCorrectionBase", 1,
                                                       0 ); // TODO: probably change IF version (?)

/** @class CaloFutureCorrectionBase CaloFutureCorrectionBase.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-05-07
 */

namespace CaloFutureCorrectionUtils {
  class DigitFromCaloFuture {
  public:
    explicit DigitFromCaloFuture( const int calo ) : m_calo( calo ){};
    explicit DigitFromCaloFuture( const std::string& calo )
        : DigitFromCaloFuture( CaloCellCode::CaloNumFromName( calo ) ){};
    inline bool operator()( const LHCb::CaloDigit* digit ) const {
      return digit && ( ( (int)digit->cellID().calo() ) == m_calo );
    };
    DigitFromCaloFuture() = delete;

  private:
    int m_calo{0};
  };
} // namespace CaloFutureCorrectionUtils
// DO NOT CHANGE THE FUNCTION ORDER FOR BACKWARD COMPATIBILITY WITH EXISTING CONDITIONS DB
namespace CaloFutureCorrection {
  enum Function {
    InversPolynomial     = 0,
    Polynomial           = 1,
    ExpPolynomial        = 2,
    ReciprocalPolynomial = 3,
    Sigmoid              = 4,
    Sshape               = 5,
    ShowerProfile        = 6,
    SshapeMod            = 7,
    Sinusoidal           = 8,
    ParamList            = 9,  // simple parameter access (by area)
    GlobalParamList      = 10, // simple parameter access (ind. of area)
    Unknown                    // type of correction from DB that is unspecified here. MUST be the last item.
  };

  enum Type {
    // NB: numbering is not continuous due to removed PRS and SPD/Converted_photons -related parameters for Run 1-2
    // CondDB compatibility
    // E-Correction parameters
    alphaG = 0,             // global alpha factor #0
    alphaE,                 // alpha(E)    #1
    alphaB,                 // alpha(Bary) #2
    alphaX,                 // alpha(Dx)   #3
    alphaY,                 // alpha(Dy)   #4
    globalT = 13,           // global(DeltaTheta) function of incidence angle #13
    offsetT,                // offset(DeltaTheta) function of incidence angle #14
    offset,                 // offset( sinTheta ) energy (or ET ) offset #15
    ClusterCovariance = 17, // parameters for cluster covariance estimation #17
    // L-Correction parameters
    gamma0, // #18
    delta0, // #19
    gammaP, // Prs-related L-correction (non-zero at ePrs = 0) #20
    deltaP, // Prs-related L-correction (non-sero at ePrs = 0) #21
    // S-correction parameters
    shapeX,    // #22
    shapeY,    // #23
    residual,  // #24
    residualX, // #25
    residualY, // #26
    asymP,     // #27
    asymM,     // #28
    angularX,  // #29
    angularY,  // #30
    // ShowerShape profile
    profile, // #31
    // Cluster masking
    EnergyMask = 33, // #33
    PositionMask,    // #34
    lastType         // MUST BE THE LAST LINE
  };

  constexpr int        nT = lastType + 1;
  constexpr int        nF = Unknown + 1;
  const std::string&   toString( Type t );
  inline std::ostream& operator<<( std::ostream& os, Type t ) { return os << toString( t ); }
  inline Type          toCorrectionType( std::string_view type ) {
    for ( int i = 0; i < CaloFutureCorrection::nT; ++i ) {
      if ( auto t = static_cast<Type>( i ); toString( t ) == type ) return t;
    }
    return CaloFutureCorrection::lastType;
  }
} // namespace CaloFutureCorrection

class CaloFutureCorrectionBase : public GaudiTool {

public:
  static const InterfaceID& interfaceID() { return IID_CaloFutureCorrectionBase; }

  CaloFutureCorrectionBase( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;

  StatusCode setConditionParams( const std::string& cond,
                                 bool               force = false ); // force = true : forcing access via condDB only

  struct CorrectionResult {
    double value;
    double derivative;
  };

  std::optional<CorrectionResult> getCorrectionAndDerivative( const CaloFutureCorrection::Type type,
                                                              const LHCb::CaloCellID id, const double var = 0. ) const;

  std::optional<double> getCorrection( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id,
                                       const double var = 0. ) const;

  const std::vector<double>& getParamVector( const CaloFutureCorrection::Type type,
                                             const LHCb::CaloCellID           id = LHCb::CaloCellID() ) const {
    return std::get<ParamVector>( m_params[type][id.area()] ).params;
  }

  std::optional<double> getParameter( CaloFutureCorrection::Type type, unsigned int i,
                                      const LHCb::CaloCellID id = LHCb::CaloCellID() ) const {
    const auto& data = getParamVector( type, id );
    if ( i >= data.size() ) return std::nullopt;
    return data[i];
  }

protected:
  Gaudi::Property<std::string>              m_conditionName{this, "ConditionName", "none"};
  Gaudi::Property<std::vector<std::string>> m_corrections{this, "Corrections", {"All"}};
  //
  std::vector<LHCb::CaloHypo::Hypothesis> m_hypos;
  Gaudi::Property<std::vector<int>>       m_hypos_{this,
                                             "Hypotheses",
                                             {
                                                 (int)LHCb::CaloHypo::Hypothesis::Photon,
                                                 (int)LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                                                 (int)LHCb::CaloHypo::Hypothesis::EmCharged,
                                             },
                                             [=]( Property& ) {
                                               // transform vector of accepted hypos
                                               m_hypos.clear();
                                               for ( const auto& hypo : m_hypos_ ) {
                                                 if ( hypo <= (int)LHCb::CaloHypo::Hypothesis::Undefined ||
                                                      hypo >= (int)LHCb::CaloHypo::Hypothesis::Other ) {
                                                   throw Error( "Invalid/Unknown  Calorimeter hypothesis object!" );
                                                 }
                                                 m_hypos.push_back( LHCb::CaloHypo::Hypothesis( hypo ) );
                                               }
                                             },
                                             Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                             "acceptable hypotheses"

  };

  LHCb::ClusterFunctors::ClusterArea           m_area;
  LHCb::ClusterFunctors::ClusterFromCaloFuture m_calo{DeCalorimeterLocation::Ecal};
  std::string                                  m_detData{DeCalorimeterLocation::Ecal};
  const DeCalorimeter*                         m_det = nullptr;
  Gaudi::Property<bool>                        m_correctCovariance{this, "CorrectCovariance", true};

  template <typename TYPE>
  static TYPE myexp( const TYPE x ) {
    return vdt::fast_exp( x );
  }

  template <typename TYPE>
  static TYPE mylog( const TYPE x ) {
    return vdt::fast_log( x );
  }

  template <typename TYPE>
  static TYPE myatan( const TYPE x ) {
    return vdt::fast_atan( x );
  }

  template <typename TYPE>
  static TYPE myatan2( const TYPE x, const TYPE y ) {
    return vdt::fast_atan2( x, y );
  }

  template <typename TYPE>
  static TYPE mycos( const TYPE x ) {
    return vdt::fast_cos( x );
  }

  template <typename TYPE>
  static TYPE mysin( const TYPE x ) {
    return vdt::fast_sin( x );
  }

  template <typename TYPE>
  static TYPE mytanh( const TYPE x ) {
    const auto y = myexp( -2.0 * x );
    return ( 1.0 - y ) / ( 1.0 + y );
  }

  template <typename TYPE>
  static TYPE mysinh( const TYPE x ) {
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) - y );
  }

  template <typename TYPE>
  static TYPE mycosh( const TYPE x ) {
    const auto y = myexp( -x );
    return 0.5 * ( ( 1.0 / y ) + y );
  }

private:
  std::optional<CaloFutureCorrection::Type> accept( const std::string& name ) const {
    auto b = std::any_of( m_corrections.begin(), m_corrections.end(),
                          [&]( const auto& i ) { return i == name || i == "All"; } );
    if ( !b ) return std::nullopt;
    // check if known type
    CaloFutureCorrection::Type type = CaloFutureCorrection::toCorrectionType( name );
    if ( type != CaloFutureCorrection::lastType ) return type;
    if ( CaloFutureCorrection::toCorrectionType( std::string{"RESERVED("}.append( name ).append( ")" ) ) ==
         CaloFutureCorrection::lastType )
      warning() << " o Type " << name << " is not known" << endmsg;
    return std::nullopt;
  }

  StatusCode updParams();
  StatusCode setOptParams();
  StatusCode setDBParams();

  /// Cache counters, as looking them up as
  ///    counter( toString( type ) + " correction processing (" + areaName + ")" )
  /// requires the creation of a temporary string every time, which in turn
  /// involves (eventually) a call to 'new' and 'delete'  -- and the above was the source of 10% (!)
  /// of the # of calls to 'new' and 'delete' in the HLT!!!! (FYI: there are, on average, 1K calls
  /// per event to this method!!!)
  ///
  /// On top of that, this also speeds up the actual search for the correct counter,
  /// by making it a two-step process, and the first step is a direct lookup
  /// instead of a 'find'.
  ///
  /// in the end, this change alone speeds up the total HLT by about 1%...
  StatEntity& kounter( const CaloFutureCorrection::Type type, const LHCb::CaloCellID id ) const {
    assert( type < CaloFutureCorrection::lastType + 1 );
    LHCb::CaloCellID key{id.calo(), id.area(), 0, 0}; // FIXME: just zero everything but calo & area in id instead?
    auto             a = m_counters[type].find( key );
    if ( UNLIKELY( a == std::end( m_counters[type] ) ) ) {
      const auto name = toString( type ) + " correction processing (" + id.areaName() + ")";
      auto [iter, ok] = m_counters[type].insert( key, &counter( name ) );
      assert( ok );
      a = iter;
    }
    assert( a->second );
    return *( a->second );
  }

private:
  struct Polynomial {
    static constexpr const char*                name = "Polynomial";
    boost::container::static_vector<double, 10> pars; // FIXME: how do I know that 10 is enough? Seems to depend on the
                                                      // database contents... maybe use small_vector instead???
    CorrectionResult correction( double var ) const;
  };
  struct InversePolynomial final : private Polynomial {
    InversePolynomial( LHCb::span<const double> p ) : Polynomial{{p.begin(), p.end()}} {}
    static constexpr const char* name = "InversePolynomial";
    CorrectionResult             correction( double var ) const;
  };
  struct ExpPolynomial final : private Polynomial {
    static constexpr const char* name = "ExpPolynomial";
    double                       cached;
    ExpPolynomial( LHCb::span<const double> p ) : Polynomial{{p.begin(), p.end()}}, cached{myexp( p[0] )} {}
    CorrectionResult correction( double var ) const;
  };
  struct ReciprocalPolynomial final : private Polynomial {
    static constexpr const char* name = "ReciprocalPolynomial";
    ReciprocalPolynomial( LHCb::span<const double> p ) : Polynomial{{p.begin(), p.end()}} {}
    CorrectionResult correction( double var ) const;
  };
  struct Sigmoid final {
    static constexpr const char* name = "Sigmoid";
    double                       a, b, c, d;
    Sigmoid( LHCb::span<const double, 4> p ) : a{p[0]}, b{p[1]}, c{p[2]}, d{p[3]} {}
    CorrectionResult correction( double var ) const;
  };
  struct Sshape {
    static constexpr const char* name  = "Sshape";
    constexpr static double      delta = 0.5;
    double                       b;
    double                       cache;
    Sshape( double b, bool modified = false ) : b{b}, cache{( b != 0 ) ? mysinh( delta / b ) : INFINITY} {
      if ( !modified ) cache = sqrt( 1. + cache * cache );
    }
    CorrectionResult correction( double var ) const;
  };
  struct SshapeMod final : Sshape {
    static constexpr const char* name = "SshapeMod";
    SshapeMod( double p ) : Sshape( p, true ) {}
  };
  struct ShowerProfile final {
    static constexpr const char* name = "ShowerProfile";
    std::array<double, 10>       pars;
    ShowerProfile( LHCb::span<const double, 10> p ) { std::copy( p.begin(), p.end(), pars.begin() ); }
    CorrectionResult correction( double var ) const;
  };
  struct Sinusoidal final {
    static constexpr const char* name = "Sinusoidal";
    double                       A;
    Sinusoidal( double s ) : A{s} {}
    CorrectionResult correction( double var ) const;
  };
  struct ParamVector final {
    static constexpr const char* name = "ParamVector";
    std::vector<double>          params;
  };

  using Function = std::variant<ParamVector, InversePolynomial, Polynomial, ExpPolynomial, ReciprocalPolynomial,
                                Sigmoid, Sshape, ShowerProfile, SshapeMod, Sinusoidal>;

  static const char* correctionName( const Function& cp ) {
    return std::visit( []( const auto& i ) { return i.name; }, cp );
  }
  static constexpr int area_size = 3; // size of m_params (outer, middle, inner)
  std::array<std::array<Function, area_size>, CaloFutureCorrection::lastType + 1> m_params;
  void constructParams( std::array<Function, area_size>&, const LHCb::span<const double> );

  Gaudi::Property<std::map<std::string, std::vector<double>>> m_optParams{this, "Parameters"};

  mutable std::array<GaudiUtils::VectorMap<LHCb::CaloCellID, StatEntity*>, CaloFutureCorrection::lastType + 1>
                               m_counters;
  Condition*                   m_cond = nullptr;
  Gaudi::Property<std::string> m_cmLoc{this, "ClusterMatchLocation",
                                       LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )};
  Gaudi::Property<bool>        m_useCondDB{this, "UseCondDB", true};

  PublicToolHandle<IFutureCounterLevel> m_counterStat{this, "CounterLevel", "FutureCounterLevel"};
};
