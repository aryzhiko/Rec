###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @package TrackSys
#  High level configuration for LHCb Tracking software
#  @author Marco Cattaneo <Marco.Cattaneo@cern.ch>
#  @date   15/08/2008

__author__ = "Marco Cattaneo <Marco.Cattaneo@cern.ch>"

from LHCbKernel.Configuration import *
from Configurables import DecodeRawEvent
from GaudiKernel.SystemOfUnits import mm
from GaudiKernel.SystemOfUnits import GeV


## @class TrackSys
#  High level configuration for LHCb Tracking software
#  @author Marco Cattaneo <Marco.Cattaneo@cern.ch>
#  @date   15/08/2008
class TrackSys(LHCbConfigurableUser):
    #not actually used in *this* configurable, but used in the tracking
    #python functions which are sensitive to this configurable
    #some of which change the active decoders, see task #50179
    __used_configurables__ = [DecodeRawEvent]

    # Steering options
    __slots__ = {
        "ExpertHistos":
        False  # set to True to write out expert histos
        ,
        "SpecialData": []  # Various special data processing options.
        ,
        "ExpertTracking":
        []  # list of expert Tracking options, see KnownExpertTracking
        ,
        "TrackExtraInfoAlgorithms": [
        ]  # List of track 'extra info' algorithms to run
        ,
        "WithMC":
        False  # set to True to use MC truth
        ,
        "OutputType":
        ""  # DST type, currently not used
        ,
        "FilterBeforeFit":
        True  #Clone kill before fit of the Best container only. False = fit before clone killing
        ,
        "DataType":
        "Upgrade"  # propagated from Brunel(), used to determine which monitors to run
        ,
        "GlobalCuts": {}  # global event cuts for tracking
        ,
        "HltMode":
        False  # Do not setup tracking sequence
        ,
        "Simulation":
        False  # True if using simulated data/SIMCOND
        ,
        "TrackTypes": [],
        "TrackingSequence": [],
        "VPLinkingOfflineClusters":
        True  # Re-Do VPClustering for offline VPClusters and build MC-Linking based on that
        ,
        "TracksToConvert": [
            "Velo", "Upstream", "ForwardFast", "ForwardBest", "Seeding",
            "Match", "Downstream"
        ]  # Tracks to convert from vector<LHCb::Track> to LHCb::Tracks> for fitting and MCLinking
        ,
        "VeloUpgradeOnly":
        False,
        "BestThroughput":
        False,
        "ConfigHLT1": {
            "GEC":
            -1,  #GEC to be applied , if negative value, the PrGECFilter is not executed
            "VeloUTHLT1": {
                "IPCut": False,
                "IPCutVal": 0.1 * mm,
                "minPT": 0.3 * GeV
            },
            "ForwardHLT1": {
                "minPT": 0.4 * GeV,
                "SecondLoop": True,
                "PreselectionPT": 0.3 * GeV
            },
            "UseMomentumGuidedSearchWindow":
            False,  #controlls PrForwardTracking.UseMomentumGuidedSearchWindow
        }
    }
    ## Possible expert options
    KnownExpertTracking = [
        "noDrifttimes", "simplifiedGeometry", "kalmanSmoother",
        "noMaterialCorrections", "fastSequence", "timing",
        "disableOTTimeWindow", "fullGeometryHLT1", "fullGeometryHLT2",
        "fullClustersHLT1", "fullClustersHLT2", "VeloForwardKalmanHLT1",
        "ParameterizedKalman", "vectorFitter", "useParabolicExtrapolator"
    ]

    ## Default track 'extra info' algorithms to run
    DefaultExtraInfoAlgorithms = ["CloneFlagging", "GhostProbability"]

    DefaultTrackTypes = [
        "Velo", "Upstream", "Forward", "Seeding", "Match", "Downstream"
    ]

    #Default track location for Upgrade
    DefaultTrackLocations = {
        "Velo": {
            "Location": "Rec/Track/Velo",
            "BestUsed": False
        },
        "VeloFitted": {
            "Location": "Rec/Track/Velo",
            "BestUsed": False
        },
        "Upstream": {
            "Location": "Rec/Track/Upstream",
            "BestUsed": False
        },
        "ForwardFast": {
            "Location": "Rec/Track/ForwardFast",
            "BestUsed": False
        },
        "ForwardFastFitted": {
            "Location": "Rec/Track/ForwardFastFitted",
            "BestUsed": False
        },
        "ForwardBest": {
            "Location": "Rec/Track/ForwardBest",
            "BestUsed": False
        },
        "Seeding": {
            "Location": "Rec/Track/Seed",
            "BestUsed": False
        },
        "MuonMatchVeloUT": {
            "Location": "Rec/Track/MuonMatchVeloUT",
            "BestUsed": False
        },
        "Match": {
            "Location": "Rec/Track/Match",
            "BestUsed": False
        },
        "Downstream": {
            "Location": "Rec/Track/Downstream",
            "BestUsed": False
        },
        "Best": {
            "Location": "Rec/Track/Best",
            "BestUsed": False
        }
    }

    DefaultConvertedTrackLocations = {
        "Velo": {
            "Location": "Rec/Track/Keyed/Velo",
            "BestUsed": False
        },
        "VeloFitted": {
            "Location": "Rec/Track/Keyed/Velo",
            "BestUsed": False
        },
        "Upstream": {
            "Location": "Rec/Track/Keyed/Upstream",
            "BestUsed": False
        },
        "ForwardFast": {
            "Location": "Rec/Track/Keyed/ForwardFast",
            "BestUsed": False
        },
        "ForwardFastFitted": {
            "Location": "Rec/Track/ForwardFastFitted",
            "BestUsed": False
        },
        "ForwardBest": {
            "Location": "Rec/Track/Keyed/ForwardBest",
            "BestUsed": False
        },
        "Seeding": {
            "Location": "Rec/Track/Keyed/Seed",
            "BestUsed": False
        },
        "MuonMatchVeloUT": {
            "Location": "Rec/Track/Keyed/MuonMatchVeloUT",
            "BestUsed": False
        },
        "Match": {
            "Location": "Rec/Track/Keyed/Match",
            "BestUsed": False
        },
        "Downstream": {
            "Location": "Rec/Track/Keyed/Downstream",
            "BestUsed": False
        }
    }

    ## @brief Check the options are sane etc.
    def defineOptions(self):

        for track in self.getProp("TrackTypes"):
            if track not in self.DefaultTrackTypes:
                log.warning("Using non-default track type: '%s'" % track)
                log.warning(
                    "Default track types are: %s" % self.DefaultTrackTypes)

        if len(self.getProp("TrackingSequence")) == 0:
            self.setProp("TrackingSequence", ["TrFast", "TrBest"])

        if "cosmics" in self.getProp("SpecialData"):
            raise RunTimeError(
                "cosmics reconstruction not implemented for upgrade detectors")

        if len(self.getProp("TrackExtraInfoAlgorithms")) == 0:
            self.setProp("TrackExtraInfoAlgorithms",
                         self.DefaultExtraInfoAlgorithms)

        for prop in self.getProp("ExpertTracking"):
            if prop not in self.KnownExpertTracking:
                raise RuntimeError("Unknown expertTracking option '%s'" % prop)

        if len(self.getProp("GlobalCuts")) != 0:
            raise RuntimeError(
                "GlobalCuts not supported for upgrade detectors")

    ## @brief Shortcut to the fieldOff option
    def fieldOff(self):
        return "fieldOff" in self.getProp("SpecialData")

    ## @brief Shortcut to the veloOpen option
    def veloOpen(self):
        return "veloOpen" in self.getProp("SpecialData")

    ## @brief Shortcut to the cosmics option
    def cosmics(self):
        return "cosmics" in self.getProp("SpecialData")

    ## @brief Shortcut to the beamGas option
    def beamGas(self):
        return "beamGas" in self.getProp("SpecialData")

    ## @brief Shortcut to the noDrifttimes option
    def noDrifttimes(self):
        return "noDrifttimes" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the simplifiedGeometry option
    def simplifiedGeometry(self):
        return "simplifiedGeometry" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the fullGeometry option for HLT1 in Run 2
    def fullGeometryHLT1(self):
        return "fullGeometryHLT1" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the fullGeometry option for HLT2 in Run 2
    def fullGeometryHLT2(self):
        return "fullGeometryHLT2" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the fullCluster option for HLT1 (in fitter) in Run 2
    def fullClustersHLT1(self):
        return "fullClustersHLT1" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the fullCluster option for HLT2 (in fitter) in Run 2
    def fullClustersHLT2(self):
        return "fullClustersHLT2" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the kalmanSmoother option
    def kalmanSmoother(self):
        return "kalmanSmoother" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the noMaterialCorrections option
    def noMaterialCorrections(self):
        return "noMaterialCorrections" in self.getProp("ExpertTracking")

    ## @brief Shortcut to the timing option
    def timing(self):
        return "timing" in self.getProp("ExpertTracking")

    ### @brief Shortcut to the upgrade option
    #def upgrade(self): return "upgrade" in self.getProp("SpecialData")

    ## @brief Option to enable setting the special data options easily
    def setSpecialDataOption(self, option, value):
        optSet = option in self.getProp("SpecialData")
        if True == value and False == optSet:
            self.getProp("SpecialData").append(option)
        elif False == value and True == optSet:
            self.getProp("SpecialData").remove(option)

    ## @brief Apply the configuration
    def __apply_configuration__(self):
        self.defineOptions()
        if not self.isPropertySet("TrackTypes"):
            self.setProp("TrackTypes", self.DefaultTrackTypes)

        from TrackSys import RecoUpgradeTracking
        defTr = self.DefaultTrackLocations
        sType = "None"
        if "TrHlt" in self.getProp("TrackingSequence"):
            seq = GaudiSequencer("RecoTrFastSeq")
            decoding_seq = GaudiSequencer("RecoDecodingSeq")
            defTr = RecoUpgradeTracking.RecoFastTrackingStage(
                simplifiedGeometryFit=self.simplifiedGeometry(),
                defTracks=defTr,
                sequence=seq,
                decoding_sequence=decoding_seq)
        elif "TrFast" in self.getProp("TrackingSequence"):
            seq = GaudiSequencer("RecoTrFastSeq")
            decoding_seq = GaudiSequencer("RecoDecodingSeq")
            defTr = RecoUpgradeTracking.RecoFastTrackingStage(
                simplifiedGeometryFit=self.simplifiedGeometry(),
                defTracks=defTr,
                sequence=seq,
                decoding_sequence=decoding_seq)
        elif "TrMuonMatch" in self.getProp("TrackingSequence"):
            seq = GaudiSequencer("RecoTrFastSeq")
            decoding_seq = GaudiSequencer("RecoDecodingSeq")
            defTr = RecoUpgradeTracking.RecoMuonMatchTrackingStage(
                simplifiedGeometryFit=self.simplifiedGeometry(),
                defTracks=defTr,
                sequence=seq,
                decoding_sequence=decoding_seq)
        if "TrBest" in self.getProp("TrackingSequence"):
            seq = GaudiSequencer("RecoTrBestSeq")
            defTr = RecoUpgradeTracking.RecoBestTrackingStage(
                simplifiedGeometryFit=self.simplifiedGeometry(),
                defTracks=defTr,
                sequence=seq)
            sType = "TrBest"
        else:
            sType = "TrFast"
        seq = GaudiSequencer("BestTrackCreatorSeq")
        RecoUpgradeTracking.RecoBestTrackCreator(
            defTracks=defTr,
            tracksToConvert=self.getProp("TracksToConvert"),
            defTracksConverted=self.DefaultConvertedTrackLocations,
            simplifiedGeometryFit=self.simplifiedGeometry(),
            sequence=seq)
        best_seq = GaudiSequencer("RecoTrBestSeq")
        best_seq.Members.append(seq)

        extra_seq = GaudiSequencer("TrackAddExtraInfoSeq")
        extra_seq.Members = RecoUpgradeTracking.ExtraInformations()
        best_seq.Members.append(extra_seq)
        #        else:
        #            if self.getProp("FilterBeforeFit"):
        #                if not self.getProp("HltMode"):
        #                    if self.getProp("DataType") in self.Run2DataTypes:
        #                        from TrackSys import RecoTrackingRun2
        #                        # per default, run simplified geometry and use liteClusters
        #                        RecoTrackingRun2.RecoTrackingHLT1(
        #                            simplifiedGeometryFit=not self.fullGeometryHLT1(),
        #                            liteClustersFit=not self.fullClustersHLT1())
        #                        RecoTrackingRun2.RecoTrackingHLT2(
        #                            simplifiedGeometryFit=not self.fullGeometryHLT2(),
        #                            liteClustersFit=not self.fullClustersHLT2())
        #                    else:
        #                        from TrackSys import RecoTracking
        #                        RecoTracking.RecoTracking()
        #            else:
        #                raise RuntimeError("'FilterBeforeFit' needs to be set...")

        self.configurePublicTrackingTools()

        if self.getProp("WithMC"):
            from TrackSys import PrUpgradeChecking
            tracksToConvert = []
            extraTracksToCheck = []
            if self.isPropertySet("TracksToConvert"):
                tracksToConvert = self.getProp("TracksToConvert")
            else:
                if "TrFast" in self.getProp("TrackingSequence"):
                    if self.getProp("VeloUpgradeOnly"):
                        tracksToConvert += ["Velo"]
                    else:
                        tracksToConvert += ["Velo", "Upstream", "ForwardFast"]
                if "TrBest" in self.getProp("TrackingSequence"):
                    tracksToConvert += [
                        "ForwardBest", "Seeding", "Match", "Downstream"
                    ]
                    extraTracksToCheck += ["Best"]
            convertedTrackLocations = self.DefaultConvertedTrackLocations.copy(
            )
            if "ParameterizedKalman" in self.getProp("ExpertTracking"):
                convertedTrackLocations["ForwardFastFitted"] = {
                    "Location": "Rec/Track/Keyed/ForwardFastFitted",
                    "BestUsed": False
                }
                tracksToConvert += ["ForwardFastFitted"]
            else:
                if not TrackSys().getProp("VeloUpgradeOnly"):
                    extraTracksToCheck += ["ForwardFastFitted"]

            PrUpgradeChecking.PrUpgradeChecking(
                defTracks=self.DefaultTrackLocations,
                tracksToConvert=tracksToConvert,
                defTracksConverted=convertedTrackLocations,
                extraTracksToCheck=extraTracksToCheck)


#           else:
#               from TrackSys import PatChecking
#               PatChecking.PatChecking()

    def configurePublicTrackingTools(self):
        from Configurables import (
            ToolSvc, TrackMasterExtrapolator, TrackStateProvider,
            TrackInterpolator, StateThickMSCorrectionTool,
            SimplifiedMaterialLocator, DetailedMaterialLocator)
        # We seem to be using the Run1 default for Upgrade...
        #        if self.getProp("DataType") in self.Run2DataTypes:
        #            useSimplifiedGeometry = True
        #            useNewMSCorrection = True
        useSimplifiedGeometry = False
        useNewMSCorrection = False

        # Configure expert settings
        if len(self.getProp("ExpertTracking")) > 0:
            # If one requires simplified, we use simplified
            useSimplifiedGeometry = self.simplifiedGeometry()
        tsvc = ToolSvc()
        tsvc.addTool(TrackMasterExtrapolator, "TrackMasterExtrapolator")
        tsvc.addTool(TrackInterpolator, "TrackInterpolator")
        tsvc.TrackInterpolator.addTool(
            TrackMasterExtrapolator, name='Extrapolator')
        tsvc.addTool(TrackStateProvider, name="TrackStateProvider")
        tsvc.TrackStateProvider.addTool(
            TrackMasterExtrapolator, name="Extrapolator")
        tsvc.TrackStateProvider.addTool(TrackInterpolator, "Interpolator")
        tsvc.TrackStateProvider.Interpolator.addTool(
            TrackMasterExtrapolator, name="Extrapolator")
        if (useSimplifiedGeometry):
            tsvc.TrackMasterExtrapolator.addTool(
                SimplifiedMaterialLocator, name="MaterialLocator")
            tsvc.TrackInterpolator.Extrapolator.addTool(
                SimplifiedMaterialLocator, name="MaterialLocator")
            tsvc.TrackStateProvider.Extrapolator.addTool(
                SimplifiedMaterialLocator, name="MaterialLocator")
            tsvc.TrackStateProvider.Interpolator.Extrapolator.addTool(
                SimplifiedMaterialLocator, name="MaterialLocator")
        else:
            tsvc.TrackMasterExtrapolator.addTool(
                DetailedMaterialLocator, name="MaterialLocator")
            tsvc.TrackInterpolator.Extrapolator.addTool(
                DetailedMaterialLocator, name="MaterialLocator")
            tsvc.TrackStateProvider.Extrapolator.addTool(
                DetailedMaterialLocator, name="MaterialLocator")
            tsvc.TrackStateProvider.Interpolator.Extrapolator.addTool(
                DetailedMaterialLocator, name="MaterialLocator")

        if useNewMSCorrection:
            tsvc.TrackMasterExtrapolator.MaterialLocator.addTool(
                StateThickMSCorrectionTool, name="StateMSCorrectionTool")
            tsvc.TrackInterpolator.Extrapolator.MaterialLocator.addTool(
                StateThickMSCorrectionTool, name="StateMSCorrectionTool")
            tsvc.TrackStateProvider.Extrapolator.MaterialLocator.addTool(
                StateThickMSCorrectionTool, name="StateMSCorrectionTool")
            tsvc.TrackStateProvider.Interpolator.Extrapolator.MaterialLocator.addTool(
                StateThickMSCorrectionTool, name="StateMSCorrectionTool")
            tsvc.TrackMasterExtrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = True
            tsvc.TrackInterpolator.Extrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = True
            tsvc.TrackStateProvider.Extrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = True
            tsvc.TrackStateProvider.Interpolator.Extrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = True
