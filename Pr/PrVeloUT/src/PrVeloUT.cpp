/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Kernel/STLExtensions.h"
#include "UTDet/DeUTSector.h"
#include <boost/container/small_vector.hpp>

// local
#include "LHCbMath/GeomFun.h"
#include "PrVeloUT.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08: Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
// 2019-04-26: Arthur Hennequin (change data Input/Output)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::VeloUT, "PrVeloUT" )

namespace LHCb::Pr {
  namespace {

    using simd = SIMDWrapper::avx2::types;

    // -- parameters that describe the z position of the kink point as a function of ty in a 4th order polynomial (even
    // terms only)
    constexpr auto magFieldParams = std::array{2010.0f, -2240.0f, -71330.f};

    // perform a fit using trackhelper's best hits with y correction, improve qop estimate
    float fastfitter( const TrackHelper& helper, const LHCb::Pr::UT::Mut::Hits& hits,
                      std::array<float, 4>& improvedParams, const float zMidUT, const float qpxz2p ) {

      const float ty        = helper.state.ty;
      const float zKink     = magFieldParams[0] - ty * ty * magFieldParams[1] - ty * ty * ty * ty * magFieldParams[2];
      const float xMidField = helper.state.x + helper.state.tx * ( zKink - helper.state.z );

      const float zDiff = 0.001f * ( zKink - zMidUT );

      // -- This is to avoid division by zero...
      const float pHelper = std::max( float( std::abs( helper.bestParams[0] * qpxz2p ) ), float( 1e-9 ) );
      const float invP    = pHelper * vdt::fast_isqrtf( 1.0f + ty * ty );

      // these resolution are semi-empirical, could be tuned and might not be correct for low momentum.
      const float error1 =
          0.14f + 10000.0f * invP; // this is the resolution due to multiple scattering between Velo and UT
      const float error2 = 0.12f + 3000.0f * invP; // this is the resolution due to the finite Velo resolution
      const float error  = error1 * error1 + error2 * error2;
      const float weight = 1.0f / error;

      float mat[6] = {weight, weight * zDiff, weight * zDiff * zDiff, 0.0f, 0.0f, 0.0f};
      float rhs[3] = {weight * xMidField, weight * xMidField * zDiff, 0.0f};

      for ( int index : helper.bestIndices ) {

        // -- only the last one can be a nullptr
        if ( index == -1 ) break;

        const float ui = hits.xs[index];
        const float dz = 0.001f * ( hits.zs[index] - zMidUT );
        const float w  = hits.weights[index];
        const float t  = hits.sins[index];
        mat[0] += w;
        mat[1] += w * dz;
        mat[2] += w * dz * dz;
        mat[3] += w * t;
        mat[4] += w * dz * t;
        mat[5] += w * t * t;
        rhs[0] += w * ui;
        rhs[1] += w * ui * dz;
        rhs[2] += w * ui * t;
      }

      ROOT::Math::CholeskyDecomp<float, 3> decomp( mat );
      if ( UNLIKELY( !decomp ) ) {
        return helper.bestParams[0];
      } else {
        decomp.Solve( rhs );
      }

      const float xSlopeUTFit = 0.001f * rhs[1];
      const float xUTFit      = rhs[0];
      const float offsetY     = rhs[2];

      const float distX = ( xMidField - xUTFit - xSlopeUTFit * ( zKink - zMidUT ) );
      // -- This takes into account that the distance between a point and track is smaller than the distance on the
      // x-axis
      const float distCorrectionX2 = 1.0f / ( 1 + xSlopeUTFit * xSlopeUTFit );
      float       chi2 = weight * ( distX * distX * distCorrectionX2 + offsetY * offsetY / ( 1.0f + ty * ty ) );

      for ( int index : helper.bestIndices ) {
        if ( index == -1 ) break;

        const float w    = hits.weights[index];
        const float dz   = hits.zs[index] - zMidUT;
        const float dist = ( hits.xs[index] - xUTFit - xSlopeUTFit * dz - offsetY * hits.sins[index] );
        chi2 += w * dist * dist * distCorrectionX2;
      }

      // new VELO slope x
      const float xb = 0.5f * ( ( xUTFit + xSlopeUTFit * ( zKink - zMidUT ) ) + xMidField ); // the 0.5 is empirical
      const float xSlopeVeloFit = ( xb - helper.state.x ) / ( zKink - helper.state.z );

      improvedParams = {xUTFit, xSlopeUTFit, helper.state.y + helper.state.ty * ( zMidUT - helper.state.z ) + offsetY,
                        chi2};

      // calculate q/p
      const float sinInX  = xSlopeVeloFit * vdt::fast_isqrtf( 1.0f + xSlopeVeloFit * xSlopeVeloFit + ty * ty );
      const float sinOutX = xSlopeUTFit * vdt::fast_isqrtf( 1.0f + xSlopeUTFit * xSlopeUTFit + ty * ty );
      return ( sinInX - sinOutX );
    }

    // -- Evaluate the linear discriminant
    // -- Coefficients derived with LD method for p, pT and chi2 with TMVA
    template <std::size_t nHits>
    float evaluateLinearDiscriminant( const std::array<float, 3> inputValues ) {

      constexpr auto coeffs =
          ( nHits == 3 ? std::array{0.162880166064f, -0.107081172665f, 0.134153123662f, -0.137764853657f}
                       : std::array{0.235010729187f, -0.0938323617311f, 0.110823681145f, -0.170467109599f} );

      assert( coeffs.size() == inputValues.size() + 1 );
      return std::inner_product( std::next( coeffs.begin() ), coeffs.end(), inputValues.begin(), coeffs.front(),
                                 std::plus{}, []( float c, float iv ) { return c * vdt::fast_logf( iv ); } );
    }

    // -- These things are all hardcopied from the PrTableForFunction
    // -- and PrUTMagnetTool
    // -- If the granularity or whatever changes, this will give wrong results

    int masterIndex( const int index1, const int index2, const int index3 ) {
      return ( index3 * 11 + index2 ) * 31 + index1;
    }

    constexpr auto minValsBdl = std::array{-0.3f, -250.0f, 0.0f};
    constexpr auto maxValsBdl = std::array{0.3f, 250.0f, 800.0f};
    constexpr auto deltaBdl   = std::array{0.02f, 50.0f, 80.0f};
    // constexpr auto dxDyHelper = std::array{0.0f, 1.0f, -1.0f, 0.0f};
    // ===========================================================================================
    // -- 2 helper functions for fit
    // -- Pseudo chi2 fit, templated for 3 or 4 hits
    // ===========================================================================================
    void addHit( span<float, 3> mat, span<float, 2> rhs, const LHCb::Pr::UT::Mut::Hits& hits, int index,
                 float zMidUT ) {
      const float ui = hits.xs[index];
      const float ci = hits.coss[index];
      const float dz = 0.001f * ( hits.zs[index] - zMidUT );
      const float wi = hits.weights[index];
      mat[0] += wi * ci;
      mat[1] += wi * ci * dz;
      mat[2] += wi * ci * dz * dz;
      rhs[0] += wi * ui;
      rhs[1] += wi * ui * dz;
    }
    template <std::size_t N>
    void simpleFit( const std::array<int, N>& indices, const LHCb::Pr::UT::Mut::Hits& hits, TrackHelper& helper,
                    float zMidUT, float zKink, float invSigmaVeloSlope ) {
      static_assert( N == 3 || N == 4 );

      // commented, as the threshold bit might / will be removed
      // -- Veto hit combinations with no high threshold hit
      // -- = likely spillover
      // const int nHighThres = std::count_if( hits.begin(),  hits.end(),
      //                                      []( const UT::Mut::Hit* hit ){ return hit && hit->HitPtr->highThreshold();
      //                                      });

      // if( nHighThres < m_minHighThres ) return;

      // -- Scale the z-component, to not run into numerical problems
      // -- with floats
      const float zDiff = 0.001f * ( zKink - zMidUT );
      auto        mat   = std::array{helper.wb, helper.wb * zDiff, helper.wb * zDiff * zDiff};
      auto        rhs   = std::array{helper.wb * helper.xMidField, helper.wb * helper.xMidField * zDiff};

      std::for_each( indices.begin(), indices.end(),
                     [&]( const auto index ) { addHit( mat, rhs, hits, index, zMidUT ); } );

      ROOT::Math::CholeskyDecomp<float, 2> decomp( mat.data() );
      if ( UNLIKELY( !decomp ) ) return;

      decomp.Solve( rhs );

      const float xSlopeTTFit = 0.001f * rhs[1];
      const float xTTFit      = rhs[0];

      // new VELO slope x
      const float xb            = xTTFit + xSlopeTTFit * ( zKink - zMidUT );
      const float xSlopeVeloFit = ( xb - helper.state.x ) * helper.invKinkVeloDist;
      const float chi2VeloSlope = ( helper.state.tx - xSlopeVeloFit ) * invSigmaVeloSlope;

      const float chi2TT =
          std::accumulate( indices.begin(), indices.end(), chi2VeloSlope * chi2VeloSlope,
                           [&]( float chi2, const int index ) {
                             const float du = ( xTTFit + xSlopeTTFit * ( hits.zs[index] - zMidUT ) ) - hits.xs[index];
                             return chi2 + hits.weights[index] * ( du * du );
                           } ) /
          ( N + 1 - 2 );

      if ( chi2TT < helper.bestParams[1] ) {

        // calculate q/p
        const float sinInX  = xSlopeVeloFit * vdt::fast_isqrtf( 1.0f + xSlopeVeloFit * xSlopeVeloFit );
        const float sinOutX = xSlopeTTFit * vdt::fast_isqrtf( 1.0f + xSlopeTTFit * xSlopeTTFit );
        const float qp      = ( sinInX - sinOutX );

        helper.bestParams = {qp, chi2TT, xTTFit, xSlopeTTFit};

        std::copy( indices.begin(), indices.end(), helper.bestIndices.begin() );
        if constexpr ( N == 3 ) { helper.bestIndices[3] = -1; }
      }
    }
  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  VeloUT::VeloUT( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"InputTracksName", "Rec/Track/Velo"}, KeyValue{"UTHits", ::UT::Info::HitLocation},
                      KeyValue{"GeometryInfo", "AlgorithmSpecific-" + name + "-UTGeometryInfo"}},
                     KeyValue{"OutputTracksName", "Rec/Track/UT"} ) {}

  /// Initialization
  StatusCode VeloUT::initialize() {
    return Transformer::initialize().andThen( [&] { return m_PrUTMagnetTool.retrieve(); } ).andThen( [&] {
      // m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
      // Cached once in VeloUTTool at initialization. No need to update with small UT movement.
      m_zMidUT = m_PrUTMagnetTool->zMidUT();
      // zMidField and distToMomentum is properly recalculated in PrUTMagnetTool when B field changes
      m_distToMomentum = m_PrUTMagnetTool->averageDist2mom();

      if ( m_doTiming ) {
        m_timerTool->increaseIndent();
        m_veloUTTime = m_timerTool->addTimer( "Internal VeloUT Tracking" );
        m_timerTool->decreaseIndent();
      }
      addConditionDerivation( DeUTDetLocation::UT, inputLocation<UTDAQ::GeomCache>(),
                              []( const DeUTDetector& utDet ) { return UTDAQ::computeGeometry( utDet ); } );
    } );
  }
  //=============================================================================
  // Main execution
  //=============================================================================
  Upstream::Tracks VeloUT::operator()( const EventContext& evtCtx, const Velo::Tracks& inputTracks,
                                       const LHCb::Pr::UT::HitHandler& hh, const UTDAQ::GeomCache& geometry ) const {
    if ( m_doTiming ) m_timerTool->start( m_veloUTTime );
    Upstream::Tracks outputTracks{&inputTracks, LHCb::getMemResource( evtCtx )};
    m_seedsCounter += inputTracks.size();

    const auto& fudgeFactors = m_PrUTMagnetTool->DxLayTable();
    const auto& bdlTable     = m_PrUTMagnetTool->BdlTable();

    LHCb::Pr::UT::Mut::Hits hitsInLayers;

    // for now only scalar, but with some adaptation it can be vectorized
    using dType = SIMDWrapper::scalar::types;

    for ( int t = 0; t != inputTracks.size(); t++ ) {
      MiniState trState;
      if ( !getState<dType>( inputTracks, t, trState, outputTracks ) ) continue;

      for ( auto& it : hitsInLayers.layerIndices ) it = -1;
      if ( !getHits( hitsInLayers, hh, fudgeFactors, geometry, trState ) ) continue;

      TrackHelper helper( trState, c_zKink, c_sigmaVeloSlope, m_maxPseudoChi2 );

      if ( !formClusters<true>( hitsInLayers, helper ) ) {
        // std::reverse( hitsInLayers.begin(), hitsInLayers.end() ); // need some thinking
        formClusters<false>( hitsInLayers, helper );
        // std::reverse( hitsInLayers.begin(), hitsInLayers.end() );
      }

      if ( helper.bestIndices[0] != -1 )
        prepareOutputTrack<dType>( inputTracks, t, helper, hitsInLayers, outputTracks, bdlTable );
    }

    m_tracksCounter += outputTracks.size();
    if ( m_doTiming ) m_timerTool->stop( m_veloUTTime );
    return outputTracks;
  }
  //=============================================================================
  // Get the state, do some cuts
  //=============================================================================
  template <typename dType>
  bool VeloUT::getState( const Velo::Tracks& inputTracks, int at, MiniState& trState,
                         Upstream::Tracks& outputTracks ) const {
    using I = typename dType::int_v;
    using F = typename dType::float_v;

    const int EndVelo = 1;
    auto      pos     = inputTracks.statePos<F>( at, EndVelo );
    auto      dir     = inputTracks.stateDir<F>( at, EndVelo );
    auto      covX    = inputTracks.stateCovX<F>( at, EndVelo );

    // -- reject tracks outside of acceptance or pointing to the beam pipe
    trState.tx = dir.x.cast();
    trState.ty = dir.y.cast();
    trState.x  = pos.x.cast();
    trState.y  = pos.y.cast();
    trState.z  = pos.z.cast();

    const float xMidUT = trState.x + trState.tx * ( m_zMidUT - trState.z );
    const float yMidUT = trState.y + trState.ty * ( m_zMidUT - trState.z );

    if ( xMidUT * xMidUT + yMidUT * yMidUT < m_centralHoleSize * m_centralHoleSize ) return false;
    if ( ( std::abs( trState.tx ) > m_maxXSlope ) || ( std::abs( trState.ty ) > m_maxYSlope ) ) return false;

    if ( m_passTracks && std::abs( xMidUT ) < m_passHoleSize && std::abs( yMidUT ) < m_passHoleSize ) {
      int i    = outputTracks.size();
      int mask = true; // dummy mask to be replace if we want to vectorize

      outputTracks.compressstore_trackVP<I>( i, mask, at ); // ancestor
      outputTracks.compressstore_statePos<F>( i, mask, pos );
      outputTracks.compressstore_stateDir<F>( i, mask, dir );
      outputTracks.compressstore_stateCov<F>( i, mask, covX );
      outputTracks.compressstore_stateQoP<F>( i, mask, 0.f ); // no momentum
      outputTracks.compressstore_nHits<I>( i, mask, 0 );      // no hits

      outputTracks.size() += dType::popcount( mask );

      return false;
    }

    return true;
  }

  //=============================================================================
  // Find the hits
  //=============================================================================
  template <typename FudgeTable>
  bool VeloUT::getHits( LHCb::Pr::UT::Mut::Hits& hitsInLayers, const LHCb::Pr::UT::HitHandler& hh,
                        const FudgeTable& fudgeFactors, const UTDAQ::GeomCache& geom, MiniState& trState ) const {

    using simd = SIMDWrapper::avx2::types;

    hitsInLayers.size = 0;
    // -- This is hardcoded, so faster
    // -- If you ever change the Table in the magnet tool, this will be wrong
    const float          absSlopeY = std::abs( trState.ty );
    const int            index     = (int)( absSlopeY * 100 + 0.5f );
    span<const float, 4> normFact{&fudgeFactors.table()[4 * index], 4};

    // -- this 500 seems a little odd...
    const float invTheta =
        std::min( 500.0f, 1.0f * vdt::fast_isqrtf( trState.tx * trState.tx + trState.ty * trState.ty ) );
    const float minMom = std::max( m_minPT.value() * invTheta, m_minMomentum.value() );
    const float xTol   = std::abs( 1.0f / ( m_distToMomentum * minMom ) );
    const float yTol   = m_yTol + m_yTolSlope * xTol;

    const simd::float_v yTolV{m_yTol.value()};
    const simd::float_v yTolSlopeV{m_yTolSlope.value()};

    int                                                    nLayers = 0;
    boost::container::small_vector<std::pair<int, int>, 9> sectors;

    std::size_t nSize = 0;
    for ( int iStation = 0; iStation < 2; ++iStation ) {

      if ( iStation == 1 && nLayers == 0 ) { return false; }

      for ( int iLayer = 0; iLayer < 2; ++iLayer ) {
        if ( iStation == 1 && iLayer == 1 && nLayers < 2 ) return false;

        const unsigned int layerIndex  = 2 * iStation + iLayer;
        const float        z           = geom.layers[layerIndex].z;
        const float        yAtZ        = trState.y + trState.ty * ( z - trState.z );
        const float        xLayer      = trState.x + trState.tx * ( z - trState.z );
        const float        yLayer      = yAtZ + yTol * geom.layers[layerIndex].dxDy;
        const float        normFactNum = normFact[layerIndex];
        const float        invNormFact = 1.0f / normFactNum;

        const simd::float_v invNormFactV{invNormFact};
        const simd::float_v xTolInvNormFactV{xTol * invNormFact};

        UTDAQ::findSectors( layerIndex, xLayer, yLayer,
                            xTol * invNormFact - std::abs( trState.tx ) * m_intraLayerDist.value(),
                            m_yTol + m_yTolSlope * std::abs( xTol * invNormFact ), geom.layers[layerIndex], sectors );

        std::pair pp{-1, -1};
        for ( auto& p : sectors ) {
          // sectors can be duplicated in the list, but they are ordered
          if ( p == pp ) continue;
          pp                    = p;
          const int fullChanIdx = ( layerIndex * 3 + ( p.first - 1 ) ) * 98 + ( p.second - 1 );
          findHits( hh, fullChanIdx, trState, xTolInvNormFactV, invNormFactV, hitsInLayers, yTolV, yTolSlopeV );
        }
        sectors.clear();
        nLayers += int( nSize != hitsInLayers.size );
        hitsInLayers.layerIndices[layerIndex] = nSize;
        nSize                                 = hitsInLayers.size;
      }
    }
    return nLayers > 2;
  }

  // ==============================================================================
  // -- Method that finds the hits in a given layer within a certain range
  // ==============================================================================
  void inline VeloUT::findHits( const LHCb::Pr::UT::HitHandler& hh, const int fullChanIndex, const MiniState& myState,
                                const simd::float_v xTolNormFact, const simd::float_v invNormFact,
                                LHCb::Pr::UT::Mut::Hits& mutHits, const simd::float_v yTol,
                                const simd::float_v yTolSlope ) const {

    const LHCb::Pr::UT::Hits& myHits  = hh.hits();
    std::pair<int, int>       indices = hh.indices( fullChanIndex );
    if ( indices.first == indices.second ) return;
    int firstIndex = indices.first;

    using simd   = SIMDWrapper::avx2::types;
    using scalar = SIMDWrapper::scalar::types;

    simd::float_v xOnTrackProto{myState.x +
                                myState.tx * ( myHits.zAtYEq0<scalar::float_v>( firstIndex ).cast() - myState.z )};
    simd::float_v yProto{myState.y - myState.ty * myState.z};
    simd::float_v tyV{myState.ty};
    simd::float_v tolProto{yTolSlope * invNormFact};

    for ( int i = indices.first; i < indices.second; i += simd::size ) {

      auto loop_mask = simd::loop_mask( i, indices.second );

      simd::float_v yy    = yProto + tyV * myHits.zAtYEq0<simd::float_v>( i );
      simd::float_v xx    = myHits.xAtYEq0<simd::float_v>( i ) + yy * myHits.dxDy<simd::float_v>( i );
      simd::float_v absdx = abs( xx - xOnTrackProto );

      if ( none( absdx < xTolNormFact ) ) continue;

      // template this
      simd::float_v yMin = min( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );
      simd::float_v yMax = max( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );

      simd::float_v tol  = yTol + absdx * tolProto;
      auto          mask = ( yMin - tol < yy && yy < yMax + tol ) && loop_mask && ( absdx < xTolNormFact );

      if ( none( mask ) ) continue;

      auto index = mutHits.size;
      auto nPlus = simd::popcount( mask );

      if ( ( index + simd::size ) >= LHCb::Pr::UT::Mut::Hits::max_hits ) {
        error() << "Reached maximum number of hits. This is a temporary limitation and needs to be fixed" << endmsg;
        break;
      }

      mutHits.compressstore_x( index, mask, xx );
      mutHits.compressstore_z( index, mask, myHits.zAtYEq0<simd::float_v>( i ) );
      mutHits.compressstore_cos( index, mask, myHits.cos<simd::float_v>( i ) );
      mutHits.compressstore_sin( index, mask,
                                 myHits.cos<simd::float_v>( i ) * -1.0f * myHits.dxDy<simd::float_v>( i ) );
      mutHits.compressstore_weight( index, mask, myHits.weight<simd::float_v>( i ) );
      mutHits.compressstore_channelID( index, mask, myHits.channelID<simd::int_v>( i ) );
      mutHits.size += nPlus;
    }
  }
  //=========================================================================
  // Form clusters
  //=========================================================================
  template <bool forward>
  bool VeloUT::formClusters( const LHCb::Pr::UT::Mut::Hits& hitsInLayers, TrackHelper& helper ) const {

    const int begin0 = forward ? hitsInLayers.layerIndices[0] : hitsInLayers.layerIndices[3];
    const int end0   = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.size;

    const int begin1 = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.layerIndices[2];
    const int end1   = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[3];

    const int begin2 = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[1];
    const int end2   = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[2];

    const int begin3 = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[0];
    const int end3   = forward ? hitsInLayers.size : hitsInLayers.layerIndices[1];

    bool fourLayerSolution = false;

    // -- this is scalar for the moment
    for ( int i0 = begin0; i0 < end0; ++i0 ) {

      const float xhitLayer0 = hitsInLayers.xs[i0];
      const float zhitLayer0 = hitsInLayers.zs[i0];

      // Loop over Second Layer
      for ( int i2 = begin2; i2 < end2; ++i2 ) {

        const float xhitLayer2 = hitsInLayers.xs[i2];
        const float zhitLayer2 = hitsInLayers.zs[i2];

        const float tx = ( xhitLayer2 - xhitLayer0 ) / ( zhitLayer2 - zhitLayer0 );

        if ( std::abs( tx - helper.state.tx ) > m_deltaTx2 ) continue;

        int   bestHit1Index = -1;
        float hitTol        = m_hitTol2;

        for ( int i1 = begin1; i1 < end1; ++i1 ) {

          const float xhitLayer1 = hitsInLayers.xs[i1];
          const float zhitLayer1 = hitsInLayers.zs[i1];

          const float xextrapLayer1 = xhitLayer0 + tx * ( zhitLayer1 - zhitLayer0 );
          if ( std::abs( xhitLayer1 - xextrapLayer1 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer1 - xextrapLayer1 );
            bestHit1Index = i1;
          }
        }

        if ( fourLayerSolution && bestHit1Index == -1 ) continue;

        int bestHit3Index = -1;
        hitTol            = m_hitTol2;
        for ( int i3 = begin3; i3 < end3; ++i3 ) {

          const float xhitLayer3 = hitsInLayers.xs[i3];
          const float zhitLayer3 = hitsInLayers.zs[i3];

          const float xextrapLayer3 = xhitLayer2 + tx * ( zhitLayer3 - zhitLayer2 );

          if ( std::abs( xhitLayer3 - xextrapLayer3 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer3 - xextrapLayer3 );
            bestHit3Index = i3;
          }
        }

        // -- All hits found
        if ( bestHit1Index != -1 && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2, bestHit3Index}, hitsInLayers, helper, m_zMidUT, c_zKink,
                     c_invSigmaVeloSlope );

          if ( !fourLayerSolution && helper.bestIndices[0] != -1 ) { fourLayerSolution = true; }
          continue;
        }

        // -- Nothing found in layer 3
        if ( !fourLayerSolution && bestHit1Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2}, hitsInLayers, helper, m_zMidUT, c_zKink, c_invSigmaVeloSlope );
          continue;
        }
        // -- Noting found in layer 1
        if ( !fourLayerSolution && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit3Index, i2}, hitsInLayers, helper, m_zMidUT, c_zKink, c_invSigmaVeloSlope );
          continue;
        }
      }
    }
    return fourLayerSolution;
  }

  //=========================================================================
  // Create the Velo-UT tracks
  //=========================================================================
  template <typename dType, typename BdlTable>
  void VeloUT::prepareOutputTrack( const Velo::Tracks& inputTracks, int ancestor, const TrackHelper& helper,
                                   const LHCb::Pr::UT::Mut::Hits& hitsInLayers, Upstream::Tracks& outputTracks,
                                   const BdlTable& bdlTable ) const {

    using I = typename dType::int_v;
    using F = typename dType::float_v;

    //== Handle states. copy Velo one, add TT.
    const float zOrigin = ( std::fabs( helper.state.ty ) > 0.001f ) ? helper.state.z - helper.state.y / helper.state.ty
                                                                    : helper.state.z - helper.state.x / helper.state.tx;

    // const float bdl1    = m_PrUTMagnetTool->bdlIntegral(helper.state.ty,zOrigin,helper.state.z);

    // -- These are calculations, copied and simplified from PrTableForFunction
    // -- FIXME: these rely on the internal details of PrTableForFunction!!!
    //           and should at least be put back in there, and used from here
    //           to make sure everything _stays_ consistent...
    const auto var = std::array{helper.state.ty, zOrigin, helper.state.z};

    const int index1 = std::clamp( int( ( var[0] + 0.3f ) / 0.6f * 30 ), 0, 30 );
    const int index2 = std::clamp( int( ( var[1] + 250 ) / 500 * 10 ), 0, 10 );
    const int index3 = std::clamp( int( var[2] / 800 * 10 ), 0, 10 );

    float bdl = bdlTable.table()[masterIndex( index1, index2, index3 )];

    const auto bdls = std::array{bdlTable.table()[masterIndex( index1 + 1, index2, index3 )],
                                 bdlTable.table()[masterIndex( index1, index2 + 1, index3 )],
                                 bdlTable.table()[masterIndex( index1, index2, index3 + 1 )]};

    const auto boundaries = std::array{-0.3f + float( index1 ) * deltaBdl[0], -250.0f + float( index2 ) * deltaBdl[1],
                                       0.0f + float( index3 ) * deltaBdl[2]};

    // -- This is an interpolation, to get a bit more precision
    float addBdlVal = 0.0;
    for ( int i = 0; i < 3; ++i ) {

      if ( var[i] < minValsBdl[i] || var[i] > maxValsBdl[i] ) continue;

      const float dTab_dVar = ( bdls[i] - bdl ) / deltaBdl[i];
      const float dVar      = ( var[i] - boundaries[i] );
      addBdlVal += dTab_dVar * dVar;
    }
    bdl += addBdlVal;
    // ----

    // -- order is: x, tx, y, chi2
    std::array<float, 4> finalParams = {helper.bestParams[2], helper.bestParams[3],
                                        helper.state.y + helper.state.ty * ( m_zMidUT - helper.state.z ),
                                        helper.bestParams[1]};

    const float qpxz2p = -1.0f / bdl * 3.3356f / Gaudi::Units::GeV;
    const float qp     = m_finalFit ? fastfitter( helper, hitsInLayers, finalParams, m_zMidUT, qpxz2p )
                                : helper.bestParams[0] * vdt::fast_isqrtf( 1.0f + helper.state.ty * helper.state.ty );
    const float qop = ( std::abs( bdl ) < 1.e-8f ) ? 0.0f : qp * qpxz2p;

    // -- Don't make tracks that have grossly too low momentum
    // -- Beware of the momentum resolution!
    const float p  = std::abs( 1.0f / qop );
    const float pt = p * std::sqrt( helper.state.tx * helper.state.tx + helper.state.ty * helper.state.ty );

    if ( p < m_minMomentumFinal || pt < m_minPTFinal ) return;

    const float xUT  = finalParams[0];
    const float txUT = finalParams[1];
    const float yUT  = finalParams[2];

    // -- apply some fiducial cuts
    // -- they are optimised for high pT tracks (> 500 MeV)
    if ( m_fiducialCuts ) {
      const float magSign = m_magFieldSvc->signedRelativeCurrent();

      if ( magSign * qop < 0.0f && xUT > -48.0f && xUT < 0.0f && std::abs( yUT ) < 33.0f ) return;
      if ( magSign * qop > 0.0f && xUT < 48.0f && xUT > 0.0f && std::abs( yUT ) < 33.0f ) return;

      if ( magSign * qop < 0.0f && txUT > 0.09f + 0.0003f * pt ) return;
      if ( magSign * qop > 0.0f && txUT < -0.09f - 0.0003f * pt ) return;
    }

    // -- evaluate the linear discriminant and reject ghosts
    // -- the values only make sense if the final fit is performed
    if ( m_finalFit ) {
      const auto nHits = std::count_if( helper.bestIndices.begin(), helper.bestIndices.end(),
                                        []( auto index ) { return index != -1; } );
      if ( nHits == 3 ) {
        if ( evaluateLinearDiscriminant<3>( {p, pt, finalParams[3]} ) < m_LD3Hits ) return;
      } else {
        if ( evaluateLinearDiscriminant<4>( {p, pt, finalParams[3]} ) < m_LD4Hits ) return;
      }
    }

    // Make tracks :
    int i    = outputTracks.size();
    int mask = true; // dummy mask

    // Refined state ?
    // auto pos = Vec3<F>( helper.state.x, helper.state.y, helper.state.z );
    // auto dir = Vec3<F>( helper.state.tx, helper.state.ty, 1.f );

    // Or EndVelo state ?
    auto pos  = inputTracks.statePos<F>( ancestor, 1 );
    auto dir  = inputTracks.stateDir<F>( ancestor, 1 );
    auto covX = inputTracks.stateCovX<F>( ancestor, 1 );

    outputTracks.compressstore_trackVP<I>( i, mask, ancestor );
    outputTracks.compressstore_statePos<F>( i, mask, pos );
    outputTracks.compressstore_stateDir<F>( i, mask, dir );
    outputTracks.compressstore_stateCov<F>( i, mask, covX );
    outputTracks.compressstore_stateQoP<F>( i, mask, qop );

    int n_hits = 0;
    for ( int index : helper.bestIndices ) {
      if ( index == -1 ) break; // only the last one can be a nullptr.

      LHCb::LHCbID id( LHCb::UTChannelID( hitsInLayers.channelIDs[index] ) );
      const int    stationLayer = 2 * ( id.utID().station() - 1 ) + ( id.utID().layer() - 1 );

      outputTracks.compressstore_hit<I>( i, n_hits, mask, id.lhcbID() ); // not sure if correct
      n_hits++;

      const float xhit = hitsInLayers.xs[index];
      const float zhit = hitsInLayers.zs[index];

      const int begin = hitsInLayers.layerIndices[stationLayer];
      const int end   = ( stationLayer == 3 ) ? hitsInLayers.size : hitsInLayers.layerIndices[stationLayer + 1];

      for ( int index2 = begin; index2 < end; ++index2 ) {
        const float zohit = hitsInLayers.zs[index2];
        if ( zohit == zhit ) continue;

        const float xohit   = hitsInLayers.xs[index2];
        const float xextrap = xhit + txUT * ( zohit - zhit );
        if ( xohit - xextrap < -m_overlapTol ) continue;
        if ( xohit - xextrap > m_overlapTol ) break;

        if ( n_hits > 30 ) continue;
        LHCb::LHCbID oid( LHCb::UTChannelID( hitsInLayers.channelIDs[index2] ) );

        outputTracks.compressstore_hit<I>( i, n_hits, mask, oid.lhcbID() );
        n_hits++;

        // only one overlap hit
        // break;
      }
    }

    outputTracks.compressstore_nHits<I>( i, mask, n_hits );
    outputTracks.size() += dType::popcount( mask );
  }
} // namespace LHCb::Pr
