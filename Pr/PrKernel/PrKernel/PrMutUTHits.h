/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRKERNEL_PRUTMUTHITS_H
#define PRKERNEL_PRUTMUTHITS_H

#include "LHCbMath/SIMDWrapper.h"
#include "UTDAQ/UTInfo.h"

namespace LHCb::Pr::UT {

  namespace Mut {

    struct Hits {

      // -- These hits only live inside the algorithm
      // -- so it should be ok to put them on the stack
      // -- need to handle the case of too many hits though
      constexpr static int max_hits = align_size( 256 );
      alignas( 64 ) std::array<float, max_hits> xs;
      alignas( 64 ) std::array<float, max_hits> zs;
      alignas( 64 ) std::array<float, max_hits> coss;
      alignas( 64 ) std::array<float, max_hits> sins;
      alignas( 64 ) std::array<float, max_hits> weights;
      alignas( 64 ) std::array<int, max_hits> channelIDs;

      std::array<int, UTInfo::TotalLayers> layerIndices;

      std::size_t size{0};
      SOA_ACCESSOR( x, xs.data() )
      SOA_ACCESSOR( z, zs.data() )
      SOA_ACCESSOR( cos, coss.data() )
      // at some point one needs to calculate the sin, we'll see if calculating or storing it is faster
      SOA_ACCESSOR( sin, sins.data() )
      SOA_ACCESSOR( weight, weights.data() )
      SOA_ACCESSOR( channelID, channelIDs.data() )
    };

  } // namespace Mut
} // namespace LHCb::Pr::UT
#endif
