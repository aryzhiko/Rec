/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "GaudiAlg/Transformer.h"

#include "Event/PrForwardTracks.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/StateParameters.h"
#include "Event/Track_v2.h"
#include "FTDet/DeFTDetector.h"

#include "Kernel/ILHCbMagnetSvc.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrSelection.h"
#include "SciFiTrackForwardingHits.h"
#include "vdt/log.h"

#include <limits>
#include <string>
#include <tuple>

#include <boost/container/static_vector.hpp>

/** @class SciFiTrackForwarding SciFiTrackForwarding.cpp
 *
 *  \brief Alternative algorithm for the HLT1 Forward tracking of the upgrade
 *
 *   The general approach is to find a hit in each x-layer in station 3
 *   which is used to form a track seed from which one can forward this track
 *   into the other stations.
 *
 *   For further information see presentations in
 *   - https://indico.cern.ch/event/810764/
 *   - https://indico.cern.ch/event/786084/
 *
 *   In the current implementation, the seeding doublets are searched over two iterations.
 *   First iteration, performed over all VeloUT reconstructed tracks:
 *     doublets of hits are searched in the S3L0 --> S3L3 layers,
 *     they are then prolonged to S2 and S1.
 *   Second iteration, performed over the VeloUT tracks
 *     which have not been prolonged to the SciFi in the first iteration:
 *     doublets of hits are searched in the S2L0 --> S2L3 layers,
 *     they are then prolonged to S3 and S1.
 *
 *   Most important FIXMEs:
 * - Whenever a lighter eventmodel is used, it will be a good idea to vectorize the linear and binary search for further
 *   speedups
 *
 * - The currently employed linear fit leaves a couple things to be desired. Thus this should at some point be replaced
 *   by a better solution. Mainly we don't get a real y-slope or offset since we only fit x-hits, but it's resolution
 *   for high momentum tracks could be better which leads to a momentum resolution of up to ~1.8% while something closer
 *   to ~1% should be possible
 */

///////////////////////////////////////////////////////////////////////////////
// anonymous namespace for helper functions local to this compilation unit
///////////////////////////////////////////////////////////////////////////////
namespace {
  using TracksUT = LHCb::Pr::Upstream::Tracks;
  using TracksFT = LHCb::Pr::Forward::Tracks;

  // average z-position of the kink position inside the magnet (tuned on MC),
  // for S3 and S2
  float constexpr zPosKinkMagnet_S3 = 5282.5f;
  float constexpr zPosKinkMagnet_S2 = 5195.f;

  // constants for extrapolation polynomials from x hit in S3L0
  // to the corresponding x hit in other stations and layers
  double constexpr ExtFacS3_alpha = 1.4706824654297932e-5;
  double constexpr ExtFacS3_beta  = -3.152273942420754e-09;
  double constexpr ExtFacS3_gamma = -0.0003351012155394656;

  // the same as above, but starting from S2L0 extrapolating to other layers
  double constexpr ExtFacS2_alpha = 2.6591242069515776e-5;
  double constexpr ExtFacS2_beta  = -7.989227792416518e-09;
  double constexpr ExtFacS2_gamma = -0.000461618451581078;

  struct GeomCache {
    std::array<float, 24> LayerZPos{std::numeric_limits<float>::signaling_NaN()};
    std::array<float, 24> dZdYFiber{std::numeric_limits<float>::signaling_NaN()};
    std::array<float, 12> dXdYFiber{std::numeric_limits<float>::signaling_NaN()};

    // polynomial coefficients for the seeding starting from S3
    std::array<float, 24> ExtFacS3{std::numeric_limits<float>::signaling_NaN()};

    // polynomial coefficients for the seeding starting from S2
    std::array<float, 24> ExtFacS2{std::numeric_limits<float>::signaling_NaN()};

    PrFTZoneHandler zoneHandler;

    GeomCache( DeFTDetector const& ftDet ) : zoneHandler( ftDet ) {
      // get the layer z-positions and slopes to cache them for later usage
      for ( int i{0}; i < 12; ++i ) {
        // the first 12 values are the bottom layers
        LayerZPos[i] = zoneHandler.zone( 2 * i ).z();
        dZdYFiber[i] = zoneHandler.zone( 2 * i ).dzDy();

        // the last 12 values are the top layers
        LayerZPos[12 + i] = zoneHandler.zone( 2 * i + 1 ).z();
        dZdYFiber[12 + i] = zoneHandler.zone( 2 * i + 1 ).dzDy();
      }

      // get the uv layer fiber slopes to cache them for later usage
      for ( int i{0}; i < 6; ++i ) {
        // the first 6 values are the bottom uv layers
        dXdYFiber[i] = zoneHandler.zone( PrFTInfo::stereoZones[2 * i] ).dxDy();

        // the last 6 values are the top uv layers
        dXdYFiber[6 + i] = zoneHandler.zone( PrFTInfo::stereoZones[2 * i + 1] ).dxDy();
      }

      // calculate some coefficients for the extrapolation into other stations and put them in a cache
      int cnt = 0;

      // used in the second iteration, seeding from S2
      for ( int i : {0, 3, 8, 11} ) {
        int const from = i < 4 ? 4 : 7;

        ExtFacS2[cnt]        = ExtFacS2_alpha * std::pow( LayerZPos[i] - LayerZPos[from], 2 );
        ExtFacS2[12 + cnt++] = ExtFacS2_alpha * std::pow( LayerZPos[12 + i] - LayerZPos[from + 12], 2 );

        ExtFacS2[cnt]        = ExtFacS2_beta * std::pow( LayerZPos[i] - LayerZPos[from], 3 );
        ExtFacS2[12 + cnt++] = ExtFacS2_beta * std::pow( LayerZPos[12 + i] - LayerZPos[from + 12], 3 );

        ExtFacS2[cnt]        = ExtFacS2_gamma * std::pow( LayerZPos[i] - LayerZPos[from], 2 );
        ExtFacS2[12 + cnt++] = ExtFacS2_gamma * std::pow( LayerZPos[12 + i] - LayerZPos[from + 12], 2 );
      }

      cnt = 0;

      // used in the first iteration, seeding from S3
      for ( int i : {0, 3, 4, 7} ) {
        ExtFacS3[cnt]        = ExtFacS3_alpha * std::pow( LayerZPos[i] - LayerZPos[8], 2 );
        ExtFacS3[12 + cnt++] = ExtFacS3_alpha * std::pow( LayerZPos[12 + i] - LayerZPos[20], 2 );

        ExtFacS3[cnt]        = ExtFacS3_beta * std::pow( LayerZPos[i] - LayerZPos[8], 3 );
        ExtFacS3[12 + cnt++] = ExtFacS3_beta * std::pow( LayerZPos[12 + i] - LayerZPos[20], 3 );

        ExtFacS3[cnt]        = ExtFacS3_gamma * std::pow( LayerZPos[i] - LayerZPos[8], 2 );
        ExtFacS3[12 + cnt++] = ExtFacS3_gamma * std::pow( LayerZPos[12 + i] - LayerZPos[20], 2 );
      }
    }
  };

  // constants for the extrapolation polynomial from Velo to SciFi, used to determine searchwindows and minPT cut border
  std::array<float, 8> constexpr toSciFiExtParams{4824.31956565f,  426.26974766f,   7071.08408876f, 12080.38364257f,
                                                  14077.79607408f, 13909.31561208f, 9315.34184959f, 3209.49021545f};

  // constants for the y-correction polynomial
  std::array<float, 8> constexpr deltaYParams{3.78837f, 73.1636f, 7353.89f,  -6347.68f,
                                              20270.3f, 3721.02f, -46038.2f, 230943.f};

  // parameters used for the momentum determination at the very end
  std::array<float, 8> constexpr MomentumParams{1239.4073749458162, 486.05664058906814, 6.7158701518424815,
                                                632.7283787142547,  2358.5758035677504, -9256.27946160669,
                                                241.4601040854867,  42.04859549174048};

  // Parameters for the linear discriminant which we use to reject ghosts
  std::array<float, 8> LDAParams{5.35770606, 1.22786675, -1.17615119, 2.41396581,
                                 2.09624748, 1.7029565,  -2.69818528, 1.88471171};

  // internal helper class to keep track of our best candidate
  struct SciFiTrackForwardingCand {
    // array used to store indices of hits
    boost::container::static_vector<int, 12> ids;
    float                                    PQ{0.f};    // Momentum times charge of candidate
    float                                    finaltx{0}; // x-slope at the first x-hit in station 3 after linear fit
    float                                    newx0{0};   // x position of x-hit in S3 after linear fit
    // float               quality{50.f};  // the default value of quality acts as cut in the later selection of
    // candidates
    float quality{-4.5f}; // the default value of quality acts as cut in the later selection of candidates
    int   numHits{0};     // number of hits this candidate holds
  };

  // Custom span that allow for negative indices
  // needed for the upper_bound below
  template <typename T>
  class span {
  public:
    span() : m_data( nullptr ), m_size( 0 ) {}
    span( T* data, int size ) : m_data( data ), m_size( size ) {}
    T&  operator[]( int i ) const { return m_data[i]; }
    int size() const { return m_size; }

  private:
    T*  m_data;
    int m_size;
  };

  // fast_upper_bound from Arthur
  template <class T>
  int fast_upper_bound( const span<const T>& vec, const T value ) {
    int size = vec.size();
    int low  = 0;

    int half1 = size / 2;
    low += ( vec[low + half1] <= value ) * ( size - half1 );
    size      = half1;
    int half2 = size / 2;
    low += ( vec[low + half2] <= value ) * ( size - half2 );
    size      = half2;
    int half3 = size / 2;
    low += ( vec[low + half3] <= value ) * ( size - half3 );
    size      = half3;
    int half4 = size / 2;
    low += ( vec[low + half4] <= value ) * ( size - half4 );
    size      = half4;
    int half5 = size / 2;
    low += ( vec[low + half5] <= value ) * ( size - half5 );
    size = half5;

    do {
      int half = size / 2;
      low += ( vec[low + half] <= value ) * ( size - half );
      size = half;
    } while ( size > 0 );

    return low;
  }

  [[using gnu: const, hot]] int getUpperBoundBinary( SciFiTrackForwardingHits::hits_t const& vec, int const start,
                                                     int const end, float const val ) noexcept {
    return start + fast_upper_bound( {vec.data() + start, end - start}, val );
  }

  // Simple linear search
  // since my vector container is padded by sentinels I can get rid of a check in the loop and simply advance until
  // condition is met
  [[using gnu: const, hot]] int getUpperBoundLinear( SciFiTrackForwardingHits::hits_t const& vec, int start,
                                                     float const val ) noexcept {

    using simd = SIMDWrapper::avx2::types;
    using F    = simd::float_v;

    // vector of comparison value
    F vval{val};
    for ( ;; ) {
      // load vector of data
      F vvec{vec.data() + start};
      // comparison mask with true/false values
      auto const mask = vval > vvec;
      // sum of true values
      auto const sum{popcount( mask )};
      // if less than simd::size whe can stop
      if ( sum < static_cast<int>( simd::size ) ) return start + sum;
      // all values where smaller thus do next iteration
      start += simd::size;
    }
  }

  // shared implementation between linear and binary hit search
  [[using gnu: const, hot]] std::tuple<int, float>
  __Impl_SearchLayerForHit( SciFiTrackForwardingHits::hits_t const& hits, int const upperIdx, float const prediction ) {
    bool dIdx = std::abs( hits[upperIdx] - prediction ) >= std::abs( hits[upperIdx - 1] - prediction );
    return {upperIdx - dIdx, std::abs( prediction - hits[upperIdx - dIdx] )};
  }

  // given the hits of a station are in the vector hits+StartOffset ---- hits+EndOffset
  // this will find the closest x-hit to the x-prediction using binary search
  [[using gnu: const, hot]] std::tuple<int, float>
  SearchLayerForHitBinary( SciFiTrackForwardingHits::hits_t const& hits, int const StartOffset, int const EndOffset,
                           float const prediction ) {
    int const upperIdx = getUpperBoundBinary( hits, StartOffset, EndOffset, prediction );
    return __Impl_SearchLayerForHit( hits, upperIdx, prediction );
  }

  // same as above, although we use a linear search, EndOffset is not needed as the sentinel value ends the range
  [[using gnu: const, hot]] std::tuple<int, float>
  SearchLayerForHitLinear( SciFiTrackForwardingHits::hits_t const& hits, int const StartOffset,
                           float const prediction ) {
    int const upperIdx = getUpperBoundLinear( hits, StartOffset, prediction );
    return __Impl_SearchLayerForHit( hits, upperIdx, prediction );
  }

  // helper function to fill a vector with some information later used in the linear fit
  // only to reduce code duplication and readability later on
  [[using gnu: always_inline]] void inline fillFitVec( std::array<float, 24>& fitvec, int& fitcnt, float const x,
                                                       float const zdelta, float const curve, float const residual ) {
    fitvec[fitcnt++] = x;
    fitvec[fitcnt++] = zdelta;
    fitvec[fitcnt++] = curve;
    fitvec[fitcnt++] = residual;
  }

} // namespace

///////////////////////////////////////////////////////////////////////////////
//
//  Fast Upgrade Forward Tracking Algorithm
//
//  The general approach is to find a hit in each x-layer in station 3
//  which is used to form a track seed from which one can forward this track
//  into the other stations.
//
//  For further information see presentations in
//  - https://indico.cern.ch/event/810764/
//  - https://indico.cern.ch/event/786084/
//
//
///////////////////////////////////////////////////////////////////////////////
class SciFiTrackForwarding
    : public Gaudi::Functional::Transformer<TracksFT( EventContext const&, SciFiTrackForwardingHits const&,
                                                      TracksUT const&, GeomCache const& ),
                                            LHCb::DetDesc::usesConditions<GeomCache>> {

public:
  SciFiTrackForwarding( std::string const& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"HitsLocation", "Rec/SciFiTrackForwarding/Hits"},
                      KeyValue{"InputTracks", "Rec/Track/UT"},
                      KeyValue{"GeometryCache", "AlgorithmSpecific-" + name + "-ZoneHandlerCache"}},
                     KeyValue{"Output", "Rec/Track/FT"} ) {}

  StatusCode initialize() override {
    return Transformer::initialize().andThen( [&] {
      addConditionDerivation<GeomCache( DeFTDetector const& )>( DeFTDetectorLocation::Default,
                                                                inputLocation<GeomCache>() );

      // factor of -1 because everything was trained with magdown, so for magdown we want factor = 1, magup = -1
      m_magscalefactor = -1 * m_magFieldSvc->signedRelativeCurrent();
    } );
  }

  TracksFT operator()( EventContext const&, SciFiTrackForwardingHits const&, TracksUT const&,
                       GeomCache const& ) const override;

  mutable Gaudi::Accumulators::SummingCounter<unsigned> m_CounterOutput{this, "Created long tracks"};

  mutable Gaudi::Accumulators::Counter<> m_CounterAccepted{this, "Accepted input tracks"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS1{this, "Search S1"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS1UV{this, "Search S1UV"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS2{this, "Search S2"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS2UV{this, "Search S2 UV"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS3{this, "Search S3"};
  mutable Gaudi::Accumulators::Counter<> m_CounterS3UV{this, "Search S3UV"};
  mutable Gaudi::Accumulators::Counter<> m_CounterSecondLoop{this, "1-  Second loops"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS3{this, "2-  X doublets in S3"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS2_2ndloop{this, "2b- X doublets in S2, 2nd loop"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS3S2{this, "3-  X doublets in S3 and S2"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS3S2_2ndloop{this, "3b- X doublets in S3 and S2, 2nd loop"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS3S2S1{this, "4-  X doublets in S3, S2 and S1"};
  mutable Gaudi::Accumulators::Counter<> m_CounterXDoubletsS3S2S1_2ndloop{this,
                                                                          "4b- X doublets in S3, S2 and S1, 2nd loop"};
  mutable Gaudi::Accumulators::Counter<> m_CounterCandsBeforeQuality{this, "5-  Candidates before the quality cut"};
  mutable Gaudi::Accumulators::Counter<> m_CounterCandsBeforeQuality_2ndloop{
      this, "5b- Candidates before the quality cut, 2nd loop"};
  mutable Gaudi::Accumulators::Counter<> m_CounterCandsAfterQuality{this, "6-  Candidates after the quality cut"};
  mutable Gaudi::Accumulators::Counter<> m_CounterCandsAfterQuality_2ndloop{
      this, "6b- Candidates after the quality cut, 2nd loop"};
  mutable Gaudi::Accumulators::Counter<> m_CounterCands{this, "7-  Candidates"};

  Gaudi::Property<float> p_preSelPT{this, "PreSelectionPT", 400.0f};
  Gaudi::Property<float> p_minPt{this, "MinPt", 490.f};

  // if  a track exhibits a higher PT we open a symmetric search window
  // this is to counter the possibility of a wrong charge estimate from the UT
  Gaudi::Property<float> p_wrongSignPT{this, "WrongSignPT", 2000.f};

  // parameters for the momentum dependent upper error limit on the x-search window estimation
  Gaudi::Property<float> p_UpperLimitOffset{this, "UpperLimit_offset", 100.f};
  Gaudi::Property<float> p_UpperLimitSlope{this, "UpperLimit_slope", 2800.f};
  Gaudi::Property<float> p_UpperLimitMax{this, "UpperLimit_max", 600.f};
  Gaudi::Property<float> p_UpperLimitMin{this, "UpperLimit_min", 150.f};

  // same as above for the lower limit
  Gaudi::Property<float> p_LowerLimitOffset{this, "LowerLimit_offset", 50.f};
  Gaudi::Property<float> p_LowerLimitSlope{this, "LowerLimit_slope", 1400.f};
  Gaudi::Property<float> p_LowerLimitMax{this, "LowerLimit_max", 600.f};

  // station 2 x-doublet search window parameters
  Gaudi::Property<float> p_DW_m{this, "DoubletWindow_slope", 4.25f};
  Gaudi::Property<float> p_DW_b{this, "DoubletWindow_offset", 0.48f};
  Gaudi::Property<float> p_DW_max{this, "DoubletWindow_max", 3.f};

  // UV window station 3
  Gaudi::Property<float> p_UV3W_m{this, "UV3Window_slope", 10.0f};
  Gaudi::Property<float> p_UV3W_b{this, "UV3Window_offset", .75f};

  // UV window station 2
  Gaudi::Property<float> p_UV2W_m{this, "UV2Window_slope", 10.0f};
  Gaudi::Property<float> p_UV2W_b{this, "UV2Window_offset", .75f};

  // UV window station 1
  Gaudi::Property<float> p_UV1W_m{this, "UV1Window_slope", 7.0f};
  Gaudi::Property<float> p_UV1W_b{this, "UV1Window_offset", 1.f};

  // S2L0 extrapolation window
  Gaudi::Property<float> p_S2L0W_m{this, "S2L0Window_slope", 2.0f};
  Gaudi::Property<float> p_S2L0W_b{this, "S2L0Window_offset", 2.0f};

  // S2L3 extrapolation window
  Gaudi::Property<float> p_S2L3W_m{this, "S2L3Window_slope", 1.0f};
  Gaudi::Property<float> p_S2L3W_b{this, "S2L3Window_offset", 1.8f};

  // S1L0 extrapolation window
  Gaudi::Property<float> p_S1L0W_m{this, "S1L0Window_slope", 2.6f};
  Gaudi::Property<float> p_S1L0W_b{this, "S1L0Window_offset", 3.6f};

  // S1L3 extrapolation window
  Gaudi::Property<float> p_S1L3W_m{this, "S1L3Window_slope", 2.6f};
  Gaudi::Property<float> p_S1L3W_b{this, "S1L3Window_offset", 3.5f};

  // second loop for searching doublets
  Gaudi::Property<bool> p_SecondLoop{this, "SecondLoop", true};

  // parameters for the momentum dependent upper error limit on the x-search window estimation
  // Very large values of these parameters (x5-x6 wrt the values of the first iteration)
  // do increase the efficiency for high momentum tracks of 0.5%-0.8% (with a +0.5% ghost rate price),
  // but also slow down considerably the overall execution time by a 3x-4x factor
  Gaudi::Property<float> p_UpperLimitOffset_2ndloop{this, "UpperLimit_offset_2ndloop", 100.f};
  Gaudi::Property<float> p_UpperLimitSlope_2ndloop{this, "UpperLimit_slope_2ndloop", 2800.f};
  Gaudi::Property<float> p_UpperLimitMax_2ndloop{this, "UpperLimit_max_2ndloop", 600.f};
  Gaudi::Property<float> p_UpperLimitMin_2ndloop{this, "UpperLimit_min_2ndloop", 150.f};

  // same as above for the lower limit
  Gaudi::Property<float> p_LowerLimitOffset_2ndloop{this, "LowerLimit_offset_2ndloop", 50.f};
  Gaudi::Property<float> p_LowerLimitSlope_2ndloop{this, "LowerLimit_slope_2ndloop", 1400.f};
  Gaudi::Property<float> p_LowerLimitMax_2ndloop{this, "LowerLimit_max_2ndloop", 600.f};

  // second loop: S2L3 extrapolation window
  Gaudi::Property<float> p_S2L3W_m_2ndloop{this, "S2L3Window_slope_2ndloop", 4.3f};
  Gaudi::Property<float> p_S2L3W_b_2ndloop{this, "S2L3Window_offset_2ndloop", 0.6f};
  Gaudi::Property<float> p_DW_max_2ndloop{this, "DoubletWindow_max_2ndloop", 3.2f};

  // second loop: UV window station 2
  Gaudi::Property<float> p_UV2W_m_2ndloop{this, "UV2Window_slope_2ndloop", 10.f};
  Gaudi::Property<float> p_UV2W_b_2ndloop{this, "UV2Window_offset_2ndloop", 1.f};

  // second loop: S3L0 extrapolation window
  Gaudi::Property<float> p_S3L0W_m_2ndloop{this, "S3L0Window_slope", 1.5f};
  Gaudi::Property<float> p_S3L0W_b_2ndloop{this, "S3L0Window_offset", 2.3f};

  // second loop: S3L3 extrapolation window
  Gaudi::Property<float> p_S3L3W_m_2ndloop{this, "S3L3Window_slope", 1.5f};
  Gaudi::Property<float> p_S3L3W_b_2ndloop{this, "S3L3Window_offset", 1.5f};

  // second loop: UV window station 3
  Gaudi::Property<float> p_UV3W_m_2ndloop{this, "UV3Window_slope_2ndloop", 10.f};
  Gaudi::Property<float> p_UV3W_b_2ndloop{this, "UV3Window_offset_2ndloop", 1.f};

  // second loop: S1L0 extrapolation window
  Gaudi::Property<float> p_S1L0W_m_2ndloop{this, "S1L0Window_slope_2ndloop", 2.5f};
  Gaudi::Property<float> p_S1L0W_b_2ndloop{this, "S1L0Window_offset_2ndloop", 2.f};

  // second loop: S1L3 extrapolation window
  Gaudi::Property<float> p_S1L3W_m_2ndloop{this, "S1L3Window_slope_2ndloop", 1.5f};
  Gaudi::Property<float> p_S1L3W_b_2ndloop{this, "S1L3Window_offset_2ndloop", 2.3f};

  // second loop: UV window station 1
  Gaudi::Property<float> p_UV1W_m_2ndloop{this, "UV1Window_slope_2ndloop", 7.5f};
  Gaudi::Property<float> p_UV1W_b_2ndloop{this, "UV1Window_offset_2ndloop", 1.5f};

  // second loop: threshold of number of hits in S3 to reject a candidate
  Gaudi::Property<int> p_numHitsThrS2_2ndloop{this, "numHitsThrS2_2ndloop", 4};

  // second loop: threshold of number of x-hits in S1 and S2 to reject a candidate
  // it is put in OR with the (numuvhits < 3) condition
  Gaudi::Property<int> p_numXHitsThrS1S2_2ndloop{this, "numXHitsThrS1S2_2ndloop", 4};

private:
  // this enables me to print things automagically space separated (less typing)
  // also when enable is set to false, all of this code is removed which is nice for me to test what this actually costs
  template <bool enable = true, typename... Args>
  void mydebug( Args&&... args ) const {
    if constexpr ( enable ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { ( ( debug() << std::forward<Args>( args ) << " " ), ... ) << endmsg; }
    }
  }

  float m_magscalefactor{std::numeric_limits<float>::signaling_NaN()};

  ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticFieldService", "MagneticFieldSvc"};
};

DECLARE_COMPONENT( SciFiTrackForwarding )

TracksFT SciFiTrackForwarding::operator()( EventContext const& evtCtx, SciFiTrackForwardingHits const& hithandler,
                                           TracksUT const& tracks, GeomCache const& cache ) const {
  TracksFT Output{tracks.getVeloAncestors(), &tracks, Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};

  mydebug( "LayerZPos", cache.LayerZPos );

  // get buffers for the counters
  auto S1buffer                          = m_CounterS1.buffer();
  auto S2buffer                          = m_CounterS2.buffer();
  auto S3buffer                          = m_CounterS3.buffer();
  auto S3UVbuffer                        = m_CounterS3UV.buffer();
  auto S2UVbuffer                        = m_CounterS2UV.buffer();
  auto S1UVbuffer                        = m_CounterS1UV.buffer();
  auto CounterAcceptedBuffer             = m_CounterAccepted.buffer();
  auto CounterSecondLoopBuffer           = m_CounterSecondLoop.buffer();
  auto CounterCandsBuffer                = m_CounterCands.buffer();
  auto CounterCandsBeforeQuality         = m_CounterCandsBeforeQuality.buffer();
  auto CounterCandsBeforeQuality_2ndloop = m_CounterCandsBeforeQuality_2ndloop.buffer();
  auto CounterCandsAfterQuality          = m_CounterCandsAfterQuality.buffer();
  auto CounterCandsAfterQuality_2ndloop  = m_CounterCandsAfterQuality_2ndloop.buffer();
  auto CounterXDoubletsS3                = m_CounterXDoubletsS3.buffer();
  auto CounterXDoubletsS2_2ndloop        = m_CounterXDoubletsS2_2ndloop.buffer();
  auto CounterXDoubletsS3S2              = m_CounterXDoubletsS3S2.buffer();
  auto CounterXDoubletsS3S2_2ndloop      = m_CounterXDoubletsS3S2_2ndloop.buffer();
  auto CounterXDoubletsS3S2S1            = m_CounterXDoubletsS3S2S1.buffer();
  auto CounterXDoubletsS3S2S1_2ndloop    = m_CounterXDoubletsS3S2S1_2ndloop.buffer();

  // I want this in GeV but default in LHCb is MeV so this is a compromise to have the property in MeV
  float const minPTCutValue = p_minPt / float{Gaudi::Units::GeV};

  using dType = SIMDWrapper::scalar::types;
  using F     = dType::float_v;
  using I     = dType::int_v;

  // number of iterations for searching doublets
  // First iteration, performed over all VeloUT reconstructed tracks:
  //     doublets of hits are searched in the S3L0 --> S3L3 layers,
  //     they are then prolonged to S2 and S1
  // Second iteration, performed over the VeloUT tracks
  // which have not been prolonged to the SciFi in the first iteration:
  //     doublets of hits are searched in the S2L0 --> S2L3 layers,
  //     they are then prolonged to S3 and S1
  const unsigned int num_iterations = p_SecondLoop ? 2 : 1;

  // start our loop over UT tracks
  // for ( auto const& uttrack : tracks ) {
  for ( int uttrack = 0; uttrack < tracks.size(); uttrack += dType::size ) {

    // placeholder for the best candidate
    SciFiTrackForwardingCand bestcandidate{};
    mydebug( "++++++++++ Start new UT track ++++++++++++++++" );

    Vec3<F> const endv_pos = tracks.statePos<F>( uttrack );
    Vec3<F> const endv_dir = tracks.stateDir<F>( uttrack );

    // everything I need from the end velo state
    float const endv_x    = endv_pos.x.cast();
    float const endv_y    = endv_pos.y.cast();
    float const endv_tx   = endv_dir.x.cast();
    float const endv_ty   = endv_dir.y.cast();
    float const endv_qp   = tracks.stateQoP<F>( uttrack ).cast();
    float const endv_z    = endv_pos.z.cast();
    float const endv_tx2  = endv_tx * endv_tx;
    float const endv_ty2  = endv_ty * endv_ty;
    float const endv_txy2 = endv_tx2 + endv_ty2;
    float const endv_txy  = std::sqrt( endv_txy2 );
    float const endv_pq   = 1.f / endv_qp;
    float const endv_p    = std::abs( endv_pq );
    float const endv_pz   = endv_p / std::sqrt( 1.f + endv_txy2 );
    float const endv_pt   = endv_pz * endv_txy;

    mydebug( "pt: ", endv_pt, "preselpt", p_preSelPT );
    if ( endv_pt < p_preSelPT ) continue;
    CounterAcceptedBuffer += 1;

    // determine if a track is expected to intersect the seeding station from the top or the bottom
    // check is performed at last layer of second station.
    bool const TrackGoesTroughUpperHalf = ( endv_y + ( cache.LayerZPos[8] - endv_z ) * endv_ty ) > 0;

    // extrapolate velo track y-position into all 12 Layers
    // IIFE (Immediately invoked function expression) to keep yAtZ const
    // compiler explorer tested that this isn't producing overhead over simple loop
    auto const yAtZ = [&]() {
      std::array<float, 12> tmp;

      // start at beginning or middle of array depending on if track goes through upper or lower half of detector
      // see creation of GeomCache for more info
      const auto halfSize = cache.LayerZPos.size() / 2;
      int const  cnt      = TrackGoesTroughUpperHalf ? halfSize : 0;

      std::transform( cache.LayerZPos.begin() + cnt, cache.LayerZPos.begin() + 12 + cnt, tmp.begin(),
                      [=]( float const z ) { return endv_y + ( z - endv_z ) * endv_ty; } );
      return tmp;
    }();

    mydebug( "yatz: ", yAtZ );

    // adjust the z-position to account for the predicted y-position of the track
    auto const betterZ = [&]() {
      std::array<float, 12> tmp;

      // start at beginning or middle of array depending on if track goes through upper or lower half of detector
      // see creation of GeomCache for more info
      const auto halfSize = cache.LayerZPos.size() / 2;
      int        cnt      = TrackGoesTroughUpperHalf ? halfSize : 0;

      std::transform( yAtZ.begin(), yAtZ.end(), cache.LayerZPos.begin() + cnt, tmp.begin(),
                      [&]( float const y, float const z ) { return z + y * cache.dZdYFiber[cnt++]; } );
      return tmp;
    }();

    mydebug( "betterz: ", TrackGoesTroughUpperHalf ? " upper half" : " lower half", betterZ );

    auto const dXdYLayer = [&]() {
      std::array<float, 6> tmp;
      // start at beginning or middle of array depending on if track goes through upper or lower half of detector
      // see creation of GeomCache for more info
      const auto halfSize = cache.dXdYFiber.size() / 2;
      int        cnt      = TrackGoesTroughUpperHalf ? halfSize : 0;
      std::transform( tmp.begin(), tmp.end(), cache.dXdYFiber.begin() + cnt, tmp.begin(),
                      [=]( float const /*unused*/, float const dxdy ) { return -dxdy; } );
      return tmp;
    }();

    mydebug( "dXdYLayer", TrackGoesTroughUpperHalf ? " upper half" : " lower half", dXdYLayer );

    // start at beginning or middle of array depending on if track goes through upper or lower half of detector
    // see creation of GeomCache for more info
    const auto halfSize      = cache.ExtFacS2.size() / 2;
    int const  ExtFac_Offset = TrackGoesTroughUpperHalf ? halfSize : 0;

    // charge * magnet polarity factor needed in various polynomials below
    float const factor = std::copysign( 1.f, endv_qp ) * m_magscalefactor;

    // charge over momentum is usally in MeV so make it GeV
    float const qOpGeV = endv_qp * float{Gaudi::Units::GeV} * m_magscalefactor;

    // extrapolate the track x-position into the scifi to determine search windows
    // common term for both extrapolations below
    float const term1 =
        toSciFiExtParams[0] + endv_tx * ( -toSciFiExtParams[1] * factor + toSciFiExtParams[2] * endv_tx ) +
        endv_ty2 * ( toSciFiExtParams[3] + endv_tx * ( toSciFiExtParams[4] * factor + toSciFiExtParams[5] * endv_tx ) );

    // prediction of tracks x position with ut momentum estimate
    float const xExt = qOpGeV * ( term1 + qOpGeV * ( toSciFiExtParams[6] * endv_tx + toSciFiExtParams[7] * qOpGeV ) );

    // 1/p, given a minPT cut and the tracks slopes
    float const minInvPGeV = factor / minPTCutValue * endv_txy / std::sqrt( 1.f + endv_txy2 );

    // given the above value of 1/p this should be the tracks x-position
    // we can use this to make our windows smaller given a minPT cut
    float const minPTBorder =
        minInvPGeV * ( term1 + minInvPGeV * ( toSciFiExtParams[6] * endv_tx + toSciFiExtParams[7] * minInvPGeV ) );

    // set start and end points in 1D hit array depending on if a track is in upper or lower detector half
    mydebug( "Selecting upper or lower  hits", TrackGoesTroughUpperHalf );
    auto const Xzones    = TrackGoesTroughUpperHalf ? PrFTInfo::xZonesUpper : PrFTInfo::xZonesLower;
    auto const UVzones   = TrackGoesTroughUpperHalf ? PrFTInfo::uvZonesUpper : PrFTInfo::uvZonesLower;
    int const  startS3L0 = hithandler.zonerange[Xzones[4]].first;
    int const  startS3L1 = hithandler.zonerange[UVzones[4]].first;
    int const  startS3L2 = hithandler.zonerange[UVzones[5]].first;
    int const  startS3L3 = hithandler.zonerange[Xzones[5]].first;

    int const startS2L0 = hithandler.zonerange[Xzones[2]].first;
    int const startS2L1 = hithandler.zonerange[UVzones[2]].first;
    int const startS2L2 = hithandler.zonerange[UVzones[3]].first;
    int const startS2L3 = hithandler.zonerange[Xzones[3]].first;

    // each variable is a pair [startIdx, size]
    auto const S1L0range = hithandler.zonerange[Xzones[0]];
    auto const S1L1range = hithandler.zonerange[UVzones[0]];
    auto const S1L2range = hithandler.zonerange[UVzones[1]];
    auto const S1L3range = hithandler.zonerange[Xzones[1]];

    //
    // ########################################################
    // loop over the iterations for searching doublets
    // ########################################################
    //
    for ( unsigned int iteration = 0; iteration < num_iterations; ++iteration ) {

      mydebug( "++++++++++ Start iteration", iteration, " ++++++++++" );

      // define some magic parameters for all the layers, depending on the current iteration
      float const ExtFac_alpha_S1L0 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset] : cache.ExtFacS2[ExtFac_Offset];
      float const ExtFac_beta_S1L0 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 1] : cache.ExtFacS2[ExtFac_Offset + 1];
      float const ExtFac_gamma_S1L0 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 2] : cache.ExtFacS2[ExtFac_Offset + 2];

      float const ExtFac_alpha_S1L3 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 3] : cache.ExtFacS2[ExtFac_Offset + 3];
      float const ExtFac_beta_S1L3 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 4] : cache.ExtFacS2[ExtFac_Offset + 4];
      float const ExtFac_gamma_S1L3 =
          ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 5] : cache.ExtFacS2[ExtFac_Offset + 5];

      // well-defined for the first iteration only,
      // for which the seeding doublet is searched in S3
      float const ExtFac_alpha_S2L0 = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 6] : 0.f;
      float const ExtFac_beta_S2L0  = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 7] : 0.f;
      float const ExtFac_gamma_S2L0 = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 8] : 0.f;

      float const ExtFac_alpha_S2L3 = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 9] : 0.f;
      float const ExtFac_beta_S2L3  = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 10] : 0.f;
      float const ExtFac_gamma_S2L3 = ( iteration == 0 ) ? cache.ExtFacS3[ExtFac_Offset + 11] : 0.f;

      // well-defined for the second iteration only,
      // for which the seeding doublet is searched in S2
      float const ExtFac_alpha_S3L0 = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 6];
      float const ExtFac_beta_S3L0  = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 7];
      float const ExtFac_gamma_S3L0 = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 8];

      float const ExtFac_alpha_S3L3 = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 9];
      float const ExtFac_beta_S3L3  = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 10];
      float const ExtFac_gamma_S3L3 = ( iteration == 0 ) ? 0.f : cache.ExtFacS2[ExtFac_Offset + 11];

      // first iteration: delta in Z between kink position in magnet and z position of S3L0
      float const InvDeltaZMagS3L0 = 1.f / ( betterZ[8] - zPosKinkMagnet_S3 );

      // second iteration: delta in Z between kink position in magnet and z position of S2L0
      float const InvDeltaZMagS2L0 = 1.f / ( betterZ[4] - zPosKinkMagnet_S2 );

      // extrapolate velo track x-position into magnet,
      // for S3 (used in the first iteration) and S2 (used in the second iteration)
      float const xAtMagnet_S2 = endv_x + ( zPosKinkMagnet_S2 - endv_z ) * endv_tx;
      float const xAtMagnet_S3 = endv_x + ( zPosKinkMagnet_S3 - endv_z ) * endv_tx;

      // extrapolate the velo straight into the SciFi
      // first iteration:  use the z position of S3L0
      // second iteration: use the z position of S2L0
      float const straighExt =
          ( iteration == 0 ) ? endv_x + ( betterZ[8] - endv_z ) * endv_tx : endv_x + ( betterZ[4] - endv_z ) * endv_tx;

      // calculate an upper error boundary on the tracks x-prediction,
      // depending on the current iteration
      float const upperError =
          ( iteration == 0 ) ? std::clamp( p_UpperLimitOffset + p_UpperLimitSlope * std::abs( qOpGeV ),
                                           p_UpperLimitMin.value(), p_UpperLimitMax.value() )
                             : std::clamp( p_UpperLimitOffset_2ndloop + p_UpperLimitSlope_2ndloop * std::abs( qOpGeV ),
                                           p_UpperLimitMin_2ndloop.value(), p_UpperLimitMax_2ndloop.value() );

      // calculate a lower error boundary on the tracks x-prediction,
      // depending on the current iteration
      float const lowerError =
          ( iteration == 0 )
              ? std::min( p_LowerLimitOffset + p_LowerLimitSlope * std::abs( qOpGeV ), p_LowerLimitMax.value() )
              : std::min( p_LowerLimitOffset_2ndloop + p_LowerLimitSlope_2ndloop * std::abs( qOpGeV ),
                          p_LowerLimitMax_2ndloop.value() );

      // depending on the sign of q set the lower and upper error in the right direction
      // on top of that make sure that the upper error isn't larger than the minPTBorder
      // and the lower error isn't bigger than xExt which means we don't look further than the straight line prediction
      float xMin = factor > 0 ? straighExt + ( ( xExt < lowerError ) ? 0 : ( xExt - lowerError ) )
                              : straighExt + std::max( xExt - upperError, minPTBorder );

      float xMax = factor > 0 ? straighExt + std::min( xExt + upperError, minPTBorder )
                              : straighExt + ( ( xExt > -lowerError ) ? 0 : ( xExt + lowerError ) );

      if ( endv_pt > p_wrongSignPT ) {
        xMin = straighExt - std::abs( xExt ) - upperError;
        xMax = straighExt + std::abs( xExt ) + upperError;
      }

      mydebug( "==================" );
      mydebug( "qOpGeV", qOpGeV, "straight: ", straighExt, "xExt ", xExt, " dxref ", minPTBorder, "lowError",
               lowerError, "upError: ", upperError, "xmin", xMin, "xmax: ", xMax, "minInvPGeV", minInvPGeV );

      // a range is a pair of start and size
      // first iteration: start with S3L0
      // second iteration: start with S2L0
      auto startRange = ( iteration == 0 ) ? hithandler.zonerange[Xzones[4]] : hithandler.zonerange[Xzones[2]];

      int const startL0 =
          getUpperBoundBinary( hithandler.hits, startRange.first, startRange.first + startRange.second, xMin );
      int const EndL0 = getUpperBoundBinary( hithandler.hits, startL0, startRange.first + startRange.second, xMax );

      // some variables to cache offsets
      int startoffsetS3L0{startS3L0};
      int startoffsetS3L1{startS3L1};
      int startoffsetS3L2{startS3L2};
      int startoffsetS3L3{startS3L3};

      // FIXME these offsets seem to work in the tested minPt=.49 scenario
      // These are not always 100% correct though
      int startoffsetS2L0{startS2L0};
      int startoffsetS2L1{startS2L1};
      int startoffsetS2L2{startS2L2};
      int startoffsetS2L3{startS2L3};

      // z delta between the two layers of the seeding station
      // first iteration:  z(S3L3) - z(S3L0)
      // second iteration: z(S2L3) - z(S2L0)
      float const zdelta_doublet_XLayers = ( iteration == 0 ) ? betterZ[11] - betterZ[8] : betterZ[7] - betterZ[4];

      // inverse of the above quantity
      float const inv_zdelta_doublet_XLayers =
          ( iteration == 0 ) ? 1.f / ( betterZ[11] - betterZ[8] ) : 1.f / ( betterZ[7] - betterZ[4] );

      // ready to start a new second loop
      if ( iteration > 0 ) ++CounterSecondLoopBuffer;

      // Loop over the hits in the first x-layer within the search window
      for ( auto idxL0{startL0}; idxL0 < EndL0; ++idxL0 ) {

        mydebug( "#####################Start new hit################################" );

        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        //                  Start of search in the first station               //
        //                  First iteration:  S3                               //
        //                  Second iteration: S2                               //
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        // get the x value of hit
        // first iteration:  S3x0
        // second iteration: S2x0
        float const firstXhit = hithandler.hits[idxL0];

        // calculate a rough estimate of the expected slope,
        // depending on the current iteration
        float const tx = ( iteration == 0 ) ? ( firstXhit - xAtMagnet_S3 ) * InvDeltaZMagS3L0
                                            : ( firstXhit - xAtMagnet_S2 ) * InvDeltaZMagS2L0;

        // this is a small correction mainly for lower momentum tracks for which the zkink isn't perfect
        // and they'll have some bending. This is solely empirical.
        // first iteration:  curvature to S3L3
        // second iteration: curvature to S2L3
        float const doubletCurve =
            ( iteration == 0 )
                ? ( tx - endv_tx ) * ( -0.231615f + ( endv_tx * 33.1256f + tx * 10.4693f ) * ( tx - endv_tx ) )
                : ( tx - endv_tx ) * ( -1.050f + ( endv_tx * 17.74f + tx * 20.62f ) * ( tx - endv_tx ) );

        // piece together the x-prediction in the last x layer
        float const secondXhitPred = firstXhit + tx * zdelta_doublet_XLayers + doubletCurve;

        mydebug( "firstXhit", firstXhit, "lhcbid", hithandler.IDs[idxL0], "secondXhitPred", secondXhitPred, "tx", tx );

        // quit if we are outside of the sensitive area
        if ( std::abs( secondXhitPred ) > 3200.f ) continue;

        float secondXhit;

        if ( iteration == 0 ) S3buffer += 1;

        // FIXME this is the most expensive search, luckily we can cache the starting point
        // however the first search is still costly, maybe we can perform the first search with an avx2 search
        // before the loop?
        auto const startoffset               = ( iteration == 0 ) ? startoffsetS3L3 : startoffsetS2L3;
        auto const [selectsecondXhit, Delta] = SearchLayerForHitLinear( hithandler.hits, startoffset, secondXhitPred );

        // cache an offset for the next search
        if ( iteration == 0 )
          startoffsetS3L3 = selectsecondXhit;
        else
          startoffsetS2L3 = selectsecondXhit;

        // x position of the second hit of the doublet
        secondXhit = hithandler.hits[selectsecondXhit];

        // calculate x-slope of the found doublet
        float const newtx = ( secondXhit - firstXhit ) * inv_zdelta_doublet_XLayers;

        // deltaSlope is proportional to the momentum and used in many places to tune momentum dependant search windows
        float const deltaSlope = newtx - endv_tx;

        // the minimum value of 0.011 avoids too small search windows for tracks with P > 100 GeV
        float const absDSlope = std::max( 0.011f, std::abs( deltaSlope ) );

        // check if the x-hit is within our search window acceptance, depending on the current iteration
        if ( iteration == 0 ) {
          if ( Delta > std::min( p_DW_b + p_DW_m * absDSlope, p_DW_max.value() ) ) continue;
        } else {
          // second iteration
          if ( Delta > std::min( p_S2L3W_b_2ndloop + p_S2L3W_m_2ndloop * absDSlope, p_DW_max_2ndloop.value() ) )
            continue;
        }

        // we have a doublet and are going to try and build a track
        // for that we need a couple tmp objects to keep track of stuff
        // tmpidvec will store the indices of the hit to refind it later
        boost::container::static_vector<int, 12> tmpidvec{selectsecondXhit, idxL0};

        if ( iteration == 0 ) {
          ++CounterXDoubletsS3;
        } else {
          ++CounterXDoubletsS2_2ndloop;
        }

        // this is a vector that will remember some values for the linear fit performed at the very end.
        // for each hit this will store 4 values, [xvalue, zdelta, curvature, residual]
        // where zdelta is the distance between the hit and the hit in layer 0 of the seeding station
        // curvature is the non-linear part of the prediction. See x3curve variable for example
        std::array<float, 24> fitvec{firstXhit, 0, 0, 0, secondXhit, zdelta_doublet_XLayers, doubletCurve, 0};

        // fitcnt simply keeps track of what's in the array
        int fitcnt = 8;

        float const direction = -1.f * std::copysign( 1.f, deltaSlope );

        // correct the straight line estimate of the y-position at station 3 layer 0
        float const ycorr =
            absDSlope * ( direction * deltaYParams[0] +
                          endv_ty * ( deltaYParams[1] + deltaYParams[5] * endv_ty2 +
                                      endv_tx * ( direction * ( deltaYParams[2] + deltaYParams[6] * endv_ty2 ) +
                                                  endv_tx * ( deltaYParams[3] + deltaYParams[7] * endv_ty2 +
                                                              deltaYParams[4] * direction * endv_tx ) ) ) );

        // these variables are saved for later as it is quite good for ghost rejection
        float S2UVDelta( 1.0f ), S3UVDelta( 1.0f );

        // keep track of how many hits we find
        int numuvhits{0};
        int numxhits{2};

        // to handle correctly the ordering of tmpidvec in the second iteration
        int selectS2L1( -1 ), selectS2L2( -1 );

        if ( iteration == 0 ) {

          // first iteration:
          // calculate the x-predictions for the U V layers in station 3
          float const S3x1pred = firstXhit + newtx * ( betterZ[9] - betterZ[8] ) + ( yAtZ[9] - ycorr ) * dXdYLayer[4];
          float const S3x2pred =
              firstXhit + newtx * ( betterZ[10] - betterZ[8] ) + ( yAtZ[10] - 1.03f * ycorr ) * dXdYLayer[5];

          S3UVbuffer += 1;

          // search for hits in layer
          auto const [selectS3L1, DeltaS3L1] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L1, S3x1pred );
          auto const [selectS3L2, DeltaS3L2] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L2, S3x2pred );

          // cache the position for the next search
          startoffsetS3L1 = selectS3L1;
          startoffsetS3L2 = selectS3L2;
          mydebug( "deltas x1, x2: ", DeltaS3L1, DeltaS3L2, hithandler.IDs[selectS3L1], hithandler.IDs[selectS3L2] );

          // calculate acceptance window for uv hits based on momentum
          float const S3uvwindow = ( p_UV3W_b + p_UV3W_m * absDSlope );

          if ( DeltaS3L2 < S3uvwindow ) {
            // I found a hit in S3L2
            tmpidvec.push_back( selectS3L2 );
            ++numuvhits;
          }

          if ( DeltaS3L1 < S3uvwindow ) {
            // I found a hit in S3L1
            tmpidvec.push_back( selectS3L1 );
            ++numuvhits;
          } else if ( numuvhits < 1 )
            // didn't find any uv hits let's try our luck in the next loop iteration
            continue;

          // this variable is saved for later as it is quite good for ghost rejection
          S3UVDelta =
              numuvhits < 2
                  ? 1.0f
                  : 1.0f + std::abs( hithandler.hits[selectS3L1] - S3x1pred + hithandler.hits[selectS3L2] - S3x2pred );

        } else {
          // second iteration:
          // calculate the x-predictions for the U V layers in station 2
          float const S2x1pred =
              firstXhit + newtx * ( betterZ[5] - betterZ[4] ) + ( yAtZ[5] - .83f * ycorr ) * dXdYLayer[2];
          float const S2x2pred =
              firstXhit + newtx * ( betterZ[6] - betterZ[4] ) + ( yAtZ[6] - .85f * ycorr ) * dXdYLayer[3];

          // search for hits in the layers
          auto const [selectS2L1_temp, DeltaS2L1] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L1, S2x1pred );
          auto const [selectS2L2_temp, DeltaS2L2] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L2, S2x2pred );

          // cache the position for the next search
          startoffsetS2L1 = selectS2L1_temp;
          startoffsetS2L2 = selectS2L2_temp;
          mydebug( "deltas x1, x2: ", DeltaS2L1, DeltaS2L2, hithandler.IDs[selectS2L1],
                   hithandler.IDs[selectS2L2_temp] );

          // calculate the acceptance window for uv hits based on momentum
          float const S2uvwindow = ( p_UV2W_b_2ndloop + p_UV2W_m_2ndloop * absDSlope );

          if ( DeltaS2L2 < S2uvwindow ) {
            // I found a hit in S2L2
            selectS2L2 = selectS2L2_temp;
            tmpidvec.push_back( selectS2L2 );
            ++numuvhits;
          }

          if ( DeltaS2L1 < S2uvwindow ) {
            // I found a hit in S2L1
            selectS2L1 = selectS2L1_temp;
            tmpidvec.push_back( selectS2L1 );
            ++numuvhits;

          } else if ( numuvhits < 1 )
            // didn't find any uv hits let's try our luck in the next loop iteration
            continue;

          // this variable is saved for later as it is quite good for ghost rejection
          S2UVDelta =
              numuvhits < 2
                  ? 1.0f
                  : 1.0f + std::abs( hithandler.hits[selectS2L1] - S2x1pred + hithandler.hits[selectS2L2] - S2x2pred );

          // hit threshold in the second iteration
          if ( ( numuvhits + numxhits ) < p_numHitsThrS2_2ndloop ) continue;

        } // end of the S2 doublet search in the second iteration

        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        //                  Start of search in the second station              //
        //                  First iteration:  S3                               //
        //                  Second iteration: S2                               //
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        // most of what happens below is similar to station 2

        // these variables will stay true if x-hits are found in both layers
        // first iteration: S3Doublet
        // second iteration: S2Doublet
        bool S3Doublet = true;
        bool S2Doublet = true;

        // temp objects to handle correctly the different iterations
        int selectS3L0( -1 ), selectS2L0( -1 );
        int selectS3L3( -1 ), selectS2L3( -1 );

        if ( iteration == 0 ) {
          // first iteration: search for doublets in S2
          // doublets have been found in S3 already

          float const S2x0curve = ExtFac_alpha_S2L0 * deltaSlope + ExtFac_beta_S2L0 * deltaSlope +
                                  ExtFac_gamma_S2L0 * deltaSlope * endv_ty2;

          float S2x0pred = firstXhit + newtx * ( betterZ[4] - betterZ[8] ) + S2x0curve;

          float const S2x3curve = deltaSlope * ExtFac_alpha_S2L3 + deltaSlope * ExtFac_beta_S2L3 +
                                  deltaSlope * ExtFac_gamma_S2L3 * endv_ty2;

          float S2x3pred = firstXhit + newtx * ( betterZ[7] - betterZ[8] ) + S2x3curve;

          S2buffer += 1;

          // get the best hit candidate
          auto const [selectS2L3_temp, DeltaS2L3] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L3, S2x3pred );

          // cache the position for the next search
          startoffsetS2L3 = selectS2L3_temp;

          // did we find a hit?
          if ( DeltaS2L3 < ( p_S2L3W_b + p_S2L3W_m * absDSlope ) ) {
            selectS2L3 = selectS2L3_temp;

            tmpidvec.push_back( selectS2L3 );
            ++numxhits;

            // fill fit vec with the x-hit information
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS2L3], betterZ[7] - betterZ[8], S2x3curve,
                        hithandler.hits[selectS2L3] - S2x3pred );
          } else {
            // we didn't find a hit and thus also not a doublet
            S2Doublet = false;

            // do we even really want to continue with this track or is it crap?
            if ( numxhits + numuvhits < 4 ) continue;
          }

          // get the hit candidate for S2L0
          auto const [selectS2L0_temp, DeltaS2L0] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L0, S2x0pred );

          // cache position for next search
          startoffsetS2L0 = selectS2L0_temp;

          // did we find a hit?
          if ( DeltaS2L0 < ( p_S2L0W_b + p_S2L0W_m * absDSlope ) ) {
            selectS2L0 = selectS2L0_temp;
            tmpidvec.push_back( selectS2L0 );
            ++numxhits;

            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS2L0], betterZ[4] - betterZ[8], S2x0curve,
                        hithandler.hits[selectS2L0] - S2x0pred );
          } else {
            S2Doublet = false;
            if ( numxhits + numuvhits < 5 ) continue;
          }
        } else {
          // second iteration: search for doublets in S3
          // a doublet has been found in S2 already

          // make x-predictions
          float const S3x0curve = ExtFac_alpha_S3L0 * deltaSlope + ExtFac_beta_S3L0 * deltaSlope +
                                  ExtFac_gamma_S3L0 * deltaSlope * endv_ty2;

          float const S3x0pred = secondXhit + newtx * ( betterZ[8] - betterZ[7] ) + S3x0curve;

          float const S3x3curve = deltaSlope * ExtFac_alpha_S3L3 + deltaSlope * ExtFac_beta_S3L3 +
                                  deltaSlope * ExtFac_gamma_S3L3 * endv_ty2;

          float const S3x3pred = secondXhit + newtx * ( betterZ[11] - betterZ[7] ) + S3x3curve;

          // quit if we are outside of the sensitive area
          if ( std::abs( S3x3pred ) > 3200.f ) continue;

          mydebug( "S3X0, S3X3", S3x0pred, S3x3pred, "newtx", newtx, "dZsS3L0", betterZ[8] - betterZ[7], "dZsS3L3",
                   betterZ[11] - betterZ[7] );

          // get the best hit candidate
          auto const [selectS3L3_temp, DeltaS3L3] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS3L3, S3x3pred );

          // cache the position for the next search
          startoffsetS3L3 = selectS3L3_temp;
          mydebug( "selectS3L3", selectS3L3, " delta S3L3: ", DeltaS3L3, hithandler.IDs[selectS3L3_temp] );

          // did we find a hit?
          if ( DeltaS3L3 < ( p_S3L3W_b_2ndloop + p_S3L3W_m_2ndloop * absDSlope ) ) {
            selectS3L3 = selectS3L3_temp;
            tmpidvec.push_back( selectS3L3 );

            ++numxhits;

            // here I use (betterZ[11] - betterZ[4]) instead of (betterZ[11] - betterZ[7])
            // otherwise most of the candidates won't survive the quality factor.
            // The factors used to get the candidate quality should be retuned, I guess
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS3L3], betterZ[11] - betterZ[4], S3x3curve,
                        hithandler.hits[selectS3L3] - S3x3pred );
          } else {
            // we didn't find a hit and thus also not a doublet
            S3Doublet = false;

            // do we even really want to continue with this track or is it crap?
            if ( numxhits + numuvhits < 4 ) continue;
          }

          // get the hit candidate for S3L0
          auto const [selectS3L0_temp, DeltaS3L0] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS3L0, S3x0pred );

          // cache the position for the next search
          startoffsetS3L0 = selectS3L0_temp;
          mydebug( "selectS3L0", selectS3L0, " delta S3L0: ", DeltaS3L0, hithandler.IDs[selectS3L0] );

          // did we find a hit?
          if ( DeltaS3L0 < ( p_S3L0W_b_2ndloop + p_S3L0W_m_2ndloop * absDSlope ) ) {
            selectS3L0 = selectS3L0_temp;
            tmpidvec.push_back( selectS3L0 );

            ++numxhits;

            // here I use (betterZ[8] - betterZ[4]) instead of (betterZ[8] - betterZ[7])
            // otherwise most of the candidates won't survive the quality factor.
            // The factors used to get the candidate quality should be retuned, I guess
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS3L0], betterZ[8] - betterZ[4], S3x0curve,
                        hithandler.hits[selectS3L0] - S3x0pred );
          } else {
            S3Doublet = false;
            if ( numxhits + numuvhits < 5 ) continue;
          }
        } // end of the second iteration

        if ( iteration == 0 ) {
          ++CounterXDoubletsS3S2;
        } else {
          ++CounterXDoubletsS3S2_2ndloop;
        }

        // we only check for uv hits if we found a doublet
        // this choice was made since a doublet and it's slope allow for small search windows
        // FIXME we could potentially allow this search in other/all scenarios but this need more tweaking
        // Note that this search will only help with ghost rejection, not help with efficiencies
        // Thus as long as ghosts aren't a problem there isn't really a big reason to loosen this requirement
        if ( ( iteration == 0 ) && S2Doublet ) {

          S2UVbuffer += 1;

          float const S2Tx =
              ( hithandler.hits[selectS2L3] - hithandler.hits[selectS2L0] ) / ( betterZ[7] - betterZ[4] );

          float const S2x1pred = hithandler.hits[selectS2L0] + S2Tx * ( betterZ[5] - betterZ[4] ) +
                                 ( yAtZ[5] - 0.83f * ycorr ) * dXdYLayer[2];

          float const S2x2pred = hithandler.hits[selectS2L0] + S2Tx * ( betterZ[6] - betterZ[4] ) +
                                 ( yAtZ[6] - 0.85f * ycorr ) * dXdYLayer[3];

          auto const [selectS2L1_temp, DeltaS2L1] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L1, S2x1pred );
          startoffsetS2L1 = selectS2L1_temp;

          auto const [selectS2L2_temp, DeltaS2L2] =
              SearchLayerForHitLinear( hithandler.hits, startoffsetS2L2, S2x2pred );
          startoffsetS2L2 = selectS2L2_temp;

          float const S2uvwindow = ( p_UV2W_b + p_UV2W_m * absDSlope );

          if ( DeltaS2L1 > S2uvwindow and DeltaS2L2 > S2uvwindow ) { continue; }

          if ( DeltaS2L2 < S2uvwindow ) {
            selectS2L2 = selectS2L2_temp;
            tmpidvec.push_back( selectS2L2 );
            ++numuvhits;
          }

          if ( DeltaS2L1 < S2uvwindow ) {
            selectS2L1 = selectS2L1_temp;
            tmpidvec.push_back( selectS2L1 );
            ++numuvhits;
          }

          if ( DeltaS2L1 < S2uvwindow and DeltaS2L2 < S2uvwindow ) {
            S2UVDelta += std::abs( hithandler.hits[selectS2L1] + hithandler.hits[selectS2L2] - S2x1pred - S2x2pred );
          }
        } // end of the UV work in the first iteration

        // similar thing, but for the second iteration: UV layers of station 3
        if ( ( iteration > 0 ) && S3Doublet ) {

          float const S3Tx =
              ( hithandler.hits[selectS3L3] - hithandler.hits[selectS3L0] ) / ( betterZ[11] - betterZ[8] );

          float const S3x1pred = hithandler.hits[selectS3L0] + S3Tx * ( betterZ[9] - betterZ[8] ) +
                                 ( yAtZ[9] - 1.f * ycorr ) * dXdYLayer[4];

          float const S3x2pred = hithandler.hits[selectS3L0] + S3Tx * ( betterZ[10] - betterZ[8] ) +
                                 ( yAtZ[10] - 1.03f * ycorr ) * dXdYLayer[5];

          mydebug( S3Tx, betterZ[11] - betterZ[8], hithandler.hits[selectS3L0], hithandler.hits[selectS3L3], S3x1pred,
                   S3x2pred );
          mydebug( hithandler.hits[startS3L1 + 1], hithandler.hits[startS3L2 - 2] );
          mydebug( LHCb::LHCbID( hithandler.IDs[startS3L1 + 1] ), LHCb::LHCbID( hithandler.IDs[startS3L2 - 2] ) );
          mydebug( hithandler.hits[startS3L2 + 1], hithandler.hits[startS3L0 - 2] );
          mydebug( LHCb::LHCbID( hithandler.IDs[startS3L2 + 1] ), LHCb::LHCbID( hithandler.IDs[startS3L0 - 2] ) );

          auto const [selectS3L1, DeltaS3L1] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L1, S3x1pred );
          startoffsetS3L1                    = selectS3L1;

          auto const [selectS3L2, DeltaS3L2] = SearchLayerForHitLinear( hithandler.hits, startoffsetS3L2, S3x2pred );
          startoffsetS3L2                    = selectS3L2;

          float const S3uvwindow = ( p_UV3W_b_2ndloop + p_UV3W_m_2ndloop * absDSlope );

          if ( DeltaS3L1 > S3uvwindow and DeltaS3L2 > S3uvwindow ) { continue; }

          if ( DeltaS3L2 < S3uvwindow ) {
            tmpidvec.push_back( selectS3L2 );
            ++numuvhits;
          }

          if ( DeltaS3L1 < S3uvwindow ) {
            tmpidvec.push_back( selectS3L1 );
            ++numuvhits;
          }

          if ( DeltaS3L1 < S3uvwindow and DeltaS3L2 < S3uvwindow ) {
            S3UVDelta += std::abs( hithandler.hits[selectS3L1] + hithandler.hits[selectS3L2] - S3x1pred - S3x2pred );
          }

        } // end of the UV work for the second iteration

        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        //                      Start of search in Station 1                   //
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        mydebug( "Start Station 1 Work" );

        S1buffer += 1;

        float const S1x3curve =
            deltaSlope * ExtFac_alpha_S1L3 + deltaSlope * ExtFac_beta_S1L3 + deltaSlope * ExtFac_gamma_S1L3 * endv_ty2;

        // the layer of the seeding hit, thus its z position, depends on the current iteration
        float const S1x3pred = ( iteration == 0 ) ? firstXhit + newtx * ( betterZ[3] - betterZ[8] ) + S1x3curve
                                                  : firstXhit + newtx * ( betterZ[3] - betterZ[4] ) + S1x3curve;

        mydebug( "S1X3straight", firstXhit + newtx * ( betterZ[3] - betterZ[4] ), "S1X3curve", S1x3curve, "pred",
                 S1x3pred );

        auto const [selectS1L3, DeltaS1L3] =
            SearchLayerForHitBinary( hithandler.hits, S1L3range.first, S1L3range.first + S1L3range.second, S1x3pred );

        bool S1Doublet = true;

        mydebug( "selectS1L3", selectS1L3 );
        mydebug( "delta S1L3: ", DeltaS1L3, hithandler.IDs[selectS1L3] );

        // threshold of xhits in S1 and S2 to reject a candidate, depending on the iteration
        // this is put in OR with the (numuvhits < 3) condition
        int const numxhits_thr = ( iteration == 0 ) ? 4 : static_cast<int>( p_numXHitsThrS1S2_2ndloop );

        // threshold on DeltaS1L3, depending on the iteration
        float const DeltaS1L3_thr =
            ( iteration == 0 ) ? p_S1L3W_b + p_S1L3W_m * absDSlope : p_S1L3W_b_2ndloop + p_S1L3W_m_2ndloop * absDSlope;

        if ( DeltaS1L3 < DeltaS1L3_thr ) {
          tmpidvec.push_back( selectS1L3 );
          ++numxhits;

          // the delta z wrt the seeding layer depends on the current iteration
          if ( iteration == 0 )
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L3], betterZ[3] - betterZ[8], S1x3curve,
                        hithandler.hits[selectS1L3] - S1x3pred );
          else
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L3], betterZ[3] - betterZ[4], S1x3curve,
                        hithandler.hits[selectS1L3] - S1x3pred );

          mydebug( "Hit Selected" );
        } else {
          S1Doublet = false;
          if ( numxhits < numxhits_thr or numuvhits < 3 ) continue;
        }

        float const S1x0curve =
            ExtFac_alpha_S1L0 * deltaSlope + ExtFac_beta_S1L0 * deltaSlope + deltaSlope * ExtFac_gamma_S1L0 * endv_ty2;

        // the layer of the seeding hit, thus its z position, depends on the current iteration
        float const S1x0pred = ( iteration == 0 ) ? firstXhit + newtx * ( betterZ[0] - betterZ[8] ) + S1x0curve
                                                  : firstXhit + newtx * ( betterZ[0] - betterZ[4] ) + S1x0curve;

        mydebug( "S1x0straight", firstXhit + newtx * ( betterZ[0] - betterZ[4] ), "S1x0curve", S1x0curve, "pred",
                 S1x0pred );

        auto const [selectS1L0, DeltaS1L0] =
            SearchLayerForHitBinary( hithandler.hits, S1L0range.first, S1L0range.first + S1L0range.second, S1x0pred );

        mydebug( "selectS1L0", selectS1L0, "delta S1L0: ", DeltaS1L0, hithandler.IDs[selectS1L0] );

        // to make the threshold on DeltaS1L0 depending on the iteration
        float const DeltaS1L0_thr =
            ( iteration == 0 ) ? p_S1L0W_b + p_S1L0W_m * absDSlope : p_S1L0W_b_2ndloop + p_S1L0W_m_2ndloop * absDSlope;

        if ( DeltaS1L0 < DeltaS1L0_thr ) {
          tmpidvec.push_back( selectS1L0 );
          ++numxhits;

          // the delta z wrt the seeding layer depends on the current iteration
          if ( iteration == 0 )
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L0], betterZ[0] - betterZ[8], S1x0curve,
                        hithandler.hits[selectS1L0] - S1x0pred );
          else
            fillFitVec( fitvec, fitcnt, hithandler.hits[selectS1L0], betterZ[0] - betterZ[4], S1x0curve,
                        hithandler.hits[selectS1L0] - S1x0pred );

          mydebug( "Hit Selected" );
        } else {
          S1Doublet = false;
          if ( numxhits < ( numxhits_thr + 1 ) or numuvhits < 3 ) continue;
        }

        if ( iteration == 0 ) {
          ++CounterXDoubletsS3S2S1;
        } else {
          ++CounterXDoubletsS3S2S1_2ndloop;
        }

        float S1UVDelta = 1.0f;

        if ( S1Doublet ) {
          S1UVbuffer += 1;
          float const S1Tx =
              ( hithandler.hits[selectS1L3] - hithandler.hits[selectS1L0] ) / ( betterZ[3] - betterZ[0] );

          float const S1x1pred = hithandler.hits[selectS1L0] + S1Tx * ( betterZ[1] - betterZ[0] ) +
                                 ( yAtZ[1] - .62f * ycorr ) * dXdYLayer[0];

          float const S1x2pred = hithandler.hits[selectS1L0] + S1Tx * ( betterZ[2] - betterZ[0] ) +
                                 ( yAtZ[2] - .63f * ycorr ) * dXdYLayer[1];

          auto const [selectS1L1, DeltaS1L1] =
              SearchLayerForHitBinary( hithandler.hits, S1L1range.first, S1L1range.first + S1L1range.second, S1x1pred );

          auto const [selectS1L2, DeltaS1L2] =
              SearchLayerForHitBinary( hithandler.hits, S1L2range.first, S1L2range.first + S1L2range.second, S1x2pred );

          // the UV window value depends on the current iteration
          float const S1uvwindow = ( iteration == 0 ) ? ( p_UV1W_b + p_UV1W_m * absDSlope )
                                                      : ( p_UV1W_b_2ndloop + p_UV1W_m_2ndloop * absDSlope );

          if ( DeltaS1L1 > S1uvwindow and std::abs( DeltaS1L2 ) > S1uvwindow ) { continue; }

          if ( DeltaS1L2 < S1uvwindow ) {
            tmpidvec.push_back( selectS1L2 );
            ++numuvhits;
          }

          if ( DeltaS1L1 < S1uvwindow ) {
            tmpidvec.push_back( selectS1L1 );
            ++numuvhits;
          }

          if ( DeltaS1L1 < S1uvwindow and DeltaS1L2 < S1uvwindow )
            S1UVDelta += std::abs( hithandler.hits[selectS1L1] + hithandler.hits[selectS1L2] - S1x1pred - S1x2pred );
        }

        mydebug( "End Station 1 Work" );

        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////
        //                      Start of finalization procedure                //
        /////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////

        // quick linear fit in x0 and tx, curvature terms remain fixed
        float s0  = 0;
        float sz  = 0;
        float sz2 = 0;
        float sd  = 0;
        float sdz = 0;

        for ( int idx{0}; idx < fitcnt; idx += 4 ) {
          // for now the weight is just 1
          s0 += 1;
          sz += fitvec[idx + 1];
          sz2 += fitvec[idx + 1] * fitvec[idx + 1];
          sd += fitvec[idx + 3];
          sdz += fitvec[idx + 3] * fitvec[idx + 1];
        }

        float const den     = 1.f / ( sz * sz - 6 * sz2 );
        float const xoff    = ( sdz * sz - sd * sz2 ) * den;
        float const txoff   = ( sd * sz - 6 * sdz ) * den;
        float const finaltx = newtx + txoff;
        float const newx0   = firstXhit + xoff;

        // calculate a chi2 after the fit
        float chi2 = ( fitvec[0] - newx0 ) * ( fitvec[0] - newx0 );
        for ( int idx{4}; idx < fitcnt; idx += 4 ) {
          // for now the weight is just 1
          float const diff = fitvec[idx] - ( newx0 + fitvec[idx + 1] * finaltx + fitvec[idx + 2] );
          chi2 += diff * diff;
        }

        // determine momentum * charge estimate, polynomial was fitted on MC
        const float finaltx2 = finaltx * finaltx;
        const float coef =
            ( MomentumParams[0] + finaltx2 * ( MomentumParams[1] + MomentumParams[2] * finaltx2 ) +
              MomentumParams[3] * finaltx * endv_tx + endv_ty2 * ( MomentumParams[4] + MomentumParams[5] * endv_ty2 ) +
              MomentumParams[6] * endv_tx2 );

        float const candPQ = ( iteration == 0 ? 1.f : 0.9818f ) * m_magscalefactor * coef / ( finaltx - endv_tx ) +
                             factor * MomentumParams[7];

        mydebug( finaltx, endv_tx, endv_ty2, factor, coef, candPQ );

        // the condition evaluates to true if the track is a charge flipped one
        float deltaMom = ( endv_pt > p_wrongSignPT and ( factor * ( firstXhit - straighExt ) < 0 ) )
                             ? 3.f * std::abs( endv_qp * ( endv_pq + candPQ ) )
                             : std::abs( endv_qp * ( endv_pq - candPQ ) );

        // counteract worse momentum resolution at higher momenutm by scaling down the observed delta
        if ( std::abs( candPQ ) > 40000 ) deltaMom *= 40000.f / std::abs( candPQ );

        // For now a linear discriminant seems to do well enough. But if needed this could be substituted by a neural
        // net for better rejection of ghosts
        float const quality =
            LDAParams[0] * approx_log( SIMDWrapper::scalar::float_v( 1.f + deltaMom ) ).cast() +
            LDAParams[1] * approx_log( SIMDWrapper::scalar::float_v( 1.f + chi2 ) ).cast() +
            LDAParams[2] *
                approx_log( SIMDWrapper::scalar::float_v( std::abs( candPQ ) * endv_txy / std::sqrt( 1 + endv_txy2 ) ) )
                    .cast() +
            LDAParams[3] * approx_log( SIMDWrapper::scalar::float_v( S3UVDelta ) ).cast() +
            LDAParams[4] * approx_log( SIMDWrapper::scalar::float_v( S2UVDelta ) ).cast() +
            LDAParams[5] * approx_log( SIMDWrapper::scalar::float_v( S1UVDelta ) ).cast() +
            LDAParams[6] * static_cast<float>( numuvhits ) + LDAParams[7] * static_cast<float>( numxhits );

        // float const quality =
        // LDAParams[0] * vdt::fast_logf( 1.f + deltaMom ) + LDAParams[1] * vdt::fast_logf( 1.f + chi2 ) +
        // LDAParams[2] * vdt::fast_logf( std::abs( candPQ ) * endv_txy / std::sqrt( 1 + endv_txy2 ) ) +
        // LDAParams[3] * vdt::fast_logf( S3UVDelta ) + LDAParams[4] * vdt::fast_logf( S2UVDelta ) +
        // LDAParams[5] * vdt::fast_logf( S1UVDelta ) + LDAParams[6] * static_cast<float>( numuvhits ) +
        // LDAParams[7] * static_cast<float>( numxhits );

        mydebug( "+++++++++Trackcandidate++++++++++++++++" );
        mydebug( candPQ, finaltx, endv_pq );
        mydebug( "deltaMom", deltaMom, "chi2", chi2, "pt", std::abs( candPQ ) * endv_txy / std::sqrt( 1 + endv_txy2 ),
                 "S3UVDelta", S3UVDelta );
        mydebug( "final quality", quality, "worst candidate", bestcandidate.quality );

        if ( iteration == 0 ) {
          ++CounterCandsBeforeQuality;
        } else {
          ++CounterCandsBeforeQuality_2ndloop;
        }

        if ( quality < bestcandidate.quality ) {
          bestcandidate = {tmpidvec, candPQ, finaltx, newx0, quality, numuvhits + numxhits};

          // we got a new candidate!
          if ( iteration == 0 ) {
            ++CounterCandsAfterQuality;
          } else {
            ++CounterCandsAfterQuality_2ndloop;
          }
        }
      } // loop over possible station three doublets

      mydebug( "Search done!" );

      // save the bestcandidate if we built one
      if ( !bestcandidate.ids.empty() ) {
        int i    = Output.size();
        int mask = true;

        ++CounterCandsBuffer;

        Output.compressstore_trackVP<I>( i, mask, tracks.trackVP<I>( uttrack ) );
        Output.compressstore_trackUT<I>( i, mask, uttrack );

        float const qop = 1.f / bestcandidate.PQ;
        Output.compressstore_stateQoP<F>( i, mask, qop );

        int n_hits = 0;

        for ( auto idx{bestcandidate.ids.begin()}; idx != bestcandidate.ids.end(); ++idx, ++n_hits ) {
          Output.compressstore_hit<I>( i, n_hits, mask, hithandler.IDs[*idx] );
        }

        Output.compressstore_nHits<I>( i, mask, bestcandidate.numHits );

        // AtT State
        float const endT_z = cache.LayerZPos[8];
        float const endT_x = bestcandidate.newx0;
        auto        endT_y = endv_pos.y + endv_dir.y * ( endT_z - endv_pos.z );

        auto endT_pos = Vec3<F>( endT_x, endT_y, endT_z );
        auto endT_dir = Vec3<F>( bestcandidate.finaltx, endv_dir.y, 1.f );

        Output.compressstore_statePos<F>( i, mask, endT_pos );
        Output.compressstore_stateDir<F>( i, mask, endT_dir );

        Output.size() += dType::popcount( mask );

        // if we found a candidate we won't start the second loop
        break;
      } // save the bestcandidate if we built one

    } // end loop over iterations
  }   // loop over the UT tracks

  m_CounterOutput += Output.size();
  return Output;
}
