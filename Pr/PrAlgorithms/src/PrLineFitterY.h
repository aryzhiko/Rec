/*******************************************************************************\
 * (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#ifndef PRLINEFITTERY_H
#define PRLINEFITTERY_H 1

// Include files
#include "PrHybridSeedTrack.h"
#include <array>

/** @class PrLineFitterY PrLineFitterY.h
 *  Support class to perform fit of lines using solely hits in UV layers setting the parameters of
 *  the xz projection and fitting passing iterators of the hough-like cluster
 *  @author Renato Quagliani
 *  @date   2015-08-01
 *  @author Salvatore Aiola, salvatore.aiola@cern.ch
 *  @date   2020-02-29
 */

class PrLineFitterY final {
public:
  /// Standard constructor setting
  PrLineFitterY() = default;
  /// Standard constructor
  PrLineFitterY( const Pr::Hybrid::SeedTrackX& track )
      : m_ax( track.ax() ), m_bx( track.bx() ), m_cx( track.cx() ), m_dRatio( track.dRatio() ){};
  /** Set values of the xzprojection
   */
  void setXProj( const Pr::Hybrid::SeedTrackX& track ) noexcept {
    m_ax     = track.ax();
    m_bx     = track.bx();
    m_cx     = track.cx();
    m_dRatio = track.dRatio();
  }
  void reset() {
    m_ay       = std::numeric_limits<float>::max();
    m_by       = std::numeric_limits<float>::max();
    m_chi2line = std::numeric_limits<float>::max();
  }
  // return xzprojection slope at a given z
  float xSlope( const float z ) const noexcept {
    const float dz = z - Pr::Hybrid::zReference;
    return m_bx + 2.f * m_cx * dz + 3.f * m_cx * m_dRatio * dz * dz;
  }
  // return xzprojection x position at a given z
  float x( const float z ) const noexcept {
    const float dz = z - Pr::Hybrid::zReference;
    return ( m_ax + m_bx * dz + m_cx * dz * dz * ( 1.f + m_dRatio * dz ) );
  }
  // return track-hit distance
  float distance( const ModPrHit& hit ) const noexcept {
    const float z    = hit.hit->z();
    const float yAtZ = y( z );
    return ( hit.hit->x( yAtZ ) - x( z ) );
  }

  float distanceAt0( const ModPrHit& hit ) const noexcept {
    const float z = hit.hit->z();
    return ( hit.hit->x() - x( z ) );
  }

  // return the chi2 contribution from a hit
  float chi2hit( const ModPrHit& hit ) const noexcept {
    float       erry = hit.hit->w();
    const float dist = distance( hit );
    return dist * dist * erry;
  }

  // return yOnTrack in rotated ref frame taking into account the dzDy
  float yOnTrack( const ModPrHit& hit ) const noexcept {
    return hit.hit->yOnTrack( m_ay - Pr::Hybrid::zReference * m_by, m_by );
  }

  // return the y of the track at a given z
  float y( float z ) const noexcept { return m_ay + m_by * ( z - Pr::Hybrid::zReference ); }

  // Fit hit positions with a straight line using Cramer's rule
  template <typename T>
  bool fit( T itBeg, T itEnd ) noexcept {
    std::array<float, 3> m_mat = {0, 0, 0};
    std::array<float, 2> m_rhs = {0, 0};
    for ( auto it = itBeg; itEnd != it; ++it ) {
      const PrHit* hit     = ( *it )->hit;
      const float  dz      = hit->z() - Pr::Hybrid::zReference;
      const float  dist    = distanceAt0( **it );
      const float  dxDy    = hit->dxDy();
      const float  wdxDy   = hit->w() * dxDy;
      const float  wdxDydz = wdxDy * dz;
      m_mat[0] += wdxDy * dxDy;
      m_mat[1] += wdxDy * dxDy * dz;
      m_mat[2] += wdxDydz * dz * dxDy;
      m_rhs[0] -= wdxDy * dist;
      m_rhs[1] -= wdxDydz * dist;
    }
    float det = m_mat[0] * m_mat[2] - m_mat[1] * m_mat[1];
    if ( det == 0 ) {
      m_ay       = std::numeric_limits<float>::max();
      m_by       = std::numeric_limits<float>::max();
      m_chi2line = std::numeric_limits<float>::max();
      return false;
    }
    m_ay       = ( m_rhs[0] * m_mat[2] - m_mat[1] * m_rhs[1] ) / det;
    m_by       = ( m_mat[0] * m_rhs[1] - m_rhs[0] * m_mat[1] ) / det;
    m_chi2line = 0.f;
    for ( auto hit = itBeg; itEnd != hit; ++hit ) m_chi2line += chi2hit( **hit );
    m_chi2line /= ( itEnd - itBeg - 2 );
    return true;
  }

  float ay() const noexcept { return m_ay; }
  float ay0() const noexcept { return m_ay - m_by * Pr::Hybrid::zReference; }
  float by() const noexcept { return m_by; }
  float Chi2DoF() const noexcept { return m_chi2line; }

private:
  float m_ay{std::numeric_limits<float>::max()};
  float m_by{std::numeric_limits<float>::max()};
  float m_chi2line{std::numeric_limits<float>::max()};
  float m_ax{std::numeric_limits<float>::max()};
  float m_bx{std::numeric_limits<float>::max()};
  float m_cx{std::numeric_limits<float>::max()};
  float m_dRatio{std::numeric_limits<float>::max()};
};

#endif // PRLINEFITTERY_H
