/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SystemOfUnits.h"

namespace {
  using TracksVP    = LHCb::Pr::Velo::Tracks;
  using TracksUT    = LHCb::Pr::Upstream::Tracks;
  using Transformer = Gaudi::Functional::Transformer<TracksUT( TracksVP const& )>;
} // namespace

namespace Pr {
  /** @class UpstreamFromVelo PrUpstreamFromVelo.cpp
   *
   *  Converts a container of Velo tracks into a container of Upstream ones
   *  with some fixed pT value.
   */
  struct UpstreamFromVelo final : public Transformer {
    UpstreamFromVelo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}

    TracksUT operator()( TracksVP const& inputTracks ) const override {
      using dType = SIMDWrapper::best::types;
      using I     = dType::int_v;
      using F     = dType::float_v;
      TracksUT outputTracks{&inputTracks};
      float    invAssumedPT{1.f / m_assumedPT};
      for ( int i = 0; i < inputTracks.size(); i += dType::size ) {
        auto mask = dType::loop_mask( i, inputTracks.size() ); // true if i < tracks.size() else false
        auto pos  = inputTracks.statePos<F>( i, 1 );
        auto dir  = inputTracks.stateDir<F>( i, 1 );
        auto cov  = inputTracks.stateCovX<F>( i, 1 );
        // Assign q/p assuming q=+1 and pT is 'AssumedPT'
        auto txy2 = dir.x * dir.x + dir.y * dir.y;
        auto qop  = invAssumedPT * sqrt( txy2 / ( 1 + txy2 ) );
        outputTracks.compressstore_nHits<I>( i, mask, 0 );
        outputTracks.compressstore_trackVP<I>( i, mask, dType::indices( i ) );
        outputTracks.compressstore_statePos<F>( i, mask, pos );
        outputTracks.compressstore_stateDir<F>( i, mask, dir );
        outputTracks.compressstore_stateCov<F>( i, mask, cov );
        outputTracks.compressstore_stateQoP<F>( i, mask, qop );
        outputTracks.size() += dType::popcount( mask );
      }
      return outputTracks;
    }

    Gaudi::Property<float> m_assumedPT{this, "AssumedPT", 4.5 * Gaudi::Units::GeV};
  };
} // namespace Pr

DECLARE_COMPONENT_WITH_ID( Pr::UpstreamFromVelo, "PrUpstreamFromVelo" )