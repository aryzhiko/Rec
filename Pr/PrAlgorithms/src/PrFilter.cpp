/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PrAlgorithms/PrFilter.h"
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/PrIterableVeloTracks.h"
#include "Event/TrackWithMuonPIDSkin.h"
#include "Event/Track_v2.h"
#include "PrKernel/PrSelection.h"

namespace Pr {

  namespace {
    template <>
    struct TypeHelper<Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>> {
      using T = Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>;
      using AlgInputType                 = T const&;
      using InputType                    = Zipping::SelectionView<T const> const&;
      using OutputType                   = Zipping::ExportedSelection<>;
      constexpr static auto ExtraHeaders = {"Event/MuonPID_v2.h", "Event/TrackWithMuonPIDSkin.h",
                                            "SOAExtensions/ZipSelection.h", "SOAContainer/SOAContainer.h"};
    };
  } // namespace

  // Pr::Selection<v2::Track> -> Pr::Selection<v2::Track>
  DECLARE_COMPONENT_WITH_ID( Filter<Pr::Selection<LHCb::Event::v2::Track>>, "PrFilter__Track_v2" )

  // LHCb::Pr::Velo::Tracks -> LHCb::Pr::Velo::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Velo::Tracks>, "PrFilter__PrVeloTracks" )

  // LHCb::Pr::Fitted::Forward::Tracks -> LHCb::Pr::Fitted::Forward::Tracks
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::Tracks>, "PrFilter__PrFittedForwardTracks" )

  // LHCb::Pr::Fitted::Forward::TracksWithPVs
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                             "PrFilter__PrFittedForwardTracksWithPVs" )

  // LHCb::Pr::Fitted::Forward::TracksWithMuonID
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                             "PrFilter__PrFittedForwardTracksWithMuonID" )

  // SOA::Container: refine a selection on a view
  DECLARE_COMPONENT_WITH_ID(
      SOAFilter<Zipping::ZipContainer<SOA::contiguous_const_view_from_skin_t<LHCb::Event::v2::TrackWithMuonID>>>,
      "SOAFilter__TrackWithMuonIDView" )
} // namespace Pr
