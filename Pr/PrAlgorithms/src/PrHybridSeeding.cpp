/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

// Range v3
#include "range/v3/version.hpp"
#include <range/v3/algorithm/max.hpp>
#include <range/v3/view/transform.hpp>
#if RANGE_V3_VERSION < 900
namespace ranges::views {
  using namespace ranges::view;
}
#endif

#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Kernel/STLExtensions.h" // ASSUME statement
#include "Math/CholeskyDecomp.h"

// local
#include "Event/FTCluster.h"
#include "Event/FTLiteCluster.h"
#include "PrHybridSeeding.h"
#include "PrLineFitterY.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PrHybridSeeding
//
// @author Renato Quagliani (rquaglia@cern.ch), Louis Henry (louis.henry@cern.ch), Salvatore Aiola
// (salvatore.aiola@cern.ch)
// @date   2020-01-21
//-----------------------------------------------------------------------------

namespace {
  using namespace ranges;

  // Hit handling/ sorting etc...
  struct compX {
    bool operator()( float lv, float rv ) const { return lv < rv; }
    bool operator()( const ModPrHit& lhs, float rv ) const { return ( *this )( lhs.x(), rv ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.x() ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.x(), rhs.x() ); }
    bool operator()( const ModPrHit* lhs, float rv ) const { return ( *this )( lhs->x(), rv ); }
    bool operator()( float lv, const ModPrHit* rhs ) const { return ( *this )( lv, rhs->x() ); }
    bool operator()( const ModPrHit* lhs, const ModPrHit* rhs ) const { return ( *this )( lhs->x(), rhs->x() ); }
  };
  struct compXreverse {
    bool operator()( float lv, float rv ) const { return lv > rv; }
    bool operator()( const ModPrHit& lhs, float rv ) const { return ( *this )( lhs.x(), rv ); }
    bool operator()( float lv, const ModPrHit& rhs ) const { return ( *this )( lv, rhs.x() ); }
    bool operator()( const ModPrHit& lhs, const ModPrHit& rhs ) const { return ( *this )( lhs.x(), rhs.x() ); }
    bool operator()( const ModPrHit* lhs, float rv ) const { return ( *this )( lhs->x(), rv ); }
    bool operator()( float lv, const ModPrHit* rhs ) const { return ( *this )( lv, rhs->x() ); }
    bool operator()( const ModPrHit* lhs, const ModPrHit* rhs ) const { return ( *this )( lhs->x(), rhs->x() ); }
  };

  // search lowerBound from a known position to another known position (linear time)
  HitIter get_lowerBound_lin( HitIter begin, HitIter end, float xMin ) {
    return std::find_if_not( begin, end, [xMin, cmp = compX{}]( const auto& i ) { return cmp( i, xMin ); } );
  }
  // search linearly a lowerBound in low/high range starting from high
  HitIter get_lowerBound_lin_reverse( HitIter low, HitIter high, float xMin ) {
    return std::find_if( std::reverse_iterator{high}, std::reverse_iterator{low},
                         [xMin, cmp = compXreverse{}]( const auto& i ) { return cmp( xMin, i ); } )
        .base();
  }
  // search upperBound from known position to another known position (linear time)
  HitIter get_upperBound_lin( HitIter begin, HitIter end, float xMax ) {
    return std::find_if( begin, end, [xMax, cmp = compX{}]( const auto& i ) { return cmp( xMax, i ); } );
  }

  IterPairs InitializeBB( const unsigned int& ZoneNumber, const PrFTHitHandler<ModPrHit>& FTHitHandler ) noexcept {
    auto r = FTHitHandler.hits( ZoneNumber );
    return {r.begin(), r.end()};
  }

  // Update the bounds (Hiterator Pairs) and old Min => new Min searching linearly around current boundary begin
  void LookAroundMin( IterPairs& bound, float& oMin, const float& nMin, const ModPrHitConstIter& eZone,
                      const ModPrHitConstIter& bZone ) {
    bound.begin = ( nMin < oMin ? get_lowerBound_lin_reverse( bZone, bound.begin, nMin )
                                : get_lowerBound_lin( bound.begin, eZone, nMin ) );
    oMin        = nMin;
  }
} // namespace

// Clone removal namespace
namespace {
  template <typename T>
  bool areClones( const T& tr1, const T& tr2, const unsigned int& maxCommon ) {
    unsigned int nCommon = maxCommon;
    auto         itH1    = tr1.hits().begin();
    auto         itBeg2  = tr2.hits().begin();
    auto         itEnd1  = tr1.hits().end();
    auto         itEnd2  = tr2.hits().end();
    while ( nCommon != 0 && itH1 != itEnd1 ) {
      for ( auto itH2 = itBeg2; itH2 != itEnd2; itH2++ )
        if ( itH1->hit->id() == itH2->hit->id() ) {
          --nCommon;
          break;
        }
      ++itH1;
    }
    return ( nCommon == 0 );
  }

  /** @brief Given two tracks it checks if they are clones. Clone threshold is defined by the
      amount of shared hits ( clones if nCommon > maxCommon)
  */
  template <typename T>
  bool removeWorstTrack( T& t1, T& t2 ) {
    bool ret = !T::LowerBySize( t1, t2 );
    if ( !ret )
      t2.setValid( false );
    else
      t1.setValid( false );
    return ret;
  }

  /** @brief Check if two tracks are passing through T1/T2/T3 at a distance less than "distance".
   *  @param tr1 First track in comparison
   *  @param tr2 Second track in comparison
   *  @param distance Distance to check if two tracks are close one to anohter
   *  @return bool Success, tracks are close enough
   */
  bool CloseEnough( const Pr::Hybrid::AbsSeedTrack& tr1, const Pr::Hybrid::AbsSeedTrack& tr2, const float& distance ) {
    return ( std::abs( tr1.xT1() - tr2.xT1() ) < std::abs( distance ) ||
             std::abs( tr1.xT2() - tr2.xT2() ) < std::abs( distance ) ||
             std::abs( tr1.xT3() - tr2.xT3() ) < std::abs( distance ) );
  }

  // Removes the worst of two tracks
  template <typename CommonHits, typename T>
  struct removeClonesT {
    void operator()( unsigned int part, T& candidates, CommonHits commonHits = CommonHits() ) {
      //---LoH: it is surely faster to do by a simpler alg
      for ( auto itT1 = candidates[part].begin(); itT1 < candidates[part].end() - 1; ++itT1 ) {
        if ( !( *itT1 ).valid() ) continue;
        for ( auto itT2 = itT1 + 1; itT2 < candidates[part].end(); ++itT2 ) {
          if ( !( *itT2 ).valid() ) continue;
          if ( !CloseEnough( ( *itT1 ), ( *itT2 ), 2.f ) ) continue;
          if ( areClones( ( *itT1 ), ( *itT2 ), commonHits( itT1->size(), itT2->size() ) ) ) {
            if ( removeWorstTrack( *itT1, *itT2 ) ) break;
          }
        }
      }
    }
  };

  auto removeClonesX = removeClonesT<PrHybridSeeding::CommonXHits, XCandidates>();
  auto removeClones  = removeClonesT<PrHybridSeeding::CommonXYHits, TrackCandidates>();

  // Remove tracks to recover
  void removeClonesRecover( unsigned int part, TrackToRecover& trackToRecover, XCandidates& xCandidates,
                            PrHybridSeeding::CommonRecoverHits commonHits = PrHybridSeeding::CommonRecoverHits() ) {
    for ( auto itT1 = trackToRecover[part].begin(); itT1 != trackToRecover[part].end(); ++itT1 ) {
      if ( !( *itT1 ).recovered() ) continue;
      for ( auto itT2 = itT1 + 1; itT2 != trackToRecover[part].end(); ++itT2 ) {
        if ( !( *itT2 ).recovered() ) continue;
        if ( !CloseEnough( ( *itT1 ), ( *itT2 ), 5.f ) ) continue;
        if ( areClones( ( *itT1 ), ( *itT2 ), commonHits( itT1->size(), itT2->size() ) ) ) {
          if ( Pr::Hybrid::SeedTrackX::LowerBySize( *itT1, *itT2 ) )
            itT2->setRecovered( false );
          else {
            itT1->setRecovered( false );
            break;
          }
        }
      }
      if ( ( *itT1 ).recovered() ) { xCandidates[int( part )].push_back( ( *itT1 ) ); }
    }
  }

  inline size_t nUsed( SeedTrackHits::const_iterator itBeg, SeedTrackHits::const_iterator itEnd,
                       const PrFTHitHandler<ModPrHit>& hitHandler ) {
    auto condition = [&hitHandler]( const ModPrHit& h ) { return !hitHandler.hit( h.hitIndex ).isValid(); };
    return std::count_if( itBeg, itEnd, condition );
  }

  template <typename Range>
  static bool hasT1T2T3Track( const Range& hits ) noexcept {
    std::bitset<3> T{};
    for ( const auto& hit : hits ) {
      int planeBit = hit.planeCode / 4;
      ASSUME( planeBit < 3 );
      T[planeBit] = true;
      if ( T.all() ) return true;
    }
    return false;
  }

} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrHybridSeeding )
//=============================================================================
// Standard constructor, initializes variable
//=============================================================================

PrHybridSeeding::PrHybridSeeding( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation},
                   KeyValue{"OutputName", LHCb::TrackLocation::Seed} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PrHybridSeeding::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

  if constexpr ( NCases > 3 ) {
    error() << "Algorithm does not support more than 3 Cases" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( m_minTot[RecoverCase] < 9u ) {
    error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
    return StatusCode::FAILURE;
  }
  for ( unsigned int i = 0; i < NCases; i++ )
    if ( m_minTot[i] < 9 ) {
      error() << "Algorithm does not support fewer than 9 hits in total (due to addStereo)" << endmsg;
      return StatusCode::FAILURE;
    }

  // Zones cache, retrieved from the detector store
  registerCondition<PrHybridSeeding>( PrFTInfo::FTZonesLocation, m_zoneHandler );

  //---LoH: UV zones
  m_dxDy[0] = m_zoneHandler->zone( T1U ).dxDy(); // u-layers
  m_dxDy[1] = m_zoneHandler->zone( T1V ).dxDy(); // v-layers
  for ( auto layer : {T1X1, T1X2, T2X1, T2X2, T3X1, T3X2, T1U, T1V, T2U, T2V, T3U, T3V} )
    for ( auto part : {0, 1} ) {
      m_z[layer - part]         = m_zoneHandler->zone( layer - part ).z();
      m_planeCode[layer - part] = m_zoneHandler->zone( layer - part ).planeCode();
    }

  //---LoH: Precalculate y borders
  m_yMins[0][0] = m_yMin;
  m_yMins[0][1] = m_yMin_TrFix;
  m_yMins[1][0] = -m_yMax_TrFix;
  m_yMins[1][1] = -std::abs( m_yMax );

  m_yMaxs[0][0] = std::abs( m_yMax );
  m_yMaxs[0][1] = m_yMax_TrFix;
  m_yMaxs[1][0] = -m_yMin_TrFix;
  m_yMaxs[1][1] = -m_yMin; // positive

  // Create the m_minUV here
  m_minUV[0][0] = m_minUV6[0];
  m_minUV[0][1] = m_minUV5[0];
  m_minUV[0][2] = m_minUV4[0];
  m_minUV[1][0] = m_minUV6[1];
  m_minUV[1][1] = m_minUV5[1];
  m_minUV[1][2] = m_minUV4[1];
  m_minUV[2][0] = m_minUV6[2];
  m_minUV[2][1] = m_minUV5[2];
  m_minUV[2][2] = m_minUV4[2];
  m_minUV[3][0] = m_recover_minUV[0];
  m_minUV[3][1] = m_recover_minUV[1];
  m_minUV[3][2] = m_recover_minUV[2];

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<PrHybridSeeding::Track> PrHybridSeeding::operator()( const PrFTHitHandler<PrHit>& FTHitHandler ) const {
  // Containers
  TrackCandidates trackCandidates; //---LoH: probably could be changed to array(static_vector)
  trackCandidates[0].reserve( maxCandidates );
  trackCandidates[1].reserve( maxCandidates );
  XCandidates    xCandidates;    //---LoH: probably could be changed to static_vector
  TrackToRecover trackToRecover; //---LoH: probably could be changed to static_vector

  //==========================================================
  // Hits are ready to be processed
  //==========================================================
  PrFTHitHandler<ModPrHit> hitHandler = makeHitHandler( FTHitHandler );
  //========================================================
  //------------------MAIN SEQUENCE IS HERE-----------------
  //========================================================
  auto main_loop = [&]( auto icase ) {
    //----- Loop through lower and upper half
    for ( unsigned int part = 0; 2 > part; ++part ) {
      xCandidates[part].clear(); // x candidates up cleaned every Case!
      xCandidates[part].reserve( maxXCandidates );
      findXProjections( part, icase, hitHandler, xCandidates );
      if ( m_removeClonesX ) { removeClonesX( part, xCandidates ); }
      addStereo<icase> addStereoCase( *this, trackToRecover );
      addStereoCase( part, hitHandler, trackCandidates, xCandidates );
      // Flag found Hits at the end of each single case ( exclude the latest one )
      if ( ( icase + 1 < NCases ) ) flagHits( icase, part, trackCandidates, hitHandler );
    }
  };
  static_for<NCases>( main_loop );

  // Recovering step
  if ( m_recover ) {
    xCandidates[0].clear();
    xCandidates[1].clear();
    xCandidates[0].reserve( maxXCandidates );
    xCandidates[1].reserve( maxXCandidates );
    RecoverTrack( hitHandler, trackCandidates, xCandidates, trackToRecover );
  }

  // Clone removal ( up/down )
  for ( unsigned int part = 0; part < 2; part++ ) { removeClones( part, trackCandidates ); }

  std::vector<Track> result;
  result.reserve( trackCandidates[0].size() + trackCandidates[1].size() );
  // Convert LHCb tracks
  for ( unsigned int part = 0; part < 2; part++ ) { makeLHCbTracks( result, part, trackCandidates ); }
  return result;
}

//---LoH: Can be put outside of the classx
PrFTHitHandler<ModPrHit> PrHybridSeeding::makeHitHandler( const PrFTHitHandler<PrHit>& FTHitHandler ) const noexcept {
  // Construct hit handler of ModPrHits
  // The track candidates will contain copies of the ModHits, but each will contain their
  // own index in the hit container, which can be used used to flag the original hits.
  ModPrHits hits;
  hits.reserve( FTHitHandler.hits().size() );
  size_t i = 0;
  for ( const auto& hit : FTHitHandler.hits().range() ) {
    hits.emplace_back( &hit, 0, hit.planeCode(), i );
    ++i;
  }
  return {std::move( hits ), FTHitHandler.hits().offsets()};
}

//===========================================================================================================================
// EVERYTHING RELATED TO UV-HIT ADDITION
//===========================================================================================================================

template <int CASE>
template <int C>
PrHybridSeeding::addStereo<CASE>::addStereo( const PrHybridSeeding& hybridSeeding,
                                             typename std::enable_if<C == RecoverCase>::type* )
    : addStereoBase<true>(), m_hybridSeeding( hybridSeeding ) {}

template <int CASE>
template <int C>
PrHybridSeeding::addStereo<CASE>::addStereo( const PrHybridSeeding& hybridSeeding, TrackToRecover& trackToRecover,
                                             typename std::enable_if<C != RecoverCase>::type* )
    : addStereoBase<false>( trackToRecover ), m_hybridSeeding( hybridSeeding ) {}

template <int CASE>
void PrHybridSeeding::addStereo<CASE>::operator()( unsigned int part, const PrFTHitHandler<ModPrHit>& FTHitHandler,
                                                   TrackCandidates& trackCandidates, XCandidates& xCandidates ) const
    noexcept {
  // Initialise bounds
  BoundariesUV                                       Bounds;
  BoundariesUV                                       borderZones;
  std::array<std::array<std::array<float, 2>, 3>, 2> xMinPrev;
  initializeUVBounds( FTHitHandler, Bounds, borderZones, xMinPrev );

  PrLineFitterY BestLine;

  //---LoH: Loop on the xCandidates
  //---LoH: They are not sorted by xProje and their order can vary
  for ( auto&& itT : xCandidates[part] ) {
    if ( !itT.valid() ) continue;
    auto            hough = initializeHoughSearch( part, itT, borderZones, Bounds, xMinPrev );
    HoughCandidates houghCand;
    bool            hasAdded      = false;
    auto            lastCandidate = hough.search( houghCand.begin() );
    int             n_cand        = lastCandidate - houghCand.begin();
    if ( n_cand > 0 ) {
      // you have a minimal number of total hits to find on track dependent if standard 3 cases or from Recover routine
      BestLine.setXProj( itT );
      hasAdded = createTracksFromHough( trackCandidates[part], itT, BestLine, houghCand, n_cand );
    }
    if constexpr ( CASE != RecoverCase ) {
      if ( !hasAdded ) this->m_trackToRecover[part].push_back( itT );
    }
  }
}

//---LoH: Can be put outside of class
template <int CASE>
void PrHybridSeeding::addStereo<CASE>::initializeUVBounds(
    const PrFTHitHandler<ModPrHit>& FTHitHandler, BoundariesUV& Bounds, BoundariesUV& borderZones,
    std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const noexcept {
  for ( const unsigned int& layer : {T1U, T2U, T3U, T1V, T2V, T3V} ) {
    auto uv       = ( ( layer + 1 ) / 2 ) % 2;
    auto iStation = layer / 8;
    for ( unsigned int iPart = 0; iPart < 2; iPart++ ) {
      // Fill the UV bounds
      Bounds[uv][iStation][iPart]      = InitializeBB( layer - iPart, FTHitHandler );
      auto r                           = FTHitHandler.hits( layer - iPart );
      borderZones[uv][iStation][iPart] = {r.begin(), r.end()};
      xMinPrev[uv][iStation][iPart]    = std::numeric_limits<float>::lowest();
    }
  }
}

//---LoH: Can be put outside of class if takes the seeding as argument.
template <int CASE>
PrHybridSeeding::HoughSearch PrHybridSeeding::addStereo<CASE>::initializeHoughSearch(
    unsigned int part, const Pr::Hybrid::SeedTrackX& xProje, const BoundariesUV& borderZones, BoundariesUV& Bounds,
    std::array<std::array<std::array<float, 2>, 3>, 2>& xMinPrev ) const noexcept {

  auto calculate_factor = [this, shift = 0.,
                           partSign = float( 1 - static_cast<int>( part ) * 2 )]( int layer ) -> float {
    auto UV = layer / 4 % 2;
    return partSign / ( ( m_hybridSeeding.m_z[layer] - shift ) * m_hybridSeeding.m_dxDy[UV] );
  };
  auto calculate_factors = [&]( auto... layers ) { return std::array{calculate_factor( layers )...}; };

  auto hough = HoughSearch{2,
                           -m_hybridSeeding.m_YSlopeBinWidth[CASE] / 2,
                           m_hybridSeeding.m_YSlopeBinWidth[CASE],
                           {calculate_factors( T1U, T1V, T2U, T2V, T3U, T3V )}};
  // u-layers
  for ( const unsigned int& layer : {T1U, T2U, T3U} )
    this->CollectLayerUV<0>( part, layer, xProje, borderZones[0], Bounds[0], xMinPrev[0], hough );
  // v-layers
  for ( const unsigned int& layer : {T1V, T2V, T3V} )
    this->CollectLayerUV<1>( part, layer, xProje, borderZones[1], Bounds[1], xMinPrev[1], hough );
  return hough;
}

template <int CASE>
template <int UV> // 0 if U, 1 if V
void PrHybridSeeding::addStereo<CASE>::CollectLayerUV( unsigned int part, unsigned int layer,
                                                       const Pr::Hybrid::SeedTrackX& xProje,
                                                       const BoundaryUV& borderZones, BoundaryUV& Bounds,
                                                       std::array<std::array<float, 2>, 3>& xMinPrev,
                                                       HoughSearch&                         hough ) const noexcept {
  static_assert( UV == 0 || UV == 1, "UV must be 0 (U) or 1 (V)!" );
  float        zPlane   = m_hybridSeeding.m_z[layer];
  float        dxDy     = m_hybridSeeding.m_dxDy[UV]; // 0 if U, 1 if V
  float        xPred    = xProje.x( zPlane );
  unsigned int iStation = layer / 8;
  unsigned int iLayer   = layer / 4;

  // Part 0
  std::array<float, 2> yMinMax{m_hybridSeeding.m_yMins[part][0], m_hybridSeeding.m_yMaxs[part][0]};
  // The 1 - UV expression will be optmized away by the compiler (const expression)
  std::array<float, 2> xMinMax{xPred - yMinMax[1 - UV] * dxDy, xPred - yMinMax[UV] * dxDy};
  LookAroundMin( Bounds[iStation][0], xMinPrev[iStation][0], xMinMax[0], borderZones[iStation][0].end,
                 borderZones[iStation][0].begin );
  Bounds[iStation][0].end = get_upperBound_lin( Bounds[iStation][0].begin, borderZones[iStation][0].end, xMinMax[1] );
  if constexpr ( UV == 0 ) {
    for ( auto itH = Bounds[iStation][0].end - 1; itH >= Bounds[iStation][0].begin; --itH )
      AddUVHit( iLayer, itH, xPred, hough );
  } else {
    for ( auto itH = Bounds[iStation][0].begin; itH != Bounds[iStation][0].end; ++itH )
      AddUVHit( iLayer, itH, xPred, hough );
  }
  // Part 1
  yMinMax = {m_hybridSeeding.m_yMins[part][1], m_hybridSeeding.m_yMaxs[part][1]};
  // The 1 - UV expression will be optmized away by the compiler (const expression)
  xMinMax = {xPred - yMinMax[1 - UV] * dxDy, xPred - yMinMax[UV] * dxDy};
  LookAroundMin( Bounds[iStation][1], xMinPrev[iStation][1], xMinMax[0], borderZones[iStation][1].end,
                 borderZones[iStation][1].begin );
  for ( Bounds[iStation][1].end = Bounds[iStation][1].begin; Bounds[iStation][1].end != borderZones[iStation][1].end;
        Bounds[iStation][1].end++ ) {
    if ( Bounds[iStation][1].end->hit->x() > xMinMax[1] ) break;
    AddUVHit( iLayer, Bounds[iStation][1].end, xPred, hough );
  }
}

template <int CASE>
void PrHybridSeeding::addStereo<CASE>::AddUVHit( unsigned int layer, const ModPrHitConstIter& it, const float& xPred,
                                                 HoughSearch& hough ) const noexcept {
  if ( !it->isValid() ) return;
  const PrHit* hit = it->hit;
  float        y   = xPred - hit->x();
  hough.add( layer, y, &( *it ) );
}

template <int CASE>
bool PrHybridSeeding::addStereo<CASE>::createTracksFromHough( std::vector<Pr::Hybrid::SeedTrack>& trackCandidates,
                                                              const Pr::Hybrid::SeedTrackX&       xCandidate,
                                                              PrLineFitterY& BestLine, HoughCandidates& houghCand,
                                                              size_t nHoughCand ) const noexcept {
  constexpr int      LOW_NTOT_YSEL_TH = CASE == 2 ? 10 : 11;
  constexpr int      LOW_NTOT_TH      = 11;
  constexpr int      LOW_NUV_TH       = CASE == 2 ? 5 : 6;
  const unsigned int nXHits           = xCandidate.size();
  const unsigned int minUV            = m_hybridSeeding.m_minUV[CASE][6 - nXHits];
  unsigned int       nValidCandidates = 0;
  unsigned int       nTarget          = m_hybridSeeding.m_minTot[CASE];
  float              chi2Target       = m_hybridSeeding.m_maxChi2PerDofFullLow[CASE];
  for ( size_t iCand = 0; iCand < nHoughCand; ++iCand ) {
    auto& hcand = houghCand[iCand];
    auto  itBeg = hcand.begin();
    auto  itEnd = hcand.end();
    while ( ( itEnd - itBeg ) > 4 && *( itEnd - 1 ) == nullptr ) --itEnd;
    if ( *( itEnd - 1 ) == nullptr ) continue;

    unsigned int nUVHits = itEnd - itBeg;
    unsigned int nHits   = nXHits + nUVHits;

    if ( nHits < nTarget || nUVHits < minUV ) continue;

    bool fitY = BestLine.fit( itBeg, itEnd );
    while ( fitY ) {
      if ( !removeWorstY( BestLine, itBeg, itEnd ) ) break;
      --nHits;
      --nUVHits;
      if ( nHits < nTarget || nUVHits < minUV ) {
        fitY = false;
        break;
      } else {
        fitY = BestLine.fit( itBeg, itEnd );
      }
    }

    if ( !fitY ) continue;

    if ( ( nUVHits >= LOW_NUV_TH && BestLine.Chi2DoF() > m_hybridSeeding.m_maxChi2PerDofYHigh[CASE] ) ||
         ( nUVHits < LOW_NUV_TH && BestLine.Chi2DoF() > m_hybridSeeding.m_maxChi2PerDofYLow[CASE] ) )
      continue;

    Pr::Hybrid::SeedTrack trackCand;
    trackCand.setFromXCand( xCandidate );
    for ( auto hit = itBeg; hit != itEnd; ++hit ) trackCand.hits().push_back( *( *hit ) );
    trackCand.setYParam( BestLine.ay(), BestLine.by() );

    float worstHitChi2 = 0;
    auto  worstHit     = trackCand.hits().cend();
    bool  fitXY        = fitSimultaneouslyXY( trackCand, worstHitChi2, worstHit );
    bool  canRemoveHit = nHits > nTarget && nUVHits > m_hybridSeeding.m_minUV[CASE][6 - nXHits];
    while ( fitXY && ( worstHitChi2 >= m_hybridSeeding.m_minChi2HitFullRemove[CASE] ||
                       trackCand.chi2PerDoF() >= m_hybridSeeding.m_minChi2PerDofFullRemove[CASE] ) ) {
      if ( !canRemoveHit ) {
        fitXY = false;
        break;
      }
      if ( !worstHit->hit->isX() ) --nUVHits;
      --nHits;
      trackCand.hits().erase( worstHit );
      worstHitChi2 = 0;
      worstHit     = trackCand.hits().cend();
      fitXY        = fitSimultaneouslyXY( trackCand, worstHitChi2, worstHit );
      canRemoveHit = nHits > nTarget && nUVHits > m_hybridSeeding.m_minUV[CASE][6 - nHits + nUVHits];
    }

    if ( !fitXY ) continue;

    if ( nHits >= LOW_NTOT_TH && nHits > nTarget ) chi2Target = m_hybridSeeding.m_maxChi2PerDofFullHigh[CASE];
    if ( trackCand.chi2PerDoF() > chi2Target ) continue;

    float absY0   = std::fabs( trackCand.y0() );
    float absYref = std::fabs( trackCand.yRef() );
    if ( nHits < LOW_NTOT_YSEL_TH ) {
      if ( absY0 >= m_hybridSeeding.m_maxYAt0Low[CASE] || absYref >= m_hybridSeeding.m_maxYAtRefLow[CASE] ||
           absYref < m_hybridSeeding.m_minYAtRefLow[CASE] )
        continue;
    } else {
      if ( absY0 >= m_hybridSeeding.m_maxYAt0High[CASE] || absYref >= m_hybridSeeding.m_maxYAtRefHigh[CASE] ||
           absYref < m_hybridSeeding.m_minYAtRefHigh[CASE] )
        continue;
    }

    trackCand.setnXnY( nHits - nUVHits, nUVHits );
    if ( nValidCandidates == 0 ) {
      trackCandidates.push_back( trackCand );
    } else {
      trackCandidates.back() = std::move( trackCand );
    }
    ++nValidCandidates;
    nTarget    = trackCandidates.back().size();
    chi2Target = trackCandidates.back().chi2PerDoF();
  }
  return ( nValidCandidates != 0 );
}

template <int CASE>
template <typename T>
bool PrHybridSeeding::addStereo<CASE>::removeWorstY( PrLineFitterY& BestLine, T& hitBegin, T& hitEnd ) const noexcept {
  std::array<float, 6> chi2hits;
  for ( int i = 0; i < ( hitEnd - hitBegin ); ++i ) chi2hits[i] = BestLine.chi2hit( **( hitBegin + i ) );
  const auto worst_chi2 = std::max_element( chi2hits.begin(), chi2hits.begin() + ( hitEnd - hitBegin ) );
  if ( *worst_chi2 < m_hybridSeeding.m_minChi2HitYRemove[CASE] &&
       BestLine.Chi2DoF() < m_hybridSeeding.m_minChi2PerDofYRemove[CASE] )
    return false;
  const auto worst = hitBegin + ( worst_chi2 - chi2hits.begin() );
  --hitEnd;
  std::copy( worst + 1, hitEnd + 1, worst );
  *hitEnd = nullptr;
  return true;
}

template <int CASE>
bool PrHybridSeeding::addStereo<CASE>::fitTrackSimultaneouslyXY( Pr::Hybrid::SeedTrack& track, float sumw ) const
    noexcept {
  std::array<float, 5>  rhs{0};
  std::array<float, 15> mat{sumw, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  for ( const auto& modHit : track.hits() ) {
    const PrHit& hit      = *( modHit.hit );
    const float  w        = hit.w();
    const float  dxdy     = hit.dxDy();
    const float  yOnTrack = track.yOnTrack( hit );
    const float  dz       = 0.001f * ( hit.z( yOnTrack ) - Pr::Hybrid::zReference );
    const float  deta     = dz * dz * ( 0.001f + dz * track.dRatio() );
    const float  wdz      = w * dz;
    const float  weta     = w * deta;
    const float  wdxdy    = w * dxdy;
    const float  wdxdydz  = wdxdy * dz;
    const float  dist     = track.distance( hit );
    mat[1] += wdz;
    mat[2] += wdz * dz;
    mat[3] += weta;
    mat[4] += weta * dz;
    mat[5] += weta * deta;
    mat[6] -= wdxdy;
    mat[7] -= wdxdydz;
    mat[8] -= wdxdy * deta;
    mat[9] += wdxdy * dxdy;
    mat[10] -= wdxdydz;
    mat[11] -= wdxdydz * dz;
    mat[12] -= wdxdydz * deta;
    mat[13] += wdxdydz * dxdy;
    mat[14] += wdxdydz * dz * dxdy;
    // fill right hand side
    rhs[0] += w * dist;
    rhs[1] += wdz * dist;
    rhs[2] += weta * dist;
    rhs[3] -= wdxdy * dist;
    rhs[4] -= wdxdydz * dist;
  } // Loop over Hits to fill the matrix
  // decompose matrix, protect against numerical trouble
  ROOT::Math::CholeskyDecomp<float, 5> decomp( mat.data() );
  if ( !decomp ) return false;
  decomp.Solve( rhs );
  rhs[1] *= 1.e-3f;
  rhs[2] *= 1.e-9f;
  rhs[4] *= 1.e-3f;
  rhs[3] -= rhs[4] * Pr::Hybrid::zReference;
  track.updateParameters( rhs[0], rhs[1], rhs[2], rhs[3], rhs[4] );
  return true;
}

//=========================================================================
//  Fit the track, return OK if fit sucecssfull
//=========================================================================
template <int CASE>
bool PrHybridSeeding::addStereo<CASE>::fitSimultaneouslyXY( Pr::Hybrid::SeedTrack& track, float& maxChi2,
                                                            SeedTrackHits::const_iterator& worst ) const noexcept {
  float sumw = 0.f;
  for ( const auto& modHit : track.hits() ) sumw += modHit.hit->w();

  constexpr int loopMax = CASE == 0 ? 2 : 3;

  for ( int loop = 0; loop < loopMax; ++loop ) {
    if ( loop == 1 ) {
      float RadiusPosition =
          std::sqrt( ( 5.f * 5.f * 1.e-8f * track.ax() * track.ax() + 1e-6f * track.yRef() * track.yRef() ) );
      float dRatioPos = -( m_hybridSeeding.m_dRatioPar[0] + m_hybridSeeding.m_dRatioPar[1] * RadiusPosition +
                           m_hybridSeeding.m_dRatioPar[2] * RadiusPosition * RadiusPosition );
      track.setdRatio( dRatioPos );
    }

    bool fitOk = fitTrackSimultaneouslyXY( track, sumw );
    if ( !fitOk ) return false;
  }

  float sumChi2 = 0;
  maxChi2       = std::numeric_limits<float>::min();
  for ( SeedTrackHits::const_iterator iterH = track.hits().begin(); iterH != track.hits().end(); ++iterH ) {
    const auto& itH        = *iterH;
    float       chi2_onHit = track.chi2( *( itH.hit ) );
    sumChi2 += chi2_onHit;
    if ( chi2_onHit > maxChi2 ) {
      maxChi2 = chi2_onHit;
      worst   = iterH;
    }
  }
  track.setChi2( sumChi2, track.hits().size() - 5 );

  return true;
}

void PrHybridSeeding::RecoverTrack( PrFTHitHandler<ModPrHit>& FTHitHandler, TrackCandidates& trackCandidates,
                                    XCandidates& xCandidates, TrackToRecover& trackToRecover ) const noexcept {
  // Flagging all hits in the track candidates
  for ( int part = 0; part < 2; part++ ) {
    for ( auto&& itT1 : trackCandidates[part] ) {
      if ( !itT1.valid() ) continue;
      for ( auto&& hit : itT1.hits() ) { FTHitHandler.hit( hit.hitIndex ).setInvalid(); }
    }
  }

  for ( unsigned int part = 0; part < 2; part++ ) {
    for ( auto&& itT1 : trackToRecover[part] ) {
      int nUsed_threshold = m_nusedthreshold[6 - itT1.size()]; // LoH
      //---LoH: this should be asserted: size must be 4, 5 or 6
      //---LoH: this checks if we have enough valid hits.
      int nUsedHits = nUsed( itT1.hits().begin(), itT1.hits().end(), FTHitHandler );
      if ( nUsedHits < nUsed_threshold ) {
        if ( nUsedHits > 0 ) {
          while ( nUsedHits > 0 ) {
            auto it = std::remove_if( itT1.hits().begin(), itT1.hits().end(), [&FTHitHandler]( const ModPrHit& hit ) {
              return !FTHitHandler.hit( hit.hitIndex ).isValid();
            } );
            auto n  = std::distance( it, itT1.hits().end() );
            itT1.hits().erase( it, itT1.hits().end() );
            nUsedHits -= n;
          }
          if ( itT1.hits().size() < m_minXPlanes ) continue;
          if ( hasT1T2T3Track( itT1.hits() ) ) continue;
          fitXProjection( itT1, 2 );
        }
        itT1.setRecovered( true );
      }
    }
    removeClonesRecover( part, trackToRecover, xCandidates );
    addStereo<RecoverCase> addStereoReco( *this );
    addStereoReco( part, FTHitHandler, trackCandidates, xCandidates );
  }
}

void PrHybridSeeding::flagHits( unsigned int icase, unsigned int part, TrackCandidates& trackCandidates,
                                PrFTHitHandler<ModPrHit>& hitHandler ) const noexcept {
  // The hits in the track candidates are copies, but they each contain their own index
  // in the original hit container, which is used to flag the original hits.
  for ( auto& track : trackCandidates[part] ) {
    if ( !track.valid() ) continue;
    if ( ( track.size() == 12 ) || ( ( track.size() == 11 && track.chi2PerDoF() < m_MaxChi2Flag[icase] &&
                                       std::fabs( track.ax() - track.bx() * Pr::Hybrid::zReference +
                                                  track.cx() * m_ConstC ) < m_MaxX0Flag[icase] ) ) )
      for ( const auto& hit : track.hits() ) {
        if ( static_cast<int>( part ) == hit.hit->zone() % 2 ) continue; // hit.hit->zone() is signed int
        hitHandler.hit( hit.hitIndex ).setInvalid();
      }
  }
}

//=========================================================================
//  Convert to LHCb tracks
//=========================================================================
//---LoH: there are some things to understand, such as:
// - does the scaleFactor or the momentumScale change? If not, momentumScale/(-scaleFactor) is much better.
void PrHybridSeeding::makeLHCbTracks( std::vector<Track>& result, unsigned int part,
                                      const TrackCandidates& trackCandidates ) const noexcept {
  for ( const auto& track : trackCandidates[part] ) {
    if ( !track.valid() ) continue;
    //***** EXACT SAME IMPLEMENTATION OF PatSeedingTool *****//
    auto& out = result.emplace_back();
    out.setType( Track::Type::Ttrack );
    out.setHistory( Track::History::PrSeeding );
    out.setPatRecStatus( Track::PatRecStatus::PatRecIDs );
    for ( const auto& hit : track.hits() ) { out.addToLhcbIDs( hit.hit->id() ); }
    LHCb::State temp( Gaudi::TrackVector( track.x0(), track.yRef(), track.xSlope0(), //---LoH: can be simplified into
                                                                                     // ax(), ay()
                                          track.ySlope(), 0.f ),
                      Pr::Hybrid::zReference, LHCb::State::Location::AtT );
    double      qOverP, sigmaQOverP;
    const float scaleFactor = m_magFieldSvc->signedRelativeCurrent();
    if ( m_momentumTool->calculate( &temp, qOverP, sigmaQOverP, true ).isFailure() ) {
      if ( std::abs( scaleFactor ) < 1.e-4f ) {
        qOverP      = ( ( track.cx() < 0 ) ? -1.f : 1.f ) * ( ( scaleFactor < 0.f ) ? -1.f : 1.f ) / Gaudi::Units::GeV;
        sigmaQOverP = 1.f / Gaudi::Units::MeV;
      } else {
        qOverP      = track.cx() * m_momentumScale / ( -1.f * scaleFactor );
        sigmaQOverP = 0.5f * qOverP;
      }
    }
    temp.setQOverP( qOverP );
    Gaudi::TrackSymMatrix& cov = temp.covariance();
    cov( 0, 0 )                = m_stateErrorX2;
    cov( 1, 1 )                = m_stateErrorY2;
    cov( 2, 2 )                = m_stateErrorTX2;
    cov( 3, 3 )                = m_stateErrorTY2;
    cov( 4, 4 )                = sigmaQOverP * sigmaQOverP;
    for ( const float z : m_zOutputs ) {
      temp.setX( track.x( z ) );
      temp.setY( track.y( z ) );
      temp.setZ( z );
      temp.setTx( track.xSlope( z ) );
      temp.setTy( track.ySlope() );
      out.addToStates( temp );
    }
    out.setChi2PerDoF( {track.chi2PerDoF(), static_cast<int>( track.hits().size() ) - 5} );
  }
}

//=======================================
// Fit Only X Projection
//=======================================
bool PrHybridSeeding::fitXProjection( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept {
  //---LoH: called 30k times
  //  if ( track.size() < m_minXPlanes ) return false;
  float mat[6];
  std::fill( mat, mat + 6, 0.f );
  float rhs[3];
  std::fill( rhs, rhs + 3, 0.f );
  const float dRatio = track.dRatio();
  for ( const auto& modHit : track.hits() ) {
    const PrHit& hit  = *( modHit.hit );
    float        w    = hit.w(); // squared
    const float  dz   = 0.001f * ( hit.z() - Pr::Hybrid::zReference );
    const float  deta = dz * dz * ( 0.001f + dRatio * dz );
    const float  dist = track.distance( hit );
    mat[0] += w;
    mat[1] += w * dz;
    mat[2] += w * dz * dz;
    mat[3] += w * deta;
    mat[4] += w * dz * deta;
    mat[5] += w * deta * deta;
    rhs[0] += w * dist;
    rhs[1] += w * dist * dz;
    rhs[2] += w * dist * deta;
  }
  ROOT::Math::CholeskyDecomp<float, 3> decomp( mat ); //---LoH: can probably be made more rapidly
  if ( !decomp ) { return false; }
  // Solve linear system
  decomp.Solve( rhs );
  rhs[1] *= 1.e-3f;
  rhs[2] *= 1.e-9f;
  // protect against unreasonable track parameter corrections
  //---LoH: commented as it slows down more that accelerates things
  //    if ( std::fabs( rhs[0] ) > 1.e4f || std::fabs( rhs[1] ) > 5.f || std::fabs( rhs[2] ) > 1.e-3f ) return false;
  // Small corrections
  track.updateParameters( rhs[0], rhs[1], rhs[2] );
  // Compute some values on the track
  auto       hitChi2 = [&track]( const ModPrHit& hit ) { return track.chi2( *hit.hit ); };
  const auto chi2s   = track.hits() | ranges::views::transform( hitChi2 );
  float      sum( 0.f );
  for ( auto chi2 : chi2s ) sum += chi2;
  track.setChi2( sum, track.hits().size() - 3 );
  return ranges::max( chi2s ) < m_maxChi2HitsX[iCase];
}

//=========================================================================
//  Remove the worst hit and refit.
//=========================================================================
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::removeWorstAndRefitX( Pr::Hybrid::SeedTrackX& track, unsigned int iCase ) const noexcept {
  track.hits().erase(
      std::min_element( track.hits().begin(), track.hits().end(), [&track]( ModPrHit& lhs, ModPrHit& rhs ) {
        return track.chi2( *( lhs.hit ) ) > track.chi2( *( rhs.hit ) );
      } ) );
  return fitXProjection( track, iCase );
}

//=========================================================================
//  Finding the X projections. This is the most time-consuming part of the algorithm
//=========================================================================

void PrHybridSeeding::findXProjections( unsigned int part, unsigned int iCase,
                                        const PrFTHitHandler<ModPrHit>& FTHitHandler, XCandidates& xCandidates ) const
    noexcept {
  ZonesXSearch xZones = {0.f, 0.f, {}, {}, {}, {}, {}}; // zones, planeCodes, z, dz, dz2
  initializeXProjections( iCase, part, xZones );

  float slope = ( m_tolAtX0Cut[iCase] - m_tolX0SameSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange[iCase] );
  float slopeopp =
      ( m_tolAtx0CutOppSign[iCase] - m_tolX0OppSign[iCase] ) / ( m_x0Cut[iCase] - m_x0SlopeChange2[iCase] );
  float accTerm1 = slope * m_x0SlopeChange[iCase] - m_tolX0SameSign[iCase];
  float accTerm2 = slopeopp * m_x0SlopeChange2[iCase] - m_tolX0OppSign[iCase];

  //======================================================================================================================================================
  ParabolaParams solver0;
  ParabolaParams solver1;
  buildParabola( xZones, 2, solver0 );
  buildParabola( xZones, 3, solver1 );
  Boundaries Bounds;
  Boundaries svgBounds;
  //============Initialization
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> begZones;
  std::array<HitIter, PrFTInfo::Numbers::NFTXLayers> endZones;
  // Always: first=0 last=1 middles=2,3 remains=4,5 (order of loops)
  for ( unsigned int i = 0; i < PrFTInfo::Numbers::NFTXLayers; i++ ) {
    Bounds[i]    = InitializeBB( xZones.zones[i], FTHitHandler );
    svgBounds[i] = InitializeBB( xZones.zones[i], FTHitHandler );
    auto r       = FTHitHandler.hits( xZones.zones[i] );
    begZones[i]  = r.begin();
    endZones[i]  = r.end();
  }
  for ( unsigned int i = 1; i < PrFTInfo::Numbers::NFTXLayers; i++ ) {
    Bounds[i].end    = Bounds[i].begin;
    svgBounds[i].end = svgBounds[i].begin;
  }
  ModPrHitConstIter Fhit;
  ModPrHitConstIter Lhit;
  // Used to cache the position to do a "look-around search"
  std::array<float, PrFTInfo::Numbers::NFTXLayers> xminPrev;
  xminPrev.fill( std::numeric_limits<float>::lowest() );

  std::array<std::vector<ModPrHit>, 3> parabolaSeedHits; // 0: layer T2x1, 1: layer T2x2, 2: extrapolated T2x2
  parabolaSeedHits[0].reserve( 100 );
  parabolaSeedHits[1].reserve( 100 );
  parabolaSeedHits[2].reserve( 100 );
  bool              OK = false;
  bool              first;
  TwoHitCombination hitComb;
  float             tolHp = m_TolFirstLast[iCase];
  for ( ; Bounds[0].begin != Bounds[0].end; ++Bounds[0].begin ) // for a hit in first layer
  {
    Fhit = Bounds[0].begin;
    if ( !Fhit->isValid() ) continue;
    const auto* fHit      = Fhit->hit;
    float       xFirst    = fHit->x();
    float       tx_inf    = xFirst * xZones.invZf;
    float       xProjeInf = tx_inf * xZones.zLays[1];
    float       maxXl     = xProjeInf + tx_inf * m_alphaCorrection[iCase] + tolHp;
    float       minXl     = maxXl - 2 * tolHp;
    // Setting the last-layer bounds
    Bounds[1].begin = get_lowerBound_lin( Bounds[1].begin, endZones[1], minXl ); //---LoH: should be very small
    Bounds[1].end   = get_upperBound_lin( Bounds[1].end, endZones[1], maxXl ); //---LoH: between 6 and 20 times roughly
    // Reinitialise
    Bounds[2].begin = svgBounds[2].begin;
    Bounds[3].begin = svgBounds[3].begin;
    first           = true;
    for ( Lhit = Bounds[1].begin; Lhit != Bounds[1].end; ++Lhit ) { // for a hit in last layer
      if ( !Lhit->isValid() ) continue;
      //---LoH: For 100 events, this is ran 1.6M times (number of updateXZCombination)
      float xLast = Lhit->hit->x();
      updateXZCombinationPars( iCase, xFirst, xLast, xZones, slope, slopeopp, accTerm1, accTerm2,
                               hitComb ); // New parameters
      parabolaSeedHits[0].clear();
      parabolaSeedHits[1].clear();
      parabolaSeedHits[2].clear();
      // Look for parabola hits and update bounds
      OK = findParabolaHits( 2, xZones, hitComb, endZones, Bounds, parabolaSeedHits[0] );
      OK += findParabolaHits( 3, xZones, hitComb, endZones, Bounds, parabolaSeedHits[1] );
      if ( first ) {
        svgBounds[2].begin = Bounds[2].begin;
        svgBounds[3].begin = Bounds[3].begin;
        first              = false;
      }
      if ( !OK ) continue;
      updateParabola( xFirst, xLast, solver0 );
      updateParabola( xFirst, xLast, solver1 );
      // First parabola
      for ( unsigned int i = 0; i < parabolaSeedHits[0].size(); ++i )
        fillXhits0( iCase, part, Fhit, parabolaSeedHits[0][i], Lhit, xZones, begZones, endZones, xCandidates,
                    parabolaSeedHits[2], Bounds, xminPrev, solver0 );

      //---LoH: Remove hits in T2x2 that were already built in T2x1 extension. Happens 42k times in 100 events.
      for ( auto hit1 : parabolaSeedHits[2] ) {
        auto pos = std::find( parabolaSeedHits[1].begin(), parabolaSeedHits[1].end(), hit1 );
        if ( pos != parabolaSeedHits[1].end() ) { parabolaSeedHits[1].erase( pos ); }
      }
      // Second parabola
      for ( unsigned int i = 0; i < parabolaSeedHits[1].size(); ++i )
        fillXhits1( iCase, part, Fhit, parabolaSeedHits[1][i], Lhit, xZones, begZones, endZones, xCandidates, Bounds,
                    xminPrev, solver1 );
    }
  } // end loop first zone
}

// Builds the parabola solver from solely z information
//---LoH: Can be put outside of the class if m_dRatio is externalised
void PrHybridSeeding::buildParabola( const ZonesXSearch& xZone, unsigned int iLayer, ParabolaParams& pars ) const
    noexcept {
  pars.z1 = xZone.dzLays[0];
  pars.z2 = xZone.dzLays[iLayer];
  pars.z3 = xZone.dzLays[1];

  pars.corrZ1 = ( 1.f + m_dRatio * pars.z1 ) * pars.z1 * pars.z1;
  pars.corrZ2 = ( 1.f + m_dRatio * pars.z2 ) * pars.z2 * pars.z2;
  pars.corrZ3 = ( 1.f + m_dRatio * pars.z3 ) * pars.z3 * pars.z3;
  pars.det    = pars.z1 * ( pars.corrZ3 - pars.corrZ2 ) + pars.z2 * ( pars.corrZ1 - pars.corrZ3 ) +
             pars.z3 * ( pars.corrZ2 - pars.corrZ1 );
  pars.recdet = 1.f / pars.det;

  pars.dz13     = pars.z1 - pars.z3;
  pars.dcorr13  = pars.corrZ1 - pars.corrZ3;
  pars.dzcorr13 = pars.z1 * pars.corrZ3 - pars.z3 * pars.corrZ1;
}

// Updates the parabola solver with two-hit information
//---LoH: Can be put outside of the class.
void PrHybridSeeding::updateParabola( const float& x1, const float& x3, ParabolaParams& pars ) const noexcept {
  pars.det1 = x1 * ( pars.z2 - pars.z3 ) + x3 * ( pars.z1 - pars.z2 );
  pars.det2 = x1 * ( pars.corrZ3 - pars.corrZ2 ) + x3 * ( pars.corrZ2 - pars.corrZ1 );
  pars.det3 = pars.corrZ1 * pars.z2 * x3 + pars.corrZ2 * ( pars.z3 * x1 - x3 * pars.z1 ) - pars.corrZ3 * pars.z2 * x1;
}

// Solves the parabola
//---LoH: Can be put outside of the class.
void PrHybridSeeding::solveParabola( const ParabolaParams& pars, const float& x2, float& a1, float& b1,
                                     float& c1 ) const noexcept {
  const float det1 = pars.det1 - x2 * pars.dz13;
  const float det2 = pars.det2 + x2 * pars.dcorr13;
  const float det3 = pars.det3 + x2 * pars.dzcorr13;

  a1 = pars.recdet * det1;
  b1 = pars.recdet * det2;
  c1 = pars.recdet * det3;
}

// Fill hits due to parabola seeds in T2x1
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits0( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, std::vector<ModPrHit>& parabolaSeedHits, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev,
                                  ParabolaParams&                                   Pars ) const noexcept {
  float          a( 0.f ), b( 0.f ), c( 0.f );
  SeedTrackHitsX xHits;
  solveParabola( Pars, Phit.hit->x(), a, b, c ); // Extrapolation with dRatio
  if ( fillXhitParabola( iCase, 3, xHits, a * xZones.dz2Lays[3] + b * xZones.dzLays[3] + c, Bounds ) )
    parabolaSeedHits.push_back( xHits[0] );
  fillXhitRemaining( iCase, 4, a * xZones.dz2Lays[4] + b * xZones.dzLays[4] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );   //---Called 767k times in total
  if ( xHits.size() == 0 ) return; //---LoH: ~650k times out of 750k
  // Add the last hit
  fillXhitRemaining( iCase, 5, a * xZones.dz2Lays[5] + b * xZones.dzLays[5] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );  // Missing T3x
  if ( xHits.size() < 2 ) return; //---LoH: ~50k times out of 750k
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x1
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits ); //---LoH: called 27k times
  return;
}

// Fill hits due to parabola seeds in T2x2
//---LoH: Can be put outside of the class.
//---LoH: Can be templated
void PrHybridSeeding::fillXhits1( unsigned int iCase, unsigned int part, const ModPrHitConstIter& Fhit,
                                  const ModPrHit& Phit, const ModPrHitConstIter& Lhit, const ZonesXSearch& xZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                  const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                  XCandidates& xCandidates, Boundaries& Bounds,
                                  std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev,
                                  ParabolaParams&                                   Pars ) const noexcept {
  float          a( 0.f ), b( 0.f ), c( 0.f );
  SeedTrackHitsX xHits;
  solveParabola( Pars, Phit.hit->x(), a, b, c ); // Extrapolation with dRatio
  fillXhitParabola( iCase, 2, xHits, a * xZones.dz2Lays[2] + b * xZones.dzLays[2] + c, Bounds );
  fillXhitRemaining( iCase, 4, a * xZones.dz2Lays[4] + b * xZones.dzLays[4] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );   // Missing T1x
  if ( xHits.size() == 0 ) return; //---LoH: ~650k times out of 750k
  // Add the last hit
  fillXhitRemaining( iCase, 5, a * xZones.dz2Lays[5] + b * xZones.dzLays[5] + c, begZones, endZones, xHits, Bounds,
                     xminPrev );  // Missing T3x
  if ( xHits.size() < 2 ) return; //---LoH: ~50k times out of 750k
  xHits.push_back( *Fhit );
  xHits.push_back( Phit ); // T2x1
  xHits.push_back( *Lhit );
  createXTrack( iCase, part, xCandidates, xHits );
}

void PrHybridSeeding::createXTrack( const unsigned int& iCase, const unsigned int& part, XCandidates& xCandidates,
                                    SeedTrackHitsX& xHits ) const noexcept {
  //---LoH: called 27k times
  //------    Create the track
  Pr::Hybrid::SeedTrackX xCand( xHits );
  //------    Set the dRatio value for the track here
  xCand.setdRatio( m_dRatio );
  //------    Algorithm allows to go down to 4 hits only from 6 hits track refit by construction.
  //-----     OK is the status of the fit
  bool OK = fitXProjection( xCand, iCase );
  while ( !OK ) {
    if ( xHits.size() != 6 || xCand.size() <= m_minXPlanes ) { break; }
    OK = removeWorstAndRefitX( xCand, iCase );
  }
  if ( !OK ) return;
  //---- Fit is successful and  make a x/z-candidate
  if ( ( ( xCand.chi2PerDoF() < m_maxChi2DoFX[iCase] ) ) ) {
    xCand.setXT1( xCand.x( StateParameters::ZBegT ) );
    xCand.setXT2( xCand.x0() ); //---LoH: it is zReference
    xCand.setXT3( xCand.x( StateParameters::ZEndT ) );
    xCandidates[part].push_back( xCand ); // The X Candidate is created
  }
}

bool PrHybridSeeding::fillXhitParabola( const int& iCase, const int& iLayer, SeedTrackHitsX& xHits, const float& xAtZ,
                                        const std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>& Bounds ) const
    noexcept {
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase]; // * (1. + dz*invZref );
  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
  ModPrHitConstIter bestProj = Bounds[iLayer].end;
  float             bestDist = m_tolRemaining[iCase]; // 1 cm around predicted position!!
  const PrHit*      hit;
  float             tmpDist;
  for ( auto itH = Bounds[iLayer].begin; itH != Bounds[iLayer].end; ++itH ) {
    if ( !itH->isValid() ) { continue; }
    hit = itH->hit;
    if ( hit->x() < xMinAtZ ) { continue; }
    tmpDist = std::fabs( hit->x() - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = itH;
    } else
      break;
  }
  // Can happen if there is only one hit in the loop.
  if ( bestProj != Bounds[iLayer].end ) {
    xHits.push_back( *bestProj );
    return true;
  }
  return false;
}

void PrHybridSeeding::fillXhitRemaining( const int& iCase, const int& iLayer, const float& xAtZ,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& begZones,
                                         const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                         SeedTrackHitsX&                                           xHits,
                                         std::array<IterPairs, PrFTInfo::Numbers::NFTXLayers>&     Bounds,
                                         std::array<float, PrFTInfo::Numbers::NFTXLayers>& xminPrev ) const noexcept {
  // Prediction of the position with the cubic correction!
  float xMinAtZ = xAtZ - m_tolRemaining[iCase];
  // Parabola here is larger than tolerances in x0! ( 0.75 mm up / down when x0 ~0 )
  // may we want ot make the m_tolRemaining[iCase] ( x0 dependent too ?? )
  // here you are re-using the tolerance defined before from the linear extrapolation of 2-hit combo
  LookAroundMin( Bounds[iLayer], xminPrev[iLayer], xMinAtZ, endZones[iLayer], begZones[iLayer] ); // only costs 33%
  ModPrHitConstIter bestProj = endZones[iLayer];
  const PrHit*      hit;
  float             bestDist = m_tolRemaining[iCase];
  float             tmpDist;
  for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end < endZones[iLayer]; Bounds[iLayer].end++ ) {
    if ( !Bounds[iLayer].end->isValid() ) continue;
    hit = Bounds[iLayer].end->hit;
    if ( hit->x() < xMinAtZ ) { continue; }
    tmpDist = std::fabs( hit->x() - xAtZ );
    if ( tmpDist < bestDist ) {
      bestDist = tmpDist;
      bestProj = Bounds[iLayer].end;
    } else
      break;
  }
  if ( bestProj != endZones[iLayer] ) { xHits.push_back( *bestProj ); }
  return;
}

void PrHybridSeeding::updateXZCombinationPars( const unsigned int& iCase, const float& xFirst, const float& xLast,
                                               const ZonesXSearch& xZones, const float& slope, const float& slopeopp,
                                               const float& accTerm1, const float& accTerm2,
                                               TwoHitCombination& hitComb ) const noexcept {
  hitComb.tx    = ( xLast - xFirst ) * xZones.invZlZf;
  hitComb.x0    = xFirst - hitComb.tx * xZones.zLays[0];
  hitComb.x0new = hitComb.x0 * ( 1.f + m_x0Corr[iCase] );
  if ( !m_parabolaSeedParabolicModel ) {
    if ( hitComb.x0 > 0.f ) {
      hitComb.minPara = hitComb.x0 > m_x0SlopeChange[iCase] ? -slope * hitComb.x0 + accTerm1 : -m_tolX0SameSign[iCase];
      hitComb.maxPara =
          hitComb.x0 > m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 - accTerm2 : +m_tolX0OppSign[iCase];
    } else {
      hitComb.maxPara = hitComb.x0 < -m_x0SlopeChange[iCase] ? -slope * hitComb.x0 - accTerm1 : m_tolX0SameSign[iCase];
      hitComb.minPara =
          hitComb.x0 < -m_x0SlopeChange2[iCase] ? slopeopp * hitComb.x0 + accTerm2 : -m_tolX0OppSign[iCase];
    }
  } else {
    hitComb.maxPara =
        m_parPol0[iCase] + hitComb.x0new * m_parPol1[iCase] + hitComb.x0new * hitComb.x0new * m_parPol2[iCase];
    hitComb.minPara = hitComb.maxPara - 2 * hitComb.x0new * m_parPol1[iCase];
  }
}

void PrHybridSeeding::initializeXProjections( unsigned int iCase, unsigned int part, ZonesXSearch& xZones ) const
    noexcept {
  unsigned int firstZoneId( 0 ), lastZoneId( 0 );
  if ( 0 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X2 - part;
  } else if ( 1 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 2 == iCase ) {
    firstZoneId = T1X1 - part;
    lastZoneId  = T3X1 - part;
  } else if ( 3 == iCase ) {
    firstZoneId = T1X2 - part;
    lastZoneId  = T3X2 - part;
  }

  // Array[0] = first layer in T1 for 2-hit combo

  xZones.zones[0]     = firstZoneId;
  xZones.zLays[0]     = m_z[firstZoneId];
  xZones.dzLays[0]    = xZones.zLays[0] - Pr::Hybrid::zReference;
  xZones.dz2Lays[0]   = xZones.dzLays[0] * xZones.dzLays[0] * ( 1.f + m_dRatio * xZones.dzLays[0] );
  xZones.planeCode[0] = m_planeCode[firstZoneId];
  // Array[1] = last  layer in T3 for 2-hit combo
  xZones.zones[1]     = lastZoneId;
  xZones.zLays[1]     = m_z[lastZoneId];
  xZones.dzLays[1]    = xZones.zLays[1] - Pr::Hybrid::zReference;
  xZones.dz2Lays[1]   = xZones.dzLays[1] * xZones.dzLays[1] * ( 1.f + m_dRatio * xZones.dzLays[1] );
  xZones.planeCode[1] = m_planeCode[lastZoneId];

  // Array[2] = T2-1st x-layers
  xZones.zones[2]     = T2X1 - part;
  xZones.zLays[2]     = m_z[T2X1 - part];
  xZones.dzLays[2]    = xZones.zLays[2] - Pr::Hybrid::zReference;
  xZones.dz2Lays[2]   = xZones.dzLays[2] * xZones.dzLays[2] * ( 1.f + m_dRatio * xZones.dzLays[2] );
  xZones.planeCode[2] = m_planeCode[lastZoneId];
  // Array[3] = T2-2nd x-layers
  xZones.zones[3]     = T2X2 - part;
  xZones.zLays[3]     = m_z[T2X2 - part];
  xZones.dzLays[3]    = xZones.zLays[3] - Pr::Hybrid::zReference;
  xZones.dz2Lays[3]   = xZones.dzLays[3] * xZones.dzLays[3] * ( 1.f + m_dRatio * xZones.dzLays[3] );
  xZones.planeCode[3] = m_planeCode[T2X2 - part];

  unsigned int i = 4;
  // Add extra layer HERE, if needed!!!!
  for ( unsigned int xZoneId : {T1X1, T1X2, T3X1, T3X2} ) {
    xZoneId = xZoneId - part;
    if ( xZoneId != firstZoneId && xZoneId != lastZoneId ) {
      xZones.zones[i]     = xZoneId;
      xZones.zLays[i]     = m_z[xZoneId];
      xZones.dzLays[i]    = xZones.zLays[i] - Pr::Hybrid::zReference;
      xZones.dz2Lays[i]   = xZones.dzLays[i] * xZones.dzLays[i] * ( 1.f + m_dRatio * xZones.dzLays[i] );
      xZones.planeCode[i] = m_planeCode[xZoneId];
      i++;
    }
  }
  xZones.invZf   = 1.f / xZones.zLays[0];
  xZones.invZlZf = 1.f / ( xZones.zLays[1] - xZones.zLays[0] );
}

//---LoH: 40% of the findXProjection timing.
//---LoH: Can be put outside of the class.
bool PrHybridSeeding::findParabolaHits( const unsigned int& iLayer, const ZonesXSearch& xZones,
                                        const TwoHitCombination&                                  hitComb,
                                        const std::array<HitIter, PrFTInfo::Numbers::NFTXLayers>& endZones,
                                        Boundaries& Bounds, std::vector<ModPrHit>& parabolaSeedHits ) const noexcept {
  float        xProjectedCorrected = xZones.zLays[iLayer] * hitComb.tx + hitComb.x0new; // Target
  float        xMin                = xProjectedCorrected + hitComb.minPara;             // may add a 1.0 mm here?
  float        xMax                = xProjectedCorrected + hitComb.maxPara;             // may add a 1.0 mm here?
  const PrHit* hit;
  Bounds[iLayer].begin = get_lowerBound_lin( Bounds[iLayer].begin, endZones[iLayer], xMin );
  for ( Bounds[iLayer].end = Bounds[iLayer].begin; Bounds[iLayer].end != endZones[iLayer]; ++Bounds[iLayer].end ) {
    if ( !Bounds[iLayer].end->isValid() ) continue;
    hit = Bounds[iLayer].end->hit;
    if ( hit->x() > xMax ) break;
    parabolaSeedHits.push_back( *( Bounds[iLayer].end ) );
  }
  return ( parabolaSeedHits.size() != 0 );
}
