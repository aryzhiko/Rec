/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDecoder.h"
#include "Kernel/UTTell1Board.h"
#include "PrKernel/PrUTHitHandler.h"
#include "PrKernel/UTHitHandler.h"
#include "UTDet/DeUTDetector.h"
#include <memory>
#include <numeric>

namespace LHCb::Pr::UT {

  struct UTGeomCache {
    static constexpr int NBoards         = 240;
    static constexpr int NSectorPerBoard = 6;
    /** Faster access to sectors **/
    std::array<const DeUTSector*, NBoards * NSectorPerBoard> sectors;

    struct FullChan {
      unsigned int idx{0};
      unsigned int chanID{0};
    };
    std::array<FullChan, NBoards * NSectorPerBoard> fullchan;

    UTGeomCache( const DeUTDetector& utDet, const IUTReadoutTool& ro ) {
      for ( int srcId = 0; srcId < UTGeomCache::NBoards; srcId++ ) {
        const UTTell1ID tel1ID( srcId );
        const auto*     aBoard = ro.findByBoardID( tel1ID );
        if ( !aBoard ) continue;
        std::size_t i = 0;
        for ( const auto& sector : aBoard->sectorIDs() ) {

          const auto* aSector = utDet.getSector( sector );
          if ( !aSector ) throw std::runtime_error{"DeUTDetector::getSector returned nullptr"};

          // Check dzDy here, so the check can be removed from downstream code
          // TODO: move this check into aSector...
          double dxDy{0};
          double dzDy{0};
          double xAtYEq0{0};
          double zAtYEq0{0};
          double yBegin{0};
          double yEnd{0};
          aSector->trajectory( 0, 0, dxDy, dzDy, xAtYEq0, zAtYEq0, yBegin, yEnd );
          if ( dzDy != 0 ) throw std::runtime_error{"UTHitHandler: dzDy is not zero"};

          const std::size_t geomIdx = srcId * UTGeomCache::NSectorPerBoard + i++;
          assert( geomIdx < sectors.size() );
          assert( geomIdx < fullchan.size() );

          const unsigned int fullChanIdx =
              ::UT::HitHandler::HitsInUT::idx( sector.station(), sector.layer(), sector.detRegion(), sector.sector() );

          sectors[geomIdx]  = aSector;
          fullchan[geomIdx] = {fullChanIdx, (unsigned int)sector};
        }
      }
    };
  };
  template <typename HANDLER>
  using Transformer =
      Gaudi::Functional::Transformer<HANDLER( const EventContext&, const RawEvent&, const UTGeomCache& ),
                                     LHCb::DetDesc::usesConditions<UTGeomCache>>;

  template <typename HANDLER>
  class StoreHit : public Transformer<HANDLER> {
  public:
    using KeyValue = typename Transformer<HANDLER>::KeyValue;

    StoreHit( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer<HANDLER>( name, pSvcLocator,
                                {KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives(
                                                                   RawEventLocation::Tracker, RawEventLocation::Other,
                                                                   RawEventLocation::Default )},
                                 KeyValue{"GeomCache", "AlgorithmSpecific-" + name + "-UTGeomCache"}},
                                KeyValue{"UTHitsLocation", ::UT::Info::HitLocation} ) {}

    StatusCode initialize() override {
      return Transformer<HANDLER>::initialize().andThen( [&] {
        // TODO : alignment need the updateSvc for detector ( UT experts needed )
        this->addConditionDerivation( *this, DeUTDetLocation::UT, [this]( const DeUTDetector& utDet ) {
          return UTGeomCache{utDet, *m_readoutTool};
        } );
      } );
    }

    HANDLER operator()( const EventContext& evtCtx, const LHCb::RawEvent& rawEvt,
                        const UTGeomCache& cache ) const override {
      HANDLER hitHandler{LHCb::getMemResource( evtCtx )};
      if constexpr ( std::is_same_v<HANDLER, ::UT::HitHandler> ) hitHandler.reserve( 10000 );

      const auto tBanks = rawEvt.banks( RawBank::UT );
      for ( const auto& bank : tBanks ) {
        // make local decoder
        UTDecoder decoder( bank->data() );
        // read in the first half of the bank
        for ( const auto& aWord : decoder.posRange() ) {
          const auto fracStrip = aWord.fracStripBits();

          const std::size_t geomIdx = bank->sourceID() * UTGeomCache::NSectorPerBoard +
                                      ( aWord.channelID() / 512 ); // FIXME: move functionality into channelID
          const std::size_t strip =
              ( aWord.channelID() & 0x1ff ) +
              1; // FIXME: move functionality into channelID -- why is this _different_ from UTChennelID::strip() ??

          assert( geomIdx < cache.sectors.size() );
          assert( geomIdx < cache.fullchan.size() );
          const auto& aSector  = cache.sectors[geomIdx];
          const auto& fullChan = cache.fullchan[geomIdx];

          // note that the channel ID given to AddHit does no have strip bits set
          // this is fine as they are never used. The only use of the given chanID
          // is in the call to planeCode that uses only the station and layer bits
          hitHandler.AddHit( aSector, fullChan.idx, strip, fracStrip,
                             UTChannelID{(int)( fullChan.chanID + strip )}, // rebuild full ID by adding the strip part
                                                                            // FIXME: move functionality into channelID
                             aWord.pseudoSizeBits(), aWord.hasHighThreshold() );
        }
      }

      m_nBanks += tBanks.size();

      if constexpr ( std::is_same_v<HANDLER, ::LHCb::Pr::UT::HitHandler> ) hitHandler.addPadding();

      return hitHandler;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nBanks{this, "#banks"};
    ToolHandle<IUTReadoutTool>                    m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( StoreHit<::UT::HitHandler>, "PrStoreUTHit" ) // scalar hits
  DECLARE_COMPONENT_WITH_ID( StoreHit<HitHandler>, "PrStorePrUTHits" )    // SoA hits

} // namespace LHCb::Pr::UT
