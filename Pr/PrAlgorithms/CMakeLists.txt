###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: PrAlgorithms
################################################################################
gaudi_subdir(PrAlgorithms v1r18)

gaudi_depends_on_subdirs(Det/FTDet
                         Det/STDet
                         Event/DAQEvent
                         Event/RecEvent
                         Tr/TrackInterfaces
                         Phys/FunctorCore
                         Pr/PrKernel
                         Rec/LoKiTrack
                         Rec/LoKiTrack_v2)

find_package(Boost)
find_package(ROOT)
find_package(Vc)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Vc_INCLUDE_DIR})

gaudi_add_module(PrAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Tr/TrackInterfaces UT/UTDAQ Vc
                 LINK_LIBRARIES FTDetLib DAQEventLib RecEvent PrKernel STDetLib Vc FTDAQLib UTDAQLib LoKiTrackLib LoKiTrack_v2Lib FunctorCoreLib)

gaudi_install_headers(PrAlgorithms)
