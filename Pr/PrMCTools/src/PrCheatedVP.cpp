/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/VPCluster.h"
#include "GaudiAlg/Transformer.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "PrFitParams/IPrFitTool.h"
#include "PrFitParams/PrFitTool.h"

using namespace LHCb;

/** @class PrCheatedVP PrCheatedVP.h
 *  Cheated pattern recognition for the upgraded VELO
 *
 *  @author Olivier Callot
 *  @date   2012-07-26
 */

template <bool useMCHits>
class PrCheatedVPBase : public Gaudi::Functional::Transformer<
                            std::conditional_t<useMCHits, LHCb::Tracks( const LHCb::MCParticles&, const LHCb::MCHits& ),
                                               LHCb::Tracks( const LHCb::MCParticles& )>> {
public:
  /// Using Transfomer's constructor
  using PrCheatedVPBase::Transformer::Transformer;

protected:
  ToolHandle<const IPrFitTool> m_fitTool{"PrFitTool", this};
};

class PrCheatedVP final : public PrCheatedVPBase<false> {
public:
  PrCheatedVP( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::Tracks operator()( const LHCb::MCParticles& ) const override;
};

class PrCheatedVPMCHits final : public PrCheatedVPBase<true> {
public:
  PrCheatedVPMCHits( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::Tracks operator()( const LHCb::MCParticles&, const LHCb::MCHits& ) const override;
};

DECLARE_COMPONENT( PrCheatedVP )
DECLARE_COMPONENT( PrCheatedVPMCHits )

namespace {
  LHCb::Tracks getTracks( const GaudiAlgorithm& alg, const MCTrackInfo& trackInfo, const IPrFitTool& fitTool,
                          const LHCb::MCParticles& particles,
                          const std::function<void( LHCb::Track&, std::vector<Gaudi::XYZPoint>&,
                                                    const LHCb::MCParticle*, LinkedFrom<LHCb::VPCluster, MCParticle>& )>
                              getPoints ) {
    LHCb::Tracks tracks;

    // Get the association table between MC particles and clusters.
    LinkedFrom<LHCb::VPCluster, MCParticle> link( alg.evtSvc(), alg.msgSvc(), LHCb::VPClusterLocation::Default );

    constexpr double zVelo = 0.;

    for ( const LHCb::MCParticle* const particle : particles ) {
      // Skip particles without track info.
      if ( 0 == trackInfo.fullInfo( particle ) ) continue;
      // Skip particles not linked to a VELO track.
      if ( !trackInfo.hasVelo( particle ) ) continue;
      // Skip electrons.
      if ( abs( particle->particleID().pid() ) == 11 ) continue;

      LHCb::Track* const           track = new LHCb::Track;
      std::vector<Gaudi::XYZPoint> points;
      getPoints( *track, points, particle, link );

      // Make a straight-line fit of the track.
      const auto xResult = fitTool.fitLine( points, IPrFitTool::XY::X, zVelo );
      const auto yResult = fitTool.fitLine( points, IPrFitTool::XY::Y, zVelo );
      if ( !xResult || !yResult ) {
        alg.err() << "Fit matrix is singular" << endmsg;
        continue;
      }

      const auto& [x0, x1] = *xResult;
      const auto& [y0, y1] = *yResult;

      LHCb::State state;
      state.setLocation( LHCb::State::Location::ClosestToBeam );
      state.setState( x0, y0, zVelo, x1, y1, 0. );
      track->addToStates( state );
      if ( 0 > particle->momentum().z() ) {
        track->setFlag( LHCb::Track::Flags::Backward, true );
        // Cut out backwards tracks.
        // delete track;
        // continue;
      }
      track->setType( LHCb::Track::Types::Velo );
      tracks.insert( track );
    }

    return tracks;
  }
} // namespace

//=============================================================================
// Constructor
//=============================================================================
PrCheatedVP::PrCheatedVP( const std::string& name, ISvcLocator* pSvcLocator )
    : PrCheatedVPBase( name, pSvcLocator, {KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}},
                       KeyValue{"Tracks", LHCb::TrackLocation::Velo} ) {}

PrCheatedVPMCHits::PrCheatedVPMCHits( const std::string& name, ISvcLocator* pSvcLocator )
    : PrCheatedVPBase(
          name, pSvcLocator,
          {KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}, KeyValue{"MCHits", LHCb::MCHitLocation::VP}},
          KeyValue{"Tracks", LHCb::TrackLocation::Velo} ) {}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrCheatedVP::operator()( const LHCb::MCParticles& particles ) const {
  return getTracks( *this, make_MCTrackInfo( evtSvc(), msgSvc() ), *m_fitTool, particles,
                    []( LHCb::Track& track, std::vector<Gaudi::XYZPoint>& points,
                        const LHCb::MCParticle* const particle, LinkedFrom<LHCb::VPCluster, MCParticle>& link ) {
                      for ( const LHCb::VPCluster* cluster = link.first( particle ); cluster != nullptr;
                            cluster                        = link.next() ) {
                        track.addToLhcbIDs( LHCb::LHCbID( cluster->channelID() ) );
                        points.emplace_back( cluster->x(), cluster->y(), cluster->z() );
                      }
                    } );
}

LHCb::Tracks PrCheatedVPMCHits::operator()( const LHCb::MCParticles& particles, const LHCb::MCHits& hits ) const {
  return getTracks( *this, make_MCTrackInfo( evtSvc(), msgSvc() ), *m_fitTool, particles,
                    [&hits]( LHCb::Track& track, std::vector<Gaudi::XYZPoint>& points,
                             const LHCb::MCParticle* const particle, LinkedFrom<LHCb::VPCluster, MCParticle>& link ) {
                      for ( const auto id : link.keyRange( particle ) ) {
                        track.addToLhcbIDs( LHCb::LHCbID( LHCb::VPChannelID( id ) ) );
                      }
                      for ( const LHCb::MCHit* const hit : hits ) {
                        if ( hit->mcParticle() == particle ) { points.emplace_back( hit->midPoint() ); }
                      }
                    } );
}
