electron
Downstream
NoTrackPreSelection.txt
TMVA
GlobalPID_Electron_Downstream_TMVA.weights.xml
TrackP
TrackPt
TrackChi2PerDof
TrackNumDof
TrackGhostProbability
#TrackCloneDist
TrackFitTChi2
TrackFitTNDoF
RichUsedR1Gas
RichUsedR2Gas
#RichAboveElThres
RichAboveMuThres
#RichAbovePiThres
RichAboveKaThres
#RichAbovePrThres
RichDLLe
RichDLLmu
RichDLLk
RichDLLp
RichDLLd
RichDLLbt
InAccMuon
MuonMVA1
MuonBkgLL
MuonMuLL
MuonIsLooseMuon
MuonIsMuon
MuonNShared
InAccEcal
EcalPIDe
EcalPIDmu
InAccHcal
HcalPIDe
HcalPIDmu
#new
CaloEoverP
CaloChargedEcal
CaloClusChi2
CaloEcalChi2
CaloEcalE
CaloHcalE
CaloElectronMatch
CaloTrMatch
CaloTrajectoryL
CombDLLe
CombDLLmu
