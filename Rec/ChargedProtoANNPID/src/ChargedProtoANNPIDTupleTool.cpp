/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <memory>
#include <unordered_map>

// base class
#include "ChargedProtoANNPIDToolBase.h"

// interfaces
#include "MCInterfaces/IRichMCTruthTool.h"
#include "RecInterfaces/IChargedProtoANNPIDTupleTool.h"

// Event
#include "Event/MCParticle.h"

namespace ANNGlobalPID {

  /** @class ChargedProtoANNPIDTupleTool ChargedProtoANNPIDTupleTool.h
   *
   *  Tool to fill the ANN PID variables into a tuple
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2011-02-04
   */

  class ChargedProtoANNPIDTupleTool final : public ChargedProtoANNPIDToolBase,
                                            virtual public IChargedProtoANNPIDTupleTool {

  public:
    /// Standard constructor
    ChargedProtoANNPIDTupleTool( const std::string& type, const std::string& name, const IInterface* parent );

    /// Destructor
    virtual ~ChargedProtoANNPIDTupleTool() = default;

    /// Algorithm initialization
    StatusCode initialize() override {
      const auto sc = ChargedProtoANNPIDToolBase::initialize();
      if ( !sc ) return sc;

      // get tools
      m_truth = tool<Rich::MC::IMCTruthTool>( "Rich::MC::MCTruthTool", "MCTruth", this );

      // Get a vector of input accessor objects for the configured variables
      for ( const auto& i : m_variables ) { m_inputs[i] = getInput( i ); }

      // return
      return sc;
    }

  public:
    /// Fill the tuple tool with information for the given ProtoParticle
    StatusCode fill( Tuples::Tuple& tuple, const LHCb::ProtoParticle* proto,
                     const LHCb::ParticleID pid = LHCb::ParticleID() ) const override;

  private:
    /// ProtoParticle variables as strings to add to the ntuple
    StringInputs m_variables;

    /// Use RICH tool to get MCParticle associations for Tracks (To avoid annoying Linkers)
    const Rich::MC::IMCTruthTool* m_truth = nullptr;

    /// variables to fill
    std::unordered_map<std::string, ChargedProtoANNPIDToolBase::Input::SmartPtr> m_inputs;
  };

} // namespace ANNGlobalPID

using namespace ANNGlobalPID;

// Declaration of the Tool Factory
DECLARE_COMPONENT( ChargedProtoANNPIDTupleTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoANNPIDTupleTool::ChargedProtoANNPIDTupleTool( const std::string& type, const std::string& name,
                                                          const IInterface* parent )
    : ChargedProtoANNPIDToolBase( type, name, parent ) {

  // interface
  declareInterface<IChargedProtoANNPIDTupleTool>( this );

  // Job options
  declareProperty(
      "Variables",
      m_variables = {
          // General event variables
          "NumProtoParticles", "NumCaloHypos", "NumLongTracks", "NumDownstreamTracks", "NumUpstreamTracks",
          "NumVeloTracks", "NumTTracks", "NumGhosts", "NumPVs", "NumSPDHits", "NumRich1Hits", "NumRich2Hits",
          "NumMuonTracks", "NumMuonCoordsS0", "NumMuonCoordsS1", "NumMuonCoordsS2", "NumMuonCoordsS3",
          "NumMuonCoordsS4", "NumVeloClusters", "NumITClusters", "NumTTClusters", "NumOTClusters", "RunNumber",
          "EventNumber",
          // Tracking
          "TrackP", "TrackPt", "TrackChi2PerDof", "TrackType", "TrackHistory", "TrackNumDof", "TrackLikelihood",
          "TrackGhostProbability", "TrackMatchChi2", "TrackFitMatchChi2", "TrackCloneDist", "TrackFitVeloChi2",
          "TrackFitVeloNDoF", "TrackFitTChi2", "TrackFitTNDoF", "TrackDOCA", "TrackVertexX", "TrackVertexY",
          "TrackVertexZ", "TrackRich1EntryX", "TrackRich1EntryY", "TrackRich1EntryZ", "TrackRich2EntryX",
          "TrackRich2EntryY", "TrackRich2EntryZ", "TrackRich1ExitX", "TrackRich1ExitY", "TrackRich1ExitZ",
          "TrackRich2ExitX", "TrackRich2ExitY", "TrackRich2ExitZ",
          // Combined DLLs
          "CombDLLe", "CombDLLmu", "CombDLLpi", "CombDLLk", "CombDLLp", "CombDLLd",
          // RICH
          "RichUsedAero", "RichUsedR1Gas", "RichUsedR2Gas", "RichAboveElThres", "RichAboveMuThres", "RichAbovePiThres",
          "RichAboveKaThres", "RichAbovePrThres", "RichAboveDeThres", "RichDLLe", "RichDLLmu", "RichDLLpi", "RichDLLk",
          "RichDLLp", "RichDLLd", "RichDLLbt",
          // MUON
          "InAccMuon", "MuonMuLL", "MuonBkgLL", "MuonIsMuon", "MuonIsLooseMuon", "MuonNShared", "MuonMVA1", "MuonMVA2",
          "MuonMVA3", "MuonMVA4", "MuonChi2Corr",
          // CALO
          "CaloEoverP",
          // ECAL
          "InAccEcal", "CaloChargedSpd", "CaloChargedPrs", "CaloChargedEcal", "CaloElectronMatch", "CaloTrMatch",
          "CaloEcalE", "CaloEcalChi2", "CaloClusChi2", "EcalPIDe", "EcalPIDmu", "CaloTrajectoryL",
          // HCAL
          "InAccHcal", "CaloHcalE", "HcalPIDe", "HcalPIDmu",
          // PRS
          "InAccPrs", "CaloPrsE", "PrsPIDe",
          // SPD
          "InAccSpd", "CaloSpdE",
          // BREM
          "InAccBrem", "CaloNeutralSpd", "CaloNeutralPrs", "CaloNeutralEcal", "CaloBremMatch", "CaloBremChi2",
          "BremPIDe",
          // VELO
          "VeloCharge"} );
}

//=============================================================================

StatusCode ChargedProtoANNPIDTupleTool::fill( Tuples::Tuple&             tuple, //
                                              const LHCb::ProtoParticle* proto, //
                                              const LHCb::ParticleID     pid ) const {
  bool sc = true;

  // Get track
  const auto* track = proto->track();
  if ( !track ) return Error( "ProtoParticle is neutral!" );

  // Loop over reconstruction variables
  for ( const auto& i : m_inputs ) {
    // get the variable and fill ntuple
    sc &= tuple->column( i.first, (float)i.second->value( proto ) );
  }

  // PID info
  sc &= tuple->column( "RecoPIDcode", pid.pid() );

  // MC variables

  // First get the MCParticle, if associated
  const auto* mcPart = m_truth->mcParticle( track );
  sc &= tuple->column( "HasMC", mcPart != nullptr );
  sc &= tuple->column( "MCParticleType", mcPart ? mcPart->particleID().pid() : 0 );
  sc &= tuple->column( "MCParticleP", mcPart ? mcPart->p() : -999 );
  sc &= tuple->column( "MCParticlePt", mcPart ? mcPart->pt() : -999 );
  sc &= tuple->column( "MCVirtualMass", mcPart ? mcPart->virtualMass() : -999 );

  // MC history flags
  bool fromB( false ), fromD( false );
  // Parent MC particle
  const auto*  mcParent = ( mcPart ? mcPart->mother() : nullptr );
  unsigned int iCount( 0 ); // protect against infinite loops
  while ( mcParent && ++iCount < 99999 ) {
    const auto& pid = mcParent->particleID();
    if ( pid.hasBottom() && mcParent->particleID().isHadron() ) { fromB = true; }
    if ( pid.hasCharm() && mcParent->particleID().isHadron() ) { fromD = true; }
    mcParent = mcParent->mother();
  }
  // Save MC parent info
  sc &= tuple->column( "MCFromB", fromB );
  sc &= tuple->column( "MCFromD", fromD );

  // Get info on the MC vertex type
  const auto* mcVert = ( mcPart ? mcPart->originVertex() : nullptr );
  sc &= tuple->column( "MCVertexType", mcVert ? (int)mcVert->type() : -999 );
  sc &= tuple->column( "MCVertexX", mcVert ? mcVert->position().x() : -999.0 );
  sc &= tuple->column( "MCVertexY", mcVert ? mcVert->position().y() : -999.0 );
  sc &= tuple->column( "MCVertexZ", mcVert ? mcVert->position().z() : -999.0 );

  // return
  return StatusCode{sc};
}
