/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoANNPIDTrainingTuple.h
 *
 * Header file for algorithm ChargedProtoANNPIDTrainingTuple
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2010-03-09
 */
//-----------------------------------------------------------------------------

#ifndef ChargedProtoANNPID_ChargedProtoANNPIDTrainingTuple_H
#define ChargedProtoANNPID_ChargedProtoANNPIDTrainingTuple_H 1

// local
#include "ChargedProtoANNPIDAlgBase.h"

// Event Model
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"

// Interfaces
#include "RecInterfaces/IChargedProtoANNPIDTupleTool.h"

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDTrainingTuple ChargedProtoANNPIDTrainingTuple.h
   *
   *  Makes an ntuple for PID ANN training, starting from ProtoParticles.
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoANNPIDTrainingTuple final : public ChargedProtoANNPIDAlgBase {

  public:
    /// Standard constructor
    ChargedProtoANNPIDTrainingTuple( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~ChargedProtoANNPIDTrainingTuple() = default; ///< Destructor

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    /// Pointer to the tuple tool
    const IChargedProtoANNPIDTupleTool* m_tuple = nullptr;
  };

} // namespace ANNGlobalPID

#endif // ChargedProtoANNPID_ChargedProtoANNPIDTrainingTuple_H
