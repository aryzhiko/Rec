###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#
#  "One-off" script to re-write the TMVA generated source files _in_this_package_
#  in order to make all constants `constexpr` and move initialization from runtime
#  to compile time, and remove any use of `static` #  variables.
#
#   Not generally applicable, use at your own risk!
#

import sys
import re
file1 = open(sys.argv[1], 'r')
Lines = file1.readlines()

import collections
wm = collections.OrderedDict()

veto = []
subst = []

cl = ''
klass = re.compile(
    r'class\s+(?!IClassifierReader)(\w+)(\s*(final)?\s*:\s*public\s+IClassifierReader\s*)?{'
)

fLayers = re.compile(r'\s*fLayers\s*=\s*(\d+)\s*;')
layers = -1

has_fw = re.compile(r'\s*double\*\s+fWeights\[\d+\];')
subst_fw = False

has_nVar = re.compile(r'  const int nVar = (\d+);')
nVar = -1

has_fNvars = re.compile(r'.*, fNvars\(\s*(\d+)\s* \).*')
fNvars = -1

for line in Lines:
    match = klass.match(line)
    if (match):
        cl = match.group(1)
    match = fLayers.match(line)
    if (match):
        layers = int(match.group(1))
    match = has_fw.match(line)
    if (match):
        subst_fw = True
    match = has_nVar.match(line)
    if (match):
        nVar = int(match.group(1))
    match = has_fNvars.match(line)
    if (match):
        fNvars = int(match.group(1))

assert layers != -1, "Could not determine # of layers"
veto += [has_nVar]
veto += [fLayers, re.compile(r'\s*int\s+fLayers\s*;')]
subst += ['fLayers']

if fNvars != -1:
    veto += [re.compile(r'  const size_t fNvars;')]
    wm['fNvars'] = 'size_t{ %d }' % fNvars
    subst += ['fNvars']

veto += [re.compile(r'\s*fWeights\[\d+\]\s*=.*;')]
veto += [re.compile(r'\s*double\*\s+fWeights\[\d+\];')]
veto += [re.compile(r'.*delete\[\]\s+fWeights\[.+\];')]
veto += [re.compile(r'.*delete\[\]\s+fWeight\s*;')]

fw = '????'

for var in ["fLayerSize", "fVmin", "fVmax", "fType"]:
    declaration = re.compile(r'\s*(\w+)\s+' + var + r'\[(\d+)\];', re.VERBOSE)
    assignment = re.compile(r'\s*' + var + r'\[(\d+)\]\s*=\s*([^\s]+)\s*;',
                            re.VERBOSE)
    veto += [declaration, assignment]
    subst += [var]
    item = dict()
    t = '?????'
    for line in Lines:
        match = assignment.match(line)
        if match:
            i = int(match.group(1))
            item[i] = match.group(2)
        match = declaration.match(line)
        if match:
            t = match.group(1)
            dim_i = match.group(2)

    if len(item.keys()) == 0: continue
    i_min = min(item.keys())
    i_max = max(item.keys())
    wm[var] = format("std::array<%s,%d>{ %s }") % (t, i_max + 1, ",".join(
        item[i] for i in range(i_min, i_max + 1)))
    if var == "fLayerSize":
        sLayerSize = ('DataNS::%s::fLayerSize' % (cl)) + r'[%d]'
        fw = 'auto fWorkspace = std::array<double,%s>{ {} };' % ("+".join(
            sLayerSize % i for i in range(len(item.keys()))))
        clayer = lambda i: "" if i == 0 else "+" + "+".join(sLayerSize % j for j in range(i))
        fw += 'auto fWeights = std::array<double*, %d>{ %s };' % (len(
            item.keys()), ",".join("fWorkspace.begin() %s" % clayer(i)
                                   for i in range(len(item.keys()))))

for var in [
        "fMin_1", "fMax_1", "fScal_1", "fOff_1", "fWeightMatrix0to1",
        "fWeightMatrix1to2", "fWeightMatrix2to3"
]:
    assignment = re.compile(
        r'\s*' + var + r'\[(\d+)\]\[(\d+)\]\s*=\s*([^\s]+)\s*;', re.VERBOSE)
    declaration = re.compile(r'\s*(\w+)\s+' + var + r'\[(\d+)\]\[(\d+)\];',
                             re.VERBOSE)
    veto += [declaration, assignment]
    subst += [var]
    item = dict()
    t = '?????'
    for line in Lines:
        match = assignment.match(line)
        if match:
            i = int(match.group(1))
            j = int(match.group(2))
            item[i, j] = match.group(3)
        match = declaration.match(line)
        if match:
            t = match.group(1)
            dim_i = match.group(2)
            dim_j = match.group(3)

    if len(item.keys()) == 0: continue
    i_max = max(i for i, j in item.keys())
    j_max = max(j for i, j in item.keys())
    i_min = min(i for i, j in item.keys())
    j_min = min(j for i, j in item.keys())
    assert j_min == 0
    assert i_min == 0
    for j in range(j_min, j_max - j_min + 1):
        assert max(j for k, j in item.keys() if k == i) == j_max
        assert min(j for k, j in item.keys() if k == i) == j_min

    r = lambda i : format("std::array<%s,%d>{ %s }")% (t, j_max+1, ",".join( item[i,j] for j in range(j_min,j_max+1) ) )

    wm[var] = format("std::array< std::array<%s,%d>, %d>{ %s }") % (
        t, j_max + 1, i_max + 1, ",".join(
            r(i) for i in range(i_min, i_max + 1)))

insert_fweight = re.compile('.*::GetMvaValue__(.*)\s*const\s*{\s*')
for line in Lines:
    if any(v.match(line) for v in veto): continue
    line = line.replace('std::vector<double>::const_iterator', 'auto')
    line = line.replace(
        'static std::vector<double> iV;',
        'std::vector<double> iV; iV.reserve(inputValues.size());')
    if fNvars != -1:
        line = line.replace(', fNvars( %d )' % fNvars, '')
    for i in subst:
        line = line.replace(i, 'DataNS::%s::%s' % (cl, i))
    if nVar != -1:
        line = line.replace(
            'static std::vector<double> dv( nVar );',
            'constexpr int nVar = %d; std::array<double,nVar> dv{};' % nVar)
        line = line.replace(
            'static std::vector<double> dv;',
            'constexpr int nVar = %d; std::array<double,nVar> dv{};' % nVar)
        line = line.replace('dv.resize( nVar );', '')
    if klass.match(line):
        print "namespace DataNS { namespace %s { namespace {" % cl
        print "constexpr auto fLayers = %s; " % layers
        for k, v in wm.iteritems():
            print "constexpr auto %s = %s;" % (k, v)
        print "}}}"
    line = line.replace(
        'for ( auto varIt = inputValues.begin(); varIt != inputValues.end(); varIt++ ) { iV.push_back( *varIt ); }',
        'iV.insert( iV.end(), inputValues.begin(), inputValues.end() );')
    print line.rstrip()
    if subst_fw and insert_fweight.match(line):
        print fw
