/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddBremInfo.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleAddBremInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddBremInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddBremInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddBremInfo::FutureChargedProtoParticleAddBremInfo( const std::string& name,
                                                                              ISvcLocator*       pSvcLocator )
    : FutureChargedProtoParticleCALOFUTUREBaseAlg( name, pSvcLocator ) {

  using namespace LHCb::CaloFuture2Track;
  using namespace LHCb::CaloFutureIdLocation;
  using namespace LHCb::CaloFutureAlgUtils;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddBremInfo::execute() {

  // ProtoParticle container
  auto* protos = m_protos.get();

  auto const* InBremTable   = m_InBremTable.get();
  auto const* BremTrTable   = m_bremTrTable.get();
  auto const* BremChi2Table = m_BremChi2Table.get();
  auto const* dlleBremTable = m_dlleBremTable.get();

  // Loop over proto particles and fill brem info
  for ( auto* proto : *protos ) { addBrem( proto, InBremTable, BremTrTable, BremChi2Table, dlleBremTable ); }

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Add Calo Brem info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddBremInfo::addBrem( LHCb::ProtoParticle* proto, TrAccTable const* InBremTable,
                                                     HypoTrTable2D const* BremTrTable, TrEvalTable const* BremChi2Table,
                                                     TrEvalTable const* dlleBremTable ) const {
  // First remove existing BREM info
  proto->removeCaloBremInfo();

  // Add new info

  bool hasBremPID = false;

  const auto aRange = InBremTable->relations( proto->track() );
  if ( !aRange.empty() ) {
    hasBremPID = aRange.front().to();
    if ( hasBremPID ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The Brem. extrapolated line is in Ecal acceptance" << endmsg;
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::InAccBrem, true );

      // Get the highest weight associated brem. CaloHypo (3D matching)
      const auto hRange = BremTrTable->inverse()->relations( proto->track() );
      if ( !hRange.empty() ) {
        const auto* hypo = hRange.front().to();
        proto->addToCalo( hypo );
        using namespace LHCb::Calo::Enum;
        auto data             = m_estimator->get_data( *hypo );
        auto value_or_default = [&data]( auto type ) {
          auto it = data.find( type );
          if ( it != data.end() ) return it->second;
          return Default;
        };
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd,
                        value_or_default( DataType::HypoSpdM ) > 0 );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, value_or_default( DataType::HypoPrsE ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, value_or_default( DataType::ClusterE ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, value_or_default( DataType::BremMatch ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralID, value_or_default( DataType::CellID ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal,
                        value_or_default( DataType::Hcal2Ecal ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralE49, value_or_default( DataType::E49 ) );
      }

      // Get the BremChi2 (intermediate) estimator
      {
        const auto vRange = BremChi2Table->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, vRange.front().to() );
        }
      }

      // Get the Brem DLL(e)
      {
        const auto vRange = dlleBremTable->relations( proto->track() );
        if ( !vRange.empty() ) { proto->addInfo( LHCb::ProtoParticle::additionalInfo::BremPIDe, vRange.front().to() ); }
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> BremStrahlung PID : "
                  << " Chi2-Brem  =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, -999. )
                  << " BremChi2   =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, -999. )
                  << " Dlle (Brem) =" << proto->info( LHCb::ProtoParticle::additionalInfo::BremPIDe, -999. )
                  << " Spd Digits " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd, 0. )
                  << " Prs Digits " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, 0. )
                  << " Ecal Cluster " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, 0. )
                  << endmsg;
    } else {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> The Brem. extrapolated line is NOT in Ecal acceptance" << endmsg;
    }
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " ->  No entry for that track in the Brem acceptance table" << endmsg;
  }

  return hasBremPID;
}
