/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddEcalInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddEcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H 1

#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

#include "GaudiKernel/DataObjectHandle.h"

/** @class FutureChargedProtoParticleAddEcalInfo FutureChargedProtoParticleAddEcalInfo.h
 *
 *  Updates the ECAL information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddEcalInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg {

public:
  /// Standard constructor
  FutureChargedProtoParticleAddEcalInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  using TrAccTable    = LHCb::Relation1D<LHCb::Track, bool>;
  using HypoTrTable2D = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
  using ClusTrTable2D = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
  using TrEvalTable   = LHCb::Relation1D<LHCb::Track, float>;

  /// Add Calo Ecal information to the given ProtoParticle
  bool addEcal( LHCb::ProtoParticle* proto, TrAccTable const* InEcalTable, HypoTrTable2D const* ElecTrTable,
                ClusTrTable2D const* ClusTrTable, TrEvalTable const* EcalChi2Table, TrEvalTable const* EcalETable,
                TrEvalTable const* ClusChi2Table, TrEvalTable const* dlleEcalTable,
                TrEvalTable const* dllmuEcalTable ) const;

private:
  DataObjectReadHandle<LHCb::ProtoParticles> m_protos{this, "ProtoParticleLocation",
                                                      LHCb::ProtoParticleLocation::Charged};

  DataObjectReadHandle<TrAccTable>    m_InEcalTable{this, "InputInEcalLocation", LHCb::CaloFutureIdLocation::InEcal};
  DataObjectReadHandle<HypoTrTable2D> m_elecTrTable{this, "InputElectronMatchLocation",
                                                    LHCb::CaloFutureIdLocation::ElectronMatch};
  DataObjectReadHandle<ClusTrTable2D> m_clusTrTable{this, "InputClusterMatchLocation",
                                                    LHCb::CaloFutureIdLocation::ClusterMatch};
  DataObjectReadHandle<TrEvalTable>   m_EcalChi2Table{this, "InputEcalChi2Location",
                                                    LHCb::CaloFutureIdLocation::EcalChi2};
  DataObjectReadHandle<TrEvalTable>   m_EcalETable{this, "InputEcalELocation", LHCb::CaloFutureIdLocation::EcalE};
  DataObjectReadHandle<TrEvalTable>   m_ClusChi2Table{this, "InputClusterChi2Location",
                                                    LHCb::CaloFutureIdLocation::ClusChi2};
  DataObjectReadHandle<TrEvalTable>   m_dlleEcalTable{this, "InputEcalPIDeLocation",
                                                    LHCb::CaloFutureIdLocation::EcalPIDe};
  DataObjectReadHandle<TrEvalTable>   m_dllmuEcalTable{this, "InputEcalPIDmuLocation",
                                                     LHCb::CaloFutureIdLocation::EcalPIDmu};

  /// CaloElectron tool, the tool has an internal state, therefore mutable.
  ToolHandle<LHCb::Calo::Interfaces::IElectron> m_electron{this, "CaloFutureElectron", "CaloFutureElectron"};
};

#endif // GLOBALRECO_FutureChargedProtoParticleAddEcalInfo_H
