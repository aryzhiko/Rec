/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddHcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddHcalInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddHcalInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddHcalInfo::FutureChargedProtoParticleAddHcalInfo( const std::string& name,
                                                                              ISvcLocator*       pSvcLocator )
    : FutureChargedProtoParticleCALOFUTUREBaseAlg( name, pSvcLocator ) {}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddHcalInfo::execute() {

  // ProtoParticle container
  auto* protos = m_protos.get();

  auto const* InHcalTable    = m_InHcalTable.get();
  auto const* HcalETable     = m_HcalETable.get();
  auto const* dlleHcalTable  = m_dlleHcalTable.get();
  auto const* dllmuHcalTable = m_dllmuHcalTable.get();

  // Loop over proto particles and update HCAL info
  for ( auto* proto : *protos ) { addHcal( proto, InHcalTable, HcalETable, dlleHcalTable, dllmuHcalTable ); }

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Add Calo Hcal info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddHcalInfo::addHcal( LHCb::ProtoParticle* proto, TrAccTable const* InHcalTable,
                                                     TrEvalTable const* HcalETable, TrEvalTable const* dlleHcalTable,
                                                     TrEvalTable const* dllmuHcalTable ) const {
  // clear HCAL info
  proto->removeCaloHcalInfo();

  bool hasHcalPID = false;

  const auto aRange = InHcalTable->relations( proto->track() );
  if ( !aRange.empty() ) {
    hasHcalPID = aRange.front().to();
    if ( hasHcalPID ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is in Hcal acceptance" << endmsg;
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::InAccHcal, true );

      // Get the HcalE (intermediate) estimator
      {
        const auto vRange = HcalETable->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloHcalE, vRange.front().to() );
        }
      }

      // Get the Hcal DLL(e)
      {
        const auto vRange = dlleHcalTable->relations( proto->track() );
        if ( !vRange.empty() ) { proto->addInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDe, vRange.front().to() ); }
      }

      // Get the Hcal DLL(mu)
      {
        const auto vRange = dllmuHcalTable->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, vRange.front().to() );
        }
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> Hcal PID  : "
                  << " HcalE      =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, -999. )
                  << " Dlle (Hcal) =" << proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDe, -999. )
                  << " Dllmu (Hcal) =" << proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, -999. )
                  << endmsg;

    } else {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is NOT in Hcal acceptance" << endmsg;
    }
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> No entry for that track in the Hcal acceptance table" << endmsg;
  }

  return hasHcalPID;
}
