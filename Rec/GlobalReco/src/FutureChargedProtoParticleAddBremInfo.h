/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddBremInfo.h
 *
 * Header file for algorithm ChargedProtoParticleAddBremInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H 1

#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

/** @class FutureChargedProtoParticleAddBremInfo FutureChargedProtoParticleAddBremInfo.h
 *
 *  Updates the CALO 'BREM' information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddBremInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg {

public:
  /// Standard constructor
  FutureChargedProtoParticleAddBremInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

  using TrAccTable    = LHCb::Relation1D<LHCb::Track, bool>;
  using HypoTrTable2D = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
  using TrEvalTable   = LHCb::Relation1D<LHCb::Track, float>;

private:
  /// Add Calo Brem information to the given ProtoParticle
  bool addBrem( LHCb::ProtoParticle* proto, TrAccTable const* InBremTable, HypoTrTable2D const* BremTrTable,
                TrEvalTable const* BremChi2Table, TrEvalTable const* dlleBremTable ) const;

private:
  DataObjectReadHandle<LHCb::ProtoParticles> m_protos{this, "ProtoParticleLocation",
                                                      LHCb::ProtoParticleLocation::Charged};

  DataObjectReadHandle<TrAccTable>    m_InBremTable{this, "InputInBremLocation", LHCb::CaloFutureIdLocation::InBrem};
  DataObjectReadHandle<HypoTrTable2D> m_bremTrTable{this, "InputBremMatchLocation",
                                                    LHCb::CaloFutureIdLocation::BremMatch};
  DataObjectReadHandle<TrEvalTable>   m_BremChi2Table{this, "InputBremChi2Location",
                                                    LHCb::CaloFutureIdLocation::BremChi2};
  DataObjectReadHandle<TrEvalTable>   m_dlleBremTable{this, "InputBremPIDeLocation",
                                                    LHCb::CaloFutureIdLocation::BremPIDe};
};

#endif // GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H
