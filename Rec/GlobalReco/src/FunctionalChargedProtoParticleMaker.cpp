/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @class FunctionalChargedProtoParticleMaker FunctionalChargedProtoParticleMaker.cpp
 *
 * Algorithm to build charged ProtoParticles from charged Tracks.
 * A selection can be applied to the tracks via a TrackSelector tool.
 *
 *
 * @author Sascha Stahl   Sascha.Stahl@cern.ch
 * @date 03/03/2020
 */
//-----------------------------------------------------------------------------

// from Gaudi
#include "GaudiAlg/MergingTransformer.h"

// interfaces
#include "TrackInterfaces/ITrackSelector.h"

// Event
#include "Event/ProtoParticle.h"
#include "Event/Track.h"

//-----------------------------------------------------------------------------

class FunctionalChargedProtoParticleMaker final
    : public Gaudi::Functional::MergingTransformer<LHCb::ProtoParticles(
          const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ranges )> {

public:
  /// Standard constructor
  FunctionalChargedProtoParticleMaker( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator, {"Inputs", {LHCb::TrackLocation::Default}},
                            KeyValue{"Output", LHCb::ProtoParticleLocation::Charged} ){};

  LHCb::ProtoParticles
  operator()( const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ranges ) const override; ///< Algorithm execution

private: // data
  mutable Gaudi::Accumulators::Counter<> m_count{this, "CreatedProtos"};
  /// Track selector tool
  ToolHandle<ITrackSelector> m_trSel{this, "TrackSelector", "DelegatingTrackSelector"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FunctionalChargedProtoParticleMaker )

//=============================================================================
// Main execution
//=============================================================================
LHCb::ProtoParticles FunctionalChargedProtoParticleMaker::
                     operator()( const Gaudi::Functional::vector_of_const_<LHCb::Tracks>& ranges ) const {
  LHCb::ProtoParticles protos;
  // Loop over tracks container
  for ( const auto& tracks : ranges ) {
    // Loop over tracks
    for ( const auto* tk : tracks ) {
      // Select tracks
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying Track " << tk->key() << endmsg;
      if ( !m_trSel->accept( *tk ) ) continue;

      // Make a proto-particle
      auto* proto = new LHCb::ProtoParticle();

      // If more than one Track container, cannot use Track key
      if ( tracks.size() == 1 ) {
        protos.insert( proto, tk->key() );
      } else {
        protos.insert( proto );
      }
      // Set track reference
      proto->setTrack( tk );

      // Add minimal track info
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackChi2PerDof, tk->chi2PerDoF() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackNumDof, tk->nDoF() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackHistory, tk->history() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackType, tk->type() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackP, tk->p() );
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::TrackPt, tk->pt() );

      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << " -> Created ProtoParticle : " << *proto << endmsg; }
      ++m_count;
    }
  }
  // return
  return protos;
}
