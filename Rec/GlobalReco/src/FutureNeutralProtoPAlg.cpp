/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Event
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
// Calo
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// from Gaudi
#include "GaudiAlg/Transformer.h"

/** @class FutureNeutralProtoPAlg FutureNeutralProtoPAlg.cpp
 *
 *  Creator of the neutral ProtoParticles from CaloHypos
 *
 *  The current version fills the following estimators for ProtoParticle
 *
 *  <ul>
 *  <li>  <i>CaloTrMatch</i>     as <b>minimal</b> of this estimator for all
 *        linked <i>CaloHypo</i> objects. The value is extracted from
 *        the relation table/associator as a relation weigth between
 *        <i>CaloCluster</i> and <i>TrStoredTrack</i> objects </li>
 *  <li>  <i>CaloDepositID</i>   as <b>maximal</b> of this estimator for all
 *        linked <i>CaloHypo</i> objects using Spd/Prs estimator tool
 *        written by Frederic Machefert </li>
 *  <li>  <i>CaloShowerShape</i> as <b>maximal</b> of the estimator for
 *        all linked <i>CaloHypo</i> objects. Estimator is equal to the
 *        sum of diagonal elements of cluster spread matrix (2nd order
 *        moments of the cluster) </li>
 *  <li>  <i>ClusterMass</i>     as <b>maximal</b> of the estimator of
 *        cluster mass using smart algorithm by Olivier Deschamp </li>
 *  <li>  <i>PhotonID</i>        as the estimator of PhotonID
 *        using nice identifiaction tool
 *        CaloPhotonEstimatorTool by Frederic Machefert *
 *  </ul>
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-06-09
 *  Adapted from NeutralPPsFromCPsAlg class (Vanya Belyaev Ivan.Belyaev@itep.ru)
 */

class FutureNeutralProtoPAlg final : public Gaudi::Functional::Transformer<LHCb::ProtoParticles(
                                         const LHCb::CaloHypos&, const LHCb::CaloHypos&, const LHCb::CaloHypos& )> {
  // ==========================================================================
public:
  // ==========================================================================
  /// Standard constructor
  FutureNeutralProtoPAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"MergedPi0s", LHCb::CaloHypoLocation::MergedPi0s},
                      KeyValue{"Photons", LHCb::CaloHypoLocation::Photons},
                      KeyValue{"SplitPhotons", LHCb::CaloHypoLocation::SplitPhotons}},
                     KeyValue{"ProtoParticleLocation", LHCb::ProtoParticleLocation::Neutrals} ){};

  StatusCode           initialize() override; ///< Algorithm initialization
  LHCb::ProtoParticles operator()( const LHCb::CaloHypos&, const LHCb::CaloHypos&,
                                   const LHCb::CaloHypos& ) const override;

private:
  /// use"light" mode? ( suitable fo recalibration purposes)
  inline bool lightMode() const noexcept { return m_light_mode; }

  void pushData( std::map<LHCb::Calo::Enum::DataType, double> const& data, LHCb::ProtoParticle* proto,
                 LHCb::ProtoParticle::additionalInfo pflag, LHCb::Calo::Enum::DataType hflag,
                 LHCb::CaloHypo::Hypothesis hypothesis, double def = LHCb::Calo::Enum::Default,
                 bool force = false ) const;

private: // data
  Gaudi::Property<bool> m_light_mode{this, "LightMode", false,
                                     "Use 'light' mode and do not collect all information. Useful for Calibration."};

  ToolHandle<LHCb::Calo::Interfaces::IHypoEstimator> m_estimator{this, "CaloFutureHypoEstimator",
                                                                 "CaloFutureHypoEstimator"};
  mutable Gaudi::Accumulators::StatCounter<>         m_countMergedPi0s{this, "Neutral Protos from MergedPi0s"};
  mutable Gaudi::Accumulators::StatCounter<>         m_countPhotons{this, "Neutral Protos from Photons"};
  mutable Gaudi::Accumulators::StatCounter<>         m_countSplitPhotons{this, "Neutral Protos from SplitPhotons"};
  mutable Gaudi::Accumulators::StatCounter<>         m_countProtos{this, "Neutral Protos"};
  // for now, lazily use std::map.
  // TODO: switch to index obtained from perfect hashing of all possible keys
  using Key_t = std::pair<LHCb::CaloHypo::Hypothesis, LHCb::ProtoParticle::additionalInfo>;
  std::map<Key_t, Gaudi::Accumulators::StatCounter<>> init_counters();
  std::map<Key_t, Gaudi::Accumulators::StatCounter<>> m_hypoDataCounters = init_counters();
};

inline void FutureNeutralProtoPAlg::pushData( std::map<LHCb::Calo::Enum::DataType, double> const& data,
                                              LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo pflag,
                                              LHCb::Calo::Enum::DataType hflag, LHCb::CaloHypo::Hypothesis hypothesis,
                                              const double def, const bool force ) const {
  auto value_or_default = [&data, &def]( auto type ) {
    auto it = data.find( type );
    if ( it != data.end() ) return it->second;
    return def;
  };

  auto value = value_or_default( hflag );
  if ( value != def || force ) {
    proto->addInfo( pflag, value ); // only store when different from default
    const_cast<Gaudi::Accumulators::StatCounter<>&>( m_hypoDataCounters.at( Key_t{hypothesis, pflag} ) ) += value;
  }
}
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureNeutralProtoPAlg )

// ============================================================================
// Initialization
// ============================================================================
StatusCode FutureNeutralProtoPAlg::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                    // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  if ( lightMode() ) info() << "FutureNeutral protoparticles will be created in 'Light' Mode" << endmsg;

  // TODO: Move to configuration
  auto& h2c = dynamic_cast<IProperty&>( *m_estimator->hypo2Calo() );
  h2c.setProperty( "Seed", "false" ).ignore();
  h2c.setProperty( "PhotonLine", "true" ).ignore();
  h2c.setProperty( "AddNeighbors", "false" ).ignore();

  return sc;
}

// ============================================================================
// Main execution
// ============================================================================
LHCb::ProtoParticles FutureNeutralProtoPAlg::operator()( const LHCb::CaloHypos& hypos_from_mergedPi0s,
                                                         const LHCb::CaloHypos& hypos_from_Photons,
                                                         const LHCb::CaloHypos& hypos_from_SplitPhotons ) const {
  LHCb::ProtoParticles protos{};

  // -- reset mass storage
  std::map<const int, double> mass_per_cell = {{}};

  //------ loop over all caloHypo containers

  auto append_protos_from_location = [&]( const auto& hypos, auto& count_protos ) {
    int count = 0;

    // == Loop over CaloHypos
    for ( const auto* hypo : hypos ) {
      if ( !hypo ) { continue; }
      count++;

      // == create and store the corresponding ProtoParticle
      auto* proto = new LHCb::ProtoParticle();
      protos.insert( proto );

      // == link CaloHypo to ProtoP
      using namespace LHCb::Calo::Enum;
      proto->addToCalo( hypo );
      const auto hypothesis = hypo->hypothesis();

      // ===== add data to protoparticle
      if ( !lightMode() ) {
        auto data = m_estimator->get_data( *hypo );
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralID, DataType::CellID,
                  hypothesis ); // seed cellID
        // retrieve HypoM for photon
        auto      it       = data.find( DataType::CellID );
        const int cellCode = ( it != data.end() ) ? it->second : Default;
        if ( hypothesis == LHCb::CaloHypo::Hypothesis::Photon || hypothesis == LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
          const auto it   = mass_per_cell.find( cellCode );
          const auto mass = ( it == mass_per_cell.end() ) ? 0.0 : it->second;
          if ( mass > 0. ) { proto->addInfo( LHCb::ProtoParticle::additionalInfo::ClusterMass, mass ); }
        }
        if ( hypothesis != LHCb::CaloHypo::Hypothesis::Pi0Merged ) {
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::ClusterAsX, DataType::ClusterAsX, hypothesis, 0 );
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::ClusterAsY, DataType::ClusterAsY, hypothesis, 0 );
        }

        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, DataType::HypoPrsE, hypothesis );
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, DataType::ClusterE, hypothesis );

        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloTrMatch, DataType::ClusterMatch, hypothesis,
                  +1.e+06 ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::ShowerShape, DataType::Spread,
                  hypothesis ); // ** input to neutralID  && isPhoton (as Fr2)
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd, DataType::HypoSpdM,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal, DataType::Hcal2Ecal,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloClusterCode, DataType::ClusterCode,
                  hypothesis );
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloClusterFrac, DataType::ClusterFrac, hypothesis,
                  1 );
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::Saturation, DataType::Saturation, hypothesis, 0 );

        // Was
        // auto dep = ( m_estimator->data( *hypo, DataType::ToSpdM ) > 0 ) ? -1. : +1.;
        // dep *= m_estimator->data( *hypo, DataType::ToPrsE ).value_or( Default );
        // But since SPD and PRS are removed, simplify code here.
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloDepositID,
                        Default ); // ** input to neutralID toPrsE=|caloDepositID|

        // DLL-based neutralID (to be obsolete)  :
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::PhotonID, DataType::NeutralID, hypothesis, -1.,
                  true ); // old DLL-based neutral-ID // FORCE

        // isNotX  inputs :
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE49, DataType::E49, hypothesis );
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralE19, DataType::E19,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE49, DataType::PrsE49,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE19, DataType::PrsE19,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE4max, DataType::PrsE4Max,
                  hypothesis ); // ** input to neutralID
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloNeutralPrsM, DataType::HypoPrsM,
                  hypothesis ); // ** input to neutralID
        // isNotX output :
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::IsNotH, DataType::isNotH, hypothesis, -1.,
                  true ); // new NN-based neutral-ID (anti-H) // FORCE
        pushData( data, proto, LHCb::ProtoParticle::additionalInfo::IsNotE, DataType::isNotE, hypothesis, -1.,
                  true ); // new NN-based neutral-ID (anti-E) // FORCE

        // isPhoton inputs (photon & mergedPi0 only)
        if ( hypothesis != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 ) {
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloShapeFr2r4, DataType::isPhotonFr2r4,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloShapeAsym, DataType::isPhotonAsym,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloShapeKappa, DataType::isPhotonKappa,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE1, DataType::isPhotonEseed,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloShapeE2, DataType::isPhotonE2,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeE2, DataType::isPhotonPrsE2,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeEmax, DataType::isPhotonPrsEmax,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeFr2, DataType::isPhotonPrsFr2,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsShapeAsym, DataType::isPhotonPrsAsym,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM, DataType::isPhotonPrsM,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM15, DataType::isPhotonPrsM15,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM30, DataType::isPhotonPrsM30,
                    hypothesis ); // -- input to isPhoton
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::CaloPrsM45, DataType::isPhotonPrsM45,
                    hypothesis ); // -- input to isPhoton
          // isPhoton output :
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::IsPhoton, DataType::isPhoton, hypothesis, +1.,
                    true ); // NN-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
          pushData( data, proto, LHCb::ProtoParticle::additionalInfo::IsPhotonXGB, DataType::isPhotonXGB, hypothesis,
                    +1.,
                    true ); // XGBoost-based neutral-ID (anti-pi0) // FORCE to +1 when missing (i.e. PT < 2 GeV)
        }
      } // lightmode
    }   // loop over CaloHypos
    count_protos += count;
  };

  // Get masses
  for ( const auto* hypo : hypos_from_mergedPi0s ) {
    if ( !hypo ) continue;
    using namespace LHCb::Calo::Enum;
    // The call to process is happening twice for mergedPi0s, should be made smarter.
    auto      data          = m_estimator->get_data( *hypo );
    const int cellCode      = data.at( DataType::CellID );
    auto      it            = data.find( DataType::HypoM );
    mass_per_cell[cellCode] = ( it != data.end() ) ? it->second : Default;
  }

  auto countMergedPi0s   = m_countMergedPi0s.buffer();
  auto countPhotons      = m_countPhotons.buffer();
  auto countSplitPhotons = m_countSplitPhotons.buffer();

  append_protos_from_location( hypos_from_mergedPi0s, countMergedPi0s );
  append_protos_from_location( hypos_from_Photons, countPhotons );
  append_protos_from_location( hypos_from_SplitPhotons, countSplitPhotons );

  m_countProtos += protos.size();

  return protos;
}

std::map<FutureNeutralProtoPAlg::Key_t, Gaudi::Accumulators::StatCounter<>> FutureNeutralProtoPAlg::init_counters() {

  std::map<Key_t, Gaudi::Accumulators::StatCounter<>> m;
  for ( auto i : {LHCb::ProtoParticle::additionalInfo::CaloNeutralID,
                  LHCb::ProtoParticle::additionalInfo::ClusterMass,
                  LHCb::ProtoParticle::additionalInfo::ClusterAsX,
                  LHCb::ProtoParticle::additionalInfo::ClusterAsY,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal,
                  LHCb::ProtoParticle::additionalInfo::CaloTrMatch,
                  LHCb::ProtoParticle::additionalInfo::ShowerShape,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal,
                  LHCb::ProtoParticle::additionalInfo::CaloClusterCode,
                  LHCb::ProtoParticle::additionalInfo::CaloClusterFrac,
                  LHCb::ProtoParticle::additionalInfo::Saturation,
                  LHCb::ProtoParticle::additionalInfo::PhotonID,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralE49,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralE19,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE49,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE19,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsNeutralE4max,
                  LHCb::ProtoParticle::additionalInfo::CaloNeutralPrsM,
                  LHCb::ProtoParticle::additionalInfo::IsNotH,
                  LHCb::ProtoParticle::additionalInfo::IsNotE,
                  LHCb::ProtoParticle::additionalInfo::CaloShapeFr2r4,
                  LHCb::ProtoParticle::additionalInfo::CaloShapeAsym,
                  LHCb::ProtoParticle::additionalInfo::CaloShapeKappa,
                  LHCb::ProtoParticle::additionalInfo::CaloShapeE1,
                  LHCb::ProtoParticle::additionalInfo::CaloShapeE2,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsShapeE2,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsShapeEmax,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsShapeFr2,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsShapeAsym,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsM,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsM15,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsM30,
                  LHCb::ProtoParticle::additionalInfo::CaloPrsM45,
                  LHCb::ProtoParticle::additionalInfo::IsPhoton,
                  LHCb::ProtoParticle::additionalInfo::IsPhotonXGB

        } ) {
    for ( auto j : {LHCb::CaloHypo::Hypothesis::Undefined,
                    LHCb::CaloHypo::Hypothesis::Mip,
                    LHCb::CaloHypo::Hypothesis::MipPositive,
                    LHCb::CaloHypo::Hypothesis::MipNegative,
                    LHCb::CaloHypo::Hypothesis::Photon,
                    LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0,
                    LHCb::CaloHypo::Hypothesis::BremmstrahlungPhoton,
                    LHCb::CaloHypo::Hypothesis::Pi0Resolved,
                    LHCb::CaloHypo::Hypothesis::Pi0Overlapped,
                    LHCb::CaloHypo::Hypothesis::Pi0Merged,
                    LHCb::CaloHypo::Hypothesis::EmCharged,
                    LHCb::CaloHypo::Hypothesis::Positron,
                    LHCb::CaloHypo::Hypothesis::Electron,
                    LHCb::CaloHypo::Hypothesis::EmChargedSeed,
                    LHCb::CaloHypo::Hypothesis::PositronSeed,
                    LHCb::CaloHypo::Hypothesis::ElectronSeed,
                    LHCb::CaloHypo::Hypothesis::NeutralHadron,
                    LHCb::CaloHypo::Hypothesis::ChargedHadron,
                    LHCb::CaloHypo::Hypothesis::PositiveHadron,
                    LHCb::CaloHypo::Hypothesis::NegativeHadron,
                    LHCb::CaloHypo::Hypothesis::Jet,
                    LHCb::CaloHypo::Hypothesis::Other} ) {
      std::ostringstream mess;
      mess << i << " for " << j;
      m.emplace( std::piecewise_construct, std::tuple{j, i}, std::tuple{this, mess.str()} );
    }
  }
  return m;
}
