/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddHcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H 1

// from Gaudi
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

/** @class FutureChargedProtoParticleAddHcalInfo FutureChargedProtoParticleAddHcalInfo.h
 *
 *  Updates the CALO HCAL information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddHcalInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg {

public:
  /// Standard constructor
  FutureChargedProtoParticleAddHcalInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:
  using TrAccTable  = LHCb::Relation1D<LHCb::Track, bool>;
  using TrEvalTable = LHCb::Relation1D<LHCb::Track, float>;

  /// Add Calo Hcal information to the given ProtoParticle
  bool addHcal( LHCb::ProtoParticle* proto, TrAccTable const* InHcalTable, TrEvalTable const* HcalETable,
                TrEvalTable const* dlleHcalTable, TrEvalTable const* dllmuHcalTable ) const;

private:
  DataObjectReadHandle<LHCb::ProtoParticles> m_protos{this, "ProtoParticleLocation",
                                                      LHCb::ProtoParticleLocation::Charged};

  DataObjectReadHandle<TrAccTable>  m_InHcalTable{this, "InputInHcalLocation", LHCb::CaloFutureIdLocation::InHcal};
  DataObjectReadHandle<TrEvalTable> m_HcalETable{this, "InputHcalELocation", LHCb::CaloFutureIdLocation::HcalE};
  DataObjectReadHandle<TrEvalTable> m_dlleHcalTable{this, "InputHcalPIDeLocation",
                                                    LHCb::CaloFutureIdLocation::HcalPIDe};
  DataObjectReadHandle<TrEvalTable> m_dllmuHcalTable{this, "InputHcalPIDmuLocation",
                                                     LHCb::CaloFutureIdLocation::HcalPIDmu};
};

#endif // GLOBALRECO_FutureChargedProtoParticleAddHcalInfo_H
