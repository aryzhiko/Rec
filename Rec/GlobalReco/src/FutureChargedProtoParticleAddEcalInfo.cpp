/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddEcalInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddEcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddEcalInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddEcalInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddEcalInfo::FutureChargedProtoParticleAddEcalInfo( const std::string& name,
                                                                              ISvcLocator*       pSvcLocator )
    : FutureChargedProtoParticleCALOFUTUREBaseAlg( name, pSvcLocator ) {}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddEcalInfo::execute() {
  // ProtoParticle container
  auto* protos = m_protos.get();

  auto const* InEcalTable    = m_InEcalTable.get();
  auto const* ElecTrTable    = m_elecTrTable.get();
  auto const* ClusTrTable    = m_clusTrTable.get();
  auto const* EcalChi2Table  = m_EcalChi2Table.get();
  auto const* EcalETable     = m_EcalETable.get();
  auto const* ClusChi2Table  = m_ClusChi2Table.get();
  auto const* dlleEcalTable  = m_dlleEcalTable.get();
  auto const* dllmuEcalTable = m_dllmuEcalTable.get();

  // Loop over proto particles and update ECAL info
  for ( auto* proto : *protos ) {
    addEcal( proto, InEcalTable, ElecTrTable, ClusTrTable, EcalChi2Table, EcalETable, ClusChi2Table, dlleEcalTable,
             dllmuEcalTable );
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Add Calo Ecal info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddEcalInfo::addEcal( LHCb::ProtoParticle* proto, TrAccTable const* InEcalTable,
                                                     HypoTrTable2D const* ElecTrTable, ClusTrTable2D const* ClusTrTable,
                                                     TrEvalTable const* EcalChi2Table, TrEvalTable const* EcalETable,
                                                     TrEvalTable const* ClusChi2Table, TrEvalTable const* dlleEcalTable,
                                                     TrEvalTable const* dllmuEcalTable ) const {
  // First remove existing ECAL info
  proto->removeCaloEcalInfo();

  // Now add new ECAL info

  bool hasEcalPID = false;

  const auto aRange = InEcalTable->relations( proto->track() );
  if ( !aRange.empty() ) {
    hasEcalPID = aRange.front().to();

    if ( hasEcalPID ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is in Ecal acceptance" << endmsg;
      proto->addInfo( LHCb::ProtoParticle::additionalInfo::InAccEcal, true );

      // Get the highest weight associated electron CaloHypo (3D matching)
      const auto hRange = ElecTrTable->inverse()->relations( proto->track() );
      if ( !hRange.empty() ) {

        const auto* hypo = hRange.front().to();
        proto->addToCalo( hypo );
        // CaloElectron->caloTrajectory must be after addToCalo
        auto hypoPair = m_electron->getElectronBrem( *proto );
        if ( hypoPair ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL,
                          m_electron->caloTrajectoryL( *proto, CaloPlane::ShowerMax ) );
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloEoverP, m_electron->eOverP( *proto ) );
        }

        using namespace LHCb::Calo::Enum;
        auto data             = m_estimator->get_data( *hypo );
        auto value_or_default = [&data]( auto type ) {
          auto it = data.find( type );
          if ( it != data.end() ) return it->second;
          return Default;
        };
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedSpd,
                        value_or_default( DataType::HypoSpdM ) > 0 );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedPrs, value_or_default( DataType::HypoPrsE ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, value_or_default( DataType::ClusterE ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloChargedID, value_or_default( DataType::CellID ) );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, hRange.front().weight() );
      }

      // Get the highest weight associated CaloCluster (2D matching)
      const auto cRange = ClusTrTable->inverse()->relations( proto->track() );
      if ( !cRange.empty() ) {
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, cRange.front().weight() );
      }

      // Get EcalE (intermediate) estimator
      {
        const auto vRange = EcalETable->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloEcalE, vRange.front().to() );
        }
      }

      // Get EcalChi2 (intermediate) estimator
      {
        const auto vRange = EcalChi2Table->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, vRange.front().to() );
        }
      }

      // Get ClusChi2 (intermediate) estimator
      {

        const auto vRange = ClusChi2Table->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, vRange.front().to() );
        }
      }

      // Get Ecal DLL(e)
      {
        const auto vRange = dlleEcalTable->relations( proto->track() );
        if ( !vRange.empty() ) { proto->addInfo( LHCb::ProtoParticle::additionalInfo::EcalPIDe, vRange.front().to() ); }
      }

      // Get Ecal DLL(mu)
      {
        const auto vRange = dllmuEcalTable->relations( proto->track() );
        if ( !vRange.empty() ) {
          proto->addInfo( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, vRange.front().to() );
        }
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> Ecal PID : "
                  << " Chi2-3D    =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, -999. )
                  << " Chi2-2D    =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, -999. )
                  << " EcalE      =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalE, -999. )
                  << " ClusChi2   =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, -999. )
                  << " EcalChi2   =" << proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, -999. )
                  << " Dlle (Ecal) =" << proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDe, -999. )
                  << " Dllmu (Ecal) =" << proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, -999. )
                  << " Spd Digits " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedSpd, 0. )
                  << " Prs Digits " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedPrs, 0. )
                  << " Spd Digits " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedSpd, 0. )
                  << " Ecal Cluster " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, 0. )
                  << " TrajectoryL " << proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 0. )
                  << endmsg;

    } else {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is NOT in Ecal acceptance" << endmsg;
    }
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> No entry for that track in the Ecal acceptance table " << endmsg;
  }

  return hasEcalPID;
}
