/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "SelTools/MVAUtils.h"
#include <string>

namespace Functors {
  namespace detail {
    template <typename MVAImpl, typename... Inputs>
    struct MVA final : public Function {
      MVA( Sel::MVA_config_dict const& config, std::pair<std::string, Inputs>... inputs )
          : m_impl{config}
          , m_inputs{std::move( std::get<1>( inputs ) )...}
          , m_input_names{std::move( std::get<0>( inputs ) )...} {
        // Check for duplicate entries in m_input_names
        auto input_names = m_input_names;
        std::sort( input_names.begin(), input_names.end() );
        if ( std::adjacent_find( input_names.begin(), input_names.end() ) != input_names.end() ) {
          throw GaudiException{"MVA functor given multiple definitions of an input variable.", "Functors::MVA",
                               StatusCode::FAILURE};
        }
      }

      template <typename Algorithm>
      void bind( Algorithm* alg ) {
        // Forward the call to the functors we own in m_inputs
        detail::bind( m_inputs, alg );

        // Set up the classifier
        detail::bind( m_impl, alg );

        // Ask the classifier what variables it's expecting and in what order
        auto const& variables = m_impl.variables();

        // Check the m_input_names matches the variables that the classifier is
        // expecting, and fill a list of indices that we can use when actually
        // evaluating to put the values we get from m_inputs into the right
        // order. Choose the convention that m_indices[i] is the position in
        // the classifier inputs that we should put the result of evaluating
        // std::get<i>( m_inputs ).
        if ( variables.size() != m_input_names.size() ) {
          throw GaudiException{"MVA functor given " + std::to_string( m_input_names.size() ) +
                                   " functors, but the given classifier expects " + std::to_string( variables.size() ),
                               "Functors::MVA", StatusCode::FAILURE};
        }
        for ( auto fun_index = 0ul; fun_index < m_input_names.size(); ++fun_index ) {
          auto const& input_name = m_input_names[fun_index];
          // What's the index of input_name in variables?
          auto iter = std::find( variables.begin(), variables.end(), input_name );
          if ( iter == variables.end() ) {
            throw GaudiException{"MVA functor knows how to calculate " + input_name +
                                     ", but the given classifier does not expect that",
                                 "Functors::MVA", StatusCode::FAILURE};
          }
          m_indices[fun_index] = std::distance( variables.begin(), iter );
        }
      }

      /** Prepare the functor for use. */
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
        return prepare( evtCtx, top_level, std::index_sequence_for<Inputs...>{} );
      }

    private:
      template <std::size_t... I>
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level, std::index_sequence<I...> ) const {
        // Prepare all of the functors and capture the resulting tuple
        return [this, fs = std::tuple{detail::prepare( std::get<I>( m_inputs ), evtCtx, top_level )...}](
                   auto const&... input ) {
          // Let the implementation specify if inputs should be float, double etc.
          std::array<typename MVAImpl::input_type, sizeof...( Inputs )> values;
          // Want to put the result of evaluating std::get<I>( m_inputs ) into values[m_indices[I]]
          ( ( values[m_indices[I]] = std::get<I>( fs )( input... ) ), ... );
          // Evaluate the classifier
          return m_impl( values );
        };
      }

      MVAImpl                                      m_impl;
      std::tuple<Inputs...>                        m_inputs;
      std::array<std::size_t, sizeof...( Inputs )> m_indices;
      std::array<std::string, sizeof...( Inputs )> m_input_names;
    };
  } // namespace detail

  /** Helper for specifying {name, functor} pairs. */
  template <typename F>
  auto MVAInput( std::string s, F f ) {
    return std::pair{std::move( s ), std::move( f )};
  }

  /** Helper for producing a detail::MVA instance without explicitly spelling
   *  out the functor types or using decltype. It should be transparent that
   *  this is actually a function, while in most cases functor objects are
   *  instantiated directly.
   */
  template <typename MVAImpl, typename... Inputs>
  auto MVA( Sel::MVA_config_dict const& config, std::pair<std::string, Inputs>&&... inputs ) {
    return detail::MVA<MVAImpl, Inputs...>{config, std::forward<std::pair<std::string, Inputs>>( inputs )...};
  }
} // namespace Functors