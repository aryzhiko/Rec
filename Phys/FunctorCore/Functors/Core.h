/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Gaudi/Accumulators.h"
#include "Gaudi/PluginService.h"
#include "GaudiKernel/ThreadLocalContext.h"
#include "Kernel/EventLocalUnique.h"
#include "Kernel/SynchronizedValue.h"
#include <typeinfo>

/** @file Core.h
 *  @brief Definition of the type-erased Functor<Output(Input)> type
 */
namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

/** @namespace Functors
 *
 *  Namespace for all new-style functors
 */
namespace Functors {
  /** @class Functor
   *  @brief Empty declaration of type-erased wrapper around a generic functor
   *
   *  This is specialised as Functors::Functor<OutputType(InputTypes...)>.
   */
  template <typename>
  class Functor;

  /** @class PreparedFunctor
   *  @brief Empty declaration of type-erased wrapper around a generic prepared functor.
   *
   *  This is specialised as Functors::PreparedFunctor<OutputType(InputTypes...)>.
   */
  template <typename>
  struct PreparedFunctor;

  /** @class AnyFunctor
   *  @brief Type-agnostic base class for functors, used in IFunctorFactory.
   *  @todo  Could consider using std::any's techniques for type-checking.
   */
  struct AnyFunctor {
    virtual ~AnyFunctor()                                                    = default;
    virtual                       operator bool() const                      = 0;
    virtual void                  bind( Gaudi::Algorithm* owning_algorithm ) = 0;
    virtual std::type_info const& rtype() const                              = 0;
  };

  struct TopLevelInfo {
    Gaudi::Algorithm* m_algorithm{nullptr};
    auto              algorithm() const { return m_algorithm; }
    void              bind( Gaudi::Algorithm* alg ) { m_algorithm = alg; }
    void              Warning( std::string_view message, std::size_t count = 1 ) const;

  private:
    using MsgMap             = std::map<std::string, Gaudi::Accumulators::MsgCounter<MSG::WARNING>, std::less<>>;
    using SynchronizedMsgMap = LHCb::cxx::SynchronizedValue<MsgMap>;
    // We have to use std::unique_ptr here to make sure that this structure,
    // and the functors that hold it, remain moveable. SynchronizedValue
    // contains an std::mutex, which is not moveable.
    mutable std::unique_ptr<SynchronizedMsgMap> m_msg_counters{std::make_unique<SynchronizedMsgMap>()};
  };

  namespace detail {
    /** Helper to determine if the given type has a bind() method */
    template <typename T, typename Algorithm>
    using has_bind_ = decltype( std::declval<T>().bind( std::declval<Algorithm*>() ) );

    template <typename T, typename Algorithm>
    inline constexpr bool has_bind_v = Gaudi::cpp17::is_detected_v<has_bind_, T, Algorithm>;

    /** Call obj.bind( alg ) if obj has an appropriate bind method, otherwise do nothing. */
    template <typename Obj, typename Algorithm>
    void bind( Obj& obj, [[maybe_unused]] Algorithm* alg ) {
      if constexpr ( has_bind_v<Obj, Algorithm> ) { obj.bind( alg ); }
    }

    /** Helper to determine if the given type has a prepare() method
     */
    template <typename T>
    using has_prepare_ = decltype( std::declval<T>().prepare() );

    template <typename T>
    inline constexpr bool has_prepare_v = Gaudi::cpp17::is_detected_v<has_prepare_, T>;

    /** Helper to determine if the given type has a
     *  prepare( TopLevelInfo const& ) method.
     */
    template <typename T>
    using has_prepare_TopLevelInfo_ = decltype( std::declval<T>().prepare( std::declval<TopLevelInfo>() ) );

    template <typename T>
    inline constexpr bool has_prepare_TopLevelInfo_v = Gaudi::cpp17::is_detected_v<has_prepare_TopLevelInfo_, T>;

    /** Helper to determine if the given type has a
     *  prepare( EventContext const&, TopLevelInfo const& ) method.
     */
    template <typename T>
    using has_prepare_EventContext_TopLevelInfo_ =
        decltype( std::declval<T>().prepare( std::declval<EventContext>(), std::declval<TopLevelInfo>() ) );

    template <typename T>
    inline constexpr bool has_prepare_EventContext_TopLevelInfo_v =
        Gaudi::cpp17::is_detected_v<has_prepare_EventContext_TopLevelInfo_, T>;

    template <typename Functor>
    auto prepare( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level ) {
      if constexpr ( has_prepare_EventContext_TopLevelInfo_v<Functor> ) {
        return f.prepare( evtCtx, top_level );
      } else if constexpr ( has_prepare_TopLevelInfo_v<Functor> ) {
        return f.prepare( top_level );
      } else if constexpr ( has_prepare_v<Functor> ) {
        return f.prepare();
      } else {
        return std::cref( f );
      }
    }

    template <typename>
    struct is_variant : std::false_type {};

    template <typename... Ts>
    struct is_variant<std::variant<Ts...>> : std::true_type {};

    template <typename... Variants>
    inline constexpr bool are_all_variants_v =
#if ROOT_15030_FIXED
        ( is_variant<std::decay_t<Variants>>::value && ... );
#else
        false;
#endif
  } // namespace detail

  /** @brief Type-erased wrapper around a generic prepared functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around prepared functors without exposing their extremely
   *  verbose full types. This is very similar to std::function<Out(In)>, but
   *  it supports custom allocators -- in this case LHCb's event-local one.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */
  template <typename OutputType, typename... InputType>
  struct PreparedFunctor<OutputType( InputType... )> final {
    using result_type    = OutputType;
    using allocator_type = LHCb::Allocators::EventLocal<void>;

  private:
    struct IPreparedFunctor {
      virtual ~IPreparedFunctor()                                    = default;
      virtual result_type           operator()( InputType... ) const = 0;
      virtual std::type_info const& rtype() const                    = 0;
    };

    template <typename F>
    struct PreparedFunctorImpl : public IPreparedFunctor {
      PreparedFunctorImpl( F f ) : m_f{std::move( f )} {}
      result_type           operator()( InputType... in ) const override { return std::invoke( m_f, in... ); }
      std::type_info const& rtype() const override { return typeid( std::invoke_result_t<F, InputType...> ); }

    private:
      F m_f;
    };

    LHCb::event_local_unique_ptr<IPreparedFunctor> m_functor;

  public:
    /** Move constructor.
     */
    PreparedFunctor( PreparedFunctor&& ) = default;

    /** Move assignment.
     */
    PreparedFunctor& operator=( PreparedFunctor&& ) = default;

    /** Wrap the given functor up as a type-erased PreparedFunctor, using the
     *  given allocator.
     */
    template <typename F>
    PreparedFunctor( F f, allocator_type alloc )
        : m_functor{LHCb::make_event_local_unique<PreparedFunctorImpl<F>>( std::move( alloc ), std::move( f ) )} {}

    /** Invoke the prepared functor.
     */
    result_type operator()( InputType... in ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty PreparedFunctor<Out(In...)> called" ); }
      return std::invoke( *m_functor, in... );
    }

    /** Check if the prepared functor is invocable.
     */
    operator bool() const { return bool{m_functor}; }

    /** Query the return type of the wrapped functor.
     *  This will be convertible to result_type, but might differ from it.
     */
    std::type_info const& rtype() const {
      if ( UNLIKELY( !m_functor ) ) {
        throw std::runtime_error( "Empty PreparedFunctor<Out(In...)> return type queried" );
      }
      return m_functor->rtype();
    }
  };

  /** @brief Type-erased wrapper around a generic functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around functors without exposing the extremely verbose full types
   *  of [composite] functors. This is specialised as
   *  Functors::Functor<OutputType(InputType)>, which is rather similar to
   *  std::function<Output(Input)>, but includes the extra bind( ... ) method
   *  that is needed to set up functors' data dependencies and associate them
   *  with the owning algorithm.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */
  template <typename OutputType, typename... InputType>
  class Functor<OutputType( InputType... )> final : public AnyFunctor {
  public:
    using result_type   = OutputType;
    using prepared_type = PreparedFunctor<OutputType( InputType... )>;

  private:
    struct IFunctor {
      virtual ~IFunctor()                                     = default;
      virtual void                  bind( Gaudi::Algorithm* ) = 0;
      virtual result_type           operator()( EventContext const&, TopLevelInfo const&, InputType... ) const = 0;
      virtual prepared_type         prepare( EventContext const&, TopLevelInfo const& ) const                  = 0;
      virtual std::type_info const& rtype() const                                                              = 0;
    };

    template <typename F>
    struct FunctorImpl : public IFunctor {
      FunctorImpl( F f ) : m_f( std::move( f ) ) {}
      void        bind( Gaudi::Algorithm* alg ) override { detail::bind( m_f, alg ); }
      result_type operator()( EventContext const& evtCtx, TopLevelInfo const& top_level,
                              InputType... input ) const override {
        // Prepare and call in one go, avoiding the overhead of prepared_type
        auto f = detail::prepare( m_f, evtCtx, top_level );
        // If the arguments are std::variant, use the prepared functor as a
        // visitor, otherwise just call it
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return std::visit( std::move( f ), input... );
        } else {
          return std::invoke( std::move( f ), input... );
        }
      }
      prepared_type prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const override {
        auto f = detail::prepare( m_f, evtCtx, top_level );
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return {[f = std::move( f )]( auto&&... x ) { return std::visit( f, x... ); },
                  LHCb::getMemResource( evtCtx )};
        } else {
          return {std::move( f ), LHCb::getMemResource( evtCtx )};
        }
      }
      std::type_info const& rtype() const override {
        using prepared_t =
            decltype( detail::prepare( m_f, std::declval<EventContext>(), std::declval<TopLevelInfo>() ) );
        if constexpr ( detail::are_all_variants_v<InputType...> ) {
          return typeid( decltype( std::visit( std::declval<prepared_t>(), std::declval<InputType>()... ) ) );
        } else {
          return typeid( std::invoke_result_t<prepared_t, InputType...> );
        }
      }

    private:
      F m_f;
    };
    std::unique_ptr<IFunctor> m_functor;
    TopLevelInfo              m_top_level;

  public:
    /** @brief Construct an empty Functor object.
     *
     *  Note that an empty Functor will throw an exception if the function call
     *  operator is called.
     */
    Functor() = default;

    /** Move constructor
     */
    Functor( Functor&& other ) = default;

    /** Move assignment
     */
    Functor& operator=( Functor&& other ) = default;

    /** Construct a filled Functor object
     */
    template <typename F>
    Functor( F func )
        : m_functor(
#if ROOT_15030_FIXED
              std::make_unique<FunctorImpl<F>>
#else
              new FunctorImpl<F>
#endif
              ( std::move( func ) ) ) {
    }

    /** Fill this Functor with new contents.
     */
    template <typename F>
    Functor& operator=( F func ) {
      m_functor = std::make_unique<FunctorImpl<F>>( std::move( func ) );
      return *this;
    }

    /** @brief Bind the contained object to the given algorithm.
     *
     *  The contained object may have data dependencies, which must be bound to the algorithm
     *  that owns the functor. Calling a functor with data dependencies without binding it to
     *  an algorithm will cause an exception to be thrown.
     *
     *  @param owning_algorithm The algorithm to which the data dependency should be associated.
     *
     *  @todo Change the argument type to the minimal type needed to initialise a
     *        DataObjectReadHandle, Gaudi::Algorithm is over the top.
     */
    void bind( Gaudi::Algorithm* owning_algorithm ) override {
      m_functor->bind( owning_algorithm );
      m_top_level.bind( owning_algorithm );
    }

    /** @brief Invoke the contained object.
     *
     * Invoke the contained object. If the Functor object is empty, or if the contained object
     * has data dependencies and has not been bound to an owning algorithm, an exception will
     * be thrown.
     *
     * @param input Input to the contained object
     * @return      Output of the contained object
     */
    result_type operator()( InputType... input ) const { return ( *this )( Gaudi::Hive::currentContext(), input... ); }

    /** @brief Invoke the contained object, using the given EventContext.
     */
    result_type operator()( EventContext const& evtCtx, InputType... input ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> called" ); }
      return std::invoke( *m_functor, evtCtx, m_top_level, input... );
    }

    prepared_type prepare( EventContext const& evtCtx = Gaudi::Hive::currentContext() ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> prepared" ); }
      auto prepared = m_functor->prepare( evtCtx, m_top_level );
      if ( UNLIKELY( !prepared ) ) {
        throw std::runtime_error( "Functor<Out(In...)>::prepare() yielded an empty functor." );
      }
      return prepared;
    }

    /** Query whether the Functor is empty or not.
     *
     *  @return false if the Functor is empty, true if it is not
     */
    operator bool() const override { return bool{m_functor}; }

    /** Query the return type of the contained functor.
     *
     *  This is *not* supposed to be typeid( OutputType ), rather it is the
     *  typeid of the functor return type *before* that was converted to
     *  OutputType.
     */
    std::type_info const& rtype() const override {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> return type queried" ); }
      return m_functor->rtype();
    }

    /** This is useful for the functor cache.
     *
     * @todo Give a more useful description...
     */
    using Factory = typename ::Gaudi::PluginService::Factory<AnyFunctor*()>;
  };
} // namespace Functors
