/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrZip.h"
#include "Functors/Function.h"
#include "PrKernel/PrSelection.h"
#include "SOAExtensions/ZipSelection.h"

/** @file  Filter.h
 *  @brief Wrapper type providing container-wise operations.
 */
namespace Functors {
  namespace detail {
    struct FilterImpl {
      /** @brief Conditionally copy an std::vector.
       */
      template <typename F, typename T, typename A>
      std::vector<T, A> operator()( F&& f, TopLevelInfo const&, std::vector<T, A> const& input ) const {
        std::vector<T, A> out;
        out.reserve( input.size() );
        std::copy_if( input.begin(), input.end(), std::back_inserter( out ), std::forward<F>( f ) );
        return out;
      }

      /** @brief Filter a container that can be made iterable by LHCb::Pr::Zip
       *
       *  This overload only participates if LHCb::Pr::make_zip( container ) is
       *  valid, so far this typically means LHCb::Pr::*::Tracks containers. The
       *  condition may have to be tightened up later.
       */
      template <typename F, typename PrTracks,
                typename std::enable_if_t<LHCb::Pr::is_zippable_v<std::decay_t<PrTracks>>, int> = 0>
      auto operator()( F&& f, TopLevelInfo const& top_level, PrTracks&& input ) const {
        // Try and give an informative error message if we failed to use the same
        // instruction set in the stack compilation and with cling. Explicitly
        // use 'best' (even though it's the default)
        check_simd<SIMDWrapper::Best>( top_level );

        // Get an iterable version of the input
        auto iterable_input = LHCb::Pr::make_zip<SIMDWrapper::Best>( input );

        // Filter the container using the prepared functor
        return iterable_input.filter( std::forward<F>( f ) );
      }

      /** @brief Filter a LHCb::Pr::Zip<A, B, Containers...> container
       */
      template <typename F, SIMDWrapper::InstructionSet def_simd, bool def_unwrap, typename... PrTracks>
      auto operator()( F&& f, TopLevelInfo const& top_level,
                       LHCb::Pr::Zip<def_simd, def_unwrap, PrTracks...> const& input ) const {
        // In this case the input type has a default SIMD size. We should respect
        // that, but try and give an informative error message in case that
        // setting means something different in the stack and inside Cling.
        check_simd<def_simd>( top_level );

        // Filter the container using the prepared functor
        return input.filter( std::forward<F>( f ) );
      }

      /** @brief Refine a Pr::Selection.
       */
      template <typename F, typename T>
      Pr::Selection<T> operator()( F&& f, TopLevelInfo const&, Pr::Selection<T> const& input ) const {
        return input.select( std::forward<F>( f ) );
      }

      /** @brief Refine a Zipping::SelectionView.
       */
      template <typename F, typename Skin>
      Zipping::ExportedSelection<> operator()( F&&                                 f, TopLevelInfo const&,
                                               Zipping::SelectionView<Skin> const& input ) const {
        return input.select( std::forward<F>( f ) );
      }

      template <SIMDWrapper::InstructionSet simd>
      static void check_simd( TopLevelInfo const& top_level ) {
        // Print a warning if the requested SIMD level (simd) corresponds to a
        // different *actual* SIMD level. Typically this happens when
        // JIT-compiling functors with Cling, which normally does not have
        // vector backends enabled, even if the application was built with them.
        // In this case the requested level (e.g. Best) might mean AVX2 in the
        // stack build but Scalar inside Cling.
        auto current_meaning = SIMDWrapper::type_map<simd>::instructionSet();
        auto stack_meaning   = SIMDWrapper::type_map<simd>::stackInstructionSet();
        if ( UNLIKELY( current_meaning != stack_meaning ) ) {
          top_level.Warning(
              "Stack:" + SIMDWrapper::instructionSetName( stack_meaning ) +
              ", Functor:" + SIMDWrapper::instructionSetName( current_meaning ) + ", instruction set mismatch (" +
              SIMDWrapper::instructionSetName( simd ) +
              " requested). ROOT/cling was not compiled with the same options as the stack, try the functor cache" );
        }
      }
    };
  } // namespace detail

  /** @class Filter
   *  @brief Functor that adapts a predicate to filter a container.
   *
   *  This defines the boilerplate for the different possible input container
   *  types. So far there are implementations for:
   *  @li Refining a Pr::Selection
   *  @li Copying a subset of an STL container, e.g. @p std::vector<T>
   *  @li Making a new Zipping::ExportedSelection from a Zipping::SelectionView
   *  @li Using the LHCb::Pr::Zip family of types.
   */
  template <typename F>
  struct Filter {
    template <typename std::enable_if_t<detail::is_functor_predicate_v<F>, int> = 0>
    Filter( F f ) : m_f{std::move( f )} {}

    /** Forward to the contained functor
     */
    template <typename Algorithm>
    void bind( Algorithm* alg ) {
      detail::bind( m_f, alg );
    }

    /** Prepare the contained functor and bake it into a new lamba that can
     *  filter containers using the pre-prepared functor.
     */
    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [&top_level, f = detail::prepare( m_f, evtCtx, top_level )]( auto const&... input ) {
        return detail::FilterImpl{}( f, top_level, input... );
      };
    }

  private:
    F m_f;
  };

  /** @brief Get the result type of filtering the given container.
   *
   *  Helper to get the type returned when filtering a container of type T with
   *  the ALL functor. This is useful for algorithms that need to deduce what
   *  Functor<Out( In )> type they should use when filtering some known input
   *  type.
   */
  template <typename T>
  using filtered_t = std::decay_t<decltype(
      Filter{AcceptAll{}}.prepare( std::declval<EventContext>(), std::declval<TopLevelInfo>() )( std::declval<T>() ) )>;
} // namespace Functors