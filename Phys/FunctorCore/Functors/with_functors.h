/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Functors/STLExtensions.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/ServiceHandle.h"

#include <tuple>
#include <utility>

namespace Functors::detail {
  /** @brief Check if the given type has a static member called ExtraHeaders
   */
  template <typename, typename U = int>
  struct has_extra_headers : std::false_type {};

  template <typename T>
  struct has_extra_headers<T, decltype( (void)T::ExtraHeaders, 0 )> : std::true_type {};

  template <typename T>
  inline constexpr bool has_extra_headers_v = has_extra_headers<T>::value;

  /** Type that we use to tag whether or not the functor factory service handle
   *  has been added to a class.
   */
  struct with_functor_factory_tag {};

  /** Type that actually adds the functor factory service handle, this should
   *  only be inherited from via the with_functor_factory alias below that
   *  protects against multiple inheritance.
   */
  template <typename base_t>
  struct add_functor_factory : public base_t, public with_functor_factory_tag {
    using base_t::base_t;
    Functors::IFactory& getFunctorFactory() {
      m_functor_factory.retrieve().ignore();
      return *m_functor_factory;
    }

  private:
    ServiceHandle<Functors::IFactory> m_functor_factory{this, "FunctorFactory", "FunctorFactory"};
  };

  /** Pseudo-mixin that provides the a getFunctorFactory() accessor to the
   *  functor factory service. This takes care of adding only one property and
   *  one service handle, even if the mixin appears multiple times in the
   *  inheritance structure.
   */
  template <typename base_t>
  using with_functor_factory =
      std::conditional_t<std::is_base_of_v<with_functor_factory_tag, base_t>, base_t, add_functor_factory<base_t>>;
} // namespace Functors::detail

/** @brief Add functors to an algorithm.
 */
template <typename base_t, typename... Tags>
class with_functors : public Functors::detail::with_functor_factory<base_t> {
  using TagTypesTuple = std::tuple<Tags...>;
  using FunctorsTuple = std::tuple<Functors::Functor<typename Tags::Signature>...>;
  using Property_t    = Gaudi::Property<ThOr::FunctorDesc>;
  using Properties_t  = std::array<Property_t, sizeof...( Tags )>;
  using IndexSequence = std::index_sequence_for<Tags...>;

  // Helper to initialise properties
  template <std::size_t... Is>
  constexpr Properties_t initProperties( std::index_sequence<Is...> ) {
    return {
        Property_t{this, std::tuple_element_t<Is, TagTypesTuple>::PropertyName, ThOr::Defaults::ALL, [this]( auto& ) {
                     if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) { return; }
                     this->template decode<Is>();
                   }}...};
  }

  template <std::size_t... Is>
  void initialize( std::index_sequence<Is...> ) {
    ( decode<Is>(), ... );
  }

  template <std::size_t Idx>
  void decode() {
    using TagType     = std::tuple_element_t<Idx, TagTypesTuple>;
    using FunctorType = std::tuple_element_t<Idx, FunctorsTuple>;
    // Make a copy, as we might need to add headers to it
    ThOr::FunctorDesc proxy{std::get<Idx>( m_properties )};
    // Add extra headers if needed
    if constexpr ( Functors::detail::has_extra_headers_v<TagType> ) {
      for ( auto const& h : TagType::ExtraHeaders ) { proxy.headers.emplace_back( h ); }
    }
    std::get<Idx>( m_functors ) = this->getFunctorFactory().template get<FunctorType>( this, proxy );
  }

  // Storage for the decoded functors
  FunctorsTuple m_functors;

  // Storage for the Gaudi::Property objects
  Properties_t m_properties{initProperties( IndexSequence{} )};

public:
  // Expose the base class constructtors
  using Functors::detail::with_functor_factory<base_t>::with_functor_factory;

  StatusCode initialize() override {
    auto sc = Functors::detail::with_functor_factory<base_t>::initialize();
    initialize( IndexSequence{} );
    return sc;
  }

  /** @brief Get the decoded functor corresponding to the given tag type.
   *  @todo  Extend this to despatch to getFunctor() of the parent class if it
   *         exists and we didn't know the given TagType.
   */
  template <typename TagType>
  auto const& getFunctor() const {
    return std::get<index_of_v<TagType, TagTypesTuple>>( m_functors );
  }

  template <typename TagType>
  bool is_noop() const {
    return std::get<index_of_v<TagType, TagTypesTuple>>( m_properties ) == ThOr::Defaults::ALL;
  }
};
