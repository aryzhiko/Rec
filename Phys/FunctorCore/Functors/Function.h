/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "GaudiKernel/detected.h"
#include "SelKernel/Utilities.h"

#include <cmath>

/** @file  Function.h
 *  @brief Helper types and operator overloads for composition of functions.
 */

namespace Functors {
  /** @class Function
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All functors inherit from this class.
   */
  struct Function {};

  /** @class Predicate
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All predicates inherit from this class. A predicate is a function that
   *  returns a bool-like. A predicate can be wrapped up in the Filter functor
   *  to enable container-wise operations, and logical operations (and, or,
   *  etc.) are enabled for predicates but not for functions.
   */
  struct Predicate : public Function {};

  /** @namespace Functors::detail
   *
   *  Internal helpers for new style functors.
   */
  namespace detail {
    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_function_v = std::is_base_of_v<Function, std::decay_t<F>>;

    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_predicate_v = std::is_base_of_v<Predicate, std::decay_t<F>>;

    template <typename T>
    using has_static_mask_true_ = decltype( T::mask_true() );

    template <typename T>
    inline constexpr bool has_static_mask_true_v = Gaudi::cpp17::is_detected_v<has_static_mask_true_, T>;

    template <typename T>
    using has_static_mask_false_ = decltype( T::mask_false() );

    template <typename T>
    inline constexpr bool has_static_mask_false_v = Gaudi::cpp17::is_detected_v<has_static_mask_false_, T>;
  } // namespace detail

  /** @class NumericValue
   *  @brief Trivial function that holds a numeric constant.
   *
   *  This is the type that is generated for the RHS of expressions such as
   *  "ETA > 2".
   */
  template <typename PODtype>
  struct NumericValue : public Function {
    constexpr NumericValue( PODtype value ) : m_value( value ) {}
    template <typename... Input>
    constexpr PODtype operator()( Input&&... ) const {
      return m_value;
    }

  private:
    PODtype m_value;
  };

  /** @class AcceptAll
   *  @brief Trivial functor that always returns true.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  struct AcceptAll : public Predicate {
    template <typename... Data>
    constexpr auto operator()( Data&&... ) const {
      if constexpr ( sizeof...( Data ) > 0 ) {
        using FirstType = std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
        if constexpr ( detail::has_static_mask_true_v<FirstType> ) {
          return FirstType::mask_true();
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  };

  /** @class AcceptNone
   *  @brief Trivial functor that always returns false.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  struct AcceptNone : public Predicate {
    template <typename... Data>
    constexpr auto operator()( Data&&... ) const {
      if constexpr ( sizeof...( Data ) > 0 ) {
        using FirstType = std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
        if constexpr ( detail::has_static_mask_false_v<FirstType> ) {
          return FirstType::mask_false();
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  namespace detail {
    /** Sub-helper for calling bind() on every element of a tuple of functors. */
    template <typename Algorithm, typename... F, std::size_t... I>
    void bind_tuple_helper( Algorithm* alg, std::tuple<F...>& functors, std::index_sequence<I...> ) {
      ( detail::bind( std::get<I>( functors ), alg ), ... );
    }

    /** Helper for calling bind() on every element of a tuple of functors. */
    template <typename Algorithm, typename... F>
    void bind( std::tuple<F...>& functors, Algorithm* alg ) {
      bind_tuple_helper( alg, functors, std::index_sequence_for<F...>{} );
    }

    /** @class ComposedFunctor
     *  @brief Workhorse for producing a new functor from existing ones.
     *
     *  This class is used to implement all binary function operations:
     *  @li arithmetric operators (+, -, *, /, %), two functions -> function.
     *  @li comparison operators (>, <, etc.), two functions -> predicate.
     *  @li logical operators (&, |), two predicates -> predicate.
     *
     *  It is also be used to implement functor transformations, like
     *  math::log( PT ), math::pow( PT, 2 ), math::in_range( min, PT, max )
     *  and so on. And the unary negation operators.
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... F>
    struct ComposedFunctor : public NewBase {
      template <typename... U, typename = std::enable_if_t<std::is_constructible_v<std::tuple<F...>, U...>>>
      ComposedFunctor( U&&... u ) : m_fs( std::forward<U>( u )... ) {
        check_types( std::index_sequence_for<F...>{} );
      }

      /** Prepare the composite functor for use. */
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
        return prepare( evtCtx, top_level, std::index_sequence_for<F...>{} );
      }

      /** Contained functors may have data-dependencies that need to be
       *  associated to the owning algorithm.
       */
      template <typename Algorithm>
      void bind( Algorithm* alg ) {
        detail::bind( m_fs, alg );
      }

    private:
      /** Helper for compile-time check that all owned functors inherit from
       *  the specified base class.
       */
      template <std::size_t... I>
      constexpr void check_types( std::index_sequence<I...> ) {
        static_assert( ( std::is_base_of_v<InputBase, std::tuple_element_t<I, std::tuple<F...>>> && ... ) );
      }

      /** Helper to prepare all owned functors and return a new lambda that
       *  holds the prepared functors and the knowledge (encoded by Operator)
       *  of how to combine them into the result of the new (composed) functor.
       */
      template <std::size_t... I>
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level, std::index_sequence<I...> ) const {
        // Prepare all of the functors and capture the resulting tuple
        return [fs = std::tuple{detail::prepare( std::get<I>( m_fs ), evtCtx, top_level )...}]( auto const&... input ) {
          if constexpr ( std::is_same_v<Operator, std::logical_and<>> ) {
            static_assert( sizeof...( I ) == 2 );
            // Operator{}( ... ) would not short-circuit in this case
            auto first = std::get<0>( fs )( input... );
            // Short circuit vectorised cuts as well as scalar ones
            using Sel::Utils::none; // for the scalar case
            if ( none( first ) ) return first;
            return first && std::get<1>( fs )( input... );
          } else if constexpr ( std::is_same_v<Operator, std::logical_or<>> ) {
            static_assert( sizeof...( I ) == 2 );
            // Operator{}( ... ) would not short-circuit in this case
            auto first = std::get<0>( fs )( input... );
            // Short circuit vectorised cuts as well as scalar ones
            using Sel::Utils::all; // for the scalar case
            if ( all( first ) ) return first;
            return first || std::get<1>( fs )( input... );
          } else {
            // Evaluate all the functors and let the given Operator combine the
            // results.
            return Operator{}( std::get<I>( fs )( input... )... );
          }
        };
      }
      std::tuple<F...> m_fs;
    };

    /** Helper function to make a ComposedFunctor that deduces the actual
     *  functor types from its argument types, but leaves us to specify the
     *  other details explicitly.
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... Args>
    constexpr auto compose( Args&&... args ) {
      return ComposedFunctor<Operator, InputBase, NewBase, Args...>( std::forward<Args>( args )... );
    }

    /** Use our own helper to define in one place which types we allow to be
     *  promoted to NumericValue.
     */
    template <typename F>
    inline constexpr bool is_promotable_v = std::is_arithmetic_v<std::decay_t<F>>;

    /** Helper to promote arithmetic constants to NumericValue instances while
     *  not doing anything to functors.
     */
    template <typename F>
    constexpr auto promote( F f ) {
      if constexpr ( is_promotable_v<F> ) {
        return NumericValue( f );
      } else {
        static_assert( is_functor_function_v<F>, "Functors::detail::promote( f ) got invalid input" );
        return f;
      }
    }

    /** Helper to make a ComposedFunctor from a set of arguments that can be a
     *  mix of arithmetic constants and functor expressions. Arithmetic
     *  constants are promoted to NumericValue instances using promote().
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... Args>
    constexpr auto promote_and_compose( Args&&... args ) {
      return compose<Operator, InputBase, NewBase>( promote( std::forward<Args>( args ) )... );
    }

    /** Helper for checking if our overloaded operators should be considered.
     *
     *  The condition is that either both LHS and RHS are our functions, or one
     *  of them is and the other side of the expression is an arithmetic
     *  constant that we can promote to a NumericValue.
     */
    template <typename F1, typename F2>
    using enable_function_operator =
        std::enable_if_t<( is_functor_function_v<F1> && (is_promotable_v<F2> || is_functor_function_v<F2>)) ||
                             (is_promotable_v<F1> && is_functor_function_v<F2>),
                         int>;

    /** SFINAE helper for defining boolean (bitwise...) logic overloads.
     */
    template <typename F1, typename F2>
    using enable_if_both_predicates = std::enable_if_t<is_functor_predicate_v<F1> && is_functor_predicate_v<F2>, int>;
  } // namespace detail

  /** operator+ overload covering all three combinations of function/arithmetic
   *
   *  This turns two functions (hence the first Function) into a new
   *  function (hence the second Function). The other operator overloads
   *  defined here are similar.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator+( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::plus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                         std::forward<F2>( rhs ) );
  }

  /** operator- overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator-( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::minus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                          std::forward<F2>( rhs ) );
  }

  /** operator* overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator*( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::multiplies<>, Function, Function>( std::forward<F1>( lhs ),
                                                                               std::forward<F2>( rhs ) );
  }

  /** operator/ overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator/( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::divides<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** operator% overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator%( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::modulus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** Unary operator- overload. */
  template <typename F, typename std::enable_if_t<detail::is_functor_function_v<F>, int> = 0>
  constexpr auto operator-( F&& f ) {
    return detail::compose<std::negate<>, Function, Function>( std::forward<F>( f ) );
  }

  /** operator| overload for turning two predicates into another one.
   *
   *  detail::promote_and_compose is not used here because we don't support
   *  logical operations between predicates and arithmetic constants.
   */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator|( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_or<>, Predicate, Predicate>( std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator& overload for turning two predicates into another one. */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator&( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_and<>, Predicate, Predicate>( std::forward<F1>( lhs ),
                                                                      std::forward<F2>( rhs ) );
  }

  /** operator~ overload, creates a new functor representing the logical
   *  negation of the given functor.
   */
  template <typename F, typename std::enable_if_t<detail::is_functor_predicate_v<F>, int> = 0>
  constexpr auto operator~( F&& obj ) {
    return detail::compose<std::logical_not<>, Predicate, Predicate>( std::forward<F>( obj ) );
  }

  /** operator> overload covering all three combinations of function/arithmetic
   *
   *  This, and the others below, transforms two Functions (hence Function)
   *  into a Predicate (hence the 3rd template argument).
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                             std::forward<F2>( rhs ) );
  }

  /** operator< overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                          std::forward<F2>( rhs ) );
  }

  /** operator>= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater_equal<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                   std::forward<F2>( rhs ) );
  }

  /** operator<= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less_equal<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                std::forward<F2>( rhs ) );
  }

  /** operator== overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator==( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::equal_to<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                              std::forward<F2>( rhs ) );
  }

  /** operator!= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator!=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::not_equal_to<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                  std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise AND between two contained_functor_value objects.
   *
   * It's really operator& overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto bitwise_and( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::bit_and<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise OR between two contained_functor_value objects.
   *
   * It's really operator| overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto bitwise_or( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::bit_or<>, Function, Function>( std::forward<F1>( lhs ),
                                                                           std::forward<F2>( rhs ) );
  }

  namespace detail::math {
    struct pow_helper {
      template <typename... Args>
      constexpr auto operator()( Args&&... args ) const {
        using std::pow;
        return pow( std::forward<Args>( args )... );
      }
    };

    struct log_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::log;
        return log( std::forward<Arg1>( arg1 ) );
      }
    };

    struct in_range_helper {
      /** This acts on the actual functor return values. */
      template <typename Min, typename Val, typename Max>
      constexpr auto operator()( Min min, Val val, Max max ) const {
        return ( val > min ) && ( val < max );
      }
    };
  } // namespace detail::math

  /** @namespace Functors::math
   *
   *  Mathematical functions that act on new style functors.
   */
  namespace math {
    /** Wrapper for std::pow( contained_functor_value, arg ).
     *
     *  e.g. math::pow( PT, 2 ) is a functor that returns the squared PT
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 2>>
    constexpr auto pow( Args&&... args ) {
      return detail::promote_and_compose<detail::math::pow_helper, Function, Function>( std::forward<Args>( args )... );
    }

    /** Wrapper for std::log( contained_functor_value ).
     */
    template <typename F>
    constexpr auto log( F&& f ) {
      return detail::promote_and_compose<detail::math::log_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** More efficient shorthand for (f > x) && (f < y) that only evaluates
     *  the value (f) once.
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 3>>
    constexpr auto in_range( Args&&... args ) {
      return detail::promote_and_compose<detail::math::in_range_helper, Function, Predicate>(
          std::forward<Args>( args )... );
    }

    /** Predicate returning true if bitwise AND between two contained_functor_value objects is non-zero.
     */
    template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
    constexpr auto test_bit( F1&& lhs, F2&& rhs ) {
      return bitwise_and( std::forward<F1>( lhs ), std::forward<F2>( rhs ) ) != 0;
    }
  } // namespace math
} // namespace Functors
