/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Functors/STLExtensions.h"
#include "Functors/with_functors.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/ServiceHandle.h"

#include <tuple>
#include <type_traits>
#include <utility>

namespace functor_map_details {
  template <typename T, typename... Ts>
  struct unique_impl {
    using type = T;
  };

  template <typename... Ts, typename U, typename... Us>
  struct unique_impl<std::tuple<Ts...>, U, Us...>
      : std::conditional_t<( std::is_same_v<U, Ts> || ... ), unique_impl<std::tuple<Ts...>, Us...>,
                           unique_impl<std::tuple<Ts..., U>, Us...>> {};

  template <typename... Ts>
  using unique_tuple = typename unique_impl<std::tuple<>, Ts...>::type;
} // namespace functor_map_details
/** @brief Add maps of functors to an algorithm.
 */
template <typename base_t, typename... Tags>
class with_functor_maps : public Functors::detail::with_functor_factory<base_t> {
  using TagTypesTuple  = std::tuple<Tags...>;
  using FuncTypesTuple = std::tuple<Functors::Functor<typename Tags::Signature>...>;
  using FuncMapTuple   = std::tuple<std::map<std::string, Functors::Functor<typename Tags::Signature>>...>;
  // FIXME make std::map<std::string, ThOr::FunctorDesc> work
  using FuncProxyMap  = std::map<std::string, std::vector<std::string>>;
  using Property_t    = Gaudi::Property<FuncProxyMap>;
  using Properties_t  = std::array<Property_t, sizeof...( Tags )>;
  using IndexSequence = std::index_sequence_for<Tags...>;

  // Helper to initialise properties
  template <std::size_t... Is>
  constexpr Properties_t initProperties( std::index_sequence<Is...> ) {
    // Default to an empty map
    return {Property_t{this, std::tuple_element_t<Is, TagTypesTuple>::PropertyName, {}, [this]( auto& ) {
                         if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) { return; }
                         this->template decode<Is>();
                       }}...};
  }

  template <std::size_t... Is>
  void initialize( std::index_sequence<Is...> ) {
    ( decode<Is>(), ... );
  }

  template <std::size_t Idx>
  void decode() {
    using TagType     = std::tuple_element_t<Idx, TagTypesTuple>;
    using FunctorType = std::tuple_element_t<Idx, FuncTypesTuple>;
    // This is the {nickname: decoded_functor} map we want to populate
    auto& functor_map = std::get<Idx>( m_functors );
    // Clean it up each time in case we re-decode
    functor_map.clear();
    // FIXME make std::map<std::string, ThOr::FunctorDesc> work
    for ( auto const& [func_name, string_vec] : std::get<Idx>( m_properties ) ) {
      // Local copy we can add extra headers to
      ThOr::FunctorDesc proxy{string_vec[0], {std::next( string_vec.begin(), 2 ), string_vec.end()}, string_vec[1]};
      if constexpr ( Functors::detail::has_extra_headers_v<TagType> ) {
        for ( auto const& h : TagType::ExtraHeaders ) { proxy.headers.emplace_back( h ); }
      }
      // Decode the functor
      functor_map[func_name] = this->getFunctorFactory().template get<FunctorType>( this, proxy );
    }
  }

  // Storage for the decoded functors
  FuncMapTuple m_functors;

  // Storage for the Gaudi::Property objects
  Properties_t m_properties{initProperties( IndexSequence{} )};

public:
  // Expose the base class constructtors
  using Functors::detail::with_functor_factory<base_t>::with_functor_factory;

  StatusCode initialize() override {
    auto sc = Functors::detail::with_functor_factory<base_t>::initialize();
    initialize( IndexSequence{} );
    return sc;
  }

  /** @brief Get the decoded functor map corresponding to the given tag type.
   *  @todo  Extend this to despatch to getFunctor() of the parent class if it
   *         exists and we didn't know the given TagType.
   */
  template <typename TagType>
  auto const& getFunctorMap() const {
    return std::get<index_of_v<TagType, TagTypesTuple>>( m_functors );
  }
};

namespace Functors::detail::with_functor_maps {
  template <typename>
  struct deduplicate_helper {};

  template <typename... DeDupedTags>
  struct deduplicate_helper<std::tuple<DeDupedTags...>> {
    template <typename base_t>
    using type = typename ::with_functor_maps<base_t, DeDupedTags...>;
  };
} // namespace Functors::detail::with_functor_maps

/** Extra layer on top of with_functor_maps that removes duplicate
 *  entries in the pack of tag types.
 */
template <typename base_t, typename... Tags>
using with_functor_maps_deduplicated = typename Functors::detail::with_functor_maps::deduplicate_helper<
    functor_map_details::unique_tuple<Tags...>>::template type<base_t>;
