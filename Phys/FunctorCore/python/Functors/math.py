###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""@module Functors
Mathematical functions for use in functor expressions.
"""
from Functors.common import header_prefix, top_level_namespace
from Functors.grammar import WrappedBoundFunctor


def log(functor):
    """Wrap the math::log functor.

    This allows constructions such as:
    import Functors.math as fmath
    from Functors import PT
    myFilter(Functor=(fmath.log(PT) > 1.f), ...)
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::log',
                               header_prefix + 'Function.h', functor)


def in_range(min_val, functor, max_val):
    """Wrap the math::in_range functor.

    This is efficient and only evaluates the functor once.
    """
    return WrappedBoundFunctor(top_level_namespace + 'math::in_range',
                               header_prefix + 'Function.h', min_val, functor,
                               max_val)


def test_bit(functor_a, functor_b):
    return WrappedBoundFunctor(top_level_namespace + 'math::test_bit',
                               header_prefix + 'Function.h', functor_a,
                               functor_b)
