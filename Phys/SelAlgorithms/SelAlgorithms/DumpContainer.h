/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PrZip.h"
#include "Functors/with_output_tree.h"
#include "GaudiAlg/Consumer.h"
#include <any>

namespace Dumping {

  namespace details {
    /** Take some void functors.
     */
    struct VoidDump {
      using Signature                    = std::any();
      constexpr static auto PropertyName = "VoidBranches";
    };

    /** Take some functors that act on elements of the input.
     */
    template <typename InputType>
    struct Dump {
      using IterableInputType            = typename LHCb::Pr::unwrapped_zip_t<InputType>;
      using ItemType                     = typename IterableInputType::value_type;
      using Signature                    = std::any( ItemType const& );
      constexpr static auto PropertyName = "Branches";
      constexpr static auto ExtraHeaders = LHCb::header_map_v<ItemType>;
    };

    /** Construct the base type -- defining an alias here saves typing below.
     */
    template <typename T>
    using base_t = with_output_tree<Gaudi::Functional::Consumer<void( T const& )>, VoidDump, Dump<T>>;
  } // namespace details

  /** @class  DumpContainer DumpContainer.cpp
   *  @tparam InputType     Type of the container to be dumped
   */
  template <typename InputType>
  struct DumpContainer final : public details::base_t<InputType> {
    DumpContainer( std::string const& name, ISvcLocator* pSvcLocator )
        : details::base_t<InputType>( name, pSvcLocator, {"Input", ""} ) {}

    void operator()( InputType const& input_container ) const override {
      // Prepare the functors and get a temporary object that holds prepared
      // versions of all the functors and hides any thread-synchronisation logic.
      auto prepared = this->prepareBranchFillers( /* evt_context */ );

      // The different classes of functors can be grabbed using the same tag
      // types as above.
      auto& void_fillers     = prepared.template get<details::VoidDump>();
      auto& per_item_fillers = prepared.template get<details::Dump<InputType>>();

      // These are, by definition, independent of the item in the container
      // so we can populate them now. This also means there was no real reason to
      // bother with the explicit preparation step, but it's simpler to handle it
      // consistently with per_item_fillers
      for ( auto& filler : void_fillers ) { filler(); }

      if constexpr ( LHCb::Pr::is_zippable_v<InputType> ) {
        // Get an iterable version of the input container
        auto iterable_input_container = LHCb::Pr::make_zip( input_container ).unwrap();

        // Now deal with the functors that should be called once for each item in
        // the input containers.
        for ( auto const& item : iterable_input_container ) {
          for ( auto& filler : per_item_fillers ) { filler( item ); }
          // Tell the helper object to write a row in the output tree
          prepared.fill();
        }
      } else if constexpr ( LHCb::Pr::is_zip<InputType>::value ) {
        for ( auto const& item : input_container.unwrap() ) {
          for ( auto& filler : per_item_fillers ) { filler( item ); }
          // Tell the helper object to write a row in the output tree
          prepared.fill();
        }
      } else {
        for ( auto const& item : input_container ) {
          for ( auto& filler : per_item_fillers ) { filler( item ); }
          // Tell the helper object to write a row in the output tree
          prepared.fill();
        }
      }
    }
  };

} // namespace Dumping
