/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrIterableFittedForwardTracks.h"
#include "Event/Track_v2.h"
#include "Functors/Function.h"
#include "Functors/IFactory.h"
#include "Functors/with_functors.h"
#include "Functors/with_output_tree.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/AllocatorUtils.h"
#include "PrKernel/PrSelection.h"
#include "SelKernel/TrackCombination.h"
#include "SelKernel/TrackZips.h"
#include "TrackKernel/TrackCompactVertex.h"
#include "TrackKernel/TrackNProngVertex.h"
#include <TFile.h>
#include <TTree.h>
#include <tuple>

namespace {
  using FPtype = double;

  template <std::size_t N>
  using OutputStorage = std::vector<LHCb::TrackKernel::TrackCompactVertex<N, FPtype>,
                                    LHCb::Allocators::EventLocal<LHCb::TrackKernel::TrackCompactVertex<N, FPtype>>>;

  // Shorthand for below.
  template <typename T, std::size_t N>
  using FilterTransform = Gaudi::Functional::MultiTransformerFilter<std::tuple<OutputStorage<N>>( T const& )>;

  // Tag types for functors/dumping
  template <typename T, std::size_t N>
  struct CombTypes {
    using TrackType               = typename T::value_type;
    using Combination             = TrackCombination<TrackType, N>;
    constexpr static auto Headers = LHCb::header_map_v<Combination>;
    struct Cut {
      using Signature                    = bool( Combination const& );
      constexpr static auto PropertyName = "CombinationCut";
      constexpr static auto ExtraHeaders = Headers;
    };
    struct Dump {
      using Signature                    = std::any( Combination const& );
      constexpr static auto PropertyName = "CombinationDump";
      constexpr static auto ExtraHeaders = Headers;
      constexpr static auto BranchPrefix = "comb_";
    };
  };

  template <typename T, std::size_t N>
  struct VertexTypes {
    using VertexType              = typename OutputStorage<N>::value_type;
    constexpr static auto Headers = LHCb::header_map_v<VertexType>;
    struct Cut {
      using Signature                    = bool( VertexType const& );
      constexpr static auto PropertyName = "VertexCut";
      constexpr static auto ExtraHeaders = Headers;
    };
    struct Dump {
      using Signature                    = std::any( VertexType const& );
      constexpr static auto PropertyName = "VertexDump";
      constexpr static auto ExtraHeaders = Headers;
      constexpr static auto BranchPrefix = "vertex_";
    };
  };

  struct VoidDump {
    using Signature                    = std::any();
    constexpr static auto PropertyName = "VoidDump";
  };

  template <typename T>
  struct ChildDumpFunctors {
    using TrackType                    = typename T::value_type;
    using Signature                    = std::any( TrackType const& );
    constexpr static auto ExtraHeaders = LHCb::header_map_v<T>;
    constexpr static auto PropertyName = "ChildDump";
  };

  // For this one ThisN goes from 0..N-1
  template <typename T, std::size_t ThisN>
  struct ChildDump {
    // We want branches for each child, but the functors are the same for each child
    using FunctorTagType                  = ChildDumpFunctors<T>;
    inline static auto const BranchPrefix = "child" + std::to_string( ThisN ) + "_";
  };

  // Algorithm base class if EnableDumping = false: FilterTransformer + two cuts
  template <typename T, std::size_t N>
  using nodump_base_t =
      with_functors<FilterTransform<T, N>, typename CombTypes<T, N>::Cut, typename VertexTypes<T, N>::Cut>;

  // Helper for constructing the base class type if EnableDumping = true: we
  // use the with_output_tree mixin to handle the I/O and functors, and pass it
  // a bunch of tag types to set up the various functors that we want to
  // evaluate and save to the output tree. This layer helps us to add
  // ChildDump<T, 0> ... ChildDump<T, N-1> to the list of tag types passed to
  // with_output_tree, because we need N copies of the child branches/functors
  template <typename T, std::size_t N, std::size_t... Ns>
  using dump_base_var_t = with_output_tree<nodump_base_t<T, N>, typename CombTypes<T, N>::Dump,
                                           typename VertexTypes<T, N>::Dump, ChildDump<T, Ns>..., VoidDump>;

  // Helper class to inject a pack {0..N-1} into dump_base_var_t given N
  template <typename>
  struct dump_base_helper {};

  template <std::size_t... Ns>
  struct dump_base_helper<std::index_sequence<Ns...>> {
    template <typename T, std::size_t N>
    using type = dump_base_var_t<T, N, Ns...>;
  };

  // Use the helpers to generate ChildDump<T, 0>..ChildDump<T, N-1> in the pack
  // passed to with_output_tree
  template <typename T, std::size_t N>
  using dump_base_t = typename dump_base_helper<std::make_index_sequence<N>>::template type<T, N>;

  // Select which base class we'll actually use using EnableDumping
  template <typename T, std::size_t N, bool EnableDumping>
  using base_t = std::conditional_t<EnableDumping, dump_base_t<T, N>, nodump_base_t<T, N>>;
} // namespace

/** @class CombineTracks PrCombineTracks.cpp
 *
 *  @tparam T TODO
 */
template <typename T, std::size_t N, bool EnableDumping = false>
class CombineTracks final : public base_t<T, N, EnableDumping> {
public:
  static_assert( N == 2 ); // FIXME not immediately implementing the logic for >2-body combinatorics
  using KeyValue      = typename base_t<T, N, EnableDumping>::KeyValue;
  using TrackType     = typename T::value_type;
  using StateType     = typename std::decay_t<decltype( std::declval<TrackType>().closestToBeamState() )>;
  using Combination   = typename CombTypes<T, N>::Combination;
  using VertexFitType = LHCb::TrackKernel::TrackNProngVertex<N, FPtype, StateType>;

  CombineTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t<T, N, EnableDumping>( name, pSvcLocator, {"InputTracks", ""},
                                     {KeyValue{"OutputVertices", ""} /* , KeyValue{"OutputVerticesStorage", ""}*/} ) {}

  std::tuple<bool, OutputStorage<N>> operator()( T const& in ) const override {
    // Prepare the output storage
    std::tuple<bool, OutputStorage<N>> ret{false, LHCb::make_obj_propagating_allocator<OutputStorage<N>>( in )};
    // TODO add a useful N-independent view into 'storage'
    auto& [filter_pass, storage] = ret;
    {
      auto const in_size = in.size();
      storage.reserve( in_size * ( in_size - 1 ) / 2 ); // massive over-estimate
    }

    // Do some potentially-expensive preparation before we enter the loop
    auto vertex_cut      = this->template getFunctor<typename VertexTypes<T, N>::Cut>().prepare( /* evtCtx */ );
    auto combination_cut = this->template getFunctor<typename CombTypes<T, N>::Cut>().prepare( /* evtCtx */ );

    // Buffer our counters to reduce the number of atomic operations
    auto npassed_vertex_cut      = m_npassed_vertex_cut.buffer();
    auto npassed_vertex_fit      = m_npassed_vertex_fit.buffer();
    auto npassed_combination_cut = m_npassed_combination_cut.buffer();

    // If we're dumping then we need to prepare the functors.
    // [[maybe_unused]] because of EnableDumping == false
    [[maybe_unused]] auto prepared_dumping_functors = [this] {
      if constexpr ( EnableDumping ) {
        return this->prepareBranchFillers( /* evtCtx */ );
      } else {
        // Avoid unused variable warning for 'this' in this branch
        static_cast<void>( this );
        return nullptr;
      }
    }();

    if constexpr ( EnableDumping ) {
      // Get the prepared versions of the 'void' functors (i.e. those taking
      // zero arguments) from the with_output_tree mixin and call them.
      auto& void_fillers = prepared_dumping_functors.template get<VoidDump>();
      for ( auto& filler : void_fillers ) { filler(); }
    }

    // TODO: replace with N-dependent combination generation...
    for ( auto t1_iter = in.begin(); t1_iter != in.end(); ++t1_iter ) {
      auto const& t1 = *t1_iter;
      for ( auto t2_iter = std::next( t1_iter ); t2_iter != in.end(); ++t2_iter ) {
        auto const& t2 = *t2_iter;
        Combination combination{{t1, t2}};
        auto        pass_combination_cut = combination_cut( combination );
        npassed_combination_cut += pass_combination_cut;
        if ( !pass_combination_cut ) { continue; }

        // Get the states -- this is a bit horrible for the moment
        auto const& s1 = t1.closestToBeamState();
        auto const& s2 = t2.closestToBeamState();

        // Make a new vertex
        VertexFitType vertex_fit{s1, s2};
        auto          fit_succeeded = ( vertex_fit.fit() == VertexFitType::FitSuccess );
        npassed_vertex_fit += fit_succeeded;
        if ( !fit_succeeded ) { continue; }

        // Convert it to the output vertex type
        // TODO: is this really needed? could we cut on vertex_fit and convert afterwards?
        auto& vertex = storage.emplace_back( convertToCompactVertex( vertex_fit ) );

        // Hackily add the child relations
        vertex.setChildZipIdentifier( 0, in.zipIdentifier() );
        vertex.setChildZipIdentifier( 1, in.zipIdentifier() );
        vertex.setChildIndex( 0, std::distance( in.begin(), t1_iter ) );
        vertex.setChildIndex( 1, std::distance( in.begin(), t2_iter ) );

        // Apply the vertex cut
        auto pass_vertex_cut = vertex_cut( vertex );
        npassed_vertex_cut += pass_vertex_cut;
        if ( !pass_vertex_cut ) {
          storage.pop_back();
          continue;
        }

        if constexpr ( EnableDumping ) {
          // For simplicity we only dump the children/combination/vertex
          // quantities after the vertex fit has passed. There is, therefore,
          // an implicit requirement that the vertex fit passed, and the
          // distributions are biased by this requirement + "later" cuts that
          // you choose to apply. But this way the relationship between
          // child/combination/parent in the output TTree is trivial, and if
          // you want to reduce the size of the output tuple by applying cuts
          // then that's easy too.

          // Delegate child handling because we need to duplicate the code for
          // each of the N children and we don't have a std::size_t... pack
          // available here, just the single std::size_t N.
          handle_child_fillers( prepared_dumping_functors, combination, std::make_index_sequence<N>{} );

          // No such problem with the combination/vertex cut stuff, we can just dump that here.
          auto& comb_fillers   = prepared_dumping_functors.template get<typename CombTypes<T, N>::Dump>();
          auto& vertex_fillers = prepared_dumping_functors.template get<typename VertexTypes<T, N>::Dump>();
          for ( auto& filler : comb_fillers ) { filler( combination ); }
          for ( auto& filler : vertex_fillers ) { filler( vertex ); }

          // Actually add an entry to the output tree
          prepared_dumping_functors.fill();
        }
      }
    }
    filter_pass = !storage.empty();
    return ret;
  }

private:
  // Fill the output branches for a specific child index ThisN in [0, N-1]
  template <std::size_t ThisN, typename PreparedFunctors, typename Combination>
  static void handle_single_child_fillers( PreparedFunctors& prepped, Combination const& comb ) {
    // Get the branch fillers for this child
    auto& fillers = prepped.template get<ChildDump<T, ThisN>>();
    // Pass the relevant element of the combination to each one of them.
    for ( auto& filler : fillers ) { filler( comb[ThisN] ); }
  }

  // Delegate to to the per-child-index helper
  template <std::size_t... Ns, typename PreparedFunctors, typename Combination>
  static void handle_child_fillers( PreparedFunctors& prepped, Combination const& comb, std::index_sequence<Ns...> ) {
    // Call handle_single_child_fillers<N> for each N in the pack Ns...
    ( handle_single_child_fillers<Ns>( prepped, comb ), ... );
  }

  /// Number of combinations passing the combination cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_combination_cut{this, "# passed combination cut"};
  /// Number of successfully fitted combinations
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_fit{this, "# passed vertex fit"};
  /// Number of fitted vertices passing vertex cut
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_vertex_cut{this, "# passed vertex cut"};
};

using CombineTracks__2Body__Track_v2         = CombineTracks<Pr::Selection<LHCb::Event::v2::Track>, 2>;
using CombineTracks__2Body__Track_v2__Dumper = CombineTracks<Pr::Selection<LHCb::Event::v2::Track>, 2, true>;
using CombineTracks__2Body__PrFittedForwardTracks =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::Tracks, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithPVs =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithPVs, 2, true>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID, 2>;
using CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper =
    CombineTracks<LHCb::Pr::Iterable::Scalar::Fitted::Forward::TracksWithMuonID, 2, true>;
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2, "CombineTracks__2Body__Track_v2" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__Track_v2__Dumper, "CombineTracks__2Body__Track_v2__Dumper" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracks, "CombineTracks__2Body__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithPVs,
                           "CombineTracks__2Body__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper,
                           "CombineTracks__2Body__PrFittedForwardTracksWithPVs__Dumper" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper,
                           "CombineTracks__2Body__PrFittedForwardTracksWithMuonID__Dumper" )